<?php
// Etiquetas ES para el alumno

$prefijo="LBL_";

define($prefijo."INTERNAL_ID4", "Internal code");
define($prefijo."ENTERPRISE", "Company");
define($prefijo."SECTION", "Section");
define($prefijo."#SEX", "Gender");
define($prefijo."#FEMALE", "Female");
define($prefijo."#MALE", "Male");
define($prefijo."#USER_LOGIN", "User ID");
define($prefijo."#USER_EMAIL", "Email User");
define($prefijo."USER_LANGUAGE", "User Language");
define($prefijo."#DATE_BIRTH", "Birth Date");
define($prefijo."#PLACE_BIRTH", "Birthplace");
define($prefijo."#PROVINCE", "Province");
define($prefijo."#COUNTRY_BIRTH", "Country of birth");
define($prefijo."#NACIONALITY", "Nationality");
define($prefijo."#MATERNAL_LANG", "Language");
define($prefijo."#DATE_ON", "Creation date");
define($prefijo."#DATE_OFF", "Deletion date");
define($prefijo."#REGISTRATION_DATE", "Registration date");
define($prefijo."#USER_DATA", "Ecampus Data");

define($prefijo."#INFLUENCE_AREA", "Area of influence");

define($prefijo."#STUDENT_STATUS", "State of validation of student");

define($prefijo."#INTERNAL_DATA_MANAGEMENT", "Internal data management");
define($prefijo."#N_PARTNER", "Partner number");
define($prefijo."#DELEGATION", "Delegation");
define($prefijo."#NO_DELEGATION", "Do not know to which delegation belongs");
define($prefijo."#PRIMARY_LANGUAGE", "Primary language");
define($prefijo."#SECONDARY_LANGUAGE", "Secondary language");
define($prefijo."#PANDA_EMPLOYEE", "Panda employee");

define($prefijo."#PORTAL", "Portal");

define($prefijo."#DEPARTMENT", "Department");

define($prefijo."#ADD_USER", "Create user");
define($prefijo."#UPDATE_USER", "Edit User");

define($prefijo."#SCALE_STUDENT", "Scale Student");


define($prefijo."USER_ID", "User");
define($prefijo."#EMAIL1", "Email");
define($prefijo."#OLD_PASSWORD", "Current password");
define($prefijo."#NEW_PASSWORD", "New password");
define($prefijo."#CONFIRM_PASSWORD", "Confirm password");
define($prefijo."#CONFIRM_NEW_PASSWORD", "Confirm New Password");

define($prefijo."PICTOGRAM", "Pictogram");
define($prefijo."HIGH_CONTRAST", "High contrast");
define($prefijo."LOCUTION", "Locution");
define($prefijo."PROFILE_SETTINGS", "Profile settings");

define($prefijo."POSSIBLE_STUDENTS", "Possible Students");
define($prefijo."ADD_STUDENT_ALL", "Add all");
define($prefijo."ADD_STUDENT", "Add student");
define($prefijo."REMOVE_STUDENT", "Remove student");
define($prefijo."REMOVE_STUDENT_ALL", "Remove All");
define($prefijo."ASSIGNED_STUDENTS", "Assigned students ");

define($prefijo."LEVEL", "Level");
define($prefijo."COURSE_LEVEL4", "Course for the 4th level");
define($prefijo."COURSE_LEVEL4_COMPULSORY", "Only mandatory for the 4th level");


$prefijoMsg = "MSG_";
define($prefijoMsg."#USER_DATA_INFO1","This student has not create a user yet.");
define($prefijoMsg."#USER_DATA_INFO2","You can also create it from the tab 'Ecampus Data'.");
define($prefijoMsg."#USER_DATA_INFO3","To change your email or password click the button below, or go to the tab of Ecampus Data.");
define($prefijoMsg."#USER_IDENTIFICATOR","This is one of the identifier options that we propose to you. You can choose another option if you prefer.<br/><b>Are you sure, the user ID generated contains alphanumeric characters in lowercase and without spaces.</b>");

define($prefijoMsg."#SUBJECT_USER_SITE","");
define($prefijoMsg."#USER_ACCEPTED", "Access to Panda Security granted");
define($prefijoMsg."#USER_REJECTED", "Access to Panda Security denied");

define($prefijoMsg."#USER_ACCEPTED_TXT_01","We would like to inform you that your request to access the Panda Security Portal has been accepted.");
define($prefijoMsg."#USER_ACCEPTED_TXT_02","Your details to access the Panda Security Portal are as follows:");

define($prefijoMsg."#USER_REJECTED_TXT_01","We are sorry to inform you that your request to access the Panda Security Portal has been denied.");

define($prefijoMsg."USER_SIGNATURE","\n\nSincerely,\nGureak ");



define($prefijoMsg."#NEW_USER_SUBJECT", "Panda Security - Student ");
define($prefijoMsg."NEW_USER_TEXT_01", "Dear Student,\n\nThe login data to access the platform is:");

define($prefijoMsg."#SCALE_STUDENT","You will send this student to the main company.\\n\\nContinue?"); 

define($prefijoMsg."#SELECT_FILE","You must select a file.");
define($prefijoMsg."#INFO_SIZE", "Remember that the maximum size is $1 kb. Allowed type of files: csv or txt. ");

define($prefijoMsg."#PASSWORD_SUCCESSFULLY_CHANGED", "The password was changed successfully.");
define($prefijoMsg."#EMAIL_SUCCESSFULLY_CHANGED", "The email was changed successfully.");
define($prefijoMsg."#USER_SUCCESSFULLY_CHANGED", "The user was changed successfully.");

define($prefijoMsg."#USER_SELECTED_PASSWORD","Select a user in order to change its password.");
define($prefijoMsg."#INFO_EMPLOYEE","The Information Coordinator has not yet created a user to access the application.");
define($prefijoMsg."#INFO_STUDENT","This student has not yet created a user to access the application.");

define($prefijoMsg."#EMAIL_TEXT01", "You must have an e-mail.");

define($prefijoMsg."#USERDATA_TEXT01", "The New password and confirmation must be equal.");
define($prefijoMsg."#USERDATA_TEXT02", "Must complete the New password and confirmation.");

define($prefijoMsg."#USERDATA_TEXT03", "User already exists with this name. ");
define($prefijoMsg."#USERDATA_TEXT04", "You must choose your username. ");

define($prefijoMsg."SUBJECT_USER_PASSWORD", "Gureak - Contraseņa de acceso");



?>