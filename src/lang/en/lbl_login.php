<?php
// Etiquetas EN para la pantalla de login

$prefijo="LBL_";

define($prefijo."LOGIN_MESSAGE", "To access the manager application please enter your username and password.");
define($prefijo."LOGIN_INCORRECT", "Username or password invalid.");
define($prefijo."USER_NOT_EMPLOYEE", "You are not authorized to access the application");
define($prefijo."SESSION_LOST", "Lost Session. You must login again.");
define($prefijo."NO_PERMISSION", "You do not have access to this page.");
define($prefijo."USER", "User");
define($prefijo."PASSWORD", "Password");
define($prefijo."ENTER", "Login");

?>