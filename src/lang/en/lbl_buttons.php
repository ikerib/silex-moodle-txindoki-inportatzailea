<?php
// Etiquetas EN para los botones

$prefijo="LBL_";

define($prefijo."ALT_NEW1", "Create new");
define($prefijo."ALT_OPEN", "Open");
define($prefijo."ALT_SAVE", "Save changes");
define($prefijo."ALT_DELETE", "Delete");
define($prefijo."ALT_CLOSE", "Close this form");

define($prefijo."ALT_IMPORTING_DATA", "Importing data");

?>