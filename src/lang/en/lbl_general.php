<?php
// Etiquetas EN genéricas

$prefijo = "LBL_";

define($prefijo."PROCESSING", "Processes");

define($prefijo."TITLE", "Title");

define($prefijo."FIRST_NAME", "Name");
define($prefijo."LAST_NAME1", "Surname");
define($prefijo."LAST_NAME2", "Second name");

define($prefijo."NAME", "Name");
define($prefijo."DETAIL", "Detail");
define($prefijo."REMARKS", "Comments");
define($prefijo."REMARKS1", "Notes");
define($prefijo."ES", "Spanish");
define($prefijo."EN", "English");
define($prefijo."ALT_CALENDAR", "Show calendar");

define($prefijo."DESCRIPTION", "Description");

define($prefijo."DATE_CREATION", "Created");
define($prefijo."TECHNICAL_PORTAL", "Technical portal");
define($prefijo."PARTNER_PORTAL", "Sales portal");
define($prefijo."CUSTOMER_PORTAL", "End user portal");
define($prefijo."TYPE", "Type");
define($prefijo."LANGUAGE", "Language");
define($prefijo."YES", "Yes");
define($prefijo."NO", "No");
define($prefijo."WARNING", "Warning");
define($prefijo."SELECT_IMAGE", "You must select an image.");

define($prefijo."CHANNELS", "Personal Data");

define($prefijo."TRAINING_COORDINATOR", "Training Coordinator");
define($prefijo."TRAINING_COORDINATORS", "Training Coordinators");

define($prefijo."ATTACHMENTS", "Attachments");

define($prefijo."OBJECT_TYPE", "Object Type");
define($prefijo."RELATED_OBJECT", "Linked Object");

define($prefijo."CERTIFICATION", "Certification");
define($prefijo."CERTIFICATIONS", "Certificates");
define($prefijo."COURSE", "Course");
define($prefijo."COURSES", "Courses");
define($prefijo."STUDENT", "Student");
define($prefijo."STUDENTS", "Students");

define($prefijo."COUNTRY", "Country");
define($prefijo."COUNTRIES", "Country");

define($prefijo."PASSWORD", "Password");
define($prefijo."PASSWORDS", "Passwords");

define($prefijo."PICTURE", "Image");
define($prefijo."PICTURES", "Images");

define($prefijo."ROLE", "Role");
define($prefijo."ROLES", "Roles");

define($prefijo."USERDATA", "Ecampus Data");

define($prefijo."DELETE", "Delete");


define($prefijo."ENROLLMENT", "Registration");
define($prefijo."ENROLLMENTS", "Registered");

define($prefijo."PORTAL_TECH_1", "Technology");
define($prefijo."PORTAL_PART_2", "Sales");
define($prefijo."PORTAL_CUST_3", "End user");

define($prefijo."THERE", "There");
define($prefijo."NO_EXIST", "No");

define($prefijo."MONITORING", "Monitoring");

define($prefijo."SAVE", "Save");

define($prefijo."CHANGE_PASSWORD", "Change password");

define($prefijo."SEARCH", "Search");
define($prefijo."VIEW", "View");

define($prefijo."PREVIEW", "Preview");

define($prefijo."MAIN", "Main");
define($prefijo."EXIT", "Exit");

define($prefijo."IMPORTING_DATA", "Importing data");

define($prefijo."VALID", "Valid");
define($prefijo."INVALID", "Invalid");

define($prefijo."BACK", "Back");

define($prefijo."AUX_CENTER", "This object belongs to the center");

define($prefijo."COURSES_SELECT", "Course selector ");
define($prefijo."COURSES_CONF", "Configurator courses");

define($prefijo."STUDENT_STATUS", "Student status");
define($prefijo."STATUS", "Status");

define($prefijo."CENTER", "Center");
define($prefijo."CENTERS", "Centers");

define($prefijo."GROUP", "Group");
define($prefijo."GROUPS", "Groups");

define($prefijo."CENTER_GROUPS", "Groups center");
define($prefijo."CENTER_BOOKS", "Books center");

define($prefijo."ESPANOL", "Español");
define($prefijo."EUSKARA", "Euskera");

////////////////////////////////////////////////// /////////////////////////
////////////////////////////////////////////////// /////////////////////////
////////////////////////////////////////////////// /////////////////////////

$prefijoMsg = "MSG_";
define($prefijoMsg."INFO_CREATED_CORRECTLY", "Record created. ");
define($prefijoMsg."INFO_CHANGED_CORRECTLY", "Record modified successfully. ");
define($prefijoMsg."INFO_DELETED_CORRECTLY", "Record deleted. ");
define($prefijoMsg."CHECK_EXTENSION", "Check the size of the image to upload. \\nYou can only upload images with the following extensions: ");
define($prefijoMsg."CHECK_EXTENSION_FILE", "Check the type of file to be upload. \\nYou can only upload files with the following extensions: ");

define($prefijoMsg."REQUIRED_FIELD", "Field $1 is required.");

define($prefijoMsg."SCALE_STUDENT_CORRECTLY", "Student scaled correctly.");

define($prefijoMsg."MODIFIED_BY_TEXT", "Last modification made by <b>$1</b> - <b>$2</b>");
					
define($prefijoMsg."USERNAME_VALID", "The username is valid ");
define($prefijoMsg."USERNAME_INVALID", "The username is invalid ");

define($prefijoMsg."ERROR_OBJECT_TYPE", "Should select the object type.");
define($prefijoMsg."ERROR_OBJECT_RELATED","Should select the related object.");

define($prefijoMsg."CERT_VALID","The student is not enrolled in this certification.");
define($prefijoMsg."CERT_INVALID","The student already enrolled in this certification.");

?>