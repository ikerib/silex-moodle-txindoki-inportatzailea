<?php
// Etiquetas ES para la Imágen

$prefijo="LBL_";

define($prefijo."IMAGE_NAME", "Image Name");
define($prefijo."NOTE", "Note");
define($prefijo."ORDER", "Order");
define($prefijo."NEW_PICTURE", "Add new image");

$prefijoMsg = "MSG_";
define($prefijoMsg."INFO_SIZE", "Remember that the maximum size of the image is $1 pixels wide and $2 pixels high. Allowed file types: GIF, JPEG or PNG. ");
define($prefijoMsg."INFO_ORIGINAL", "A size-reduced version of the image is shown. In order to see the original image click on it.");
define($prefijoMsg."INFO_SELECT","You must select an image.");
define($prefijoMsg."INFO_BIG_SIZE", "The image you are trying to upload is too big,  width ($1) x high ($2) pixels. It must not exceed: width ($3) x H ($4).");
define($prefijoMsg."INFO_LARGE", "The image you are trying to upload is too big ($1 bytes). It must not exceed $2 bytes.");

?>