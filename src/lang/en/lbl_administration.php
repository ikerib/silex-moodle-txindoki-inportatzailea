<?php
// Etiquetas EN para la company

$prefijo="LBL_";

define($prefijo."NAME1", "Name of the group");
define($prefijo."NAME2", "Name of the center");
define($prefijo."REGISTRATION_NUMBER", "Registration number");
define($prefijo."WEB", "Web");
define($prefijo."EMAIL_DOMAIN", "Email domain");

$prefijoMsg="MSG_";
define($prefijoMsg."ERROR_TEXT_01", "The name of the area of influence is required.");
?>