<?php
// Etiquetas ES para el archivo

$prefijo="LBL_";

define($prefijo."FILE_NAME", "File Name");
define($prefijo."FILE", "File");
define($prefijo."ATT_NOTE", "Note");

define($prefijo."NEW_ATTACHMENT", "Add new attachment");

$prefijoMsg = "MSG_";
define($prefijoMsg."ATT_SELECT_FILE","You must select a file.");
define($prefijoMsg."ATT_INFO_SIZE", "Remember that the maximum size is $1 kb. Allowed type files are: pdf, doc, xls, pps, jpg, gif, png, txt, zip or rar. ");

define($prefijoMsg."ATT_INFO_BIG_SIZE", "The file you are trying to upload is too big $1 bytes. Should be a maximum of $2 bytes.");

?>