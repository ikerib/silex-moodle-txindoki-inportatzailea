<?php
// Etiquetas EN para el menú

$prefijo="LBL_";

define($prefijo."MNU_OPEN", "Open");
define($prefijo."MNU_NEW", "New");
define($prefijo."-MNU_UTILITIES", "Utilities");
define($prefijo."-MANAGEMENT", "Management");
define($prefijo."-CUSTOMERS", "Customers");
define($prefijo."-SALES", "Sales");
define($prefijo."-MARKETING", "Marketing");
define($prefijo."-HHRR", "HHRR");
define($prefijo."-HELPDESK", "Helpdesk");
define($prefijo."-FIELDSERVICE", "Fieldservice");
define($prefijo."-SUPPORT", "Support");
define($prefijo."-EDM", "EDM");
define($prefijo."-WORKFLOW", "Workflow");
define($prefijo."-SURVEYS", "Surveys");
define($prefijo."-ELEARNING", "Manage center");
define($prefijo."-MNU_CHANNELS", "Personal Data");
define($prefijo."-MNU_REPORTS", "Reports");

define($prefijo."ELEARNING", "e-Learning");
?>