<?php
// Etiquetas EN para los roles

$prefijo="LBL_";

define($prefijo."ROLE_NAME", "Name of role");
define($prefijo."RECIPIENT_NOTIFICATION", "Recipient Notification");
define($prefijo."RECIPIENT_NONASSIGNED", "Recipient Non Assigned");
define($prefijo."ROLE_MEMBERS", "Role members");
define($prefijo."EMAILS_NOTIFICATIONS", "Notifications");
define($prefijo."RESOURCES", "Resources");
define($prefijo."EMPLOYEES", "Employees");
define($prefijo."RESOURCES_NOT_ASSIGNED", "Resources not assigned");
define($prefijo."ADD_RESOURCE_ALL", "Add all resources");
define($prefijo."ADD_RESOURCE", "Add resource");
define($prefijo."REMOVE_RESOURCE", "Remove resource");
define($prefijo."REMOVE_RESOURCE_ALL", "Remove all resources");
define($prefijo."ASSIGNED_RESOURCES", "Resources Assigned");
define($prefijo."POSSIBLE_ROLES", "Possible roles");
define($prefijo."ADD_ROLE_ALL", "Add all roles");
define($prefijo."ADD_ROLE", "Add role");
define($prefijo."REMOVE_ROLE", "Remove role");
define($prefijo."REMOVE_ROLE_ALL", "Remove all roles");
define($prefijo."ASSIGNED_ROLES", "Roles assigned");

$prefijoMsg="MSG_";
define($prefijoMsg."ROLE_ERROR_TEXT_01", "The name is required.");

?>