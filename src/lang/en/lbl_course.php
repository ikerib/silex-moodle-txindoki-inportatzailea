<?php
// Etiquetas EN para los cursos

$prefijo="LBL_";

define($prefijo."#COURSE_TYPE", "Type of course");
define($prefijo."#COURSE_CATEGORY", "Category");
define($prefijo."#VALIDATION_REQ", "Requires validation");
define($prefijo."#VALIDATION_NOREQ", "This course does not require enrollment validation.");

define($prefijo."#MOODLE_COURSES", "Moodle courses");
define($prefijo."#MOODLE_COURSE_ID", "ID moodle course");
define($prefijo."#CONTENT", "Content");
define($prefijo."#EXAM", "Exam");
define($prefijo."#ALT_DELETE_MOOD_COUR", "Delete this moodle course");
define($prefijo."#ALT_ADD_MOOD_COUR", "Add a moodle course");
define($prefijo."#TEACHER", "Professor");

define($prefijo."#COURSE_EN", "Course");
define($prefijo."#DESCRIPTION_EN", "Description");
define($prefijo."#COURSE_LOCAL", "Course in local language");
define($prefijo."#DESCRIPTION_LOCAL", "description language");

define($prefijo."#COURSE_INFO", "Course Information");
define($prefijo."#COURSE_VISIBLE", "This course will be visible in the next portal");

define($prefijo."#MAXIMUM_NUMBER_ATTENDEES", "Maximum number of attendees");
define($prefijo."#SCHEDULED", "scheduled");
define($prefijo."#PRICE", "Price");
define($prefijo."#DURATION", "Length");
define($prefijo."#MODALITY", "mode");
define($prefijo."#HEARING", "Hearing");
define($prefijo."#LOCATION", "Location");

define($prefijo."#NOTE_MINIMUM", "Minimum grade");
define($prefijo."#SAVE_NOTE_MINIMUM", "Save minimum grade");

define($prefijo."#NOTE_MINIMUM_MODIFIED", "The minimum grade has been modified");

define($prefijo."POSSIBLE_COURSES", "Possible Courses");
define($prefijo."ADD_COURSE_ALL", "Add all");
define($prefijo."ADD_COURSE", "Add course");
define($prefijo."REMOVE_COURSE", "Remove course");
define($prefijo."REMOVE_COURSE_ALL", "Remove All");
define($prefijo."ASSIGNED_COURSES", "Assigned courses ");

define($prefijo."#SELECT_CATEGORY", "You must select a type of course.");
define($prefijo."#COURSE_REQUIRED", "The name of the course is required.");

define($prefijo."FULLNAME", "Fullname");
define($prefijo."SHORTNAME", "Shortname");
define($prefijo."IDNUMBER", "ID moodle");
define($prefijo."SUMMARY", "Description");

define($prefijo."INTERNAL_ID", "Internal code");

define($prefijo."VALIDATE_FLAG", "The course is visible in the portal");
?>