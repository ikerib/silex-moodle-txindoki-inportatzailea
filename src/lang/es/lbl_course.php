<?php
// Etiquetas ES para los cursos

$prefijo="LBL_";

define($prefijo."LEVEL", "Nivel");
define($prefijo."CATEGORY", "Categoría");
define($prefijo."COURSE_MOODLE", "Curso Moodle");

define($prefijo."POSSIBLE_COURSES", "Cursos posibles");
define($prefijo."ADD_COURSE_ALL", "Añadir todos");
define($prefijo."ADD_COURSE", "Añadir curso");
define($prefijo."REMOVE_COURSE", "Quitar curso");
define($prefijo."REMOVE_COURSE_ALL", "Eliminar todos");
define($prefijo."ASSIGNED_COURSES", "Cursos asignados");

define($prefijo."FULLNAME", "Nombre completo");
define($prefijo."SHORTNAME", "Nombre corto");
define($prefijo."IDNUMBER", "Identificador moodle");
define($prefijo."SUMMARY", "Descripción");

define($prefijo."ICON", "Icono");
define($prefijo."NOTE", "Nota");
define($prefijo."NEW_PICTURE", "Añadir nueva imágen");

define($prefijo."PROGRAM", "Programa");
define($prefijo."OBJECTIVE", "Objetivo");

define($prefijo."DURACION", "Duración");
define($prefijo."RESPONSABLE", "Responsable");
define($prefijo."TIPO_INSCRIPCION", "Tipo de inscripción");

define($prefijo."INTERNAL_ID", "Código interno");

define($prefijo."VALIDATE_FLAG", "El curso es visible en el portal");

$prefijoMsg="MSG_";
define($prefijoMsg."INFO_SIZE", "Recuerde que los tamaños máximos son de $1 pixels de ancho y $2 pixels de altura. <br/>Se creará un icono de $3 pixels de ancho x $4 pixels de altura, visible en el portal. Subir iconos: GIF, JPEG o PNG.");
define($prefijoMsg."INFO_ORIGINAL", "Está viendo el icono. Si desea ver la imagen original pulse sobre ella.");
define($prefijoMsg."INFO_SELECT", "Tiene que seleccionar una imágen para convertirla en icono de este curso.");
define($prefijoMsg."INFO_BIG_SIZE", "La imágen que intenta subir es demasiado grande, sus medidas son: ancho($1) x alto($2) pixels de altura. Debe ser como máximo de: ancho($3) x alto($4).");
define($prefijoMsg."INFO_LARGE", "La imágen que intenta subir es demasiado grande $1 bytes. Debe ser como máximo de $2 bytes.");



?>