<?php
// Etiquetas ES para el empleado

$prefijo="LBL_";

define($prefijo."INTERNAL_ID4", "Código interno");
define($prefijo."NATIONAL_ID_TYPE", "Tipo documento");
define($prefijo."NATIONAL_ID", "Documento identificativo");
define($prefijo."SEX", "Sexo");
define($prefijo."FEMALE", "Mujer");
define($prefijo."MALE", "Hombre");
define($prefijo."USER_LOGIN", "Identificador de usuario");
define($prefijo."USER_EMAIL", "Correo electrónico del usuario");
define($prefijo."USER_LANGUAGE", "Idioma del usuario");
define($prefijo."DATE_BIRTH", "Fecha de nacimiento");
define($prefijo."PLACE_BIRTH", "Lugar de nacimiento");
define($prefijo."PROVINCE", "Provincia");
define($prefijo."COUNTRY_BIRTH", "País de nacimiento");
define($prefijo."NACIONALITY", "Nacionalidad");
define($prefijo."MATERNAL_LANG", "Lengua materna");
define($prefijo."DATE_ON", "Fecha alta");
define($prefijo."DATE_OFF", "Fecha baja");
define($prefijo."USER_DATA", "Datos del usuario");

define($prefijo."DEPARTMENT", "Departamento");

define($prefijo. "NEW_PASSWORD", "New password");
define($prefijo. "CONFIRM_PASSWORD", "Confirm password");

define($prefijo. "ROLE_ADMIN", "ADMIN");
define($prefijo. "ROLE_EMPLOYEE", "EMPLOYEE");

$prefijoMsg="MSG_";
define($prefijoMsg."USER_DATA_INFO1", "Si desea cambiar estos datos pulse");
define($prefijoMsg."USER_DATA_INFO2", "También puede crearlo desde la pestaña de \"Datos de usuario\"");

define($prefijoMsg."NEW_EMPLOYEE_SUBJECT", "Gureak - Empleado");
define($prefijoMsg."NEW_EMPLOYEE_TEXT_01", "Estimado empleado,\n\nLos datos para acceder a la plataforma son los siguientes:");

define($prefijoMsg."SELECT_FILE", "Tiene que seleccionar una archivo.");
define($prefijoMsg."INFO_SIZE", "Recuerde que el tamaño máximo es de $1 kb. Subir solamente archivos: csv o txt.");

define($prefijoMsg."ERROR_TEXT_01", "Debe asignar este Empleado a un Centro.");
define($prefijoMsg."ERROR_TEXT_02", "El campo nombre es obligatorio.");
define($prefijoMsg."ERROR_TEXT_03", "Debe elegir otro Identificador de Usuario. Ya existe un usuario con ese identificador.");
define($prefijoMsg."ERROR_TEXT_04", "\\nPara crear el usuario hay que rellenar los campos:\\nIdentificador de usuario,\\nCorreo electrónico del usuario e\\nIdioma del usuario.");

define($prefijoMsg."ERROR_TEXT_05", "Debe elegir un identificador de usuario para el Empleado.");
define($prefijoMsg."ERROR_TEXT_06", "Debe elegir un rol para el Empleado.");

define($prefijoMsg."EMAIL_TEXT01", "Debe tener un correo electrónico.");
define($prefijoMsg."USERDATA_TEXT01", "La Nueva contraseña y su Confirmación deben ser iguales.");
define($prefijoMsg."USERDATA_TEXT02", "Debe rellenar la Nueva contraseña y su Confirmación.");
define($prefijoMsg."USERDATA_TEXT03", "Ya existe un usuario con este nombre.");
define($prefijoMsg."USERDATA_TEXT04", "Debe elegir el nombre de usuario.");

define($prefijoMsg."USER_IDENTIFICATOR","This is one of the identifier options that we propose to you. You can choose another option if you prefer.<br/><b>Are you sure, the user ID generated contains alphanumeric characters in lowercase and without spaces.</b>");
define($prefijoMsg."SUBJECT_USER_PASSWORD", "Gureak - Contraseña de acceso");
define($prefijoMsg."NEW_USER_TEXT_01", "Estimado empleado,\n\nLos datos para acceder a la plataforma son los siguientes:");
define($prefijoMsg."USER_SIGNATURE", "\n\nAtentamente,\nGureak");
?>