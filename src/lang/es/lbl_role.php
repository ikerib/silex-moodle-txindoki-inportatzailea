<?php
// Etiquetas ES para los roles

$prefijo="LBL_";

define($prefijo."ROLE_NAME", "Nombre rol");
define($prefijo."RECIPIENT_NOTIFICATION", "Destinatario de las notificaciones");
define($prefijo."RECIPIENT_NONASSIGNED", "Destinatario no asignado");
define($prefijo."ROLE_MEMBERS", "Miembros del rol");
define($prefijo."EMAILS_NOTIFICATIONS", "Notificaciones");
define($prefijo."RESOURCES", "Recursos");
define($prefijo."RESOURCES_NOT_ASSIGNED", "Recursos no asignados");
define($prefijo."ADD_RESOURCE_ALL", "Añadir todos los recursos");
define($prefijo."ADD_RESOURCE", "Añadir recurso");
define($prefijo."REMOVE_RESOURCE", "Quitar recurso");
define($prefijo."REMOVE_RESOURCE_ALL", "Eliminar todos los recursos");
define($prefijo."ASSIGNED_RESOURCES", "Recursos asignados");
define($prefijo."POSSIBLE_ROLES", "Roles posibles");
define($prefijo."ADD_ROLE_ALL", "Añadir todos los roles");
define($prefijo."ADD_ROLE", "Añadir rol");
define($prefijo."REMOVE_ROLE", "Quitar rol");
define($prefijo."REMOVE_ROLE_ALL", "Eliminar todos los roles");
define($prefijo."ASSIGNED_ROLES", "Roles asignados");

$prefijoMsg="MSG_";
define($prefijoMsg."ROLE_ERROR_TEXT_01", "El campo nombre es obligatorio.");

?>