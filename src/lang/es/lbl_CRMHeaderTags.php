<?php
// Etiquetas ES para los Header Tags

$prefijo="MSG_";

define($prefijo."WARNING", "Atención");
define($prefijo."UNSAVED_FORWARD", "No se han guardado los últimos cambios realizados. \\nDebe guardarlos antes de continuar.\\n\\nPulse Aceptar para continuar sin guardar o pulse Cancelar.\\n");
define($prefijo."UNSAVED_BACKWARD", "No se han guardado los cambios realizados. Pulse 'Aceptar' para continuar (se perderán las modificaciones) o pulse 'Cancelar' para detener el proceso.");
define($prefijo."CONFIRM_DELETION", "Pulse 'Aceptar' si realmente desea eliminar los datos del registro actual o pulse 'Cancelar' para detener el proceso.");
define($prefijo."DATE_END_INVALID", "La fecha de finalización no puede ser anterior a la fecha de inicio.");
define($prefijo."INVALID_DECIMAL_NUMBER", "El número introducido no es un valor decimal válido.");
define($prefijo."INVALID_NUMBER", "Debe insertar un valor numérico en:");

?>