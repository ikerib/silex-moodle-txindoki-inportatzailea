<?php
// Etiquetas ES para el alumno

$prefijo="LBL_";

define($prefijo."INTERNAL_ID4", "Código interno");
define($prefijo."ENTERPRISE", "Empresa");
define($prefijo."SECTION", "Sección");
define($prefijo."#SEX", "Sexo");
define($prefijo."#FEMALE", "Mujer");
define($prefijo."#MALE", "Hombre");
define($prefijo."USER_LOGIN", "Identificador de usuario");
define($prefijo."USER_EMAIL", "Correo electrónico del usuario");
define($prefijo."USER_LANGUAGE", "Idioma del usuario");
define($prefijo."#DATE_BIRTH", "Fecha de nacimiento");
define($prefijo."#PLACE_BIRTH", "Lugar de nacimiento");
define($prefijo."#PROVINCE", "Provincia");
define($prefijo."#COUNTRY_BIRTH", "País de nacimiento");
define($prefijo."#NACIONALITY", "Nacionalidad");
define($prefijo."#MATERNAL_LANG", "Lengua materna");
define($prefijo."#DATE_ON", "Fecha alta");
define($prefijo."#DATE_OFF", "Fecha baja");
define($prefijo."REGISTRATION_DATE", "Fecha de registro");
define($prefijo."USER_DATA", "Datos del usuario");
define($prefijo."#STUDENT_STATUS", "Estado de validación del alumno");

define($prefijo."#INTERNAL_DATA_MANAGEMENT", "Datos de gestión interna");

define($prefijo."#ADD_USER", "Crear usuario");
define($prefijo."#UPDATE_USER", "Modificar usuario");

define($prefijo."USER_ID", "Usuario");
define($prefijo."#EMAIL1", "Correo electrónico");
define($prefijo."OLD_PASSWORD", "Contraseña actual");
define($prefijo."NEW_PASSWORD", "Nueva contraseña");
define($prefijo."CONFIRM_PASSWORD", "Confirmar contraseña");
define($prefijo."CONFIRM_NEW_PASSWORD", "Confirmar nueva contraseña");

define($prefijo."CARD_ID", "NIF");
define($prefijo."CITY", "Ciudad");
define($prefijo."MOBILE", "Movil");
define($prefijo."PHONE", "Teléfono");

define($prefijo."PICTOGRAM", "Pictograma");
define($prefijo."PICTOGRAM_NOTE", "Para los alumnos de Nivel 1 y 2 no es obligatorio seleccionar ningún Pictograma.");
define($prefijo."HIGH_CONTRAST", "Alto contraste");
define($prefijo."LOCUTION", "Locución");
define($prefijo."PROFILE_SETTINGS", "Configuración del perfil");

define($prefijo."POSSIBLE_STUDENTS", "Alumnos posibles");
define($prefijo."ADD_STUDENT_ALL", "Añadir todos");
define($prefijo."ADD_STUDENT", "Añadir alumno");
define($prefijo."REMOVE_STUDENT", "Quitar alumno");
define($prefijo."REMOVE_STUDENT_ALL", "Eliminar todos");
define($prefijo."ASSIGNED_STUDENTS", "Alumnos asignados");

define($prefijo."LEVEL", "Nivel");
define($prefijo."COURSE_LEVEL4", "Curso para el Nivel 4");
define($prefijo."COURSE_LEVEL4_COMPULSORY", "Solo obligatorio para el nivel 4");

define($prefijo."PHOTO", "Fotografía del alumno, extraida de su perfil en Moodle");
define($prefijo."NOTE", "Nota");
define($prefijo."NEW_PICTURE", "Añadir nueva fotografía");

$prefijoMsg="MSG_";
define($prefijoMsg."USER_DATA_INFO1", "Este alumno todavía no tiene usuario creado para poder acceder.");
define($prefijoMsg."USER_IDENTIFICATOR", "<b>Asegurese que el Identificador de usuario generado contiene carácteres alfanuméricos en minúsculas y sin espacios.</b>");

define($prefijoMsg."USER_DATA_INFO2", "Si quiere cambiarle la constraseña (el usuario la recibirá en el correo configurado).");

define($prefijoMsg."#SUBJECT_USER_SITE", "Santillana");
define($prefijoMsg."#USER_ACCEPTED", "Ingreso en Santillana Aceptado");
define($prefijoMsg."#USER_REJECTED", "Ingreso en Santillana Rechazado");
define($prefijoMsg."#USER_ACCEPTED_TXT_01", "Le comunicamos que su petición de acceso a Santillana ha sido aceptada.");
define($prefijoMsg."#USER_ACCEPTED_TXT_02", "Sus datos de acceso al Portal de Santillana son los siguientes:");
define($prefijoMsg."#USER_REJECTED_TXT_01", "Lamentamos comunicarle que su petición de acceso a Santillana ha sido rechazada.");

define($prefijoMsg."USER_SIGNATURE", "Atentamente,\nGureak");

define($prefijoMsg."#NEW_USER_SUBJECT", "Santillana - Alumno");
define($prefijoMsg."NEW_USER_TEXT_01", "Estimado Alumno,\n\nLos datos para acceder a la plataforma son los siguientes:");

define($prefijoMsg."#PASSWORD_SUCCESSFULLY_CHANGED", "La contraseña del usuario se cambió correctamente.");
define($prefijoMsg."#EMAIL_SUCCESSFULLY_CHANGED", "El correo electrónico se cambió correctamente.");
define($prefijoMsg."#USER_SUCCESSFULLY_CHANGED", "El usuario se cambió correctamente.");

define($prefijoMsg."EMAIL_TEXT01", "Debe tener un correo electrónico.");
define($prefijoMsg."USERDATA_TEXT01", "La Nueva contraseña y su Confirmación deben ser iguales.");
define($prefijoMsg."USERDATA_TEXT02", "Debe rellenar la Nueva contraseña y su Confirmación.");
define($prefijoMsg."USERDATA_TEXT03", "Ya existe un usuario con este nombre.");
define($prefijoMsg."USERDATA_TEXT04", "Debe elegir el nombre de usuario.");


define($prefijoMsg."SUBJECT_USER_PASSWORD", "Gureak - Access Password");


define($prefijoMsg."INFO_SIZE", "Recuerde que los tamaños máximos son de $1 pixels de ancho y $2 pixels de altura. <br/>Se creará una fotografía de $3 pixels de ancho x $4 pixels de altura, visible en el portal. Subir fotografía: GIF, JPEG o PNG.");
define($prefijoMsg."INFO_ORIGINAL", "Está viendo la fotografía. Si desea ver la imagen original pulse sobre ella.");
define($prefijoMsg."INFO_SELECT", "Tiene que seleccionar una imágen para convertirla en fotografía de este alumno.");
define($prefijoMsg."INFO_BIG_SIZE", "La imágen que intenta subir es demasiado grande, sus medidas son: ancho($1) x alto($2) pixels de altura. Debe ser como máximo de: ancho($3) x alto($4).");
define($prefijoMsg."INFO_LARGE", "La imágen que intenta subir es demasiado grande $1 bytes. Debe ser como máximo de $2 bytes.");






?>