<?php
// Etiquetas ES genéricas

$prefijo="LBL_";

define($prefijo."PROCESSING", "PROCESANDO");

define($prefijo."TITLE", "Título");

define($prefijo."FIRST_NAME", "Nombre");
define($prefijo."LAST_NAME1", "Primer apellido");
define($prefijo."LAST_NAME2", "Segundo apellido");

define($prefijo."NAME", "Nombre");
define($prefijo."DETAIL", "Detalle");
define($prefijo."REMARKS", "Comentarios");
define($prefijo."REMARKS1", "Notas");
define($prefijo."ES", "en español");
define($prefijo."EN", "en inglés");
define($prefijo."ALT_CALENDAR", "Mostrar calendario");
define($prefijo."DESCRIPTION", "Descripción");
define($prefijo."DATE_CREATION", "Fecha creación");
define($prefijo."TECHNICAL_PORTAL", "Portal técnicos");
define($prefijo."PARTNER_PORTAL", "Portal comerciales");
define($prefijo."CUSTOMER_PORTAL", "Portal usuario final");
define($prefijo."TYPE", "Tipo");
define($prefijo."LANGUAGE", "Lenguaje");
define($prefijo."YES", "Si");
define($prefijo."NO", "No");
define($prefijo."WARNING", "Aviso");
define($prefijo."SELECT_IMAGE", "Debe seleccionar una imágen.");


define($prefijo."CHANNELS", "Datos personales");

define($prefijo."EMPLOYEE", "Empleado");
define($prefijo."EMPLOYEES", "Empleados");

define($prefijo."ATTACHMENTS", "Adjuntos");

define($prefijo."OBJECT_TYPE", "Tipo de objeto");
define($prefijo."RELATED_OBJECT", "Objeto relacionado");

define($prefijo."CERTIFICATION", "Certificación");
define($prefijo."CERTIFICATIONS", "Certificaciones");
define($prefijo."COURSE", "Curso");
define($prefijo."COURSES", "Cursos");
define($prefijo."STUDENT", "Alumno");
define($prefijo."STUDENTS", "Alumnos");

define($prefijo."TUTOR", "Tutor");
define($prefijo."TUTORS", "Tutores");


define($prefijo."COUNTRY", "País");
define($prefijo."COUNTRIES", "Países");

define($prefijo."PASSWORD", "Contraseña");
define($prefijo."PASSWORDS", "Contraseñas");

define($prefijo."PICTURE", "Imágen");
define($prefijo."PICTURES", "Imágenes");

define($prefijo."ROLE", "Rol");
define($prefijo."ROLES", "Roles");

define($prefijo."USERDATA", "Datos de usuario");

define($prefijo."DELETE", "Borrar");


define($prefijo."ENROLLMENT", "Matrícula");
define($prefijo."ENROLLMENTS", "Matrículas");

define($prefijo."PORTAL_TECH_1", "Técnicos");
define($prefijo."PORTAL_PART_2", "Comerciales");
define($prefijo."PORTAL_CUST_3", "Usuario final");

define($prefijo."EXIST", "Existe");
define($prefijo."NO_EXIST", "No existe");

define($prefijo."MONITORING", "Seguimiento");

define($prefijo."SAVE", "Guardar");

define($prefijo."CHANGE_PASSWORD", "Cambiar contraseña");

define($prefijo."SEARCH", "Localizar");
define($prefijo."VIEW", "Ver");

define($prefijo."PREVIEW", "Vista previa");

define($prefijo."MAIN", "Inicio");
define($prefijo."EXIT", "Salir");

define($prefijo."IMPORTING_DATA", "Importar datos");

define($prefijo."VALID", "Válido");
define($prefijo."INVALID", "No válido");

define($prefijo."BACK", "Volver");

define($prefijo."AUX_CENTER", "Este objeto pertenece al centro");

define($prefijo."COURSES_SELECT", "Selector de cursos");
define($prefijo."COURSES_CONF", "Configurador de cursos");

define($prefijo."STUDENT_STATUS", "Situación del alumno");
define($prefijo."STATUS", "Estado");

define($prefijo."CENTER", "Centro");
define($prefijo."CENTERS", "Centros");

define($prefijo."GROUP", "Grupo");
define($prefijo."GROUPS", "Grupos");

define($prefijo."CENTER_GROUPS", "Grupos del centro");
define($prefijo."CENTER_BOOKS", "Libros del centro");

define($prefijo."ESPANOL", "Español");
define($prefijo."EUSKARA", "Euskera");


///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

$prefijoMsg="MSG_";
define($prefijoMsg."INFO_CREATED_CORRECTLY", "Registro creado correctamente.");
define($prefijoMsg."INFO_CHANGED_CORRECTLY", "Registro modificado correctamente.");
define($prefijoMsg."INFO_DELETED_CORRECTLY", "Registro borrado.");
define($prefijoMsg."CHECK_EXTENSION", "Compruebe la extensión de la imágen a subir.\\nSólo se pueden subir imágenes con las siguientes extensiones: ");
define($prefijoMsg."CHECK_EXTENSION_FILE", "Compruebe la extensión del archivo a subir.\\nSólo se pueden subir archivos con las siguientes extensiones: ");

define($prefijoMsg."REQUIRED_FIELD", "El campo $1 es obligatorio.");

define($prefijoMsg."SCALE_STUDENT_CORRECTLY", "Alumno escalado correctamente.");

define($prefijoMsg."MODIFIED_BY_TEXT", "Última modificación realizada por <b>$1</b> el <b>$2</b>");

define($prefijoMsg."USERNAME_VALID", "El nombre de usuario es válido");
define($prefijoMsg."USERNAME_INVALID", "El nombre de usuario no es válido");

define($prefijoMsg."ERROR_OBJECT_TYPE", "Debe seleccionar el tipo de objeto.");
define($prefijoMsg."ERROR_OBJECT_RELATED", "Debe seleccionar el objeto relacionado.");

define($prefijoMsg."CERT_VALID", "El alumno no está matriculado en esta certificación.");
define($prefijoMsg."CERT_INVALID", "El alumno ya está matriculado en esta certificación.");

define($prefijoMsg."CAMPOS_OBLIGATORIOS", "* Los campos indicados son obligatorios.");



?>