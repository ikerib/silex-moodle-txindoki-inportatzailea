<?php
// Etiquetas ES para la pantalla de login

$prefijo="LBL_";

define($prefijo."LOGIN_MESSAGE", "Escriba su nombre de usuario y contraseña para acceder al gestor de la aplicación.");
define($prefijo."LOGIN_INCORRECT", "Usuario o contraseña inválidos.");
define($prefijo."USER_NOT_EMPLOYEE", "No tiene permisos para acceder a la aplicación");
define($prefijo."SESSION_LOST", "Se ha perdido la sesión. Debe hacer login de nuevo.");
define($prefijo."NO_PERMISSION", "No tiene permisos para acceder a esta pantalla.");
define($prefijo."USER", "Usuario");
define($prefijo."PASSWORD", "Contraseña");
define($prefijo."ENTER", "Entrar");

?>