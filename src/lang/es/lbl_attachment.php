<?php
// Etiquetas ES para el archivo

$prefijo="LBL_";

define($prefijo."FILE_NAME", "Nombre del archivo");
define($prefijo."FILE", "Archivo");
define($prefijo."ATT_NOTE", "Nota");

define($prefijo."NEW_ATTACHMENT", "Añadir nuevo archivo adjunto");

$prefijoMsg="MSG_";
define($prefijoMsg."ATT_SELECT_FILE", "Tiene que seleccionar una archivo.");
define($prefijoMsg."ATT_INFO_SIZE", "Recuerde que el tamaño máximo es de $1 kb. Subir solamente archivos: pdf, doc, xls, pps, jpg, gif, png, txt, zip o rar.");

define($prefijoMsg."ATT_INFO_BIG_SIZE", "El archivo que intenta subir es demasiado grande $1 bytes. Debe ser como máximo de $2 bytes.");


?>