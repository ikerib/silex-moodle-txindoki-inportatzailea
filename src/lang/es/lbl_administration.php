<?php
// Etiquetas ES para la company

$prefijo="LBL_";

define($prefijo."NAME1", "Nombre del grupo");
define($prefijo."NAME2", "Nombre del centro");
define($prefijo."REGISTRATION_NUMBER", "Número de registro");
define($prefijo."WEB", "Web");
define($prefijo."EMAIL_DOMAIN", "Dominio de correo");

$prefijoMsg="MSG_";
define($prefijoMsg."ERROR_TEXT_01", "El nombre del grupo es obligatorio.");

?>