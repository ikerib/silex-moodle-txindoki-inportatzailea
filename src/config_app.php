<?php

$path_contents_secure_absolute=$_SERVER['DOCUMENT_ROOT']."/../logs/";
$ruta_debug=$_SERVER['DOCUMENT_ROOT']."/../logs/";
$nombre_log="log_srm.txt";
$nivel_debug=1; //DEBUG=1 INFO=2 WARN=3 ERROR=4 FATAL=5 OFF=6

$prefijo="CFG_";

//PICTURE
define($prefijo."PICTURE_URL", $path_contents_secure_absolute."company$1/images/picture/");
define($prefijo."PICTURE_WIDTH", "400");	//px
define($prefijo."PICTURE_HEIGHT", "300");	//px
define($prefijo."TH_PICTURE_WIDTH", "89");	//px
define($prefijo."TH_PICTURE_HEIGHT", "67");//px
define($prefijo."SIZE_BYTES", "102400");
define($prefijo."SIZE_KB", "100");

//COURSE
define($prefijo."PICTURE_COURSE_URL", $path_contents_secure_absolute."company$1/images/course/");
define($prefijo."PICTURE_COURSE_WIDTH", "300");	//px
define($prefijo."PICTURE_COURSE_HEIGHT", "300");	//px
define($prefijo."TH_PICTURE_COURSE_WIDTH", "90");	//px
define($prefijo."TH_PICTURE_COURSE_HEIGHT", "65");//px
define($prefijo."SIZE_COURSE_BYTES", "102400");
define($prefijo."SIZE_COURSE_KB", "100");

//CATEGORY
define($prefijo."PICTURE_CATEGORY_URL", $path_contents_secure_absolute."company$1/images/category/");
define($prefijo."PICTURE_CATEGORY_WIDTH", "200");	//px
define($prefijo."PICTURE_CATEGORY_HEIGHT", "400");	//px
define($prefijo."TH_PICTURE_CATEGORY_WIDTH", "192");	//px
define($prefijo."TH_PICTURE_CATEGORY_HEIGHT", "378");//px
define($prefijo."SIZE_CATEGORY_BYTES", "102400");
define($prefijo."SIZE_CATEGORY_KB", "100");

define($prefijo."PICTURE_CATEGORY_URL_AC", $path_contents_secure_absolute."company$1/images/category/ac/");
define($prefijo."PICTURE_CATEGORY_WIDTH_AC", "200");	//px
define($prefijo."PICTURE_CATEGORY_HEIGHT_AC", "400");	//px
define($prefijo."TH_PICTURE_CATEGORY_WIDTH_AC", "192");	//px
define($prefijo."TH_PICTURE_CATEGORY_HEIGHT_AC", "378");//px
define($prefijo."SIZE_CATEGORY_BYTES_AC", "102400");
define($prefijo."SIZE_CATEGORY_KB_AC", "100");

//ATTACHMENT
define($prefijo."ATTACHMENT_URL", $path_contents_secure_absolute."company$1/files/attachment/");
define($prefijo."ATTACHMENT_SIZE_BYTES", "3072000");
define($prefijo."ATTACHMENT_SIZE_KB", "3000");
define($prefijo."MAX_UPLOAD_FILE_SIZE", "5242880");
define($prefijo."MAX_UPLOAD_FILE_SIZE_MB", "5");

//STUDENT
define($prefijo."PICTURE_STUDENT_URL", $path_contents_secure_absolute."company$1/images/student/");
define($prefijo."PICTURE_STUDENT_WIDTH", "300");	//px
define($prefijo."PICTURE_STUDENT_HEIGHT", "300");	//px
define($prefijo."TH_PICTURE_STUDENT_WIDTH", "70");	//px
define($prefijo."TH_PICTURE_STUDENT_HEIGHT", "92");//px
define($prefijo."SIZE_STUDENT_BYTES", "102400");
define($prefijo."SIZE_STUDENT_KB", "100");

//MISCELLANEOUS
define($prefijo."MAX_NUM_ROWS", "20");	//N�mero m�ximo de registros en un locator.
define($prefijo."EMAIL_FROM_DEFAULT", "guprest@grupogureak.com");	//Email usado para enviar correo desde el from
define($prefijo."EMAIL_SMTP", "correo.sarenet.es");	//Smtp
define($prefijo."EMAIL_SMTP_USER", "guprest@ext.grupogureak.com");	//Smtp usuario
define($prefijo."EMAIL_SMTP_PASS", "gi10.pr_st"); //Smtp contrase�a

define($prefijo."MOODLE_URL", "http://moodle.gureak-akademi.local/login/index.php");	//Ruta de acceso al Moodle.
define($prefijo."MOODLE_URL_GET_IMAGE", "http://moodle.gureak-akademi.local/pluginfile.php");	//Ruta de acceso al Moodle.

define($prefijo."URL_PORTAL", "http://www.gureak-akademi.local/");	//Ruta de acceso al Portal.
?>