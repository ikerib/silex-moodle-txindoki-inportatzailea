<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

$app->get('/', function () use ($app) {
  $sql = "SELECT mu.id, mu.firstname, mu.lastname , c.fullname, gg.finalgrade
          FROM mdl_grade_items AS gi
          INNER JOIN mdl_course c
            ON c.id = gi.courseid
          LEFT JOIN mdl_grade_grades AS gg
            ON gg.itemid = gi.id
          INNER JOIN mdl_user AS mu
            ON gg.userid = mu.id
          WHERE gi.itemtype = 'course'
          ORDER BY c.id asc";
  $users = $app['dbs']['mysql_moodle']->fetchAll($sql, array());

  return $app['twig']->render('moodle.html',array('users'=>$users));

});



$app->get('/gehitu', function () use ($app) {

    // $sql = "SELECT * FROM ic_user";
    // $users = $app['db']->fetchAll($sql, array());
    $users=null;
    $obj = new Student();
    $obj->loadData("new");
    $action="NEW";
    $id="new";
    if ($action=="NEW"){
      $datuak['id']='1111';
      $datuak['name']='1111';
      $datuak['last_name_1'] = '1111';
      $datuak["last_name_2"] ="";
      $datuak["registration_date"] = date(CFG_FORMATO_FECHA);
      $datuak["user_valido"] = "NO EXISTE";
      $datuak["user_name"] =  '1111';
      $datuak["user_email"] = '1111@auskalo1.com';
      $datuak["user_lang"] = 'es'; // 'eu'
      $datuak["npass"]='1111';
      $datuak["cpass"]='1111';
      $datuak["level"] = '1';
      $datuak["course_particular_id"]='0';
      $datuak['pictograma1'] = '0';
      $datuak['pictograma2'] = '0';
      $datuak['pictograma3'] = '0';
      $datuak["locution"] = '1'; // Si
      $datuak["high_contrast"] = '2'; // No
      $datuak["card_id"]=""; // NAN
      $datuak["phone"]="";
      $datuak["mobile"]="";
      $datuak["city"]="";
      $datuak["internal_id"]="";
      $datuak["empresa"]="";
      $datuak["seccion"]="";
      $datuak['bupload_image']="no";
      $datuak['picture_file_old']="";
      $datuak["company_id"]="1";
      $datuak["remarks"]="";

      $resultado = $obj->init($datuak);
      if ($resultado==""){ //NO hay error.
        $conn = $app['db'];
        $error = $obj->createNew($datuak);

        $id = $obj->getID(); //Último insertado
      }
    }
    return $app['twig']->render('index.html',array('users'=>$users));

})
->bind('gehitu')
;



$app->error(function (\Exception $e, $code) use ($app) {
    if ($app['debug']) {
        return;
    }

    $page = 404 == $code ? '404.html' : '500.html';

    return new Response($app['twig']->render($page, array('code' => $code)), $code);
});
