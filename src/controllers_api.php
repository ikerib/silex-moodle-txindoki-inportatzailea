<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

$app->get('/api/usuarios/{usuario}', function ($usuario) use ($app) {

  $sql = "SELECT DISTINCT c.id, c.fullname, gg.id , gg.finalgrade
          FROM mdl_grade_items AS gi
            INNER JOIN mdl_course c ON c.id = gi.courseid
            LEFT JOIN mdl_grade_grades AS gg ON gg.itemid = gi.id
            INNER JOIN mdl_user AS mu ON gg.userid = mu.id
          WHERE gi.itemtype = 'course'
            AND mu.username = '$usuario'
          ORDER BY mu.id desc, c.id asc";

  $cursos = $app['dbs']['mysql_moodle']->fetchAll($sql, array());

  return new Response(json_encode($cursos), 200, array('Content-Type' => 'application/json'));
});

$app->get('/api/usuarios/{usuario}/{curso}', function ($usuario, $curso) use ($app) {

  $sql = "SELECT DISTINCT c.id, c.fullname, gg.id , gg.finalgrade
          FROM mdl_grade_items AS gi
            INNER JOIN mdl_course c ON c.id = gi.courseid
            LEFT JOIN mdl_grade_grades AS gg ON gg.itemid = gi.id
            INNER JOIN mdl_user AS mu ON gg.userid = mu.id
          WHERE gi.itemtype = 'course'
            AND mu.username = '$usuario'
            AND c.fullname LIKE '%$curso%'
          ORDER BY mu.id desc, c.id asc";

  $cursos = $app['dbs']['mysql_moodle']->fetchAll($sql, array());

  return new Response(json_encode($cursos), 200, array('Content-Type' => 'application/json'));
});

$app->get('/api/cursos/{curso}', function ($curso) use ($app) {

  $sql = "SELECT DISTINCT mu.id, mu.username, mu.firstname, mu.lastname , gg.finalgrade
          FROM mdl_grade_items AS gi
            INNER JOIN mdl_course c ON c.id = gi.courseid
            LEFT JOIN mdl_grade_grades AS gg ON gg.itemid = gi.id
            INNER JOIN mdl_user AS mu ON gg.userid = mu.id
          WHERE gi.itemtype = 'course'  and c.fullname LIKE '$curso'
          ORDER BY mu.id desc, c.id asc";

  $cursos = $app['dbs']['mysql_moodle']->fetchAll($sql, array());

  return new Response(json_encode($cursos), 200, array('Content-Type' => 'application/json'));
});

$app->error(function (\Exception $e, $code) use ($app) {
    if ($app['debug']) {
        return;
    }

    $page = 404 == $code ? '404.html' : '500.html';

    return new Response($app['twig']->render($page, array('code' => $code)), $code);
});
