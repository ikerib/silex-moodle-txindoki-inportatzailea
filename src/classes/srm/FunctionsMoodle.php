<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/database/DB_Moodle_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT']."/../src/lang/en/config.php");

class UtilMoodle
{

	function desplegableMoodle($tabla, $campo_pk, $campo_valor, $campo_selec, $language, $vacio, $active, $orderby="", $where="")
	{

		global $mysqliMoodle;
		global $errorLog;
		$resultado = "";

		try{
			$query_select = sprintf("SELECT %s as id, %s as valor FROM %s WHERE 1=1 ", $campo_pk, $campo_valor, $tabla ) ;

			if ($language!="")
				$query_select.= " AND language = '" . $language . "'";

			if ($active==1)
				$query_select.= " AND active_flag = 1 ";

			if ($where!="")
				$query_select.= " ". $where;

			if ($orderby!="")
				$query_select.= " ORDER BY ". $orderby;

			if ($vacio)
				$resultado = "<option value=\"0\">&nbsp;</option>";



			if ($result = $mysqliMoodle->query($query_select))
			{
				while($row = $result->fetch_array()){
					$selected = "";
					if ($campo_selec == $row["id"])
						$selected = "selected";
					$resultado.= "<option value=\"". $row["id"] . "\" ".$selected.">" . ($row["valor"]) . "</option>";
				}
				$result->close();
			}

		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
	    }

		$errorLog->LogDebug("SELECT: $query_select");
		return $resultado;

	}//end desplegableMoodle


}



?>