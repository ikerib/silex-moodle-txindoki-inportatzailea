<?php
require_once($_SERVER['DOCUMENT_ROOT']."/../src/classes/srm/KLogger.php");
require_once($_SERVER['DOCUMENT_ROOT']."/../src/config_app.php");

/*
NIVELES DE DEBUG
//DEBUG=1 INFO=2 WARN=3 ERROR=4 FATAL=5 OFF=6
*/

$errorLog = new KLogger ( $ruta_debug.$nombre_log , $nivel_debug );

/* EJEMPLOS

// DEBUG=1 : Print out the value of some variables
$errorLog->LogDebug("User Count: $User_Count");

// INFO=2 : Print out some information
$errorLog->LogInfo("Internal Query Time: $time_ms milliseconds");

// WARN=3 : Print out some information
$errorLog->LogWarn("Internal Query Time: $time_ms milliseconds");

// ERROR=4 : Do database work that throws an exception
$errorLog->LogError("An exception was thrown in ThisFunction()");

// FATAL=5 : Do database work that throws an exception
$errorLog->LogFatal("An exception was thrown in ThisFunction()");


*/


?>