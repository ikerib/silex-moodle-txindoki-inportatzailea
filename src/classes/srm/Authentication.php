<?php

// Clase
require_once($_SERVER['DOCUMENT_ROOT']."/../src/classes/database/DB_Connection.php");
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/admin/Role.php');

class Authentication
{
	public $msUsername = "";
	public $msPassword = "";

	function Authentication()
	{
		if (isset($_POST['username']))
			$this->msUsername = $_POST['username'];
		if (isset($_POST['password']))
			$this->msPassword = $_POST['password'];
	}

	public function getPassword(){
		return $this->msPassword;
	}

	public function setPassword($value){
		$this->msPassword = $value;
	}

	public function getUsername(){
		return $this->msUsername;
	}

	public function setUsername($value){
		$this->msUsername = $value;
	}

	function validate_user()
	{
		global $mysqli;
		$is_valid = false;

		$encodePwd = $this->encodePassword();

	  	$query_select = sprintf("SELECT ic_user.password FROM ic_user WHERE ic_user.active_flag = 1 AND ic_user.user_id='%s';", $this->msUsername);

		if ($result = $mysqli->query($query_select))
		{
			$row = $result->fetch_array();
			if ($row["password"]==$encodePwd){
				$_SESSION["user_id"] = $this->msUsername;
				$is_valid = true;
			}
			$result->close();
		}



		return $is_valid;

	}

	function validate_user_crm()
	{
		global $mysqli;
		$is_valid = false;

		$encodePwd = $this->encodePassword();

		$query_select = "SELECT ic_user.password, ".
						"		ic_user.language, ".
						"		ic_employee.company_id, ".
						"		ic_employee.employee_id, ".
						"		ic_employee.employee_first_name, ".
						"		ic_employee.employee_last_name1, ".
						"		ic_company.company_name ".
						" FROM ic_user, ic_employee, ic_company ".
						" WHERE ic_employee.user_id=ic_user.user_id ".
						"		AND ic_user.active_flag = 1 ".
						"		AND ic_company.company_id = ic_employee.company_id ".
						"		AND ic_employee.active_flag=1 ".
						"		AND ic_user.user_id='%s';";
	  	$query_select = sprintf($query_select, $this->msUsername);

		if ($result = $mysqli->query($query_select))
		{
			$row = $result->fetch_array();
			if ($row[0]==$encodePwd){
				$_SESSION["user_id"] = $this->msUsername;
				$_SESSION["language"] = $row["language"];
				$_SESSION["company_id"] = $row["company_id"];
				$_SESSION["employee_id"] = $row["employee_id"];
				$_SESSION["company_name"] = $row["company_name"];
				$_SESSION["employee_name"] = $row["employee_first_name"]." ".$row["employee_last_name1"];
				$oRole = new Role();
				$_SESSION["permissions"] = $oRole->getAllowedObjects();
				$is_valid = true;
			}
			$result->close();
		}

		return $is_valid;

	}


	function encodePassword()
	{
		return md5($this->msPassword);
	}

	function genRandomPassword()
	{
		$str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
		$cad = "";
		for($i=0;$i<8;$i++) {
			$cad .= substr($str,rand(0,62),1);
		}
		return $cad;
	}

	function accessRegister($description, $user_id, $company_id){

		global $mysqli;

		if ($company_id == 0)
			$company_id = "NULL";

		$query_insert = sprintf("INSERT INTO ic_log (description, company_id, timestamp, modified_by) VALUES (
									'%s',
									%d,
									CURRENT_TIMESTAMP,
									'%s')",
									$description,
									$company_id,
									$user_id
								);

		$result = $mysqli->query($query_insert);
		if ($mysqli->error){
			$errorLog->LogError($mysqli->error);
		}

	}//accessRegister


	function check_user_type()
	{
		global $mysqli;
		global $errorLog;

		$is_valid = 0; //ERROR
		$user_type="EMPLOYEE";

		//MIRAMOS SI EL USUARIO ES ALUMNO O TUTOR.

		$query_select = "SELECT ic_user.user_id
						FROM ic_user
							LEFT JOIN ic_tutor ON ic_user.user_id = ic_tutor.user_id
						WHERE ic_user.user_id = '%s'
							AND ic_tutor.active_flag=1
							AND ic_user.active_flag = 1 ";

	  	$query_select = sprintf($query_select, $this->msUsername);

		$errorLog->LogDebug("SELECT: $query_select");

		if ($result = $mysqli->query($query_select)) //ES TUTOR
		{
			if ($result->num_rows > 0){
				$user_type="TUTOR";
			}
		}
		$result->close();

		return $user_type;
	}


	function validate_user_tutor()
	{
		global $mysqli;
		$is_valid = false;

		$encodePwd = $this->encodePassword();

		$query_select = "SELECT ic_user.password, ".
						"		ic_user.language, ".
						"		ic_tutor.company_id, ".
						"		ic_tutor.tutor_id, ".
						"		ic_tutor.first_name, ".
						"		ic_tutor.last_name1, ".
						"		ic_company.company_name ".
						" FROM ic_user, ic_tutor, ic_company ".
						" WHERE ic_tutor.user_id=ic_user.user_id ".
						"		AND ic_user.active_flag = 1 ".
						"		AND ic_company.company_id = ic_tutor.company_id ".
						"		AND ic_tutor.active_flag=1 ".
						"		AND ic_user.user_id='%s';";
	  	$query_select = sprintf($query_select, $this->msUsername);

		if ($result = $mysqli->query($query_select))
		{
			$row = $result->fetch_array();

			if (isset($_POST["access"]) && $_POST["access"]!="" && isset($_POST["control_access"]) && $_POST["control_access"]!=""){
				if($row["password"]==$this->msPassword && $_POST["control_access"]==(md5($this->msUsername.$row["password"]))){
					$_SESSION["user_id"] = $this->msUsername;
					$_SESSION["language"] = $row["language"];
					$_SESSION["company_id"] = $row["company_id"];
					$_SESSION["employee_id"] = $row["tutor_id"];
					$_SESSION["company_name"] = $row["company_name"];
					$_SESSION["employee_name"] = $row["first_name"]." ".$row["last_name1"];
					$oRole = new Role();
					$_SESSION["permissions"] = $oRole->getAllowedObjectsTutor();
					$is_valid = true;
				}
			}else if($row["password"]==$encodePwd){
				$_SESSION["user_id"] = $this->msUsername;
				$_SESSION["language"] = $row["language"];
				$_SESSION["company_id"] = $row["company_id"];
				$_SESSION["employee_id"] = $row["tutor_id"];
				$_SESSION["company_name"] = $row["company_name"];
				$_SESSION["employee_name"] = $row["first_name"]." ".$row["last_name1"];
				$oRole = new Role();
				$_SESSION["permissions"] = $oRole->getAllowedObjectsTutor();
				$is_valid = true;
			}
			$result->close();
		}

		return $is_valid;

	}


}

?>