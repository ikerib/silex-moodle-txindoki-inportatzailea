<?php

require_once($_SERVER['DOCUMENT_ROOT']."/../src/classes/database/DB_Connection.php");
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/ObjectTypeData.php');

// Clase

class ObjectType extends ObjectTypeData
{

	public $object_type_id = 0;

	function ObjectType($value)
	{
		$this->object_type_id = $value;
	}

	public function loadData()
	{
		global $mysqli;

		try {
		$query_select = "SELECT
							object_type_id,
							language,
							object_type_name,
							icon,
							path,
							table_name,
							timestamp,
							modified_by
						FROM ica_object_type
						WHERE
							ica_object_type.language='%s'
							AND ica_object_type.object_type_id=%d";

			$query_select = sprintf($query_select, $_SESSION["language"], $this->object_type_id);

			if ($result = $mysqli->query($query_select))
			{
				$row = $result->fetch_array();

				$this->setID($row["object_type_id"]);
				$this->setName($row["object_type_name"]);
				$this->setPath($row["path"]);
				$this->setModifiedBy($row["modified_by"]);
				$this->setTimestamp($row["timestamp"]);

				$result->close();
			}
		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
	    }

	}//END loadData()


	/*
	function getMenuID()
	{
		global $mysqli;

		$menuID = "";

		$query_select = "SELECT
							 ica_menu_object_type.menu_id
						FROM ica_menu_object_type
						WHERE
							ica_menu_object_type.object_type_id=%d";

	  	$query_select = sprintf($query_select, $this->object_type_id);

		if ($result = $mysqli->query($query_select))
		{
			$row = $result->fetch_array();
			$menuID = $row["menu_id"];
			$result->close();
		}

		return $menuID;

	}
	*/
}
?>