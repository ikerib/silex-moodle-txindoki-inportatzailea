<?php

require_once($_SERVER['DOCUMENT_ROOT']."/../src/classes/database/DB_Connection.php");
require_once($_SERVER['DOCUMENT_ROOT']."/../src/lang/".$_SESSION["language"]."/lbl_menu.php");
// Clase

class MenuVertical
{

	public $object_type_id = 0;

	function MenuVertical($value)
	{
		$this->object_type_id = $value;
	}

	function getMenuVertical()
	{
		global $mysqli;
		global $errorLog;

		$host = $_SERVER['HTTP_HOST'];

		$sMenuID = 0;

        $query_select = "Select menu_id from ica_menu_object_type where object_type_id=%d";
		$query_select = sprintf($query_select, $this->object_type_id);

        if ($result = $mysqli->query($query_select))
		{
			$row = $result->fetch_array();
			$sMenuID = $row["menu_id"];
			$result->close();
		}

		if ($_SESSION['user_type']=="EMPLOYEE"){

			$query_select = "SELECT DISTINCT ica_menu.menu_id, ".
						"             ica_menu.label_code, ".
						"             ica_menu_object_type.object_type_id,  ".
						"             CASE WHEN ica_object_type.object_type_name='' THEN ica_object_type_default.object_type_name ELSE  ica_object_type.object_type_name END AS object_type_name, ".
						"             ica_object_type.icon, ".
						"             ica_menu_object_type.new_flag, ".
						"             ica_menu_object_type.tab_order, ".
						"             COALESCE(ica_object_type.path, '') as path, ".
						"             COALESCE(ica_menu_object_type.open_file,'') as open_file, ".
						"             COALESCE(ica_menu_object_type.new_file,'') as new_file ".
						"	FROM ica_menu,ica_menu_object_type, ica_object_type as ica_object_type_default ,  ica_object_type ".
						"       , ic_employee, ica_role_employee, ica_resource_role, ica_object_resource, ica_role ".
						"   WHERE ica_menu.menu_id = ica_menu_object_type.menu_id ".
						"       AND ica_menu_object_type.object_type_id = ica_object_type.object_type_id ".
						"       AND ica_object_type.language='%s' ".
						"       AND ica_menu_object_type.object_type_id = ica_object_type_default.object_type_id ".
						"       AND ica_object_type_default.language='%s' ".
						"       AND ica_menu_object_type.object_type_id = ica_object_resource.object_type_id  ".
						"       AND ic_employee.user_id = '%s' ".
						"       AND ic_employee.company_id = '%s' ".
						"       AND ica_role_employee.employee_id = ic_employee.employee_id  ".
						"       AND ica_role_employee.role_id = ica_resource_role.role_id  ".
						"       AND ica_resource_role.resource_id = ica_object_resource.resource_id  ".
						"       AND ica_role_employee.role_id = ica_role.role_id  ".
						"       AND ica_role_employee.active_flag = 1  ".
						"       AND ica_resource_role.active_flag = 1  ".
						"       AND ica_role.active_flag = 1 ".
						"       AND ica_menu.menu_id > 1 ". // Para que no muestre la pestaña de Inicio en el menu lateral
						"       ORDER BY ica_menu.menu_id, ica_menu_object_type.tab_order ";
		}else if ($_SESSION['user_type']=="TUTOR"){
			$query_select = "SELECT DISTINCT ica_menu.menu_id,
								ica_menu.label_code,
								ica_menu_object_type.object_type_id,
								CASE WHEN ica_object_type.object_type_name='' THEN ica_object_type_default.object_type_name ELSE  ica_object_type.object_type_name END AS object_type_name,
								ica_object_type.icon,
								ica_menu_object_type.new_flag,
								ica_menu_object_type.tab_order,
								COALESCE(ica_object_type.path, '') as path,
								COALESCE(ica_menu_object_type.open_file,'') as open_file,
								COALESCE(ica_menu_object_type.new_file,'') as new_file

								FROM ica_menu,
									ica_menu_object_type,
									ica_object_type as ica_object_type_default ,
									ica_object_type ,
									ica_resource_role,
									ica_object_resource, ica_role

								WHERE ica_menu.menu_id = ica_menu_object_type.menu_id
								AND ica_menu_object_type.object_type_id = ica_object_type.object_type_id
								AND ica_object_type.language='es'
								AND ica_menu_object_type.object_type_id = ica_object_type_default.object_type_id
								AND ica_object_type_default.language='es'
								AND ica_menu_object_type.object_type_id = ica_object_resource.object_type_id

								AND ica_resource_role.resource_id = ica_object_resource.resource_id
								AND ica_resource_role.role_id = 3
								AND ica_resource_role.active_flag = 1
								AND ica_role.active_flag = 1
								AND ica_menu.menu_id > 1        ";
		}
		$query_select = sprintf($query_select, $_SESSION["language"], $_SESSION["language"], $_SESSION["user_id"], $_SESSION["company_id"]);

		$errorLog->LogDebug("SELECT MENUVERTICAL: $query_select");
		error_log("SELECT MENUVERTICAL: $query_select");


		$sMenu = "<ul>";
		$sOpcion=0;
        $sEstado="cerrado";
        $sObjectoSeleccionado="";
        $sFondo="1";
        $sLabel ="";

		if ($result = $mysqli->query($query_select))
		{
			while($row = $result->fetch_array())
			{

				if ($sOpcion!=$row["menu_id"] && $sOpcion!=0){
					$sMenu .= "<li class=\"separa2\">&nbsp;</li>";
					$sMenu .= "</ul>";
					$sMenu .= "</li>";
				}
				if ($sOpcion!=$row["menu_id"]){

					if ($row["menu_id"]==$sMenuID){
						$sEstado="abierto";
					}

					$sMenu .= "<li class=\"".$sEstado."\" id=\"p".$row["menu_id"]."\"> <a href=\"javascript:P7_swapClass(1,'p".$row["menu_id"]."','abierto','cerrado','li')\">".constant($row["label_code"])."</a>";
					$sMenu .= "<ul>";
					$sMenu .= "<li class=\"separa1\">&nbsp;</li>";

					$sFondo="1";
					$sEstado="cerrado";

				}

	         	$sMenu .= "<li class=\"fondo".$sFondo."\">";
	         	$sMenu .= "<div class=\"icono\"><img src=\"/images/".$row["icon"]."\" alt=\"\" /></div>";

	         	$nValorTruncate=14;

	         	if ($this->object_type_id == $row["object_type_id"]){
	         		$sObjectoSeleccionado="_seleccionado";
	         		$nValorTruncate=13;
	         	}else
	         		$sObjectoSeleccionado="";

				$object_type_name = ($row["object_type_name"]);

				if (strlen($object_type_name)>$nValorTruncate){
					$sTruncatedString = substr($object_type_name, 0, $nValorTruncate-2).'...';
					$sMenu .= "<div class=\"nombre".$sObjectoSeleccionado."\"  title=\"".$object_type_name."\">".$sTruncatedString."</div>";
				}else{
					$sMenu .= "<div class=\"nombre".$sObjectoSeleccionado."\">".$object_type_name."</div>";
				}

				$ruta = $row["path"].$row["open_file"];
				if ($ruta==""){
					$ruta = "javascript:void();";
				}else{
					$ruta = "javascript:doOpen(".$row["object_type_id"].");";
				}
	         	$sMenu .= "<div class=\"abrir\"><a href=\"".$ruta."\"><img src=\"/images/abrir.gif\" width=\"13\" height=\"12\" alt=\"".LBL_MNU_OPEN."\" title=\"".LBL_MNU_OPEN."\" /></a></div>";

	         	if ($row["new_flag"]==1){
					$ruta = $row["path"].$row["new_file"];
					if ($ruta==""){
						$ruta = "javascript:void();";
					}else{
						$ruta = "javascript:doNew(".$row["object_type_id"].")";
					}

					//Si es tutor, no puede crear nuevos alumnos, solo modificarlos.
					if ($_SESSION['user_type']=="TUTOR" && ($row["object_type_id"]==100)){
					}else{
					    $sMenu .= "<div class=\"nuevo\"><a href=\"".$ruta."\"><img src=\"/images/nuevo.gif\" width=\"9\" height=\"12\" alt=\"".LBL_MNU_NEW."\" title=\"".LBL_MNU_NEW."\" /></a></div>";
					}

	        	}else{
	        		$sMenu .= "<div class=\"nuevo\">&nbsp;</div>";
	        	}

	         	$sMenu .= "<br class=\"limpiar\" />";
	         	$sMenu .= "</li>";

				$sOpcion = $row["menu_id"];
				if ($sFondo==1){
					$sFondo=2;
				}else {
					$sFondo=1;
				}

			}
        	$sMenu .= "<li class=\"separa2\">&nbsp;</li>";
         	$sMenu .= "</ul>";
			$sMenu .= "</li>";
			$sMenu .= "</ul>";

			$result->close();
		}

		return $sMenu;

	}

}

?>