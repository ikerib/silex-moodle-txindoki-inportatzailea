<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/database/DB_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT']."/../src/lang/en/config.php");
require_once($_SERVER['DOCUMENT_ROOT']."/../src/classes/phpmailer/class.phpmailer.php"); //Añadimos la clase de phpmailer
require_once($_SERVER['DOCUMENT_ROOT']."/../src/config_app.php");

class Util
{

	function desplegable($tabla, $campo_pk, $campo_valor, $campo_selec, $language, $vacio, $active, $orderby="", $where="")
	{
		global $mysqli;
		global $errorLog;
		$resultado = "";

		try{
			$query_select = sprintf("SELECT %s as id, %s as valor FROM %s WHERE 1=1 ", $campo_pk, $campo_valor, $tabla ) ;

			if ($language!="")
				$query_select.= " AND language = '" . $language . "'";

			if ($active==1)
				$query_select.= " AND active_flag = 1 ";

			if ($where!="")
				$query_select.= " ". $where;

			if ($orderby!="")
				$query_select.= " ORDER BY ". $orderby;

			if ($vacio)
				$resultado = "<option value=\"0\">&nbsp;</option>";

			if ($result = $mysqli->query($query_select))
			{
				while($row = $result->fetch_array()){
					$selected = "";
					if ($campo_selec == $row["id"])
						$selected = "selected";
					$resultado.= "<option value=\"". $row["id"] . "\" ".$selected.">" . ($row["valor"]) . "</option>";
				}
				$result->close();
			}

			$errorLog->LogDebug("SELECT: $query_select");

		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
	    }

		return $resultado;

	}//end desplegable




	//Si es title=false, es para mostrar en un campo normal. Si no en un title, y se muestra entera la descripción si es valor>maximo.
	function truncate($valor, $maximo, $title=false){
		$resultado="";

		if ($title==false){
			if (strlen($valor)>0 && strlen($valor)>$maximo){
				$resultado=substr($valor,0,$maximo)."...";
			}else{
				$resultado=$valor;
			}


		}else if ($title==true){
			if (strlen($valor)>0 && strlen($valor)>$maximo){
				$resultado=$valor;
			}
		}

		return $resultado;
	}



}

class Tabs
{

	function buildTab($tab_actual_id, $tab_selected_id, $object_type_id, $id, $tab_name, $tab_action)
	{

		if ($tab_actual_id == $tab_selected_id){
			echo ("<li class=\"seleccionado\"><span class=\"esquina\">&nbsp;</span><p title=\"\">$tab_name</p></li>");
		}else if ($id=="new") {
			echo ("<li><p title=\"\">$tab_name</p></li>");
		}else if (is_numeric($id)){
			echo ("<li><a href=\"javascript:goToTab('$object_type_id','$id','$tab_action')\" title=\"\">$tab_name</a></li>");
		}else{
			echo ("<li><a href=\"javascript:goToTab('$object_type_id','$id','$tab_action')\" title=\"\">$tab_name</a></li>");
		}
	}//end buildTab

}

class Format
{

	function formatea_fecha_bd($fecha){

		//Formatear para insertar, pero lo que recibo puede ser en Ingles o Castellano.
		if ($fecha!=null && $fecha!=""){
			$dia=substr($fecha, 0, 2);
			$mes=substr($fecha, 3, 2);
			$anyo=substr($fecha, 6, 4);

			return "$anyo-$mes-$dia";
		}else{
			return "NULL";
		}


	}

	function formatea_fecha($fecha){
		$valor = "";
		if ($fecha!=null && $fecha!=""){
			$valor = date(CFG_FORMATO_FECHA,strtotime($fecha));
		}
		return $valor;
	}

	//Función que resuelve el problema de convertir en mayúsculas una cadena utf8.
	function strtoupper_utf8($string){
		$string=utf8_decode($string);
		$string=strtoupper($string);
		$string=utf8_encode($string);
		return $string;
	}

	function formatea_decimal_bd($num, $pos_decimales){

		//Formatear para insertar, pero lo que recibo puede ser en Ingles o Castellano.
		if ($num!=null && $num!=""){
			return number_format($num,$pos_decimales);
		}else{
			return "NULL";
		}


	}

}

class Mail
{

	/* function send_mail($to, $from, $subject, $contents, $cc="", $bcc=""){

		$headers="";
		$headers .= "MIME-Version: 1.0\n";
		$headers .= "Content-Type: text/plain;\n";
		$headers .= "	charset=utf-8;\n";
		$headers .= "	DelSp=\"Yes\";\n";
		$headers .= "	format=\"flowed\"\n";
		$headers .= "Content-Disposition: inline\n";
		$headers .= "Content-Transfer-Encoding: 7bit\n";
		$headers .= "From: ".$from."\n";
		if ($cc!="")
			$headers .= "Cc: ".$cc."\n";
		if ($bcc!="")
			$headers .= "Bcc: ".$bcc."\n";
		$headers .= "Reply-To: ".$from."\r\n";


		//echo $headers;
		$subject = "=?UTF-8?B?".base64_encode($subject)."?=";

		mail ($to, $subject, $contents ,$headers);

	}//end send_mail */


	function send_mail($to, $from, $subject, $contents, $cc="", $bcc=""){

		global $errorLog;

		//Instanciamos la clase
		$mail = new PHPMailer();

		//Datos de configuración
		$mail->IsSMTP();
		$mail->Host = CFG_EMAIL_SMTP;

		if (CFG_EMAIL_SMTP_USER!="" && CFG_EMAIL_SMTP_PASS!=""){
			$mail->SMTPAuth = true;
			//$mail->SMTPSecure = "ssl"; //tls
			$mail->Username = CFG_EMAIL_SMTP_USER;
			$mail->Password = CFG_EMAIL_SMTP_PASS;
		}

		//$errorLog->LogDebug("email smtp= ".CFG_EMAIL_SMTP);
		//$errorLog->LogDebug("email user= ".CFG_EMAIL_SMTP_USER);
		//$errorLog->LogDebug("email pass= ".CFG_EMAIL_SMTP_PASS);

		$mail->From = $from; // Autor del email
		$mail->FromName = $from; // Nombre del Autor del email

		if ($cc!=""){ //Añadimos los destinatarios en copia
			$mail->AddCC($cc, $cc);
		}

		if ( is_array($bcc) && count($bcc)>0){
			//$bcc es un array.
			for($i=0;$i<count($bcc);$i++){
				if ($bcc[$i]!=""){
					$mail->AddBCC($bcc[$i], $bcc[$i]);
				}
			}
		}else if ($bcc!=""){ //Añadimos los destinatarios en copia OCULTA
			$mail->AddBCC($bcc, $bcc);
		}

		$mail->AddReplyTo($from, $from); //Rellenamos el email de origen donde respondernos
		$mail->AddAddress($to, $to); //Rellenamos el email de destino

		$mail->Subject = utf8_decode($subject); // Rellenamos el asunto del email tratado con utf8
		$mail->Body = utf8_decode($contents); // Rellenamos el contenido del email tratado con utf8

		//Envíamos email
		if(!$mail->Send()){
			$errorLog->LogDebug("KO = ERROR");
			$errorLog->LogDebug($mail->ErrorInfo);
		}else{
			//$errorLog->LogDebug("OK = ENVIADO");
		}

	}//end send_mail

}




class PrivateArea
{

	//Devuelve el password guardado en la BD en md5
	function getPassword($user_id)
	{
		global $mysqli;

		$resultado="";

		$query_select = sprintf("SELECT password FROM ic_user WHERE user_id='%s'", $user_id);
		//echo $query_select;

		if ($result = $mysqli->query($query_select)){

				if ($result->num_rows > 0){
					$row = $result->fetch_array();
					$resultado = $row["password"];
				}
		}

		return $resultado;
	}//end getPassword


	function paginadorWeb($actual, $total, $por_pagina, $enlace, $maxpags=0) {
	  $pagina_primera=1;
	  $total_paginas = ceil($total/$por_pagina);
	  $anterior = $actual - 1;
	  $posterior = $actual + 1;
	  $minimo = $maxpags ? max(1, $actual-ceil($maxpags/2)): 1;
	  $maximo = $maxpags ? min($total_paginas, $actual+floor($maxpags/2)): $total_paginas;

	  $texto="";

	  $texto.= "<ol class=\"pager\">";

	  if ($actual>1)
		$texto.= "<li class=\"\"><a href=\"$enlace$anterior\">Previous</a>&nbsp;|&nbsp;</li>";
	  else
	  	$texto.= "<li class=\"\">Previous&nbsp;|&nbsp;</li>";

	  for ($i=$minimo; $i<$actual; $i++)
		$texto .= "<li class=\"\"><a href=\"$enlace$i\">$i</a>&nbsp;|&nbsp;</li>";

	  $texto .= "<li class=\"selected\"><b>$actual</b>&nbsp;|&nbsp;</li>";

	  for ($i=$actual+1; $i<=$maximo; $i++)
		$texto .= "<li class=\"\"><a href=\"$enlace$i\">$i</a>&nbsp;|&nbsp;</li>";

	  if ($actual<$total_paginas)
		$texto.= "<li class=\"\"><a href=\"$enlace$posterior\">Next</a></li>";
	  else
		$texto.= "<li class=\"\">Next</li>";

	  $texto.= "</ol> ";

	  return $texto;

	}


}//end class PrivateArea


?>