<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/AbstractCRMObjectChannels.php');

interface CRMPersonData extends CRMObjectChannels {

	 public function getUserID();
	 public function setUserID($s);

     public function getUserEmail();
	 public function setUserEmail($s);

     public function getUserLanguage();
     public function setUserLanguage($S);

	 public function getPersonLastName1();
     public function setPersonLastName1($s);

	public function getPersonLastName2();
	public function setPersonLastName2($s);

	public function getSiteID();
	public function setSiteID($s);

	public function getSiteName();
    public function setSiteName($s);

	public function getInternalID();
	public function setInternalID($s);

	public function getCardID();
	public function setCardID($s);

	public function getSex();
	public function setSex($s);

	public function getBirthDate();
	public function setBirthDate($s);

	public function getBirthPlace();
	public function setBirthPlace($s);

	public function getBirthRegion();
	public function setBirthRegion($s);

	public function getBirthCountryCode();
	public function setBirthCountryCode($s);

	public function getNationalityCode();
	public function setNationalityCode($s);

    public function getMotherTongue();
	public function setMotherTongue($s);

}

?>