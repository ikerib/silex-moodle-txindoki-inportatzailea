<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/CRMObject.php');

interface CRMObjectChannels extends CRMObject{

	public function getAddressID();
	public function setAddressID($nValue);

    public function getAddress();
    public function setAddress($s);

    public function getPhoneID();
    public function setPhoneID($nValue);

	public function getPhone();
    public function setPhone($s);

	public function getMobileID();
	public function setMobileID($nValue);

	public function getMobile();
	public function setMobile($s);

	public function getFaxID();
    public function setFaxID($nValue);

	public function getFax();
    public function setFax($s);

	public function getEmailID();
    public function setEmailID($nValue);

	public function getEmail();
	public function setEmail($s);

	//public function isSaveChannels();
    //public function setSaveChannels($nValue);

}

?>