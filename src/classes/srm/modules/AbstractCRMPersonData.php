<?php
// -----------------------------------------
// AbstractCRMPersonData.php
// -----------------------------------------
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/AbstractCRMObjectChannels.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/CRMPersonData.php');

abstract class AbstractCRMPersonData extends AbstractCRMObjectChannels
								 implements CRMPersonData
{

	public $msUserID = "";
	public $msUserEmail = "";
	public $msUserLanguage = "";
	public $msPersonLastName1 = "";
	public $msPersonLastName2 = "";
	public $mnSiteID = "";
	public $msSiteName = "";
	public $msInternalID = "";
	public $mnNationalIDType = "";
	public $msIDCard = "";
	public $mnSex = "";
	public $moBirthDate = "";
	public $msBirthPlace = "";
	public $msBirthRegion = "";
	public $msBirthCountryCode = "";
	public $msNationalityCode = "";
	public $msMotherTongue = "";


	public function getUserID(){
		return $this->msUserID;
	}
	public function setUserID($value){
		$this->msUserID = $value;
	}

	public function getUserEmail(){
		return $this->msUserEmail;
	}
	public function setUserEmail($value){
		$this->msUserEmail = $value;
	}

	public function getUserLanguage(){
		return $this->msUserLanguage;
	}
	public function setUserLanguage($value){
		$this->msUserLanguage = $value;
	}

	public function getPersonLastName1(){
		return $this->msPersonLastName1;
	}
	public function setPersonLastName1($value){
		$this->msPersonLastName1 = $value;
	}

	public function getPersonLastName2(){
		return $this->msPersonLastName2;
	}
	public function setPersonLastName2($value){
		$this->msPersonLastName2 = $value;
	}

	public function getSiteID(){
		return $this->mnSiteID;
	}
	public function setSiteID($value){
		$this->mnSiteID = $value;
	}

	public function getSiteName(){
		return $this->msSiteName;
	}
	public function setSiteName($value){
		$this->msSiteName = $value;
	}

	public function getInternalID(){
		return $this->msInternalID;
	}
	public function setInternalID($value){
		$this->msInternalID = $value;
	}

	public function getNationalIDType(){
		return $this->mnNationalIDType;
	}
	public function setNationalIDType($value){
		$this->mnNationalIDType = $value;
	}

	public function getCardID(){
		return $this->msIDCard;
	}
	public function setCardID($value){
		$this->msIDCard = $value;
	}

	public function getSex(){
		return $this->mnSex;
	}
	public function setSex($value){
		$this->mnSex = $value;
	}

	public function getBirthDate(){
		return $this->moBirthDate;
	}
	public function setBirthDate($value){
		$this->moBirthDate = $value;
	}

	public function getBirthPlace(){
		return $this->msBirthPlace;
	}
	public function setBirthPlace($value){
		$this->msBirthPlace = $value;
	}

	public function getBirthRegion(){
		return $this->msBirthRegion;
	}
	public function setBirthRegion($value){
		$this->msBirthRegion = $value;
	}

	public function getBirthCountryCode(){
		return $this->msBirthCountryCode;
	}
	public function setBirthCountryCode($value){
		$this->msBirthCountryCode = $value;
	}

	public function getNationalityCode(){
		return $this->msNationalityCode;
	}
	public function setNationalityCode($value){
		$this->msNationalityCode = $value;
	}

	public function getMotherTongue(){
		return $this->msMotherTongue;
	}
	public function setMotherTongue($value){
		$this->msMotherTongue = $value;
	}
}


?>