<?php

// -----------------------------------------
// CRMObjectSummary.php
// -----------------------------------------


interface CRMObjectSummary {
	
   public function load();
   
   public function loadFilter($sType, $sID);

}
?>