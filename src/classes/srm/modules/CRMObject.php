<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/CRMAdmin.php');

interface CRMObject extends CRMAdmin {

	 public function getID();
	 public function setID($s);

     public function getReqID();
	 public function setReqID($s);

     public function getName();
     public function setName($S);

	 public function getCompanyID();
     public function setCompanyID($s);

	public function getRemarks();
	public function setRemarks($s);

	public function getModifiedBy();
	public function setModifiedBy($s);

	public function getTimestamp();
    public function setTimestamp($s);

	public function getActiveFlag();
    public function setActiveFlag($s);
	public function isActiveFlag();

	// public function setViewDescription();
    // public function setViewDescription($s);

	public function getCrmObjectName();
	public function setCrmObjectName($s);

	public function getSessionCompanyID();
	public function setSessionCompanyID($s);

	public function getSessionLanguage();
	public function setSessionLanguage($s);

	public function getSessionEmployeeID();
	public function setSessionEmployeeID($s);

	public function getSessionEmployeeName();
	public function setSessionEmployeeName($s);

	public function getOpenerID();
	public function setOpenerID($s);
	//public function existsOpener();

	public function getOpenerType();
	public function setOpenerType($s);

	public function getObjectAuxID();
	public function setObjectAuxID($s);



}
?>