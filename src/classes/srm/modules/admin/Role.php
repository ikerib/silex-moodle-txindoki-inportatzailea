<?php
// -----------------------------------------
// Role_class.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/database/DB_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/admin/RoleData.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/Functions.php');

class Role extends RoleData
{

	public function loadData ($id){

		if (null == $id || "new"==$id)
		{
            $this->setID("");
            $this->setName("");
            $this->setCompanyID($_SESSION["company_id"]);
			$this->setRecipientNotification("");
			$this->setEmailNotification("");
            $this->setRemarks("");
            $this->setActiveFlag("");
			$this->setModifiedBy("");
			$this->setTimestamp("");

			return;
		}

		global $mysqli;
		global $errorLog;

		try {
			$query_select= sprintf("SELECT ica_role.role_id,
										ica_role.company_id,
										ica_role.role_name,
										ica_role.remarks,
										ica_role.active_flag,
										ica_role.timestamp,
										ica_role.modified_by,
										ica_role.recipient_notification,
										ica_role.email_notification
									FROM ica_role
									WHERE ica_role.role_id = %d
									AND ica_role.company_id = %d", $id, $_SESSION["company_id"]);

			//echo $query_select;

			if ($result = $mysqli->query($query_select)){

				if ($result->num_rows > 0){
					$row = $result->fetch_array();

					$this->setID($row["role_id"]);
					$this->setCompanyID($row["company_id"]);
					$this->setName($row["role_name"]);
					$this->setRecipientNotification($row["recipient_notification"]);
					$this->setEmailNotification($row["email_notification"]);
					$this->setRemarks($row["remarks"]);
					$this->setActiveFlag($row["active_flag"]);
					$this->setTimestamp($row["timestamp"]);
					$this->setModifiedBy($row["modified_by"]);

					$result->close();

					$this->setAssignedResources($this->loadResources());
					$this->setNoAssignedResources($this->loadPossibleResources());
				}else{
					$errorLog->LogError("Location: /srm/index.php?error=3. ERROR: ".$query_select);
					header("Location: /index.php?error=3");
					exit;
				}
			}

		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}

	}//end loadData



	public function createNew()
	{

		global $mysqli;
		global $errorLog;

		try {
			$query_insert=sprintf("INSERT INTO ica_role
								    (company_id,
										role_name,
										recipient_notification,
										email_notification,
										remarks,
										active_flag,
										modified_by,
										timestamp)
								  VALUES (
								  '%s',
								  '%s',
								  '%s',
								  '%s',
								  '%s',
								  '%s',
								  '%s',
								  CURRENT_TIMESTAMP
								  )",

									$this->getCompanyID(),
									$this->getName(),
									$this->getRecipientNotification(),
									$this->getEmailNotification(),
									$this->getRemarks(),
									$this->getActiveFlag(),
									$_SESSION["user_id"]
								);

			//echo $query_insert.
			$result = $mysqli->query($query_insert);

			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}

			//Calculo el último ID insertado.
			$query_max = "SELECT LAST_INSERT_ID() AS max_id";
			if( $result = $mysqli->query($query_max) ){
				$row = $result->fetch_array();

				$this->setID($row["max_id"]);

				$result->close();
			}

		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}

	}//END create_new



	public function update($id)
	{
		global $mysqli;
		global $errorLog;

		try {

			$query_update=sprintf("UPDATE ica_role
								SET company_id = %d,
									role_name = '%s',
									recipient_notification = '%s',
									email_notification = '%s',
									remarks = '%s',
									modified_by = '%s',
									timestamp = CURRENT_TIMESTAMP
								WHERE
									role_id = %d",

									$this->getCompanyID(),
									$this->getName(),
									$this->getRecipientNotification(),
									$this->getEmailNotification(),
									$this->getRemarks(),
									$_SESSION["user_id"],
									$id
    							);


			$errorLog->LogDebug("UPDATE: $query_update");
			$result = $mysqli->query($query_update);

			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}


		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}

	}//END UPDATE



	public function delete($id)
	{

		global $mysqli;
		global $errorLog;

		try {

			$query_delete=sprintf("UPDATE ica_role
								SET active_flag=0,
									modified_by='%s',
									timestamp=CURRENT_TIMESTAMP
							WHERE
									role_id = %d",
									$_SESSION["user_id"],
									$_POST['id']
    							);

			//echo "*".$query_delete."*";
			$result = $mysqli->query($query_delete);

			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}

		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}


	}//END UPDATE






   public function init()
   {
		$error=false;
		$error_txt="";
		$valor = "";

		//Param name
		$valor = $_POST['name'];
		if (trim($valor)!=""){
			$this->setName($valor);
		}else{
			$error=true;
			$error_txt.="\\n- ".MSG_ROLE_ERROR_TEXT_01;
			$this->setName("");
		}

		$this->setCompanyID($_POST["company_id"]);

        $this->setRecipientNotification($_POST["recipient_notification"]);

        $this->setEmailNotification($_POST["email_notification"]);

        $this->setRemarks($_POST["remarks"]);

		$this->setModifiedBy($_SESSION['user_id']);

		return $error_txt;

   }//END init


	function getAllowedObjects()
	{
		global $mysqli;
		global $errorLog;
		$allowedObjects;

		$query_select = "SELECT DISTINCT " .
                    "       ica_object_resource.object_type_id" .
                    "  FROM ica_role_employee, ica_resource_role, ica_object_resource, ica_role " .
                    " WHERE ica_role_employee.employee_id = %s" .
                    "   AND ica_role_employee.role_id = ica_resource_role.role_id " .
                    "   AND ica_resource_role.resource_id = ica_object_resource.resource_id " .
                    "   AND ica_role_employee.role_id = ica_role.role_id " .
                    "   AND ica_role_employee.active_flag = 1 " .
                    "   AND ica_resource_role.active_flag = 1 " .
                    "   AND ica_role.active_flag = 1 ";

	  	$query_select = sprintf($query_select, $_SESSION["employee_id"]);

		$errorLog->LogDebug("SELECT: $query_select");

		if ($result = $mysqli->query($query_select))
		{
			$indice = 0;
			while($row = $result->fetch_array()){
				$allowedObjects[$indice] = $row["object_type_id"];
				$indice = $indice + 1;
			}
			$result->close();
		}


		return $allowedObjects;
    }



	private function loadResources()
	{

		global $mysqli;
		global $errorLog;

        $query_select = "SELECT ica_resource.resource_id, ".
                    "       ica_resource.resource_name ".
                    "  FROM ica_resource, ica_resource_role " .
                    " WHERE ica_resource.resource_id = ica_resource_role.resource_id " .
                    "   AND ica_resource_role.role_id = ".$this->getID().
                    "   AND ica_resource_role.active_flag = 1 ".
                    "   AND ica_resource.language = '".$_SESSION["language"]."' ";


        $result = $mysqli->query($query_select);
		return $result;

	}

	private function loadPossibleResources()
	{

		global $mysqli;

        $query_select = "SELECT DISTINCT (ica_resource.resource_id), ".
                    "       ica_resource.resource_name ".
                    "  FROM ica_resource LEFT JOIN ica_resource_role ".
                    "       ON (ica_resource.resource_id = ica_resource_role.resource_id) ".
                    " WHERE ica_resource.resource_id NOT IN ( ".
                    "          SELECT ica_resource.resource_id  ".
                    "            FROM ica_resource, ica_resource_role ".
                    "           WHERE ica_resource.resource_id = ica_resource_role.resource_id ".
                    "             AND ica_resource_role.role_id = ".$this->getID().
                    "             AND ica_resource_role.active_flag = 1 ) ".
                    "   AND ica_resource.language = '".$_SESSION["language"]."' ";


        $result = $mysqli->query($query_select);
		return $result;

	}


	public function saveResources($id)
	{
		global $mysqli;
		global $errorLog;

		try {
			// Se borran todos primero para luego guardar los selccionados
			$query_delete=sprintf("DELETE from ica_resource_role WHERE role_id=%d", $id);

			$result = $mysqli->query($query_delete);
			$errorLog->LogDebug("DELETE: $query_delete");

			$selectedResources = $_POST["assig_resources"];
			if ($selectedResources!=""){
				$tok = strtok ($selectedResources,", ");
				while ($tok !== false) {
					$query_insert = "INSERT INTO ica_resource_role (resource_id, role_id, active_flag, timestamp, modified_by ) ".
								" VALUES ( ".$tok.", ".$id.", 1, CURRENT_TIMESTAMP, '".$_SESSION["user_id"]."') ";
					$result = $mysqli->query($query_insert);
					$errorLog->LogDebug("INSERT: $query_insert");
					$tok = strtok(", ");
				}
			}

		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}

	}


	/*
		Los tutores tienen que poder acceder al SRM con lo cual necesitan unos roles.
		En la BD creamos un Rol para todos los tutores.
	*/
	function getAllowedObjectsTutor()
	{
		global $mysqli;
		global $errorLog;
		$allowedObjects;

		$query_select = "SELECT DISTINCT
							ica_object_resource.object_type_id
						FROM ica_resource_role, ica_object_resource
						WHERE ica_resource_role.resource_id = ica_object_resource.resource_id
						   AND ica_resource_role.active_flag = 1
						   AND ica_resource_role.role_id = 3 ";

	  	$query_select = sprintf($query_select, $_SESSION["employee_id"]);

		$errorLog->LogDebug("SELECT: $query_select");

		if ($result = $mysqli->query($query_select))
		{
			$indice = 0;
			while($row = $result->fetch_array()){
				$allowedObjects[$indice] = $row["object_type_id"];
				$indice = $indice + 1;
			}
			$result->close();
		}


		return $allowedObjects;
    }


}

?>