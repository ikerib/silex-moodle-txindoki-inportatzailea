/../src<?php
// -----------------------------------------
// InfoSummary.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/database/DB_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/admin/InfoSummary.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/AbstractCRMObjectSummary.php');

class InfoSummary extends AbstractCRMObjectSummary
{


   public function InfoSummary()
   {
   }

   public function getOrderByColumn()
   {
      $sOrderBy = $this->getOrderBy();

      if (null == $sOrderBy)
         return "ic_info.info_title";

      try {
         switch( $sOrderBy) {
            case 2:  $sOrderBy = "ic_info.info_desc"; break;
            case 3:  $sOrderBy = "ic_info_type.info_type_name"; break;
			case 4:  $sOrderBy = "ic_info.info_date"; break;
			case 5:  $sOrderBy = "ic_company.company_name"; break;
            default: $sOrderBy = "ic_info.info_title";
         }
      } catch (Exception $ex) {
         return $sOrderBy;
      }
      return $sOrderBy;
   }//END



    public function  load()
   {

     $nCompanyID = $_SESSION["company_id"];
	 $sLanguage = $_SESSION["language"];

     global $mysqli;
	 global $errorLog;

      try
      {

      $sFrom = " FROM ic_info LEFT OUTER JOIN ic_info_type ";
			$sFrom.= " ON ic_info_type.info_type_id=ic_info.info_type_id ";
			$sFrom.= " AND ic_info_type.language = '$sLanguage'";

			$sFrom.= "LEFT JOIN ic_company ON ic_info.company_id=ic_company.company_id ";

      $sWhere = " WHERE ic_info.active_flag = 1 ";
	  $sWhere.= " AND ic_info.company_id = $nCompanyID ";

      $sOperator = " = ";
      $sCondition = "";

	  // Param name
	  $sWhere.=$this->getWhereClause("name", "info_title", "ic_info", $this->STRING_TYPE, "", "");

	  // Param info_desc
	  $sWhere.=$this->getWhereClause("info_desc", "info_desc", "ic_info", $this->STRING_TYPE, "", "");

	  // Param contents
	  $sWhere.=$this->getWhereClause("contents", "contents", "ic_info", $this->STRING_TYPE, "", "");

	  // Param info_date
	  $sWhere.=$this->getWhereClause("info_date", "info_date", "ic_info", $this->DATE_TYPE, "", "");

	  // Param info_type_id
	  //$sWhere.=$this->getWhereClause("info_type_id", "info_type_id", "ic_info", $this->LOGICAL_TYPE, "", "");

	  $query_select = "SELECT COUNT(*) " . $sFrom . $sWhere;

	  $errorLog->LogDebug("SELECT: $query_select");

	  if ($result = $mysqli->query($query_select)){
			$row = $result->fetch_array();
			$this->init($row[0]);
			$result->close();
	  }else{
			$this->init(0);
	  }

       $query_select="SELECT ic_info.info_id,
						COALESCE(ic_info.info_title,'') as info_title,
						COALESCE(ic_info.info_desc,'') as info_desc,
						ic_info.info_date,
						ic_info_type.info_type_name,
						ic_company.company_name	" ;

       $query_select.= $sFrom . $sWhere;
       $query_select.= " ORDER BY " . $this->getOrderByColumn() . " " . $this->getOrderHowString() . " ";
       $query_select.= " LIMIT " . $this->getMaxRowsNumber() . " OFFSET " . ($this->getCurrentPage()-1)*$this->getMaxRowsNumber();

       $errorLog->LogDebug("SELECT: $query_select");

       $result = $mysqli->query($query_select);

	   return $result;

      }
      catch (Exception $ex)
      {

      }



   }//end load




}//END CLASS
?>