<?php
// -----------------------------------------
// RoleSummary.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/database/DB_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/AbstractCRMObjectSummary.php');

class RoleSummary extends AbstractCRMObjectSummary
{


	public function RoleSummary()
	{
	}

	public function getOrderByColumn()
	{
		$sOrderBy = $this->getOrderBy();

		if (null == $sOrderBy)
			return "ica_role.role_name";

		try {
			switch( $sOrderBy) {
				default: $sOrderBy = "ica_role.role_name";
			}
		}catch (Exception $ex) {
			return $sOrderBy;
		}
		return $sOrderBy;
	}//END



	public function  load()
	{

		$nCompanyID = $_SESSION["company_id"];
		$sLanguage = $_SESSION["language"];

		global $mysqli;

		try
		{

			$sFrom = " FROM ica_role ";

			$sWhere = " WHERE ica_role.active_flag = 1 ";
			$sWhere .= " AND ica_role.company_id = ".$nCompanyID;

			$sOperator = " = ";
			$sCondition = "";

			// Param name
			$sWhere.=$this->getWhereClause("name", "role_name", "ica_role", $this->STRING_TYPE, "", "");


			$query_select = "SELECT COUNT(*) " . $sFrom . $sWhere;

			//echo $query_select;

			if ($result = $mysqli->query($query_select)){
				$row = $result->fetch_array();
				$this->init($row[0]);
				$result->close();
			}else{
				$this->init(0);
			}

			$query_select="SELECT ica_role.role_id, ".
						"       ica_role.role_name ";
			$query_select.= $sFrom . $sWhere;
			$query_select.= " ORDER BY " . $this->getOrderByColumn() . " " . $this->getOrderHowString() . " ";
			$query_select.= " LIMIT " . $this->getMaxRowsNumber() . " OFFSET " . ($this->getCurrentPage()-1)*$this->getMaxRowsNumber();

			//echo $query_select;

			$result = $mysqli->query($query_select);

			return $result;

		}
		catch (Exception $ex)
		{

		}



   }//end load






   /**
    * Crea una lista con los objetos que responden a las
    * restricciones establecidas en la petición.
    *
    * @param  sType    tipo de filtrado
    * @param  sID      clave de filtrado
    * @param  oReq     petición
    * @throws          isyc.website.CRMException
    *                  si se produce algún error al obtener los datos.
    * @throws          NullPointerException
    *                  si alguno de los parámetros es un valor <CODE>null</CODE>.
    * @since           CRM 1.0.0
    */
   public function loadFilter($sType, $sID)
   {

   }



}

?>