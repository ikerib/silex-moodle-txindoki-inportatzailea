<?php
// -----------------------------------------
// RoleData_class.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/AbstractCRMObject.php');

class RoleData extends AbstractCRMObject
{

	public $mnRecipientNotification = "";
	public $msEmailNotification = "";
	public $oAssignedResources;
	public $oNoAssignedResources;

	public function getRecipientNotification(){
		return $this->mnRecipientNotification;
	}

	public function setRecipientNotification($value){
		$this->mnRecipientNotification = $value;
	}

	public function getEmailNotification(){
		return $this->msEmailNotification;
	}

	public function setEmailNotification($value){
		$this->msEmailNotification = $value;
	}

	public function getAssignedResources(){
		return $this->oAssignedResources;
	}

	public function setAssignedResources($value){
		$this->oAssignedResources = $value;
	}

	public function getNoAssignedResources(){
		return $this->oNoAssignedResources;
	}

	public function setNoAssignedResources($value){
		$this->oNoAssignedResources = $value;
	}
}

?>