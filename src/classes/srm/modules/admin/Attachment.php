<?php
// -----------------------------------------
// Attachment.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/database/DB_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/admin/AttachmentData.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/Functions.php');

require_once($_SERVER['DOCUMENT_ROOT']."/../src/lang/en/config.php");
require_once($_SERVER['DOCUMENT_ROOT']."/../src/config_app.php");

class Attachment extends AttachmentData
{






   public function loadData ($id){

	  $nCompanyID = $_SESSION["company_id"];
	  $sLanguage = $_SESSION["language"];

	  if (null == $id || "new"==$id)
      {
			$this->setID("new");
			$this->setName("");
			$this->setCompanyID($nCompanyID);
			$this->setAttachmentFile("");
			$this->setAttachmentName("");

			$this->setAttachmentDesc("");

			$this->setObjectTypeID(0);
			$this->setObjectTypeName("");
			$this->setObjectID("");
			$this->setObjectName("");

			$openerID="";
			if (isset($_GET["openerID"]) && $_GET["openerID"]!="")
				$openerID=$_GET["openerID"];
			else if (isset($_POST["openerID"]) && $_POST["openerID"]!="")
				$openerID=$_POST["openerID"];

			//echo "openerID=".$openerID;

			$openerType="";
			if (isset($_GET["openerType"]) && $_GET["openerType"]!="")
				$openerType=$_GET["openerType"];
			else if (isset($_POST["openerType"]) && $_POST["openerType"]!="")
				$openerType=$_POST["openerType"];

			//echo "openerType=".$openerType;

			if (isset($openerID) && $openerID!="")
			{
				if (isset($openerType) && $openerType!="")
				{
					if ($openerType=="19"){ //From Company
						$this->setObjectTypeID($openerType);

						$this->setObjectID($openerID);

						require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/admin/Company.php');
						$objCompany = new Company();
						$objCompany->loadData($openerID);

						$this->setObjectName($objCompany->getName());
						$this->setCompanyID($objCompany->getID());
					}else if ($openerType=="7"){ //From Employee
						$this->setObjectTypeID($openerType);

						$this->setObjectID($openerID);

						require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/admin/Employee.php');
						$objEmployee = new Employee();
						$objEmployee->loadData($openerID);

						$this->setObjectName($objEmployee->getName()." ".$objEmployee->getPersonLastName1());
						$this->setCompanyID($objEmployee->getCompanyID());
					}else if ($openerType=="22"){ //From Information
						$this->setObjectTypeID($openerType);

						$this->setObjectID($openerID);

						require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/admin/Info.php');
						$objInfo = new Info();
						$objInfo->loadData($openerID);

						$this->setObjectName($objInfo->getName());
						$this->setCompanyID($objInfo->getCompanyID());
					}

				}
			}

			$this->setRemarks("");
			$this->setActiveFlag(1);
			$this->setModifiedBy($_SESSION["user_id"]);
			$this->setTimestamp("");
			return;



      }

      global $mysqli;
	  global $errorLog;

	  $format = new Format();

	  try {
		$query_select= sprintf(	"SELECT ic_attachment.attachment_id,
									ic_attachment.attachment_file,
									ic_attachment.attachment_name,
									ic_attachment.attachment_desc,
									ic_attachment.object_type_id,
									ica_object_type.object_type_name,
									ic_attachment.object_id,
									ic_attachment.object_name,
									ic_attachment.remarks,
									ic_attachment.active_flag,
									ic_attachment.modified_by,
									ic_attachment.timestamp,
									ic_attachment.company_id
								FROM  ic_attachment LEFT JOIN ica_object_type ON ic_attachment.object_type_id = ica_object_type.object_type_id
								WHERE ic_attachment.attachment_id = %d
									AND ica_object_type.language = '%s'",
										$id,
										$_SESSION["language"]
								);

		$query_select.= " AND ic_attachment.company_id = $nCompanyID ";

		 $errorLog->LogDebug("SELECT: $query_select");
		 if ($result = $mysqli->query($query_select)){

				if ($result->num_rows > 0){
					$row = $result->fetch_array();

					$this->setID($row["attachment_id"]);
					$this->setName($row["attachment_name"]);
					$this->setAttachmentFile($row["attachment_file"]);
					$this->setAttachmentName($row["attachment_name"]);
					$this->setAttachmentDesc($row["attachment_desc"]);

					$this->setObjectTypeID($row["object_type_id"]);
					$this->setObjectID($row["object_id"]);

					//echo ("----->".$this->getObjectTypeID());
					if ($this->getObjectTypeID()=="19"){ //From Company
						require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/admin/Company.php');
						$objCompany = new Company();
						$objCompany->loadData($this->getObjectID());
						$this->setObjectName($objCompany->getName());
					}else if ($this->getObjectTypeID()=="7"){ //From Employee
						require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/admin/Employee.php');
						$objEmployee = new Employee();
						$objEmployee->loadData($this->getObjectID());
						$this->setObjectName($objEmployee->getName()." ".$objEmployee->getPersonLastName1());
					}else if ($this->getObjectTypeID()=="22"){ //From Information
						require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/admin/Info.php');
						$objInfo = new Info();
						$objInfo->loadData($this->getObjectID());
						$this->setObjectName($objInfo->getName());
					}

					//$this->setObjectName($row["object_name"]);

					$this->setCompanyID($row["company_id"]);

					$this->setRemarks($row["remarks"]);
					$this->setActiveFlag($row["active_flag"]);
					$this->setModifiedBy($row["modified_by"]);
					$this->setTimestamp($row["timestamp"]);

					Global $path_contents_secure_absolute;

					if ($this->getAttachmentFile() != null && $this->getAttachmentFile()!="")
					{
					   // Comprobación de que el AttachmentFile existe realmente
					  $sFilesPath = "";
					   try {
						  $sFilesPath = $path_contents_secure_absolute . "company".$this->getCompanyID()."/files/attachment/";
					   } catch (Exception $ex) {
						  $this->setAttachmentFileExists(false);
					   }

					   try{

						if (file_exists($sFilesPath.$this->getID() . "_" . $this->getAttachmentFile()))
							$this->setAttachmentFileExists(true);
						else
							$this->setAttachmentFileExists(false);

					   } catch (Exception $ex) {
						  $this->setAttachmentFileExists(false);
					   }
					}

					$result->close();
				}else{
					$errorLog->LogError("Location: /srm/index.php?error=3. ERROR: ".$query_select);
					header("Location: /index.php?error=3");
					exit;
				}

			}

	  } catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
	  }

	}//end loadData


   public function createNew()
   {

	  global $mysqli;
	  $format = new Format();
	  $error="";

      try {
			$query_insert=sprintf("INSERT INTO ic_attachment
								    (attachment_name,
									attachment_desc,

									object_type_id,
									object_id,
									object_name,

									active_flag,
									remarks,
									modified_by,
									timestamp,
									company_id)
								  VALUES (
								  '%s',
								  '%s',


								  '%s',
								  '%s',
								  '%s',

								   1 ,
								  '%s',
								  '%s',
								  CURRENT_TIMESTAMP,
								  %d
								  )",

									$this->getName(),
									$this->getAttachmentDesc(),

									$this->getObjectTypeID(),
									$this->getObjectID(),
									$this->getObjectName(),

									$this->getRemarks(),
									$this->getModifiedBy(),
									$this->getCompanyID()
							);

			//echo $query_insert.
			$result = $mysqli->query($query_insert);

			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}

			//Calculo el último ID insertado.
			$query_max = "SELECT LAST_INSERT_ID() AS max_id";
			if( $result = $mysqli->query($query_max) ){
				$row = $result->fetch_array();

				$this->setID($row["max_id"]);

				$result->close();
			}


			///Sube el archivo.

			//echo "this->getAttachmentUploadFile() = ".$this->getAttachmentUploadFile();

			if ($this->getAttachmentUploadFile()=="yes"){
				$upload = $this->uploadAttachment($this->getID(), $this->getCompanyID());
				$error=$upload;
			}

		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}


		return $error;



   }//END create_new






   public function update($id)
   {

	  global $mysqli;
	  $format = new Format();
	  $error="";

      try {

			$query_update=sprintf("UPDATE ic_attachment
									SET
								    attachment_name='%s',
									attachment_desc='%s',

									object_type_id='%s',
									object_id='%s',
									object_name='%s',

									remarks='%s',
									modified_by='%s',
									timestamp=CURRENT_TIMESTAMP,
									company_id=%d
								  WHERE
									attachment_id = %d",

									$this->getName(),
									$this->getAttachmentDesc(),

									$this->getObjectTypeID(),
									$this->getObjectID(),
									$this->getObjectName(),

									$this->getRemarks(),
									$_SESSION["user_id"],
									$this->getCompanyID(),
									$id
							);

			//echo "*".$query_update."*";
			$result = $mysqli->query($query_update);

			if ($mysqli->error){
				echo '<br/>Invalid query_update(): '.  $mysqli->error . "\n";
			}


			$ruta_upload = str_replace("$1",$this->getCompanyID(),CFG_ATTACHMENT_URL);

			//Borramos la anterior imágen si estamos subiendo una nueva.
			if ($this->getAttachmentUploadFile()=="yes"){
				if (file_exists($ruta_upload.$this->getID() . "_" . $this->getAttachmentUpdateFile())){
					unlink($ruta_upload.$this->getID() . "_" . $this->getAttachmentUpdateFile());
				}
				if (file_exists($ruta_upload.$this->getID() . "_th_" . $this->getAttachmentUpdateFile())){
					unlink($ruta_upload.$this->getID() . "_th_" . $this->getAttachmentUpdateFile());
				}
			}

			///Sube la imágen.
			if ($this->getAttachmentUploadFile()=="yes"){
				$upload = $this->uploadAttachment($this->getID(), $this->getCompanyID());
				$error=$upload;
			}

		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}

		return $error;

   }//END UPDATE



    public function delete($id)
   {
	 global $mysqli;

      try {

			$query_select=sprintf("SELECT
									attachment_id,
									attachment_file,
									company_id
			  				       FROM ic_attachment
								   WHERE attachment_id = %d",
									$_POST['id']
    							);

			//echo "*".$query_select."*";

			if ($result = $mysqli->query($query_select)){

				$row = $result->fetch_array();

				$this->setID($row["attachment_id"]);
				$this->setAttachmentFile($row["attachment_file"]);
				$this->setCompanyID($row["company_id"]);
				$result->close();

			}

			$query_delete=sprintf("UPDATE ic_attachment
								SET active_flag=0,
									modified_by='%s',
									timestamp=CURRENT_TIMESTAMP
							WHERE
									attachment_id = %d",

									$_SESSION["user_id"],
									$_POST['id']
    							);

			//echo "*".$query_delete."*";
			$result = $mysqli->query($query_delete);

			if ($mysqli->error){
				echo '<br/>Invalid query_delete(): '.  $mysqli->error . "\n";
			}


			//Borramos las imágenes.

			$ruta_upload = str_replace("$1",$this->getCompanyID(),CFG_ATTACHMENT_URL);

			if (file_exists($ruta_upload.$this->getID() . "_" . $this->getAttachmentFile())){
				unlink($ruta_upload.$this->getID() . "_" . $this->getAttachmentFile());
			}
			if (file_exists($ruta_upload.$this->getID() . "_th_" . $this->getAttachmentFile())){
				unlink($ruta_upload.$this->getID() . "_th_" . $this->getAttachmentFile());
			}

		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}


   }//END DELETE


   //Borra el objeto indicado, según el tipo de objecto y su id.
   public function deleteRelated($object_type_id, $id)
   {
	 global $mysqli;

      try {

			$query_delete=sprintf("UPDATE ic_attachment
								SET active_flag=0,
									modified_by='%s',
									timestamp=CURRENT_TIMESTAMP
							WHERE
									object_type_id = %d
									AND object_id = %d",

									$_SESSION["user_id"],
									$object_type_id,
									$id
    							);

			$result = $mysqli->query($query_delete);

			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}

		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}


   }//END DELETE


   public function init()
   {
		$error=false;
		$error_txt="";
		$valor = "";


		//Param id
		$valor = $_POST['id'];
		if (trim($valor)!=""){
			$this->setID($valor);
		}else{
			$this->setID("new");
		}

		//Param name
		$valor = $_POST['name'];
		if (trim($valor)!=""){
			$this->setName($valor);
		}else{
			$this->setName($_FILES['archive']['name']);
		}

		$this->setAttachmentDesc($_POST['attachment_desc']);

		//Param object_type_id
		$valor = $_POST['foreign_object_type_id'];
		if (trim($valor)!="" && trim($valor)!="0" && trim($valor)!="null"){
			$this->setObjectTypeID($valor);
		}else{
			$error=true;
			$error_txt.="\\n".MSG_ERROR_OBJECT_TYPE;
			$this->setObjectTypeID("");
		}

		//Param foreign_object_id
		$valor = $_POST['foreign_object_id'];
		if (trim($valor)!="" && trim($valor)!="0" && trim($valor)!="null"){
			$this->setObjectID($valor);
		}else{
			$error=true;
			$error_txt.="\\n".MSG_ERROR_OBJECT_RELATED;
			$this->setObjectID("");
		}

		$valor = $_POST['bupload_attachment'];
		$this->setAttachmentUploadFile($valor);

		$valor = $_POST['attachment_file_old'];
		$this->setAttachmentUpdateFile($valor);

		//Param foreign_object_name
		$valor = $_POST['foreign_object_name'];
		$this->setObjectName(trim($valor));

		$this->setCompanyID($_POST['company_id']);
		$this->setRemarks($_POST['remarks']);
		$this->setModifiedBy($_SESSION['user_id']);

		return $error_txt;

   }//END init




//Función que sube o modifica un archivo existente en la ruta indicada.

public function uploadAttachment($attachment_id, $company_id){

		$attachment_error=false;
		$attachment_error_num=0;
		$archivo_final="";

		if (is_uploaded_file($_FILES['archive']['tmp_name'])) {

			$archivo_name= $_FILES['archive']['name'];
			$archivo_size= $_FILES['archive']['size'];
			$archivo_type=  $_FILES['archive']['type'];
			$archivo= $_FILES['archive']['tmp_name'];

			//echo "<br>archivo_name=".$archivo_name;
			//echo "<br>archivo_size=".$archivo_size;
			//echo "<br>archivo_type=".$archivo_type;
			//echo "<br>archivo=".$archivo;

			$lim_tamano= $_POST['lim_tamano'];

			//echo "<br>archivo=".$lim_tamano;

			$archivo_final=$attachment_id."_".$archivo_name;

			$tamano = filesize($archivo);

				//Valida el tamaño máximo.
				if ($tamano>CFG_ATTACHMENT_SIZE_BYTES){

					$attachment_error=true;
					$attachment_error_num=2;
				}

				if (!$attachment_error){

					//echo "<br>->archivo_name=$archivo_name";
					//echo "<br>->archivo_size=$archivo_size";
					//echo "<br>->lim_tamano=$lim_tamano";

					if ($archivo_name != "none" AND $archivo_size != 0 AND $archivo_size<=$lim_tamano){
						if (copy ($archivo, (str_replace("$1",$company_id,CFG_ATTACHMENT_URL)).$archivo_final)) {
						  //echo "<center>Se ha subido el logotipo: <strong>$archivo_final</strong></center>";
						  //echo "<center>Su tamaño es: <strong>$archivo_size bytes</strong></center>";
						  $attachment_error_num=100;
						}
					}else{
						$attachment_error=true;
						$attachment_error_num=3;
					}
				}

		}else{
			$attachment_error=true;
			$attachment_error_num=1;
		}

		//echo "-->$logo_error_num";
		//echo "-->$archivo_final";

		$error_msg="";
		if ($attachment_error_num==1){
			$error_msg="";
		}else if ($attachment_error_num==2){
			$error_msg=
				str_replace("$2", CFG_ATTACHMENT_SIZE_BYTES,str_replace("$1", $tamano,MSG_ATT_INFO_BIG_SIZE));
		}else if ($attachment_error_num==3){
			$error_msg=
				str_replace("$2", CFG_ATTACHMENT_SIZE_BYTES,str_replace("$1", $tamano,MSG_ATT_INFO_BIG_SIZE));
		}



		//Save attachment...
		//Si no hay error al guardar la imágen.
		if ($attachment_error_num==100){

			global $mysqli;

			try {

				$query_update=sprintf("UPDATE ic_attachment
									SET
										attachment_file='%s',
										timestamp=CURRENT_TIMESTAMP
								WHERE
										attachment_id = %d",

										$archivo_name,
										$attachment_id
									);

				//echo "****".$query_update."*";
				$result = $mysqli->query($query_update);

				if ($mysqli->error){
					echo '<br/>Invalid query_update(): '.  $mysqli->error . "\n";
				}


			} catch (Exception $e) {
				$errorLog->LogFatal("Caught exception: ". $e->getMessage());
			}
		}else{

			return $error_msg;
		}



}//end upload_attachment




    //Función que mueve de carpeta los archivos de un objeto específico.

	public function moveAttachment($object_type_id, $object_id, $dest_company_id){


		$nCompanyID = $_SESSION["company_id"];
		$sLanguage = $_SESSION["language"];

		global $mysqli;

		  try
		  {

			$query_select = sprintf("SELECT
										attachment_id,
										attachment_file,
										company_id
									FROM ic_attachment
									WHERE
										object_type_id = %d
										AND
										object_id = %d ",
										$object_type_id,
										$object_id
										);

				if ($result = $mysqli->query($query_select)){
					while ($row=$result->fetch_array()){


						$origen = (str_replace("$1",$row["company_id"],CFG_ATTACHMENT_URL)).$row["attachment_id"]."_".$row["attachment_file"];

						$destino = (str_replace("$1",$dest_company_id,CFG_ATTACHMENT_URL)).$row["attachment_id"]."_".$row["attachment_file"];

						$this->moveFile($origen,$destino);

					}
					$result->close();
				}

			} catch (Exception $e) {
				$errorLog->LogFatal("Caught exception: ". $e->getMessage());
			}

	}//moveAttachment

	function moveFile($oldfile,$newfile){
		if(copy($oldfile,$newfile)){
			unlink($oldfile);
			return TRUE;
		}
		return TRUE;
	}


}//End Class

?>