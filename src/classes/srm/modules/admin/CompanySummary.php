<?php
// -----------------------------------------
// CompanyData_class.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/database/DB_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/admin/Company.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/AbstractCRMObjectSummary.php');

class CompanySummary extends AbstractCRMObjectSummary
{


   public function CompanySummary()
   {
   }

   public function getOrderByColumn()
   {
      $sOrderBy = $this->getOrderBy();

      if (null == $sOrderBy)
         return "ic_company.company_name";

      try {
         switch( $sOrderBy) {
            case 2:  $sOrderBy = "ic_company.company_desc"; break;
            case 4:  $sOrderBy = "ic_company.registration_number"; break;
			case 5:  $sOrderBy = "ic_center.center_name"; break;
            default: $sOrderBy = "ic_company.company_name";
         }
      } catch (Exception $ex) {
         return $sOrderBy;
      }
      return $sOrderBy;
   }//END




   /**
    * Crea una lista con los objetos que responden a las
    * restricciones establecidas en la petición.
    *
    * @param  oReq     petición
    * @throws          isyc.website.CRMException
    *                  si se produce algún error al obtener los datos.
    * @throws          NullPointerException
    *                  si la petición especificada es un valor <CODE>null</CODE>.
    * @since           CRM 1.0.0
    */
   public function  load()
   {

     $nCompanyID = $_SESSION["company_id"];
	 $sLanguage = $_SESSION["language"];

     global $mysqli;

      try
      {

      $sFrom = " FROM ic_company
					LEFT JOIN ic_center ON ic_company.center_id = ic_center.center_id ";

      $sWhere = " WHERE ic_company.active_flag = 1 ";

      $sOperator = " = ";
      $sCondition = "";

	  // Param name
	  $sWhere.=$this->getWhereClause("name", "company_name", "ic_company", $this->STRING_TYPE, "", "");

	  // Param center_id
	  $sWhere.=$this->getWhereClause("center_id", "center_id", "ic_center", $this->LOGICAL_TYPE, "", "");


	  // Param registration_number
	  //sWhere.append(getWhereClause(oReq, "registration_number", "registration_number", "ic_company", $STRING_TYPE));

	  if ($nCompanyID!=1){ //Si no es la principal solo ve sus datos.
		 $sWhere.= sprintf(" AND ic_company.company_id = %d ", $nCompanyID) ;
	  }


	  $query_select = "SELECT COUNT(*) " . $sFrom . $sWhere;

	  //echo $query_select;

	  if ($result = $mysqli->query($query_select)){
			$row = $result->fetch_array();
			$this->init($row[0]);
			$result->close();
	  }else{
			$this->init(0);
	  }

       $query_select="SELECT ic_company.company_id,
				  COALESCE(ic_company.company_name,'') as company_name,
				  COALESCE(ic_company.company_desc,''),
				  COALESCE(ic_company.registration_number,'') as registration_number,
				  ic_center.center_name ";

       $query_select.= $sFrom . $sWhere;
       $query_select.= " ORDER BY " . $this->getOrderByColumn() . " " . $this->getOrderHowString() . " ";
       $query_select.= " LIMIT " . $this->getMaxRowsNumber() . " OFFSET " . ($this->getCurrentPage()-1)*$this->getMaxRowsNumber();

       //echo $query_select;

       $result = $mysqli->query($query_select);

	   return $result;

      }
      catch (Exception $ex)
      {

      }



   }//end load






   /**
    * Crea una lista con los objetos que responden a las
    * restricciones establecidas en la petición.
    *
    * @param  sType    tipo de filtrado
    * @param  sID      clave de filtrado
    * @param  oReq     petición
    * @throws          isyc.website.CRMException
    *                  si se produce algún error al obtener los datos.
    * @throws          NullPointerException
    *                  si alguno de los parámetros es un valor <CODE>null</CODE>.
    * @since           CRM 1.0.0
    */
   public function loadFilter($sType, $sID)
   {

   }



}

?>