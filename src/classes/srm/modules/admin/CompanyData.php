<?php
// -----------------------------------------
// CompanyData.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/AbstractCRMObjectChannels.php');

class CompanyData extends AbstractCRMObjectChannels
{

	public $msDescription = "";
	public $msRegistrationNumber = "";
	public $msUrl = "";
	public $msEmailDomain = "";
	public $mnCenterID = "";
	public $msCenterName = "";



	public function getDescription(){
		return $this->msDescription;
	}

	public function setDescription($nValue){
		$this->msDescription = $nValue;
	}


	public function getRegistrationNumber(){
		return $this->msRegistrationNumber;
	}

	public function setRegistrationNumber($nValue){
		$this->msRegistrationNumber = $nValue;
	}


	public function getUrl(){
		return $this->msUrl;
	}

	public function setUrl($nValue){
		$this->msUrl = $nValue;
	}



	public function getEmailDomain(){
		return $this->msEmailDomain;
	}

	public function setEmailDomain($nValue){
		$this->msEmailDomain = $nValue;
	}



	public function getCenterID(){
		return $this->mnCenterID;
	}

	public function setCenterID($nValue){
		$this->mnCenterID = $nValue;
	}



	public function getCenterName(){
		return $this->msCenterName;
	}

	public function setCenterName($nValue){
		$this->msCenterName = $nValue;
	}



}

?>