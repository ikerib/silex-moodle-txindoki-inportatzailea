<?php
// -----------------------------------------
// Picture.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/database/DB_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/admin/PictureData.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/Functions.php');
require_once($_SERVER['DOCUMENT_ROOT']."/../src/lang/en/config.php");
require_once($_SERVER['DOCUMENT_ROOT']."/../src/config_app.php");

class Picture extends PictureData
{

   public function loadData ($id){

	  $nCompanyID = $_SESSION["company_id"];
	  $sLanguage = $_SESSION["language"];

	  if (null == $id || "new"==$id)
      {
			$this->setID("new");
			$this->setName("");
			$this->setCompanyID($nCompanyID);

			$this->setPictureFile("");
			$this->setPictureName("");
			$this->setPictureWidth("");
			$this->setPictureHeight("");

			$this->setPictureOrder(0);
			$this->setPictureDesc("");

			$this->setObjectTypeID(0);
			$this->setObjectTypeName("");
			$this->setObjectID("");
			$this->setObjectName("");

			$openerID="";
			if (isset($_GET["openerID"]) && $_GET["openerID"]!="")
				$openerID=$_GET["openerID"];
			else if (isset($_POST["openerID"]) && $_POST["openerID"]!="")
				$openerID=$_POST["openerID"];

			//echo "openerID=".$openerID;

			$openerType="";
			if (isset($_GET["openerType"]) && $_GET["openerType"]!="")
				$openerType=$_GET["openerType"];
			else if (isset($_POST["openerType"]) && $_POST["openerType"]!="")
				$openerType=$_POST["openerType"];

			//echo "openerType=".$openerType;

			if (isset($openerID) && $openerID!="")
			{
				if (isset($openerType) && $openerType!="")
				{
					if ($openerType=="22"){ //From Information
						$this->setObjectTypeID($openerType);

						$this->setObjectID($openerID);

						require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/admin/Info.php');
						$objInfo = new Info();
						$objInfo->loadData($openerID);

						$this->setObjectName($objInfo->getName());
						$this->setCompanyID($objInfo->getCompanyID());
					}else if ($openerType=="301"){ //From Course
						$this->setObjectTypeID($openerType);

						$this->setObjectID($openerID);

						require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/center/Course.php');
						$objCourse = new Course();
						$objCourse->loadData($openerID);

						$this->setObjectName($objCourse->getName());
						$this->setCompanyID($objCourse->getCompanyID());
					}

				}
			}

			$this->setCompanyID($nCompanyID);
			$this->setRemarks("");
			$this->setActiveFlag(1);
			$this->setModifiedBy($_SESSION["user_id"]);
			$this->setTimestamp("");
			return;



      }

      global $mysqli;
	  global $errorLog;
	  $format = new Format();

	  try {
		$query_select= sprintf(	"SELECT ic_picture.picture_id,
										ic_picture.picture_name,
										ic_picture.picture_file,
										ic_picture.picture_order,
										ic_picture.picture_desc,
										ic_picture.picture_width,
										ic_picture.picture_height,
										ic_picture.remarks,
										ic_picture.active_flag,
										ic_picture.modified_by,
										ic_picture.timestamp,

										ic_picture.object_type_id,
										ic_picture.object_id,
										ic_picture.object_name,
										ic_picture.company_id
								FROM ic_picture
								WHERE ic_picture.picture_id = %d",
										$id);

		$query_select.= " AND ic_picture.company_id = $nCompanyID ";

		$errorLog->LogDebug("SELECT: $query_select");
		 if ($result = $mysqli->query($query_select)){

				if ($result->num_rows > 0){

					$row = $result->fetch_array();

					$this->setID($row["picture_id"]);
					$this->setCompanyID($row["company_id"]);
					$this->setName($row["picture_name"]);
					$this->setPictureName($row["picture_name"]);
					$this->setPictureFile($row["picture_file"]);
					$this->setPictureOrder($row["picture_order"]);
					$this->setPictureDesc($row["picture_desc"]);
					$this->setPictureWidth($row["picture_width"]);
					$this->setPictureHeight($row["picture_height"]);
					$this->setRemarks($row["remarks"]);
					$this->setActiveFlag($row["active_flag"]);
					$this->setModifiedBy($row["modified_by"]);
					$this->setTimestamp($row["timestamp"]);

					$this->setObjectTypeID($row["object_type_id"]);
					$this->setObjectID($row["object_id"]);
					$this->setObjectName($row["object_name"]);

					$ruta_upload = str_replace("$1",$this->getCompanyID(),CFG_PICTURE_URL);
					$this->setPictureFileExists(file_exists($ruta_upload.$this->getID() . "_" . $this->getPictureFile()));
					$this->setThumbnailFileExists(file_exists($ruta_upload.$this->getID() . "_th_" . $this->getPictureFile()));

					$result->close();

				}else{
					$errorLog->LogError("Location: /srm/index.php?error=3. ERROR: ".$query_select);
					header("Location: /index.php?error=3");
					exit;
				}

			}

	  } catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
	  }

	}//end loadData


   public function createNew()
   {

	  global $mysqli;
	  global $errorLog;
	  $format = new Format();
	  $error="";

      try {

			$query_insert=sprintf("INSERT INTO ic_picture
								    (picture_name,
									picture_order,
									picture_desc,

									object_type_id,
									object_id,
									object_name,

									active_flag,
									remarks,
									modified_by,
									timestamp,
									company_id)
								  VALUES (
								  '%s',
								  '%s',
								  '%s',

								  '%s',
								  '%s',
								  '%s',

								   1 ,
								  '%s',
								  '%s',
								  CURRENT_TIMESTAMP,
								  %d
								  )",

									$this->getName(),
									$this->getPictureOrder(),
									$this->getPictureDesc(),

									$this->getObjectTypeID(),
									$this->getObjectID(),
									$this->getObjectName(),

									$this->getRemarks(),
									$this->getModifiedBy(),
									$this->getCompanyID()
							);

			//echo $query_insert.
			$result = $mysqli->query($query_insert);

			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}

			//Calculo el último ID insertado.
			$query_max = "SELECT LAST_INSERT_ID() AS max_id";
			if( $result = $mysqli->query($query_max) ){
				$row = $result->fetch_array();

				$this->setID($row["max_id"]);

				$result->close();
			}


			///Sube la imágen.

			//echo "this->getUpload() = ".$this->getUpload();

			if ($this->getUpload()=="yes"){
				$upload = $this->uploadPicture($this->getID(), $this->getCompanyID());
				$error=$upload;
			}

		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}

		return $error;

   }//END create_new






   public function update($id)
   {

	  global $mysqli;
	  global $errorLog;
	  $format = new Format();
	  $error="";

      try {

			$query_update=sprintf("UPDATE ic_picture
									SET
								    picture_name='%s',
									picture_order='%s',
									picture_desc='%s',

									object_type_id='%s',
									object_id='%s',
									object_name='%s',

									remarks='%s',
									modified_by='%s',
									timestamp=CURRENT_TIMESTAMP,
									company_id=%d
								  WHERE
									picture_id = %d",

									$this->getName(),
									$this->getPictureOrder(),
									$this->getPictureDesc(),

									$this->getObjectTypeID(),
									$this->getObjectID(),
									$this->getObjectName(),

									$this->getRemarks(),
									$_SESSION["user_id"],
									$this->getCompanyID(),
									$id
							);

			//echo "*".$query_update."*";
			$result = $mysqli->query($query_update);

			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}

			$ruta_upload = str_replace("$1",$this->getCompanyID(),CFG_PICTURE_URL);

			//Borramos la anterior imágen si estamos subiendo una nueva.
			if ($this->getUpload()=="yes"){
				if (file_exists($ruta_upload.$this->getID() . "_" . $this->getPictureFileOld())){
					unlink($ruta_upload.$this->getID() . "_" . $this->getPictureFileOld());
				}
				if (file_exists($ruta_upload.$this->getID() . "_th_" . $this->getPictureFileOld())){
					unlink($ruta_upload.$this->getID() . "_th_" . $this->getPictureFileOld());
				}
			}



			///Sube la imágen.
			if ($this->getUpload()=="yes"){
				$upload = $this->uploadPicture($this->getID(), $this->getCompanyID());

				$error=$upload;
			}



		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}


		return $error;

   }//END UPDATE



    public function delete($id)
   {
	 global $mysqli;
	 global $errorLog;

      try {

			$query_select=sprintf("SELECT
									picture_id,
									picture_file,
									company_id
			  				       FROM ic_picture
								   WHERE picture_id = %d",
									$_POST['id']
    							);

			//echo "*".$query_select."*";

			if ($result = $mysqli->query($query_select)){

				$row = $result->fetch_array();

				$this->setID($row["picture_id"]);
				$this->setPictureFile($row["picture_file"]);
				$this->setCompanyID($row["company_id"]);
				$result->close();

			}




			$query_delete=sprintf("UPDATE ic_picture
								SET active_flag=0,
									modified_by='%s',
									timestamp=CURRENT_TIMESTAMP
							WHERE
									picture_id = %d",

									$_SESSION["user_id"],
									$_POST['id']
    							);

			//echo "*".$query_delete."*";
			$result = $mysqli->query($query_delete);

			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}


			//Borramos las imágenes.

			$ruta_upload = str_replace("$1",$this->getCompanyID(),CFG_PICTURE_URL);

			if (file_exists($ruta_upload.$this->getID() . "_" . $this->getPictureFile())){
				unlink($ruta_upload.$this->getID() . "_" . $this->getPictureFile());
			}
			if (file_exists($ruta_upload.$this->getID() . "_th_" . $this->getPictureFile())){
				unlink($ruta_upload.$this->getID() . "_th_" . $this->getPictureFile());
			}

		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}


   }//END DELETE

     //Borra el objeto indicado, según el tipo de objecto y su id.
   public function deleteRelated($object_type_id, $id)
   {
	 global $mysqli;
	 global $errorLog;

      try {

			$query_delete=sprintf("UPDATE ic_picture
								SET active_flag=0,
									modified_by='%s',
									timestamp=CURRENT_TIMESTAMP
							WHERE
									object_type_id = %d
									AND object_id = %d",

									$_SESSION["user_id"],
									$object_type_id,
									$id
    							);

			$result = $mysqli->query($query_delete);

			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}

		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}


   }//END DELETE

   public function init()
   {
		$error=false;
		$error_txt="";
		$valor = "";


		//Param id
		$valor = $_POST['id'];
		if (trim($valor)!=""){
			$this->setID($valor);
		}else{
			$this->setID("new");
		}

		//Param name
		$valor = $_POST['name'];
		if (trim($valor)!=""){
			$this->setName($valor);
		}else{
			$this->setName($_FILES['picture']['name']);
		}

		$this->setPictureDesc($_POST['picture_desc']);
		$this->setPictureOrder($_POST['picture_order']);


		//Param object_type_id
		$valor = $_POST['foreign_object_type_id'];
		if (trim($valor)!="" && trim($valor)!="0" && trim($valor)!="null"){
			$this->setObjectTypeID($valor);
		}else{
			$error=true;
			$error_txt.="\\n".MSG_ERROR_OBJECT_TYPE;
			$this->setObjectTypeID("");
		}

		//Param foreign_object_id
		$valor = $_POST['foreign_object_id'];
		if (trim($valor)!="" && trim($valor)!="0" && trim($valor)!="null"){
			$this->setObjectID($valor);
		}else{
			$error=true;
			$error_txt.="\\n".MSG_ERROR_OBJECT_RELATED;
			$this->setObjectID("");
		}

		//Param foreign_object_name
		$valor = $_POST['foreign_object_name'];
		$this->setObjectName(trim($valor));


		$valor = $_POST['bupload_image'];
		$this->setUpload($valor);

		$valor = $_POST['picture_file_old'];
		$this->setPictureFileOld($valor);



		$this->setCompanyID($_POST['company_id']);
		$this->setRemarks($_POST['remarks']);
		$this->setModifiedBy($_SESSION['user_id']);

		return $error_txt;

   }//END init




//Función que sube o modifica una imagen existente en la ruta indicada.

public function uploadPicture($picture_id, $company_id){

		$logo_error=false;
		$logo_error_num=0;
		$archivo_final="";

		$ruta_upload = str_replace("$1",$company_id,CFG_PICTURE_URL);

		if (is_uploaded_file($_FILES['picture']['tmp_name'])) {

			$archivo_name= $_FILES['picture']['name'];
			$archivo_size= $_FILES['picture']['size'];
			$archivo_type=  $_FILES['picture']['type'];
			$archivo= $_FILES['picture']['tmp_name'];

			//echo "<br>archivo_name=".$archivo_name;
			//echo "<br>archivo_size=".$archivo_size;
			//echo "<br>archivo_type=".$archivo_type;
			//echo "<br>archivo=".$archivo;

			$lim_tamano= $_POST['lim_tamano'];

			//echo "<br>archivo=".$lim_tamano;

			$extension = explode(".",$archivo_name);
			$num = count($extension)-1;
			if(($extension[$num] == "jpg" || $extension[$num] == "gif" || $extension[$num] == "png" )){

				if ($extension[$num] == "jpg"){
					$archivo_final=$picture_id."_".$archivo_name;
				}

				if ($extension[$num] == "gif"){
					$archivo_final=$picture_id."_".$archivo_name;
				}

				if ($extension[$num] == "png"){
					$archivo_final=$picture_id."_".$archivo_name;
				}

				//echo "<br>IMAGEN BUENA";

				$tamano = getimagesize($archivo);
				//echo "<br>alto=".$tamano[1];
				//echo "<br>ancho=".$tamano[0];

				//Valida las dimensiones del logotipo.
				if (($tamano[0]>CFG_PICTURE_WIDTH || $tamano[1]>CFG_PICTURE_HEIGHT)){

					$logo_error=true;
					$logo_error_num=2;
				}

				if (!$logo_error){

					//echo "<br>->archivo_name=$archivo_name";
					//echo "<br>->archivo_size=$archivo_size";
					//echo "<br>->lim_tamano=$lim_tamano";

					if ($archivo_name != "none" AND $archivo_size != 0 AND $archivo_size<=$lim_tamano){
						if (copy ($archivo, $ruta_upload.$archivo_final)) {
						  $logo_error_num=100;
						}
					}else{
						$logo_error=true;
						$logo_error_num=3;
					}
				}

			}

		}else{
			$logo_error=true;
			$logo_error_num=1;
		}

		$error_msg="";
		if ($logo_error_num==1){
			$error_msg="";
		}else if ($logo_error_num==2){
			$error_msg=
				str_replace("$4", CFG_PICTURE_HEIGHT,
				str_replace("$3", CFG_PICTURE_WIDTH,
				str_replace("$2", $tamano[1],str_replace("$1",$tamano[0],MSG_INFO_BIG_SIZE))));
		}else if ($logo_error_num==3){
			$error_msg=
				str_replace("$2", CFG_SIZE_BYTES,str_replace("$1",$archivo_size,MSG_INFO_LARGE));
		}

		//echo $error_msg;
		//echo $logo_error_num;

		if ($logo_error_num==0 || $logo_error_num==100){ //si no hay error guardo la imagen.

			//Reduzco la imagen y la guardo. Thumbnail.



			$archivo_final = $ruta_upload.$archivo_final;

			$original = "";
			if (preg_match('/.png$/', $archivo_final)) {
				$original = imagecreatefrompng($archivo_final);
			} else if (preg_match('/.gif$/', $archivo_final)) {
				$original = imagecreatefromgif($archivo_final);
			} else if (preg_match('/.jpg$/', $archivo_final)) {
				$original = imagecreatefromjpeg($archivo_final);
			}


			$ancho = imagesx($original);
			$alto = imagesy($original);

			$ratio = $ancho/CFG_TH_PICTURE_WIDTH;
			$aux_height = $alto/$ratio;

			$thumb = imagecreatetruecolor(CFG_TH_PICTURE_WIDTH,$aux_height); // Lo haremos de un tamaño 150x150

			imagecopyresampled($thumb,$original,0,0,0,0,CFG_TH_PICTURE_WIDTH,$aux_height,$ancho,$alto);


			if (preg_match('/.png$/', $archivo_final)) {
				imagepng($thumb,$ruta_upload.$picture_id."_th_".$archivo_name); // Suprimida la calidad de compresión daba error.
			} else if (preg_match('/.gif$/', $archivo_final)) {
				imagegif($thumb,$ruta_upload.$picture_id."_th_".$archivo_name,90); // 90 es la calidad de compresión
			} else if (preg_match('/.jpg$/', $archivo_final)) {
				imagejpeg($thumb,$ruta_upload.$picture_id."_th_".$archivo_name,90); // 90 es la calidad de compresión
			}


			//Save image...
			global $mysqli;
			global $errorLog;

			try {

				$query_update=sprintf("UPDATE ic_picture
									SET
										picture_file='%s',
										picture_width='%s',
										picture_height='%s',
										timestamp=CURRENT_TIMESTAMP
								WHERE
										picture_id = %d",

										$archivo_name,
										$tamano[0],
										$tamano[1],
										$picture_id
									);

				//echo "*".$query_update."*";
				$result = $mysqli->query($query_update);

				if ($mysqli->error){
					$errorLog->LogError($mysqli->error);
				}

				/*

					Borrar las imágenes

				*/


			} catch (Exception $e) {
				$errorLog->LogFatal("Caught exception: ". $e->getMessage());
			}

		}else{

			return $error_msg;
		}


}//end upload_picture












}//End Class



 ?>