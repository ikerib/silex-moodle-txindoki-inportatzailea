<?php
// -----------------------------------------
// InfoData.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/AbstractCRMObject.php');

class InfoData extends AbstractCRMObject
{
	public $msNameEU = "";

	public $msDescription = "";
	public $msDescriptionEU = "";

	public $mnInfoTypeID = "";
	public $msInfoTypeName = "";
	public $moDate = "";
	public $msContents = "";
	public $msContentsEU = "";

	public $msLink = "";
	public $msLinkName = "";

	public $msInfoSource = "";
    public $moPublicationDate = "";
    public $moExpirationDate = "";
    public $msInfoExtra = "";


	public function getNameEU(){
		return $this->msNameEU;
	}
	public function setNameEU($nValue){
		$this->msNameEU = $nValue;
	}

    public function getDescription(){
		return $this->msDescription;
	}
	public function setDescription($nValue){
		$this->msDescription = $nValue;
	}

	public function getDescriptionEU(){
		return $this->msDescriptionEU;
	}
	public function setDescriptionEU($nValue){
		$this->msDescriptionEU = $nValue;
	}

	public function getInfoTypeID(){
		return $this->mnInfoTypeID;
	}
	public function setInfoTypeID($nValue){
		$this->mnInfoTypeID = $nValue;
	}

	public function getInfoTypeName(){
		return $this->msInfoTypeName;
	}
	public function setInfoTypeName($nValue){
		$this->msInfoTypeName = $nValue;
	}

	public function getContents(){
		return $this->msContents;
	}
	public function setContents($nValue){
		$this->msContents = $nValue;
	}
	public function getContentsEU(){
		return $this->msContentsEU;
	}
	public function setContentsEU($nValue){
		$this->msContentsEU = $nValue;
	}

	public function getLink(){
		return $this->msLink;
	}
	public function setLink($nValue){
		$this->msLink = $nValue;
	}

	public function getLinkName(){
		return $this->msLinkName;
	}
	public function setLinkName($nValue){
		$this->msLinkName = $nValue;
	}

 	public function getDate(){
		return $this->moDate;
	}
	public function setDate($nValue){
		$this->moDate = $nValue;
	}





	public function getInfoSource(){
		return $this->msInfoSource;
	}
	public function setInfoSource($nValue){
		$this->msInfoSource = $nValue;
	}

 	public function getPublicationDate(){
		return $this->moPublicationDate;
	}
	public function setPublicationDate($nValue){
		$this->moPublicationDate = $nValue;
	}

	public function getExpirationDate(){
		return $this->moExpirationDate;
	}
	public function setExpirationDate($nValue){
		$this->moExpirationDate = $nValue;
	}

	public function getInfoExtra(){
		return $this->msInfoExtra;
	}
	public function setInfoExtra($nValue){
		$this->msInfoExtra = $nValue;
	}



}
?>