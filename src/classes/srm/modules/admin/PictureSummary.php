<?php
// -----------------------------------------
// PictureSummary.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/database/DB_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/admin/PictureSummary.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/AbstractCRMObjectSummary.php');

class PictureSummary extends AbstractCRMObjectSummary
{

   public function PictureSummary()
   {
   }

   public function getOrderByColumn()
   {
      $sOrderBy = $this->getOrderBy();

      if (null == $sOrderBy)
         return "ic_picture.picture_name";

      try {
         switch( $sOrderBy) {
            case 2:  $sOrderBy = "ic_picture.picture_desc"; break;
            case 3:  $sOrderBy = "ic_picture.remarks"; break;
			case 4:  $sOrderBy = "ic_picture.picture_order"; break;
			case 5:  $sOrderBy = "ica_object_type.object_type_name"; break;
			case 6:  $sOrderBy = "ic_picture.object_name"; break;
            default: $sOrderBy = "ic_picture.picture_name";
         }
      } catch (Exception $ex) {
         return $sOrderBy;
      }
      return $sOrderBy;
   }//END


    public function  load()
   {

     $nCompanyID = $_SESSION["company_id"];
	 $sLanguage = $_SESSION["language"];

     global $mysqli;

      try
      {

      $sFrom = " FROM  ic_picture LEFT JOIN ica_object_type ON ic_picture.object_type_id = ica_object_type.object_type_id  ";

      $sWhere = " WHERE ic_picture.active_flag = 1 ";
	  $sWhere.= " AND ica_object_type.language ='$sLanguage' ";
	  $sWhere.= " AND ic_picture.company_id = $nCompanyID ";

      $sOperator = " = ";
      $sCondition = "";

	  // Param name
	  $sWhere.=$this->getWhereClause("name", "picture_name", "ic_picture", $this->STRING_TYPE, "", "");

	  // Param picture_order
	  $sWhere.=$this->getWhereClause("picture_order", "picture_order", "ic_picture", $this->STRING_TYPE, "", "");

	  // Param picture_desc
	  $sWhere.=$this->getWhereClause("picture_desc", "picture_desc", "ic_picture", $this->STRING_TYPE, "", "");

	  // Param foreign_object_type_id
	  $sWhere.=$this->getWhereClause("foreign_object_type_id", "object_type_id", "ic_picture", $this->LOGICAL_TYPE, "", "");

	  // Param foreign_object_id
	  $sWhere.=$this->getWhereClause("foreign_object_id", "object_id", "ic_picture", $this->LOGICAL_TYPE, "", "");

	  $query_select = "SELECT COUNT(*) " . $sFrom . $sWhere;

	  //echo $query_select;

	  if ($result = $mysqli->query($query_select)){
			$row = $result->fetch_array();
			$this->init($row[0]);
			$result->close();
	  }else{
			$this->init(0);
	  }

       $query_select="SELECT ic_picture.picture_id,
							COALESCE(ic_picture.picture_name,'') as picture_name,
							COALESCE(ic_picture.picture_desc,'') as picture_desc,
							COALESCE(ic_picture.picture_order,0) as picture_order,
							ic_picture.picture_file,
							ic_picture.picture_width,
							ic_picture.picture_height,
							ic_picture.object_name,
							ic_picture.object_type_id,
							ica_object_type.icon,
							ic_picture.company_id,
							COALESCE(ica_object_type.object_type_name,'') as object_type_name,
							COALESCE(ic_picture.object_name,'') as object_name" ;

       $query_select.= $sFrom . $sWhere;
       $query_select.= " ORDER BY " . $this->getOrderByColumn() . " " . $this->getOrderHowString() . " ";
       $query_select.= " LIMIT " . $this->getMaxRowsNumber() . " OFFSET " . ($this->getCurrentPage()-1)*$this->getMaxRowsNumber();

       $errorLog->LogDebug("SELECT: $query_select");

       $result = $mysqli->query($query_select);

	   return $result;

      }
      catch (Exception $ex)
      {

      }



   }//end load



   public function  loadFilter($sType, $sID)
   {

     $nCompanyID = $_SESSION["company_id"];
	 $sLanguage = $_SESSION["language"];

     global $mysqli;

      try
      {

      $sFrom = " FROM ic_picture  ";
	  $sFrom.= " , ica_object_type ";

      $sWhere = " WHERE ic_picture.active_flag = 1 ";
	  $sWhere.= " AND ica_object_type.object_type_id = ic_picture.object_type_id ";
	  $sWhere.= " AND ica_object_type.language ='$sLanguage' ";

      $sOperator = " = ";
      $sCondition = "";

	  // Param name
	  $sWhere.=$this->getWhereClause("name", "picture_name", "ic_picture", $this->STRING_TYPE, "", "");

	  // Param picture_order
	  $sWhere.=$this->getWhereClause("picture_order", "picture_order", "ic_picture", $this->STRING_TYPE, "", "");

	  // Param picture_desc
	  $sWhere.=$this->getWhereClause("picture_desc", "picture_desc", "ic_picture", $this->STRING_TYPE, "", "");

	  // Param info_date
	  $sWhere.=$this->getWhereClause("info_date", "info_date", "ic_info", $this->DATE_TYPE, "", "");

	  // Param info_type_id
	  $sWhere.=$this->getWhereClause("info_type_id", "info_type_id", "ic_info", $this->LOGICAL_TYPE, "", "");


	  $sWhere.= " AND ic_picture.company_id = $nCompanyID ";

	  if ($sType=="INFORMATION")
      {
			$sWhere.="   AND ic_picture.object_id = " . $sID ;
			$sWhere.="   AND ic_picture.object_type_id = 22";
      }else if ($sType=="COURSE")
      {
			$sWhere.="   AND ic_picture.object_id = " . $sID ;
			$sWhere.="   AND ic_picture.object_type_id = 301";
      }else{
			$sWhere.="   AND 1 = 2 " ;
	  }


	  $query_select = "SELECT COUNT(*) " . $sFrom . $sWhere;

	  //echo $query_select;

	  if ($result = $mysqli->query($query_select)){
			$row = $result->fetch_array();
			$this->init($row[0]);
			$result->close();
	  }else{
			$this->init(0);
	  }

       $query_select="SELECT ic_picture.picture_id,
							COALESCE(ic_picture.picture_name,'') as picture_name,
							COALESCE(ic_picture.picture_desc,'') as picture_desc,
							COALESCE(ic_picture.picture_order,0) as picture_order,
							ic_picture.picture_file,
							ic_picture.picture_width,
							ic_picture.picture_height,
							ic_picture.object_name,
							ic_picture.object_type_id,
							ica_object_type.icon,
							ic_picture.company_id" ;

       $query_select.= $sFrom . $sWhere;
       $query_select.= " ORDER BY " . $this->getOrderByColumn() . " " . $this->getOrderHowString() . " ";
       $query_select.= " LIMIT " . $this->getMaxRowsNumber() . " OFFSET " . ($this->getCurrentPage()-1)*$this->getMaxRowsNumber();

       //echo $query_select;

       $result = $mysqli->query($query_select);

	   return $result;

      }
      catch (Exception $ex)
      {

      }



   }//end load


}
?>