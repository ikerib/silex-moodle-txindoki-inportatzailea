<?php
// -----------------------------------------
// AttachmentSummary.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/database/DB_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/admin/AttachmentSummary.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/AbstractCRMObjectSummary.php');

class AttachmentSummary extends AbstractCRMObjectSummary
{

   public function AttachmentSummary()
   {
   }

   public function getOrderByColumn()
   {
      $sOrderBy = $this->getOrderBy();

      if (null == $sOrderBy)
         return "ic_attachment.attachment_name";

      try {
         switch( $sOrderBy) {
			case 1:  $sOrderBy = "ic_attachment.attachment_name"; break;
            case 2:  $sOrderBy = "ic_attachment.attachment_desc"; break;
            case 3:  $sOrderBy = "ica_object_type.object_type_name"; break;
			case 4:  $sOrderBy = "ic_attachment.object_name"; break;
            default: $sOrderBy = "ic_attachment.attachment_name";
         }
      } catch (Exception $ex) {
         return $sOrderBy;
      }
      return $sOrderBy;
   }//END


    public function  load()
   {

     $nCompanyID = $_SESSION["company_id"];
	 $sLanguage = $_SESSION["language"];

     global $mysqli;
	 global $errorLog;

      try
      {

      $sFrom = " FROM  ic_attachment LEFT JOIN ica_object_type ON ic_attachment.object_type_id = ica_object_type.object_type_id AND ica_object_type.language = '$sLanguage'  ";

      $sWhere = " WHERE ic_attachment.active_flag = 1 ";

	  $sWhere.= " AND ic_attachment.company_id = $nCompanyID ";

      $sOperator = " = ";
      $sCondition = "";

	  // Param name
	  $sWhere.=$this->getWhereClause("name", "attachment_name", "ic_picture", $this->STRING_TYPE, "", "");

	  // Param attachment_desc
	  $sWhere.=$this->getWhereClause("attachment_desc", "attachment_desc", "ic_attachment", $this->STRING_TYPE, "", "");

      // Param foreign_object_type_id
	  $sWhere.=$this->getWhereClause("foreign_object_type_id", "object_type_id", "ic_attachment", $this->LOGICAL_TYPE, "", "");

	  // Param foreign_object_id
	  $sWhere.=$this->getWhereClause("foreign_object_id", "object_id", "ic_attachment", $this->LOGICAL_TYPE, "", "");

	  $query_select = "SELECT COUNT(*) " . $sFrom . $sWhere;

	  $errorLog->LogDebug("SELECT: $query_select");

	  if ($result = $mysqli->query($query_select)){
			$row = $result->fetch_array();
			$this->init($row[0]);
			$result->close();
	  }else{
			$this->init(0);
	  }

       $query_select="SELECT ic_attachment.attachment_id,
							 COALESCE(ic_attachment.attachment_name,'') as attachment_name,
							 COALESCE(ic_attachment.attachment_file,'') as attachment_file,
							 COALESCE(ic_attachment.attachment_desc,'') as attachment_desc,
							 COALESCE(ica_object_type.object_type_name,'') as object_type_name,

							 COALESCE(ic_attachment.object_name,'') as object_name,

							 ic_attachment.company_id " ;

       $query_select.= $sFrom . $sWhere;
       $query_select.= " ORDER BY " . $this->getOrderByColumn() . " " . $this->getOrderHowString() . " ";
       $query_select.= " LIMIT " . $this->getMaxRowsNumber() . " OFFSET " . ($this->getCurrentPage()-1)*$this->getMaxRowsNumber();

       //echo $query_select;

       $result = $mysqli->query($query_select);

	   return $result;

      }
      catch (Exception $ex)
      {

      }



   }//end load


    public function  loadFilter($sType, $sID)
    {

     $nCompanyID = $_SESSION["company_id"];
	 $sLanguage = $_SESSION["language"];

     global $mysqli;
	 global $errorLog;

      try
      {

      $sFrom = " FROM  ic_attachment LEFT JOIN ica_object_type ON ic_attachment.object_type_id = ica_object_type.object_type_id  ";

      $sWhere = " WHERE ic_attachment.active_flag = 1 ";
	  $sWhere.= " AND ica_object_type.language = '$sLanguage' ";

	  $sOperator = " = ";
      $sCondition = "";

	  // Param name
	  $sWhere.=$this->getWhereClause("name", "attachment_name", "ic_picture", $this->STRING_TYPE, "", "");

	  // Param attachment_desc
	  $sWhere.=$this->getWhereClause("attachment_desc", "attachment_desc", "ic_attachment", $this->STRING_TYPE, "", "");

	  // Param object_type_id
	  $sWhere.=$this->getWhereClause("object_type_id", "object_type_id", "ic_attachment", $this->LOGICAL_TYPE, "", "");

  	  $sWhere.= " AND ic_attachment.company_id = $nCompanyID ";

	  if ($sType=="INFORMATION")
      {
			$sWhere.="   AND ic_attachment.object_id = " . $sID ;
			$sWhere.="   AND ic_attachment.object_type_id = 22";
      }else if ($sType=="ADMINISTRATION")
      {
			$sWhere.="   AND ic_attachment.object_id = " . $sID ;
			$sWhere.="   AND ic_attachment.object_type_id = 19";
      }else if ($sType=="EMPLOYEE")
      {
			$sWhere.="   AND ic_attachment.object_id = " . $sID ;
			$sWhere.="   AND ic_attachment.object_type_id = 7";
	  }else if ($sType=="COURSE")
      {
			$sWhere.="   AND ic_attachment.object_id = " . $sID ;
			$sWhere.="   AND ic_attachment.object_type_id = 301";
      }else if ($sType=="STUDENT")
      {
			$sWhere.="   AND ic_attachment.object_id = " . $sID ;
			$sWhere.="   AND ic_attachment.object_type_id = 302";
	  }else if ($sType=="ENROLLMENT")
      {
			$sWhere.="   AND ic_attachment.object_id = " . $sID ;
			$sWhere.="   AND ic_attachment.object_type_id = 303";
      }else{
			$sWhere.="   AND 1 = 2 " ;
	  }

	  $query_select = "SELECT COUNT(*) " . $sFrom . $sWhere;

	  //echo $query_select;

	  if ($result = $mysqli->query($query_select)){
			$row = $result->fetch_array();
			$this->init($row[0]);
			$result->close();
	  }else{
			$this->init(0);
	  }

       $query_select="SELECT ic_attachment.attachment_id,
							 COALESCE(ic_attachment.attachment_name,'') as attachment_name,
							 COALESCE(ic_attachment.attachment_file,'') as attachment_file,
							 COALESCE(ic_attachment.attachment_desc,'') as attachment_desc,
							 COALESCE(ica_object_type.object_type_name,'') as object_type_name,
							 COALESCE(ic_attachment.object_name,'') as object_name,
							 ic_attachment.company_id " ;

       $query_select.= $sFrom . $sWhere;
       $query_select.= " ORDER BY " . $this->getOrderByColumn() . " " . $this->getOrderHowString() . " ";
       $query_select.= " LIMIT " . $this->getMaxRowsNumber() . " OFFSET " . ($this->getCurrentPage()-1)*$this->getMaxRowsNumber();

       $errorLog->LogDebug("SELECT: $query_select");

       $result = $mysqli->query($query_select);

	   return $result;

      }
      catch (Exception $ex)
      {

      }

   }//end loadFilter



}
?>