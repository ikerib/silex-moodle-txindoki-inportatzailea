<?php
// -----------------------------------------
// AttachmentData.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/AbstractCRMObject.php');

class AttachmentData extends AbstractCRMObject
{

   public $msAttachmentFile = null;
   public $msAttachmentName = null;
   public $mbAttachmentFileExists = false;
   public $msAttachmentUploadFile = null;
   public $msAttachmentFileDelete = null;
   public $mbAttachmentUpdateFile = false;
   public $msAttachmentDesc = null;
   public $mnObjectTypeID = null;
   public $msObjectTypeName = "";
   public $moObjectID = null;
   public $msObjectName = "";
   //public $mbFilesTotalSize = 0;

	public function getAttachmentFile(){
		return $this->msAttachmentFile;
	}
	public function setAttachmentFile($nValue){
		$this->msAttachmentFile = $nValue;
	}

	public function getAttachmentName(){
		return $this->msAttachmentName;
	}
	public function setAttachmentName($nValue){
		$this->msAttachmentName = $nValue;
	}

	public function getAttachmentFileExists(){
		return $this->mbAttachmentFileExists;
	}
	public function setAttachmentFileExists($nValue){
		$this->mbAttachmentFileExists = $nValue;
	}

	public function getAttachmentUploadFile(){
		return $this->msAttachmentUploadFile;
	}
	public function setAttachmentUploadFile($nValue){
		$this->msAttachmentUploadFile = $nValue;
	}

	public function getAttachmentFileDelete(){
		return $this->msAttachmentFileDelete;
	}
	public function setAttachmentFileDelete($nValue){
		$this->msAttachmentFileDelete = $nValue;
	}

	public function getAttachmentUpdateFile(){
		return $this->mbAttachmentUpdateFile;
	}
	public function setAttachmentUpdateFile($nValue){
		$this->mbAttachmentUpdateFile = $nValue;
	}

	public function getAttachmentDesc(){
		return $this->msAttachmentDesc;
	}
	public function setAttachmentDesc($nValue){
		$this->msAttachmentDesc = $nValue;
	}

	public function getObjectTypeID(){
		return $this->mnObjectTypeID;
	}
	public function setObjectTypeID($nValue){
		$this->mnObjectTypeID = $nValue;
	}

	public function getObjectTypeName (){
		return $this->msObjectTypeName;
	}
	public function setObjectTypeName($nValue){
		$this->msObjectTypeName = $nValue;
	}

	public function getObjectID (){
		return $this->moObjectID ;
	}
	public function setObjectID ($nValue){
		$this->moObjectID  = $nValue;
	}

	public function getObjectName (){
		return $this->msObjectName;
	}
	public function setObjectName($nValue){
		$this->msObjectName = $nValue;
	}

}//end class

?>