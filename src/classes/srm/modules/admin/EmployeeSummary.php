<?php
// -----------------------------------------
// EmployeeSummary.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/database/DB_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/AbstractCRMObjectSummary.php');

class EmployeeSummary extends AbstractCRMObjectSummary
{

   public function EmployeeSummary()
   {
   }

   public function getOrderByColumn()
   {
      $sOrderBy = $this->getOrderBy();

      if (null == $sOrderBy)
         return "ic_employee.employee_first_name";

      try {
         switch( $sOrderBy) {
            case 2:  $sOrderBy = "ic_employee.employee_last_name1"; break;
            case 3:  $sOrderBy = "ic_employee.user_id"; break;
            case 4:  $sOrderBy = "ic_employee.internal_id"; break;
            case 5:  $sOrderBy = "ic_employee.birthdate"; break;
			case 6:  $sOrderBy = "ic_company.company_name"; break;
            default: $sOrderBy = "ic_employee.employee_first_name";
         }
      } catch (Exception $ex) {
         return $sOrderBy;
      }
      return $sOrderBy;
   }//END




   /**
    * Crea una lista con los objetos que responden a las
    * restricciones establecidas en la petición.
    *
    * @param  oReq     petición
    * @throws          isyc.website.CRMException
    *                  si se produce algún error al obtener los datos.
    * @throws          NullPointerException
    *                  si la petición especificada es un valor <CODE>null</CODE>.
    * @since           CRM 1.0.0
    */
	public function  load()
	{

		$nCompanyID = $_SESSION["company_id"];
		$sLanguage = $_SESSION["language"];

		global $mysqli;

		try
		{

			$sFrom = " FROM ic_employee LEFT JOIN ic_company ON ic_employee.company_id = ic_company.company_id ";

			$sWhere = " WHERE ic_employee.active_flag = 1 ";

			//Acceso de la company_id=1 a los otros empleados.
			if ($_SESSION["company_id"]==1){
				$sWhere .= "";
			}else{
				$sWhere .= " AND ic_employee.company_id = ".$nCompanyID;
			}

			$sOperator = " = ";
			$sCondition = "";

			// Param name
			$sWhere.=$this->getWhereClause("name", "employee_first_name", "ic_employee", $this->STRING_TYPE, "", "");

			// param last_name_1
			$sWhere.=$this->getWhereClause("last_name_1", "employee_last_name1", "ic_employee", $this->STRING_TYPE, "", "");

			// param id_card
			$sWhere.=$this->getWhereClause("id_card", "id_card", "ic_employee", $this->STRING_TYPE, "", "");

			// param internal_id
			$sWhere.=$this->getWhereClause("internal_id", "internal_id", "ic_employee", $this->STRING_TYPE, "", "");

			// param user_id
			$sWhere.=$this->getWhereClause("user_id", "user_id", "ic_employee", $this->STRING_TYPE, "", "");


			// parametro birthdate
			$sWhere.=$this->getWhereClause("birthdate", "birthdate", "ic_employee", $this->DATE_TYPE, "", "");

			// Param company_id
			$sWhere.=$this->getWhereClause("aux_company_id", "company_id", "ic_company", $this->LOGICAL_TYPE, "", "");

			$query_select = "SELECT COUNT(*) " . $sFrom . $sWhere;

			//echo $query_select;

			if ($result = $mysqli->query($query_select)){
				$row = $result->fetch_array();
				$this->init($row[0]);
				$result->close();
			}else{
				$this->init(0);
			}

			$query_select="SELECT ic_employee.employee_id, ".
						"       ic_employee.employee_first_name, ".
						"       ic_employee.employee_last_name1, ".
						"       ic_employee.id_card, ".
						"       ic_employee.internal_id, ".
						"       DATE_FORMAT(ic_employee.birthdate,'%Y/%m/%d') as birthdate, 	".
						"       ic_employee.user_id, ".
						"		ic_company.company_name";
			$query_select.= $sFrom . $sWhere;
			$query_select.= " ORDER BY " . $this->getOrderByColumn() . " " . $this->getOrderHowString() . " ";
			$query_select.= " LIMIT " . $this->getMaxRowsNumber() . " OFFSET " . ($this->getCurrentPage()-1)*$this->getMaxRowsNumber();

			//echo $query_select;

			$result = $mysqli->query($query_select);

			return $result;

		}
		catch (Exception $ex)
		{

		}



   }//end load






   public function  loadFilter($sType, $sID)
	{

		$nCompanyID = $_SESSION["company_id"];
		$sLanguage = $_SESSION["language"];

		global $mysqli;

		try
		{

			$sFrom = " FROM ic_employee ";

			$sWhere = " WHERE ic_employee.active_flag = 1 ";

			//Acceso de la company_id=1 a los otros empleados.
			if ($_SESSION["company_id"]==1){
				$sWhere .= "";
			}else{
				$sWhere .= " AND ic_employee.company_id = ".$nCompanyID;
			}

			$sOperator = " = ";
			$sCondition = "";

			// Param name
			$sWhere.=$this->getWhereClause("name", "employee_first_name", "ic_employee", $this->STRING_TYPE, "", "");

			// param last_name_1
			$sWhere.=$this->getWhereClause("last_name_1", "employee_last_name1", "ic_employee", $this->STRING_TYPE, "", "");

			// param id_card
			$sWhere.=$this->getWhereClause("id_card", "id_card", "ic_employee", $this->STRING_TYPE, "", "");

			// param internal_id
			$sWhere.=$this->getWhereClause("internal_id", "internal_id", "ic_employee", $this->STRING_TYPE, "", "");

			// parametro birthdate
			$sWhere.=$this->getWhereClause("birthdate", "birthdate", "ic_employee", $this->DATE_TYPE, "", "");


			if ($sType=="ADMINISTRATION")
			{
				$sWhere.="   AND ic_employee.company_id = " . $sID ;
			}


			$query_select = "SELECT COUNT(*) " . $sFrom . $sWhere;

			//echo $query_select;

			if ($result = $mysqli->query($query_select)){
				$row = $result->fetch_array();
				$this->init($row[0]);
				$result->close();
			}else{
				$this->init(0);
			}

			$query_select="SELECT ic_employee.employee_id, ".
						"       ic_employee.employee_first_name, ".
						"       ic_employee.employee_last_name1, ".
						"       ic_employee.id_card, ".
						"       ic_employee.internal_id, ".
						"       DATE_FORMAT(ic_employee.birthdate,'%%Y/%%m/%%d') as birthdate, 	".
						"       ic_employee.user_id ";
			$query_select.= $sFrom . $sWhere;
			$query_select.= " ORDER BY " . $this->getOrderByColumn() . " " . $this->getOrderHowString() . " ";
			$query_select.= " LIMIT " . $this->getMaxRowsNumber() . " OFFSET " . ($this->getCurrentPage()-1)*$this->getMaxRowsNumber();

			//echo $query_select;

			$result = $mysqli->query($query_select);

			return $result;

		}
		catch (Exception $ex)
		{

		}



   }//end loadFilter



}

?>