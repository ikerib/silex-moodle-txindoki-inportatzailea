<?php
// -----------------------------------------
// User_class.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/database/DB_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/admin/UserData.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/admin/Employee.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/Functions.php');

class User extends UserData
{

	public function User($userId){
		$this->setUserID($userId);
	}

	public function loadData (){

		global $mysqli;
		global $errorLog;

		try {
			$query_select= "SELECT ic_employee.employee_id, ".
						"       ic_employee.employee_first_name, ".
						"       COALESCE(ic_employee.employee_last_name1,'') as employee_last_name1, ".
						"       ic_employee.company_id, ".
						"       ic_user.language, ".
						"       ic_user.e_mail ".
						"  FROM ic_user LEFT JOIN ic_employee ON ".
						"       ic_employee.user_id = ic_user.user_id AND ic_employee.active_flag = 1 ".
						" WHERE ic_user.user_id = '".$this->getUserID()."' ";

			$errorLog->LogDebug("SELECT: $query_select");

			if ($result = $mysqli->query($query_select)){

				if ($result->num_rows > 0){
					$row = $result->fetch_array();

					$this->setEmail($row["e_mail"]);
					$this->setLanguage($row["language"]);
					$this->setCompanyID($row["company_id"]);
					$this->setEmployeeID($row["employee_id"]);
					$this->setEmployeeName($row["employee_first_name"]." ".$row["employee_last_name1"]);

					$result->close();

				}else{
					//header("Location: /index.php?error=3111");
					//exit;
				}

			}

		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}

	}//end loadData



	public function createNew()
	{

		global $mysqli;

		try {
			$query_insert=sprintf("INSERT INTO ic_user
								    (user_id,
									password,
									language,
									e_mail,
									active_flag,
									modified_by,
									timestamp)
								 VALUES (
								  '%s',
								  '%s',
								  '%s',
								  '%s',
								  '%d',
								  '%s',
								  CURRENT_TIMESTAMP
								  )",
									$this->getUserID(),
									$this->getPassword(),
									$this->getLanguage(),
									$this->getEmail(),
									1,
									$_SESSION["user_id"]
								);

			$errorLog->LogDebug("INSERT: $query_insert");

			$result = $mysqli->query($query_insert);

			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}



		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}

	}//END create_new



	public function update()
	{
		global $errorLog;
		global $mysqli;

		try {
			$query_update = "UPDATE ic_user SET ";
			if ($this->getPassword()!=""){
				$query_update .= "	password = '".$this->getPassword()."', ";
			}
			$query_update .= " language = '".$this->getLanguage()."', ".
							" e_mail = '".$this->getEmail()."', ".
							" modified_by = '".$_SESSION["user_id"]."', ".
							" timestamp =  CURRENT_TIMESTAMP ".
							" WHERE user_id='".$this->getUserID()."'";

			$errorLog->LogDebug("SELECT: $query_select");

			$result = $mysqli->query($query_update);

			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}


		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}

	}//END UPDATE


   public function updatePassword()
	{
		global $mysqli;
		global $errorLog;

		try {
			$query_update = "UPDATE ic_user SET ";
			if ($this->getPassword()!=""){
				$query_update .= "	password = '".md5($this->getPassword())."', ";
			}
			$query_update .= " modified_by = '".$_SESSION["user_id"]."', ".
							" timestamp = CURRENT_TIMESTAMP ".
							" WHERE user_id='".$this->getUserID()."'";

			$errorLog->LogDebug("UPDATE: $query_update");

			$result = $mysqli->query($query_update);

			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}


		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}

	}//END updatePassword


	public function delete()
	{
		global $mysqli;
		global $errorLog;

		try {

			$query_delete=sprintf("UPDATE ic_user
								SET active_flag=0,
									modified_by='%s',
									timestamp=CURRENT_TIMESTAMP
							WHERE
									user_id_id = %s",
									$_SESSION["user_id"],
									$this->getUserID()
    							);

			$errorLog->LogDebug("DELETE: $query_delete");

			$result = $mysqli->query($query_delete);

			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}

		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}


	}//END UPDATE


	public function init()
	{
		$error=false;
		$error_txt="";

		$this->setEmployeeID($_POST["employee_id"]);
		$this->setEmployeeName($_POST["employee_name"]);

		if (isset($_POST["npass"]) && $_POST["npass"]!="" && isset($_POST["cpass"]) && $_POST["cpass"]!=""){
			if ($_POST["npass"]==$_POST["cpass"]){
				$this->setPassword($_POST["npass"]);
			}else{
				$error_txt.="\\n".MSG_USERDATA_TEXT01;
			}
		}else{
			$error_txt.="\\n".MSG_USERDATA_TEXT02;
		}

		return $error_txt;

	}//END init


	public function exists()
	{
		global $mysqli;
		global $errorLog;

		$bResult = false;

		try {
			$query_select = sprintf("SELECT user_id FROM ic_user WHERE user_id='%s' AND active_flag=1", $this->getUserID());

			$errorLog->LogDebug("SELECT: $query_select");

			if ($result = $mysqli->query($query_select))
			{
				if ($result->num_rows>0)
					$bResult = true;

				$result->close();
			}
		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}
		return $bResult;
    }














   function updateChannelMain($table, $id, $user_email){

		global $mysqli;
		global $errorLog;

		 $object_type_id = "0";
		 switch ($table) {
			case "employee":
				$object_type_id = "7";
				break;
			case "student":
				$object_type_id = "302";
				break;

            default: $object_type_id = "0";
         }

		try {
			$query_select= sprintf("SELECT email_id, email_name
										FROM ic_email

									WHERE object_type_id = %d
									AND object_id = %d
									AND main_flag = 1
									AND active_flag = 1
									", $object_type_id, $id);

			$errorLog->LogDebug("SELECT: $query_select");

			$email_id=0;

			if ($result = $mysqli->query($query_select)){
				$row = $result->fetch_array();

				$email_id=$row["email_id"];

				$result->close();
			}


			if ($email_id==0){ //Creo uno nuevo.

					//Cargo los datos del Student o del employee.
					$obj = null;
					$aux_company_id="";
					$aux_name="";
					switch ($table) {
						case "employee":
							$obj = new Employee();
							$obj->loadData($id);
							$aux_company_id=$obj->getCompanyID();
							$aux_name=$obj->getName()." ".$obj->getPersonLastName1();
							break;
						case "student":
							$obj = new Student();
							$obj->loadData($id);
							$aux_company_id=$obj->getCompanyID();
							$aux_name=$obj->getName()." ".$obj->getPersonLastName1();
							break;

						default: $object_type_id = "0";
					 }

					$query_insert=sprintf("INSERT INTO ic_email (email_name,
																object_type_id,
																object_id,

																main_flag,

																company_id,

																remarks,
																active_flag,
																timestamp,
																modified_by,
																object_name)

																 VALUES (
																  '%s',
																  %d,
																  %d,

																  1,

																  %d,

																  '%s',
																  %d,
																  CURRENT_TIMESTAMP,
																  '%s',
																  '%s'
																  )",
																	$user_email,
																	$object_type_id,
																	$id,

																	$aux_company_id,

																	"",
																	1,
																	$aux_name,
																	$aux_name
																);

					$errorLog->LogDebug("INSERT: $query_insert");

					$result = $mysqli->query($query_insert);

					if ($mysqli->error){
						$errorLog->LogError($mysqli->error);
					}


			}else{ //Actualizo el existente.

				$query_update = sprintf("UPDATE ic_email SET
							email_name = '%s',
							timestamp = CURRENT_TIMESTAMP
							WHERE email_id='%s'", $user_email, $id);

				$errorLog->LogDebug("UPDATE: $query_update");

				$result = $mysqli->query($query_update);

				if ($mysqli->error){
					$errorLog->LogError($mysqli->error);
				}


			}


		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}


   }//updateChannelMain


   function updateEmail($object_type_id, $id, $email)
   {
		global $mysqli;
		global $errorLog;

		try {
			$query_update = sprintf("
						UPDATE ic_email SET
							email_name = '%s',
							timestamp = CURRENT_TIMESTAMP
						WHERE object_type_id=%d
							AND object_id=%d", $email, $object_type_id, $id);

			$errorLog->LogDebug("UPDATE: $query_update");

			$result = $mysqli->query($query_update);

			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}

} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}


   }//updateEmail









}

?>