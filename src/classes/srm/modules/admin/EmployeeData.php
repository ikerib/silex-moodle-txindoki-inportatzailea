<?php
// -----------------------------------------
// EmployeeData_class.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/AbstractCRMPersonData.php');

class EmployeeData extends AbstractCRMPersonData
{

	public $moDateStart = "";
	public $moDateEnd = "";
	public $oAssignedRoles = "";
	public $oNoAssignedRoles = "";
	public $isUser;
	public $userID;
	public $userLang;
	public $userEmail;

	public $msDepartment;

	public function getDateStart(){
		return $this->moDateStart;
	}

	public function setDateStart($value){
		$this->moDateStart = $value;
	}


	public function getDateEnd(){
		return $this->moDateEnd;
	}

	public function setDateEnd($value){
		$this->moDateEnd = $value;
	}


	public function getAssignedRoles(){
		return $this->oAssignedRoles;
	}

	public function setAssignedRoles($value){
		$this->oAssignedRoles = $value;
	}

	public function getNoAssignedRoles(){
		return $this->oNoAssignedRoles;
	}

	public function setNoAssignedRoles($value){
		$this->oNoAssignedRoles = $value;
	}

	public function getIsUser(){
		return $this->isUser;
	}

	public function setIsUser($value){
		$this->isUser = $value;
	}

	public function getUserID(){
		return $this->userID;
	}

	public function setUserID($value){
		$this->userID = $value;
	}

	public function getUserLang(){
		return $this->userLang;
	}

	public function setUserLang($value){
		$this->userLang = $value;
	}

	public function getUserEmail(){
		return $this->userEmail;
	}

	public function setUserEmail($value){
		$this->userEmail = $value;
	}

	public function getDepartment(){
		return $this->msDepartment;
	}
	public function setDepartment($nValue){
		$this->msDepartment = $nValue;
	}


}

?>