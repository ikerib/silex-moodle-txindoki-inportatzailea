<?php
// -----------------------------------------
// UserData_class.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/AbstractCRMObject.php');

class UserData extends AbstractCRMObject
{

	public $msUserID = "";
	public $msPassword = "";
	public $msLanguage = "";
	public $msEmail;
	public $mnEmployeeID;
	public $msEmployeeName = "";

	public function getUserID(){
		return $this->msUserID;
	}

	public function setUserID($value){
		$this->msUserID = $value;
	}

	public function getPassword(){
		return $this->msPassword;
	}

	public function setPassword($value){
		$this->msPassword = $value;
	}

	public function getLanguage(){
		return $this->msLanguage;
	}

	public function setLanguage($value){
		$this->msLanguage = $value;
	}

	public function getEmail(){
		return $this->msEmail;
	}

	public function setEmail($value){
		$this->msEmail = $value;
	}

	public function getEmployeeID(){
		return $this->mnEmployeeID;
	}

	public function setEmployeeID($value){
		$this->mnEmployeeID = $value;
	}

	public function getEmployeeName(){
		return $this->msEmployeeName;
	}

	public function setEmployeeName($value){
		$this->msEmployeeName = $value;
	}

}

?>