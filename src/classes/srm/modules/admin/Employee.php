<?php
// -----------------------------------------
// Employee_class.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/database/DB_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/admin/EmployeeData.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/admin/User.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/Authentication.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/Functions.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/config_app.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/admin/Attachment.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/admin/Role.php');

class Employee extends EmployeeData
{
	public $saveUserID = false;

	public function getSaveUserID(){
		return $this->saveUserID;
	}

	public function setSaveUserID($value){
		$this->saveUserID = $value;
	}

	public function loadData ($id){

		if (null == $id || "new"==$id)
		{

            $this->setID("");
            $this->setName("");

			$this->setCompanyID("1");

			$this->setCompanyName("");
            $this->setUserID("");
            $this->setPersonLastName1("");
            $this->setPersonLastName2("");
            $this->setSiteID("");
            $this->setSiteName("");
            $this->setInternalID("");
            $this->setNationalIDType("");
            $this->setCardID("");
            $this->setDateStart("");
            $this->setDateEnd("");
            $this->setSex("");
            $this->setBirthDate("");
            $this->setBirthPlace("");
            $this->setBirthRegion("");
            $this->setBirthCountryCode("");
            $this->setNationalityCode("");
            $this->setMotherTongue("");
            $this->setPictureFile("");
            $this->setPictureName("");
            $this->setPictureWidth("");
            $this->setPictureHeight("");

			$this->setDepartment("");

            $this->setRemarks("");
            $this->setActiveFlag("");
			$this->setModifiedBy("");
			$this->setTimestamp("");
			$this->setSaveUserID(false);

			return;
		}

		global $mysqli;
		global $errorLog;
		$format = new Format();

		try {
			$query_select= sprintf("SELECT ic_employee.employee_id,
										ic_employee.company_id,
										ic_company.company_name,
										ic_employee.employee_first_name,
										ic_employee.employee_last_name1,
										ic_employee.employee_last_name2,
										ic_employee.user_id,
										ic_employee.site_id,
										ic_employee.internal_id,
										ic_employee.id_card,
										DATE_FORMAT(ic_employee.date_start,'%%Y/%%m/%%d') as date_start,
										DATE_FORMAT(ic_employee.date_end,'%%Y/%%m/%%d') as date_end,
										ic_employee.sex,
										DATE_FORMAT(ic_employee.birthdate,'%%Y/%%m/%%d') as birthdate,
										ic_employee.birthplace,
										ic_employee.birthregion,
										ic_employee.birthcountry_code,
										ic_employee.nationality_code,
										ic_employee.mothertongue,
										ic_employee.remarks,
										ic_employee.active_flag,
										ic_employee.timestamp,
										ic_employee.modified_by,
										ic_employee.employee_first_name||' '||COALESCE(ic_employee.employee_last_name1, ''),
										ic_employee.national_id_type_id,
										ic_user.language,
										ic_user.e_mail,
										ic_employee.department
								FROM ic_employee LEFT JOIN ic_company ON ic_employee.company_id = ic_company.company_id
												 LEFT JOIN ic_user ON ic_employee.user_id = ic_user.user_id
                          		WHERE ic_employee.employee_id = %d ", $id);

			$errorLog->LogDebug("SELECT: $query_select");

			if ($result = $mysqli->query($query_select)){

				if ($result->num_rows > 0){
					$row = $result->fetch_array();

					$this->setID($row["employee_id"]);
					$this->setCompanyID($row["company_id"]);
					$this->setCompanyName($row["company_name"]);

					$this->setName($row["employee_first_name"]);
					$this->setPersonLastName1($row["employee_last_name1"]);
					$this->setPersonLastName2($row["employee_last_name2"]);
					$this->setUserID($row["user_id"]);
					if ($this->getUserID()!=null && $this->getUserID()!="")
						$this->setIsUser(true);
					else
						$this->setIsUser(false);
					$this->setSiteID($row["site_id"]);
					//$this->setSiteName($row["site_name"]);
					$this->setInternalID($row["internal_id"]);
					$this->setCardID($row["id_card"]);
					$this->setDateStart($format->formatea_fecha($row["date_start"]));
					$this->setDateEnd($format->formatea_fecha($row["date_end"]));
					$this->setSex($row["sex"]);
					$this->setBirthDate($format->formatea_fecha($row["birthdate"]));
					$this->setBirthPlace($row["birthplace"]);
					$this->setBirthRegion($row["birthregion"]);
					$this->setBirthCountryCode($row["birthcountry_code"]);
					$this->setNationalityCode($row["nationality_code"]);
					$this->setMotherTongue($row["mothertongue"]);
					$this->setRemarks($row["remarks"]);
					$this->setActiveFlag($row["active_flag"]);
					$this->setTimestamp($row["timestamp"]);
					$this->setModifiedBy($row["modified_by"]);
					$this->setNationalIDType($row["national_id_type_id"]);

					$this->setUserLang($row["language"]);
					$this->setUserEmail($row["e_mail"]);

					$this->setDepartment($row["department"]);

					$result->close();

					$this->setAssignedRoles($this->loadRoles());
					$this->setNoAssignedRoles($this->loadPossibleRoles());
					$this->setSaveUserID(false);

				}else{
					$errorLog->LogError("Location: /srm/index.php?error=3. ERROR: ".$query_select);
					header("Location: /index.php?error=3");
					exit;
				}

				$this->controlAccessObject($_SESSION["company_id"], $this->getCompanyID());

			}

		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}

		$object_type_id=7; //Employee
		$this->loadMainChannels($object_type_id , $id);

	}//end loadData



	public function createNew()
	{
		global $errorLog;
		global $mysqli;
		$format = new Format();

		try {

			$query_insert=sprintf("INSERT INTO ic_employee
								    (
										company_id,
										employee_first_name,
										employee_last_name1,
										employee_last_name2,
										user_id,

										internal_id,
										department,

										remarks,
										active_flag,
										modified_by,
										timestamp)
								  VALUES (
								   %d,

								  '%s',
								  '%s',
								  '%s',
								  '%s',

								  '%s',
								  '%s',

								  '%s',
								  '%s',
								  '%s',
								  CURRENT_TIMESTAMP
								  )",

									$this->getCompanyID(),

									$this->getName(),
									$this->getPersonLastName1(),
									$this->getPersonLastName2(),
									$this->getUserID(),

									$this->getInternalID(),
									$this->getDepartment(),

									$this->getRemarks(),
									$this->getActiveFlag(),
									$_SESSION["user_id"]
								);

			$errorLog->LogDebug("INSERT: $query_insert");

			$result = $mysqli->query($query_insert);

			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}

			//Calculo el último ID insertado.
			$query_max = "SELECT LAST_INSERT_ID() AS max_id";
			if( $result = $mysqli->query($query_max) ){
				$row = $result->fetch_array();

				$this->setID($row["max_id"]);

				$result->close();
			}

			//Crea el registro en ic_user.
			$this->updateUserID();

		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}

	}//END create_new



	public function update($id)
	{
		global $mysqli;
		$format = new Format();

		try {

			$query_update=sprintf("UPDATE ic_employee
								SET company_id = %d,

									employee_first_name = '%s',
									employee_last_name1 = '%s',
									employee_last_name2 = '%s',

									internal_id='%s',
									department = '%s',

									remarks = '%s',
									modified_by = '%s',
									timestamp = CURRENT_TIMESTAMP
								WHERE
									employee_id = %d",

									$this->getCompanyID(),

									$this->getName(),
									$this->getPersonLastName1(),
									$this->getPersonLastName2(),

									$this->getInternalID(),
									$this->getDepartment(),

									$this->getRemarks(),
									$_SESSION["user_id"],
									$id
    							);

			$this->controlAccessObject($_SESSION["company_id"], $this->getCompanyID());

			$result = $mysqli->query($query_update);

			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}

			//Crea el registro en ic_user.
			$this->updateUserID();

		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}

	}//END UPDATE

	public function delete($id)
	{

		global $mysqli;

		try {

			$query_delete=sprintf("UPDATE ic_employee
								SET active_flag=0,
									modified_by='%s',
									timestamp=CURRENT_TIMESTAMP
							WHERE
									employee_id = %d",
									$_SESSION["user_id"],
									$id
								);

			if ($_SESSION["company_id"]==1){ //Puede dar de baja cualquier TC.

			}else if ($_SESSION["company_id"]!=1){ //Solo puede dar de baja a sus TC.
				$query_delete.=sprintf("  AND company_id = %d", $_SESSION["company_id"]);
			}

			//echo "*".$query_delete."*";
			$result = $mysqli->query($query_delete);

			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}

			//Al borrar el empleado borrar otro objetos:

			//Attachment relacionados
			$objAttachment = new Attachment();
			$objAttachment->deleteRelated(7,$id);

		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}


	}//END UPDATE

   public function init()
   {
		$error=false;
		$error_txt="";
		$valor = "";


		$this->setCompanyID($_POST["company_id"]);

		//Param name
		$valor = $_POST['name'];
		if (trim($valor)!=""){
			$this->setName($valor);
		}else{
			$error=true;
			$error_txt.="\\n".MSG_ERROR_TEXT_02;
			$this->setName("");
		}


		//user_name
		//Miramos si tiene creado un usuario que no exista.
		if (isset($_POST["user_valido"]) && $_POST["user_valido"]=="NO EXISTE"){

			$this->setUserID($_POST["user_name"]);

		}else if (isset($_POST["user_valido"]) && $_POST["user_valido"]=="EXISTE"){
			if (isset($_POST["user_name"]) && $_POST["user_name"]!=""){
			$this->setUserID("");

			if ($this->existsUserID($_POST["user_valido"])){
				$error_txt.="\\n".MSG_USERDATA_TEXT03;
			}
			}else{
				$error_txt.="\\n".MSG_USERDATA_TEXT04;
			}
		}

		//user_email
		if (isset($_POST["user_email"]) && $_POST["user_email"]!=""){
			$this->setUserEmail($_POST["user_email"]);
		}else{
			$this->setUserEmail("");
			$error_txt.="\\n".MSG_EMAIL_TEXT01;
		}



		//npass y cpass
		//Si vienen ambos campos y ambos son iguales, cambiarle la contraseña también.

		if (isset($_POST["id"]) && $_POST["id"]!="new"){
			if (isset($_POST["npass"]) && $_POST["npass"]!="" && isset($_POST["cpass"]) && $_POST["cpass"]!=""){

				if ($_POST["npass"]==$_POST["cpass"]){

					//Todo OK, podemos cambiar la contraseña.
				}else{
					$error_txt.="\\n".MSG_USERDATA_TEXT01; //La nueva contraseña y su confirmación deben ser iguales.
				}


			}else{
				if ($_POST["npass"]!="" || $_POST["cpass"]!="")
					$error_txt.="\\n".MSG_USERDATA_TEXT02; //Debe escribir la nueva contraseña y su confirmación
			}
		}


        $this->setPersonLastName1($_POST["last_name_1"]);

        $this->setPersonLastName2($_POST["last_name_2"]);

        $this->setSiteID(1);

        $this->setSiteName("");

        $this->setInternalID($_POST["internal_id"]);

        $this->setNationalIDType($_POST["national_id_type"]);

        $this->setCardID($_POST["id_card"]);

        $this->setDateStart($_POST["date_start"]);

        $this->setDateEnd($_POST["date_end"]);

        $this->setSex($_POST["sex"]);

        $this->setBirthDate($_POST["birthdate"]);

        $this->setBirthPlace($_POST["birthplace"]);

        $this->setBirthRegion($_POST["birthregion"]);

        $this->setBirthCountryCode($_POST["birthcountry"]);

        $this->setNationalityCode($_POST["nationality"]);

        $this->setMotherTongue($_POST["mother_tongue"]);

		$this->setDepartment($_POST["department"]);

        $this->setRemarks($_POST["remarks"]);

		$this->setModifiedBy($_SESSION['user_id']);

		return $error_txt;

   }//END init


    private function existsUserID($sUserID)
    {
		global $mysqli;

        $bExists = false;

        if (!isset($sUserID))
            return false;

		$query_select = "";
		$query_select = sprintf("SELECT * FROM ic_user WHERE UPPER(ic_user.user_id) = UPPER('%s')", $sUserID);

		if ($result = $mysqli->query($query_select)){

			if ($result->num_rows>0){
				$bExists = true;
			}
			$result->close();
		}

        return $bExists;
    }

	//Control del acceso de otras company a ver datos.
	function controlAccessObject($actual_company_id, $view_object_id)
	{
		if ($actual_company_id==1){ //La company 1 puede hacer todo en este objeto.

		}else if ($actual_company_id==$view_object_id){ //La propia entidad puede ver sus datos.

		}else if ($actual_company_id!=$view_object_id){ //Si la entidad logada no es la principal solo ve sus datos.
			header("Location: http://".$_SERVER['HTTP_HOST']."/index.php?error=3");
			exit;
		}

	}//End controlAccessObject





	//Función que crea o modifica el registro en ic_user.

	public function updateUserID()
	{
		global $mysqli;
		global $errorLog;

		$user_id = $_POST["user_name"];
		$language = "es";
		$user_email = $_POST["user_email"];


		try {

			//Si no existe creamos nuevo
			if (!$this->existsUserID($user_id)){

				$mypass=$this->genRandomPassword();


					$query_insert=sprintf("INSERT INTO ic_user
											(user_id,
											password,
											language,
											e_mail,
											active_flag,
											modified_by,
											timestamp)
										 VALUES (
										  '%s',
										  '%s',
										  '%s',
										  '%s',
										  '%d',
										  '%s',
										  CURRENT_TIMESTAMP
										  )",
											$user_id,
											$this->encodePassword($mypass),
											$language,
											$user_email,
											1,
											$_SESSION["user_id"]
										);

					$errorLog->LogDebug("INSERT: $query_insert");

					$result = $mysqli->query($query_insert);

					if ($mysqli->error){
						$errorLog->LogError($mysqli->error);
					}

					//ENVIO POR CORREO DEL USUARIO.
					$mail = new Mail();

					$to=$user_email;
					$from=CFG_EMAIL_FROM_DEFAULT;
					$subject=MSG_SUBJECT_USER_PASSWORD;

					$contents=MSG_NEW_USER_TEXT_01;
					$contents.="\n\n".LBL_USER_LOGIN.": ". $user_id;
					$contents.="\n".LBL_PASSWORD.": ". $mypass;
					$contents.="".MSG_USER_SIGNATURE;

					////send_mail($to, $from, $asunto, $contenido, $cc, $bcc)
					if ($subject!="" && $to!="" && $from!=""){
						$ok = $mail->send_mail($to, $from, $subject, $contents , "", "");
					}

				}else{ //Si existe lo modificamos el correo por si hubiera cambiado.

					$newPass="";

					if (isset($_POST["npass"]) && $_POST["npass"]!="" && isset($_POST["cpass"]) && $_POST["cpass"]!=""){
						if ($_POST["npass"]==$_POST["cpass"]){

							$newPass=$_POST["npass"];
						}
					}

					if ($newPass==""){
						$query_update=sprintf("UPDATE ic_user
												SET
													e_mail='%s',
													timestamp=timestamp
												WHERE user_id = '%s'
											 ",
												$user_email,
												$user_id
											);

						$errorLog->LogDebug("UPDATE: $query_update");

						$result = $mysqli->query($query_update);

						if ($mysqli->error){
							$errorLog->LogError($mysqli->error);
						}
					}else if ($newPass!=""){
						$query_update=sprintf("UPDATE ic_user
												SET
													e_mail='%s',
													password='%s',
													timestamp=timestamp
												WHERE user_id = '%s'
											 ",
												$user_email,
												$this->encodePassword($newPass),
												$user_id
											);

						$errorLog->LogDebug("UPDATE: $query_update");

						$result = $mysqli->query($query_update);

						if ($mysqli->error){
							$errorLog->LogError($mysqli->error);
						}

						//ENVIO POR CORREO DEL USUARIO.
						$mail = new Mail();

						$to=$user_email;
						$from=CFG_EMAIL_FROM_DEFAULT;
						$subject=MSG_SUBJECT_USER_PASSWORD;


						$contents=MSG_NEW_USER_TEXT_01;
						$contents.="\n\n".LBL_USER_LOGIN.": ". $user_id;
						$contents.="\n".LBL_PASSWORD.": ". $newPass;
						$contents.="".MSG_USER_SIGNATURE;
						////send_mail($to, $from, $asunto, $contenido, $cc, $bcc)
						if ($subject!="" && $to!="" && $from!=""){
							$ok = $mail->send_mail($to, $from, $subject, $contents , "", "");
						}

					}

				}

		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}

	}//END createNewUserID


	function encodePassword($pass)
	{
		return md5($pass);
	}

	function genRandomPassword()
	{
		$str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890()&$+-{}*";
		$cad = "";
		for($i=0;$i<=12;$i++) {
			$cad .= substr($str,rand(0,62),1);
		}
		return $cad;
	}


	private function loadRoles()
	{

		global $mysqli;

        $query_select = "SELECT ica_role.role_id, ".
                        "       ica_role.role_name ".
                        "  FROM ica_role, ica_role_employee ".
                        " WHERE ica_role.role_id = ica_role_employee.role_id ".
                        "   AND ica_role_employee.employee_id = ".$this->getID().
                        "   AND ica_role_employee.active_flag = 1 ".
                        "   AND ica_role.active_flag = 1 ".
                        "   AND ica_role.company_id = 1";


        $result = $mysqli->query($query_select);
		return $result;

	}//loadRoles

	private function loadPossibleRoles()
	{

		global $mysqli;

		$query_select = "";

		if ($_SESSION["company_id"]==1){

			$query_select = "SELECT DISTINCT (ica_role.role_id), ".
							"       ica_role.role_name ".
							"  FROM ica_role LEFT JOIN ica_role_employee ".
							"       ON (ica_role.role_id = ica_role_employee.role_id) ".
							" WHERE ica_role.role_id NOT IN ( ".
							"          SELECT ica_role.role_id  ".
							"            FROM ica_role, ica_role_employee ".
							"           WHERE ica_role.role_id = ica_role_employee.role_id ".
							"             AND ica_role_employee.employee_id = ".$this->getID().
							"             AND ica_role_employee.active_flag = 1 ".
							"             AND ica_role.company_id = 1".
							"             AND ica_role.active_flag = 1 ) ".
							"   AND ica_role.active_flag = 1 ".
							"   AND ica_role.company_id = 1";
		}else{
			$query_select = "SELECT DISTINCT (ica_role.role_id), ".
							"       ica_role.role_name ".
							"  FROM ica_role LEFT JOIN ica_role_employee ".
							"       ON (ica_role.role_id = ica_role_employee.role_id) ".
							" WHERE ica_role.role_id NOT IN ( ".
							"          SELECT ica_role.role_id  ".
							"            FROM ica_role, ica_role_employee ".
							"           WHERE ica_role.role_id = ica_role_employee.role_id ".
							"             AND ica_role_employee.employee_id = ".$this->getID().
							"             AND ica_role_employee.active_flag = 1 ".
							"             AND ica_role.company_id = 1".
							"             AND ica_role.active_flag = 1 ) ".
							"   AND ica_role.active_flag = 1 ".
							"   AND ica_role.company_id = 1".
							" 	AND ica_role.role_id <> 1";
		}


        $result = $mysqli->query($query_select);
		return $result;

	}

	public function saveRoles($id)
	{
		global $mysqli;

		try {
			// Se borran todos primero para luego guardar los selccionados
			$query_delete=sprintf("DELETE from ica_role_employee WHERE employee_id=%d", $id);

			$result = $mysqli->query($query_delete);
			$selectedResources = $_POST["assig_roles"];

			if ($selectedResources!=""){
				$tok = strtok ($selectedResources,", ");
				while ($tok !== false) {
					$query_insert = "INSERT INTO ica_role_employee (role_id, employee_id, active_flag, timestamp, modified_by ) ".
								" VALUES ( ".$tok.", ".$id.", 1, CURRENT_TIMESTAMP, '".$_SESSION["user_id"]."') ";
					$result = $mysqli->query($query_insert);

					$tok = strtok(", ");
				}
			}

		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}

	}










}

?>