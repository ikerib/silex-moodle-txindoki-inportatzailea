<?php
// -----------------------------------------
// Company.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/database/DB_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/admin/CompanyData.php');
require_once($_SERVER['DOCUMENT_ROOT']."/../src/config_app.php");

class Company extends CompanyData
{

	public function loadData ($id){

	  $nCompanyID = $_SESSION["company_id"];
	  $sLanguage = $_SESSION["language"];

	  if (null == $id || "new"==$id)
      {
			$this->setID("new");
			$this->setName("");
			$this->setDescription("");
			$this->setRegistrationNumber("");
			$this->setUrl("");
			$this->setEmailDomain("");
			$this->setCenterID("");
			$this->setCenterName("");

			$this->setRemarks("");
			$this->setActiveFlag("");
			$this->setModifiedBy($_SESSION["user_id"]);
			$this->setTimestamp("");
			return;
      }

      global $mysqli;
	  global $errorLog;

	  try {
		$query_select= sprintf("SELECT company_id,
                     	company_name,
						company_desc,
						registration_number,
						url,
						e_mail_domain,
						remarks,
						center_id,
						active_flag,
						modified_by,
						timestamp
                 FROM ic_company
                 WHERE  active_flag=1 AND
						company_id = %d", $id);

		//echo "<br/>".$query_select;

		 if ($result = $mysqli->query($query_select)){

				if ($result->num_rows > 0){
					$row = $result->fetch_array();

					$this->setID($row["company_id"]);
					$this->setName($row["company_name"]);
					$this->setDescription($row["company_desc"]);
					$this->setRegistrationNumber($row["registration_number"]);
					$this->setUrl($row["url"]);
					$this->setEmailDomain($row["e_mail_domain"]);
					$this->setCenterID($row["center_id"]);

					$this->setRemarks($row["remarks"]);
					$this->setActiveFlag($row["active_flag"]);
					$this->setModifiedBy($row["modified_by"]);
					$this->setTimestamp($row["timestamp"]);

					$result->close();

				}else{
					$errorLog->LogError("Location: /srm/index.php?error=3. ERROR: ".$query_select);
					header("Location: /index.php?error=3");
					exit;
				}

		 }

	  } catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
	  }

	  $object_type_id=19; //Company
  	  $this->loadMainChannels($object_type_id , $id);

	}//end loadData



   public function createNew()
   {

	  global $mysqli;
	  global $errorLog;

      try {

			$query_insert=sprintf("INSERT INTO ic_company
								    (company_name,
									company_desc,
									registration_number,
									url,
									e_mail_domain,

									center_id,

									active_flag,
									remarks,
									modified_by,
									timestamp)
								  VALUES (
								  '%s',
								  '%s',
								  '%s',
								  '%s',
								  '%s',

								  %d,

								  1,
								  '%s',
								  '%s',
								  CURRENT_TIMESTAMP
								  )",

									$this->getName(),
									$this->getDescription(),
									$this->getRegistrationNumber(),
									$this->getUrl(),
									$this->getEmailDomain(),

									$this->getCenterID(),

									$this->getRemarks(),
									$this->getModifiedBy()
								);

			//echo $query_insert.
			$result = $mysqli->query($query_insert);

			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}

			//Calculo el último ID insertado.
			$query_max = "SELECT LAST_INSERT_ID() AS max_id";
			if( $result = $mysqli->query($query_max) ){
				$row = $result->fetch_array();

				$this->setID($row["max_id"]);

				$result->close();
			}


			/* //Creo la ruta de carpetas.
			$old = umask(0); // tomas el valor actual ...

			Global $path_contents_secure_absolute;

			$ruta_company=$path_contents_secure_absolute."company".$this->getID()."/";
			mkdir($ruta_company, 2755);
			chmod($ruta_company, 2755);

			mkdir($ruta_company."files/", 2755);
			chmod($ruta_company."files/", 2755);
			mkdir($ruta_company."files/attachment/", 2755);
			chmod($ruta_company."files/attachment/", 2755);
			mkdir($ruta_company."files/student/", 2755);
			chmod($ruta_company."files/student/", 2755);
			mkdir($ruta_company."files/tc/", 2755);
			chmod($ruta_company."files/tc/", 2755);

			mkdir($ruta_company."images/", 2755);
			chmod($ruta_company."images/", 2755);
			mkdir($ruta_company."images/picture/", 2755);
			chmod($ruta_company."images/picture/", 2755);

			umask($old); // y lo restauras ...  */



		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}

   }//END create_new


   public function update($id)
   {
	  global $mysqli;
	  global $errorLog;

      try {

			$query_update=sprintf("UPDATE ic_company
								SET company_name='%s',
									company_desc='%s',
									registration_number='%s',
									url='%s',
									e_mail_domain='%s',
									center_id=%d,
									remarks='%s',
									modified_by='%s',
									timestamp=CURRENT_TIMESTAMP
							WHERE
									company_id = %d",

									$this->getName(),
									$this->getDescription(),
									$this->getRegistrationNumber(),
									$this->getUrl(),
									$this->getEmailDomain(),
									$this->getCenterID(),
									$this->getRemarks(),
									$this->getModifiedBy(),
									$id
    							);

			$this->controlAccessObject($_SESSION["company_id"], $id);

			//echo "*".$query_update."*";
			$result = $mysqli->query($query_update);

			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}


		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}

   }//END UPDATE



   public function delete($id)
   {
	 //No podremos borrar ninguna company con vital_flag=1

     global $mysqli;
	 global $errorLog;

      try {

			$query_delete=sprintf("UPDATE ic_company
								SET active_flag=0,
									modified_by='%s',
									timestamp=CURRENT_TIMESTAMP
							WHERE
									vital_flag=0 AND
									company_id = %d",

									$_SESSION["user_id"],
									$_POST['id']
    							);

			$this->controlAccessObject($_SESSION["company_id"], $_POST['id']);

			//echo "*".$query_delete."*";
			$result = $mysqli->query($query_delete);

			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}

		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}


   }//END UPDATE






   public function init()
   {
		$error=false;
		$error_txt="";
		$valor = "";


		//Param id
		$valor = $_POST['id'];
		if (trim($valor)!=""){
			$this->setID($valor);
		}else{
			$this->setID("new");
		}

		//Param center_id
		$this->setCenterID($_POST['center_id']);

		//Param name
		$valor = $_POST['name'];
		if (trim($valor)!=""){
			$this->setName($valor);
		}else{
			$error=true;
			$error_txt.="\\n".MSG_ERROR_TEXT_01;
			$this->setName("");
		}

		//Param description
		$this->setDescription($_POST['center_desc']);

		$this->setRegistrationNumber($_POST['registration_number']);
		$this->setUrl($_POST['url']);
		$this->setEmailDomain($_POST['e_mail_domain']);




		$this->setRemarks($_POST['remarks']);
		$this->setModifiedBy($_SESSION["user_id"]);

		return $error_txt;

   }//END init






   	private function loadCountries()
	{

		global $mysqli;
		$sLanguage = $_SESSION["language"];

        $query_select = "SELECT ic_country.country_code, ".
                        "       ic_country.country_name ".
                        "  FROM ic_country, ic_company_country ".
                        " WHERE ic_country.country_code = ic_company_country.country_code ".
						"	 AND ic_country.language='".$sLanguage."'".
                        "   AND ic_company_country.company_id = ".$this->getID();

		//echo "<br/>".$query_select;

        $result = $mysqli->query($query_select);
		return $result;

	}

	private function loadPossibleCountries()
	{

		global $mysqli;
		global $errorLog;
		$sLanguage = $_SESSION["language"];

        $query_select = "SELECT DISTINCT (ic_country.country_code), ".
						"       ic_country.country_name ".
                        "  FROM ic_country LEFT JOIN ic_company_country ".
                        "       ON (ic_country.country_code = ic_company_country.country_code) ".
                        " WHERE ic_country.country_code NOT IN ( ".
                        "          SELECT ic_country.country_code  ".
                        "            FROM ic_country, ic_company_country ".
                        "           WHERE ic_country.country_code = ic_company_country.country_code ".
                        "             AND ic_company_country.company_id = ".$this->getID().
                        "             AND ic_country.language='".$sLanguage."') ".
						"        AND ic_country.language='".$sLanguage."'".
						" ORDER BY ic_country.country_name";

		//echo "<br/>".$query_select;

        $result = $mysqli->query($query_select);
		return $result;

	}

	public function saveCountries($id)
	{
		global $mysqli;
		global $errorLog;

		$this->controlAccessObject($_SESSION["company_id"], $id);

		try {
			// Se borran todos primero para luego guardar los seleccionados
			$query_delete=sprintf("DELETE from ic_company_country WHERE company_id=%d", $id);

			$result = $mysqli->query($query_delete);
			$errorLog->LogDebug("DELETE: $query_delete");

			$selectedCountries = $_POST["assig_countries"];
			if ($selectedCountries!=""){
				$tok = strtok ($selectedCountries,", ");
				while ($tok !== false) {
					$query_insert = sprintf("INSERT INTO ic_company_country (country_code, company_id, timestamp, modified_by ) ".
								" VALUES ( '%s', %d, CURRENT_TIMESTAMP, '%s') ", $tok, $id, $_SESSION["user_id"]);
					//echo "<br/>**".$query_insert;

					$result = $mysqli->query($query_insert);
					$errorLog->LogDebug("INSERT: $query_insert");
					$tok = strtok(", ");
				}
			}

		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}

	}







//Control del acceso de otras company a ver datos.
function controlAccessObject($actual_company_id, $view_object_id)
{
	if ($actual_company_id==1){ //La company 1 puede hacer todo en este objeto.

	}else if ($actual_company_id==$view_object_id){ //La propia entidad puede ver sus datos.

	}else if ($actual_company_id!=$view_object_id){ //Si la entidad logada no es la principal solo ve sus datos.
		header("Location: http://".$_SERVER['HTTP_HOST']."/index.php?error=3");
		exit;
	}

}//End controlAccessObject











}

?>