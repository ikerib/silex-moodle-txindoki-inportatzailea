<?php
// -----------------------------------------
// Info.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/database/DB_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/admin/InfoData.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/Functions.php');

require_once($_SERVER['DOCUMENT_ROOT']."/../src/lang/en/config.php");
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/admin/Attachment.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/admin/Picture.php');


class Info extends InfoData
{

   public $oPhoto = "";

   public function loadData ($id){

	$nCompanyID = $_SESSION["company_id"];
	$sLanguage = $_SESSION["language"];

	if (null == $id || "new"==$id)
      {
			$this->setID("new");
			$this->setName("");
			$this->setNameEU("");
			$this->setDescription("");
			$this->setDescriptionEU("");
			$this->setCompanyID($_SESSION["company_id"]);
			$this->setInfoTypeID("");
			$this->setInfoTypeName("");
			$this->setDate( date(CFG_FORMATO_FECHA,mktime())  );
			$this->setContents("");
			$this->setContentsEU("");
			$this->setLink("");
			$this->setLinkName("");

			$this->setInfoSource("");
			$this->setPublicationDate( date(CFG_FORMATO_FECHA,mktime())  ); //Fecha actual.
			$this->setExpirationDate( date(CFG_FORMATO_FECHA,mktime()+(15*60*60*24))  ); //Establecido por Tak 15 días después de la fecha actual.
			$this->setInfoExtra("");

			$this->setRemarks("");
			$this->setActiveFlag(1);
			$this->setModifiedBy($_SESSION["user_id"]);
			$this->setTimestamp("");
			return;
      }

      global $mysqli;
	  $format = new Format();

	  try {
		$query_select= sprintf("SELECT ic_info.info_id,
								ic_info.info_title,
								ic_info.info_title_eu,
								ic_info.info_desc,
								ic_info.info_desc_eu,
								ic_info.company_id,
								ic_info.info_type_id,
								ic_info_type.info_type_name,
								ic_info.info_date,
								ic_info.contents,
								ic_info.contents_eu,
								ic_info.link,
								ic_info.link_name,

								ic_info.info_source,
								ic_info.publication_date,
								ic_info.expiration_date,
								ic_info.info_extra,

								ic_info.remarks,
								ic_info.active_flag,
								ic_info.modified_by,
								ic_info.timestamp
                     FROM ic_info LEFT OUTER JOIN ic_info_type
                            ON ic_info_type.info_type_id=ic_info.info_type_id
                            AND ic_info_type.language = '%s'
                     WHERE ic_info.info_id = %d
                     ",

					 $_SESSION["language"],
					 $id);

		$query_select.= " AND ic_info.company_id = $nCompanyID ";


		 //echo $query_select;
		 if ($result = $mysqli->query($query_select)){

					$row = $result->fetch_array();

					$this->setID($row["info_id"]);
					$this->setName($row["info_title"]);
					$this->setNameEU($row["info_title_eu"]);
					$this->setDescription($row["info_desc"]);
					$this->setDescriptionEU($row["info_desc_eu"]);
					$this->setInfoTypeID($row["info_type_id"]);
					$this->setInfoTypeName($row["info_type_name"]);
					$this->setDate($format->formatea_fecha($row["info_date"]));
					$this->setContents($row["contents"]);
					$this->setContentsEU($row["contents_eu"]);
					$this->setLink($row["link"]);
					$this->setLinkName($row["link_name"]);

					$this->setInfoSource($row["info_source"]);
					$this->setPublicationDate($format->formatea_fecha($row["publication_date"]));
					$this->setExpirationDate($format->formatea_fecha($row["expiration_date"]));
					$this->setInfoExtra($row["info_extra"]);

					$this->setCompanyID($row["company_id"]);
					$this->setRemarks($row["remarks"]);
					$this->setActiveFlag($row["active_flag"]);
					$this->setModifiedBy($row["modified_by"]);
					$this->setTimestamp($row["timestamp"]);

					$result->close();

			}

	  } catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
	  }

	}//end loadData



	public function createNew()
   {

	  global $mysqli;
	  global $errorLog;
	  $format = new Format();

      try {

			$query_insert=sprintf("INSERT INTO ic_info
								    (info_title,
									info_title_eu,
									info_desc,
									info_desc_eu,
									info_type_id,

									info_date,
									contents,
									contents_eu,
									link_name,
									link,

									info_source,
									publication_date,
									expiration_date,
									info_extra,

									company_id,
									active_flag,
									remarks,
									modified_by,
									timestamp)
								  VALUES (
								  '%s',
								  '%s',
								  '%s',
								  '%s',
								   %d ,

								  '%s',
								  '%s',
								  '%s',
								  '%s',
								  '%s',

								  '%s',
								  '%s',
								  '%s',
								  '%s',

								   %d ,
								   1 ,
								  '%s',
								  '%s',
								  CURRENT_TIMESTAMP
								  )",

									$this->getName(),
									$this->getNameEU(),
									$this->getDescription(),
									$this->getDescriptionEU(),
									$this->getInfoTypeID(),

									$format->formatea_fecha_bd($this->getDate()),
									$this->getContents(),
									$this->getContentsEU(),
									$this->getLinkName(),
									$this->getLink(),

									$this->getInfoSource(),
									$format->formatea_fecha_bd($this->getPublicationDate()),
									$format->formatea_fecha_bd($this->getExpirationDate()),
									$this->getInfoExtra(),

									$this->getCompanyID(),
									$this->getRemarks(),
									$this->getModifiedBy()

								);

			$errorLog->LogDebug("INSERT: $query_insert");

			$result = $mysqli->query($query_insert);

			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}

			//Calculo el último ID insertado.
			$query_max = "SELECT LAST_INSERT_ID() AS max_id";
			if( $result = $mysqli->query($query_max) ){
				$row = $result->fetch_array();

				$this->setID($row["max_id"]);

				$result->close();
			}

		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}

   }//END create_new




   public function update($id)
   {
	  global $mysqli;
	  global $errorLog;

	  $format = new Format();

      try {

			$query_update=sprintf("UPDATE ic_info
								SET info_title='%s',
									info_title_eu='%s',
									info_desc='%s',
									info_desc_eu='%s',
									info_type_id='%s',

									info_date='%s',
									contents='%s',
									contents_eu='%s',

									link_name='%s',
									link='%s',

									info_source='%s',
									publication_date='%s',
									expiration_date='%s',
									info_extra='%s',

									company_id=%d,
									remarks='%s',
									modified_by='%s',
									timestamp=CURRENT_TIMESTAMP
							WHERE
									info_id = %d",

									$this->getName(),
									$this->getNameEU(),
									$this->getDescription(),
									$this->getDescriptionEU(),
									$this->getInfoTypeID(),

									$format->formatea_fecha_bd($this->getDate()),
									$this->getContents(),
									$this->getContentsEU(),

									$this->getLinkName(),
									$this->getLink(),

									$this->getInfoSource(),
									$format->formatea_fecha_bd($this->getPublicationDate()),
									$format->formatea_fecha_bd($this->getExpirationDate()),
									$this->getInfoExtra(),

									$this->getCompanyID(),
									$this->getRemarks(),
									$_SESSION["user_id"],
									$id
    							);


			$errorLog->LogDebug("UPDATE: $query_update");

			$result = $mysqli->query($query_update);

			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}

		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}

   }//END UPDATE



   public function delete($id)
   {
	 global $mysqli;
	 global $errorLog;

      try {

			$query_delete=sprintf("UPDATE ic_info
								SET active_flag=0,
									modified_by='%s',
									timestamp=CURRENT_TIMESTAMP
							WHERE
									info_id = %d",

									$_SESSION["user_id"],
									$_POST['id']
    							);

			//echo "*".$query_delete."*";
			$result = $mysqli->query($query_delete);

			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}


			//Al borrar la info borrar otro objetos:

			//Attachment relacionados
			$objAttachment = new Attachment();
			$objAttachment->deleteRelated(22,$id);

			//Pictures relacionados
			$objPicture = new Picture();
			$objPicture->deleteRelated(22,$id);


		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}


   }//END DELETE



   public function init()
   {
		$error=false;
		$error_txt="";
		$valor = "";


		//Param id
		$valor = $_POST['id'];
		if (trim($valor)!=""){
			$this->setID($valor);
		}else{
			$this->setID("new");
		}

		//Param name
		$valor = $_POST['name'];
		if (trim($valor)!=""){
			$this->setName($valor);
		}else{
			$error=true;
			$error_txt.="\\n".MSG_INFO_TEXT_01;
			$this->setName("");
		}

		//Param name
		$valor = $_POST['name_eu'];
		if (trim($valor)!=""){
			$this->setNameEU($valor);
		}else{
			$error=true;
			$error_txt.="\\n".MSG_INFO_TEXT_01;
			$this->setNameEU("");
		}

		$this->setDescription($_POST['info_desc']);
		$this->setDescriptionEU($_POST['info_desc_eu']);

		$this->setInfoTypeID($_POST['info_type_id']);

		//Param info_date
		$valor = $_POST['info_date'];
		if (trim($valor)!=""){
			$this->setDate($valor);
		}else{
			$error=true;
			$error_txt.="\\n".MSG_INFO_TEXT_02;
			$this->setDate("");
		}

		$this->setContents($_POST['contents']);
		$this->setContentsEU($_POST['contents_eu']);

		$this->setLink($_POST['link']);
		$this->setLinkName($_POST['link_name']);



		$this->setInfoSource($_POST['info_source']);

		//Param publication_date
		$valor = $_POST['publication_date'];
		if (trim($valor)!=""){
			$this->setPublicationDate($valor);
		}else{
			$error=true;
			$error_txt.="\\n- ".str_replace("$1",LBL_PUBLICATION_DATE,MSG_REQUIRED_FIELD);
			$this->setPublicationDate("");
		}

		//Param expiration_date
		$valor = $_POST['expiration_date'];
		if (trim($valor)!=""){
			$this->setExpirationDate($valor);
		}else{
			$error=true;
			$error_txt.="\\n- ".str_replace("$1",LBL_EXPIRATION_DATE,MSG_REQUIRED_FIELD);
			$this->setExpirationDate("");
		}

		$this->setInfoExtra($_POST['info_extra']);


		$this->setCompanyID($_POST['company_id']);


		$this->setRemarks($_POST['remarks']);

		$this->setModifiedBy($_SESSION['user_id']);

		return $error_txt;

   }//END init





}//END CLASS
?>