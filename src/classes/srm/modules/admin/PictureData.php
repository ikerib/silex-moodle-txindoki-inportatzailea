<?php
// -----------------------------------------
// PictureData.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/AbstractCRMObjectImage.php');

class PictureData extends AbstractCRMObjectImage
{

	public $mnPictureOrder = 0;
	public $msPictureDesc = "";

	public $mnObjectTypeID = null;
	public $msObjectTypeName = "";
	public $mnObjectID = null;
	public $mnObjectName = "";

	public function getPictureOrder(){
		return $this->mnPictureOrder;
	}
	public function setPictureOrder($nValue){
		$this->mnPictureOrder = $nValue;
	}

	public function getPictureDesc(){
		return $this->msPictureDesc;
	}
	public function setPictureDesc($nValue){
		$this->msPictureDesc = $nValue;
	}

	public function getObjectTypeID(){
		return $this->mnObjectTypeID;
	}
	public function setObjectTypeID($nValue){
		$this->mnObjectTypeID = $nValue;
	}
	public function getObjectTypeName (){
		return $this->msObjectTypeName;
	}
	public function setObjectTypeName($nValue){
		$this->msObjectTypeName = $nValue;
	}

	public function getObjectID(){
		return $this->mnObjectID;
	}
	public function setObjectID($nValue){
		$this->mnObjectID = $nValue;
	}

	public function getObjectName(){
		return $this->mnObjectName;
	}
	public function setObjectName($nValue){
		$this->mnObjectName = $nValue;
	}


}//end class

?>