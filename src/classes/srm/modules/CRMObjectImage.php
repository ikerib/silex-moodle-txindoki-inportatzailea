<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/CRMObject.php');

interface CRMObjectImage extends CRMObject {

	public function getPictureFile();
	public function setPictureFile($nValue);


    public function getPictureName();
    public function setPictureName($s);

    public function getPictureWidth();
    public function setPictureWidth($nValue);

	public function getPictureHeight();
    public function setPictureHeight($s);

	public function getUpload();
	public function setUpload($nValue);

	public function getPictureFileDelete();
	public function setPictureFileDelete($s);

	public function getPictureFileExists();
    public function setPictureFileExists($nValue);

	public function getThumbnailFileExists();
    public function setThumbnailFileExists($s);



}
?>