<?php
// -----------------------------------------
// AbstractCRMObjectImage.php
// -----------------------------------------
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/AbstractCRMObject.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/CRMObjectImage.php');

abstract class AbstractCRMObjectImage extends AbstractCRMObject
									  implements CRMObjectImage
{

	public $oPhoto = "";
	public $msAuxID = "";
	public $msPictureFile = "";
	public $msPictureName = "";
	public $mnPictureWidth = "";
	public $mnPictureHeight = "";
	public $msUpload = "";
	public $msPictureFileDelete = "";
	public $mbPictureFileExists = "";
	public $mbThumbnailFileExists = "";

	public $msPictureFileOld = "";


	public function getAuxID(){
		return $this->msAuxID;
	}
	public function setAuxID($nValue){
		$this->msAuxID = $nValue;
	}

	public function getPictureFile(){
		return $this->msPictureFile ;
	}
	public function setPictureFile($nValue){
		$this->msPictureFile  = $nValue;
	}

	public function getPictureName(){
		return $this->msPictureName;
	}
	public function setPictureName($nValue){
		$this->msPictureName = $nValue;
	}

	public function getPictureWidth(){
		return $this->mnPictureWidth;
	}
	public function setPictureWidth($nValue){
		$this->mnPictureWidth = $nValue;
	}

	public function getPictureHeight(){
		return $this->mnPictureHeight;
	}
	public function setPictureHeight($nValue){
		$this->mnPictureHeight = $nValue;
	}

	public function getUpload(){
		return $this->msUpload;
	}
	public function setUpload($nValue){
		$this->msUpload = $nValue;
	}

	public function getPictureFileDelete(){
		return $this->msPictureFileDelete;
	}
	public function setPictureFileDelete($nValue){
		$this->msPictureFileDelete = $nValue;
	}

	public function getPictureFileExists(){
		return $this->mbPictureFileExists;
	}
	public function setPictureFileExists($nValue){
		$this->mbPictureFileExists = $nValue;
	}

	public function getThumbnailFileExists(){
		return $this->mbThumbnailFileExists;
	}
	public function setThumbnailFileExists($nValue){
		$this->mbThumbnailFileExists = $nValue;
	}


	public function getPictureFileOld(){
		return $this->msPictureFileOld ;
	}
	public function setPictureFileOld($nValue){
		$this->msPictureFileOld  = $nValue;
	}



	//FALTAN VARIAS FUNCIONES


}
?>