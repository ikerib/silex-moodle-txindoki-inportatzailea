<?php
// -----------------------------------------
// AbstractCRMObject.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/AbstractCRMAdmin.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/CRMObject.php');

abstract class AbstractCRMObject extends AbstractCRMAdmin
								 implements CRMObject
{

	public $moID = "";
	public $msName = "";
	public $mnCompanyID = "";
	public $msCompanyName = "";
	public $msRemarks = "";
	public $msModifiedBy = "";
	public $msTimestamp = "";
	public $mnActiveFlag = 1;
	public $msViewDescription = "";
	public $msObjectName = "";
	public $mnSessionCompanyID = 0;
	public $msSessionLanguage = "en";
	public $mnEmployeeID = 0;
	public $msEmployeeName = "";
	public $msOpenerID = "";
	public $mnOpenerType = 0;
	public $msObjectAuxID = "";



	public function getID(){
		return $this->moID;
	}
	public function setID($nValue){
		$this->moID = $nValue;
	}

	public function getReqID(){
		return $this->moID;
	}
	public function setReqID($nValue){
		$this->moID = $nValue;
	}

	public function getName(){
		return $this->msName ;
	}
	public function setName($nValue){
		$this->msName  = $nValue;
	}

	public function getCompanyID(){
		return $this->mnCompanyID;
	}
	public function setCompanyID($nValue){
		$this->mnCompanyID = $nValue;
	}

	public function getCompanyName(){
		return $this->msCompanyName ;
	}
	public function setCompanyName($nValue){
		$this->msCompanyName  = $nValue;
	}

	public function getRemarks(){
		return $this->msRemarks;
	}
	public function setRemarks($nValue){
		$this->msRemarks = $nValue;
	}

	public function getModifiedBy(){
		return $this->msModifiedBy;
	}
	public function setModifiedBy($nValue){
		$this->msModifiedBy = $nValue;
	}

	public function getTimestamp(){
		return $this->msTimestamp;
	}
	public function setTimestamp($nValue){
		$this->msTimestamp = $nValue;
	}

	public function getActiveFlag(){
		return $this->mnActiveFlag;
	}
	public function setActiveFlag($nValue){
		$this->mnActiveFlag = $nValue;
	}
	public function isActiveFlag(){
		$this->mnActiveFlag = getActiveFlag();
	}

	public function getViewDescription(){
		return $this->msViewDescription;
	}
	public function setViewDescription($nValue){
		$this->msViewDescription = $nValue;
	}

	public function getCrmObjectName(){
		return $this->msObjectName;
	}
	public function setCrmObjectName($nValue){
		$this->msObjectName = $nValue;
	}

	public function getSessionCompanyID(){
		return $this->mnSessionCompanyID;
	}
	public function setSessionCompanyID($nValue){
		$this->mnSessionCompanyID = $nValue;
	}

	public function getSessionLanguage(){
		return $this->msSessionLanguage;
	}
	public function setSessionLanguage($nValue){
		$this->msSessionLanguage = $nValue;
	}


	public function getSessionEmployeeID(){
		return $this->mnEmployeeID;
	}
	public function setSessionEmployeeID($nValue){
		$this->mnEmployeeID = $nValue;
	}

	public function getSessionEmployeeName(){
		return $this->mnEmployeeName;
	}
	public function setSessionEmployeeName($nValue){
		$this->mnEmployeeName = $nValue;
	}


	public function getOpenerID(){
		return $this->msOpenerID;
	}
	public function setOpenerID($nValue){
		$this->msOpenerID = $nValue;
	}

	public function getOpenerType(){
		return $this->mnOpenerType;
	}
	public function setOpenerType($nValue){
		$this->mnOpenerType = $nValue;
	}

	public function getObjectAuxID(){
		return $this->msObjectAuxID;
	}
	public function setObjectAuxID($nValue){
		$this->msObjectAuxID = $nValue;
	}



}
?>