<?php
// -----------------------------------------
// AbstractCRMAdmin.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/../src/config_app.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/CRMObjectSummary.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/AbstractCRMDataTypes.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/Functions.php');

abstract class AbstractCRMObjectSummary extends AbstractCRMDataTypes
										implements CRMObjectSummary//, CRMDataTypes
{

   public $DESC = "DESC";

   public $ASC = "ASC";

   public $PARAM_CURRENT_PAGE = "current_page";

   public $PARAM_FIRST_ROW = "first_row";

   public $PARAM_ORDER_HOW = "order_how";

   public $PARAM_ORDER_BY = "order_by";

   public $mnCurrentPage = 1;

   public $mnPagesNumber = 1;

   public $mnFirstRow = 1;

   public $mnRowsNumber = 0;

   public $mnMaxRowsNumber = CFG_MAX_NUM_ROWS;

   public $msOrderBy = "1";

   public $mnOrderHow = "1";

   public $msCurrentPageParamName = "current_page";

   public $msFirstRowParamName = "first_row";

   public $msOrderHowParamName = "order_how";

   public $msOrderByParamName = "order_by";


   /**
    * Crea una lista con los objetos que responden a las
    * restricciones establecidas en la petición.
    *
    * @param  oReq     petición
    * @throws          isyc.website.CRMException
    *                  si se produce algún error al obtener los datos.
    * @throws          NullPointerException
    *                  si la petición especificada es un valor <CODE>null</CODE>.
    * @since           CRM 1.0.0
    */
   public function load(){

   }


   /**
    * Crea una lista con los objetos que responden a las
    * restricciones establecidas en los parámetros de la petición
    * y filtradas por el código indicado. El primer parámetro indica
    * el tipo de filtro que hay que aplicar.
    *
    * @param  sType    tipo de filtrado
    * @param  sID      clave de filtrado
    * @param  oReq     petición
    * @throws          isyc.website.CRMException
    *                  si se produce algún error al obtener los datos.
    * @throws          NullPointerException
    *                  si la petición especificada es un valor <CODE>null</CODE>.
    * @since           CRM 1.0.0
    */
   public function loadFilter($sType, $sID){

   }



   /**
    * Devuelve el número de la página actual de resultados que se ha cargado en
    * la lista del total de páginas que contienen todos los datos obtenidos
    * a partir de los criterios de búsqueda.
    *
    * @return          número de la página
    * @since           CRM 1.0.0
    */
   public function getCurrentPage()
   {
      return $this->mnCurrentPage;
   }
   public function setCurrentPage($s)
   {
      $this->mnCurrentPage = 1;
      try {
         if (null != $s && 0 < $s){
             $this->mnCurrentPage = $s;
		 }
      } catch (Exception $ex) {}
   }
   public function getPagesNumber()
   {
      return  $this->mnPagesNumber;
   }
   public function setPagesNumber($nValue)
   {
       $this->mnPagesNumber = $nValue;
   }


   /**
    * Establece el número de la primera fila de resultados que se ha cargado en
    * la lista del total de filas que contienen todos los datos obtenidos de a
    * partir de los criterios de búsqueda.
    *
    * @param  nValue   número de la primera fila
    * @since           CRM 1.0.0
    */
   public function getFirstRow()
   {
      return $this->mnFirstRow;
   }
   public function setFirstRow($s)
   {
      $this->mnFirstRow = 1;
      try {
         if (null != $s && 0 <= ($s))
            $this->mnFirstRow = ($s);
      } catch (Exception $ex) {}
   }


    /**
    * Devuelve el número total de registros (filas) que se obtienen al realizar
    * la búsqueda con los criterios de búsqueda especificados.
    *
    * @return          número total de registros
    * @since           CRM 1.0.0
    */
   public function getRowsNumber()
   {
      return $this->mnRowsNumber;
   }
   public function setRowsNumber($nValue)
   {
      $this->mnRowsNumber = $nValue;
   }


    /**
    * Devuelve el número máximo de registros (filas) que se mostrarán.
    *
    * @return          número máximo de registros
    * @since           CRM 1.0.0
    */
   public function getMaxRowsNumber()
   {
      return $this->mnMaxRowsNumber;
   }
   public function setMaxRowsNumber($nValue)
   {
      $this->mnMaxRowsNumber = $nValue;
   }

  /**
    * Devuelve la columna de ordenación de los datos. Puede ser o
    * bien el número de la columna o bien su nombre.
    *
    * @return          columna de ordenación
    * @since           CRM 1.0.0
    */
   public function getOrderBy()
   {
      return $this->msOrderBy;
   }
   public function setOrderBy($s)
   {
      if (null != $s)
         $this->msOrderBy = $s;
   }


   /**
    * Devuelve el tipo de ordenación de los datos:
    * <CODE>1</CODE> ascendente (ASC)
    * <CODE>0</CODE> descendente (DESC)
    *
    * @return          tipo de ordenación
    * @since           CRM 1.0.0
    */
   public function getOrderHow()
   {
      return $this->mnOrderHow;
   }
   public function setOrderHow($s)
   {
      if (null == $s)
      {
         return;
      }

      $this->mnOrderHow = 0;
      try {
         if (!"0"==($s) && "DESC"!=$s)
            $this->mnOrderHow = 1;
      } catch (Exception $ex) {
         $this->mnOrderHow = 1;
      }
   }


   /**
    * Devuelve la cadena que se emplea en la consulta para definir
    * el tipo de ordenación de los datos:
    * <CODE>ASC</CODE> ascendente
    * <CODE>DESC</CODE> descendente
    *
    * @return          tipo de ordenación
    * @since           CRM 1.0.0
    */
   public function getOrderHowString()
   {
      if (0 == $this->mnOrderHow)
         return "DESC";
      return "ASC";
   }


   /**
    * Devuelve la columna de ordenación de los datos. Si en el formulario
    * se ha especificado el número de la columna hay que transformarlo
    * obligatoriamente en su nombre para poder ordenar correctamente los
    * resultados de forma alfabética.
    *
    * @return          columna de ordenación
    * @since           CRM 1.0.0
    */
   public function getOrderByColumn(){
   }

   public function escapeAccents($s)
   {
     // return "replace(replace(replace(replace(replace( UPPER(" + s + ") , 'Á', 'A'), 'É','E'), 'Í', 'I'), 'Ó', 'O'), 'Ú', 'U')";
   }


   /**
    * Returns the current page parameter name in the request.
    *
    * @return          current page parameter name
    * @since           CRM 1.0.0
    */
   public function getCurrentPageParamName()
   {
      return $this->msCurrentPageParamName;
   }
   public function setCurrentPageParamName($s)
   {
      $this->msCurrentPageParamName = $s;
   }


   /**
    * Returns the first row parameter name in the request.
    *
    * @return          first row parameter name
    * @since           CRM 1.0.0
    */
   public function getFirstRowParamName()
   {
      return $this->msFirstRowParamName;
   }
   public function setFirstRowParamName($s)
   {
     $this-> msFirstRowParamName = $s;
   }


   /**
    * Returns the order how parameter name in the request.
    *
    * @return          order how parameter name
    * @since           CRM 1.0.0
    */
   public function getOrderHowParamName()
   {
      return $this->msOrderHowParamName;
   }
   public function setOrderHowParamName($s)
   {
      $this->msOrderHowParamName = $s;
   }


   /**
    * Returns the order by parameter name in the request.
    *
    * @return          order by parameter name
    * @since           CRM 1.0.0
    */
   public function getOrderByParamName()
   {
      return $this->msOrderByParamName;
   }
   public function setOrderByParamName($s)
   {
      $this->msOrderByParamName = $s;
   }


   /**
    * Lee una serie de parámetros e inicializa las variables de esta clase
    *
    * @param            rows number returned from a sql query
    * @param  oReq      request
    * @since            CRM 1.0.0
    */

   public function init($nRows)
   {
      $nMaxRows = CFG_MAX_NUM_ROWS;
	  $this->setMaxRowsNumber((0 > $nMaxRows)?CFG_MAX_NUM_ROWS:$nMaxRows);

      // Es el resultado de dividir el total de registros entre nMaxRows
      $nPages = 0;

      // Lectura de los parámetros que vienen en la petición
      // parámetro current_page: Página actual 0<current_page<=nPages

	  if (isset($_POST[$this->getCurrentPageParamName()])){
		$this->setCurrentPage($_POST[$this->getCurrentPageParamName()]);
	  }else{
		$this->setCurrentPage(0);
	  }

      // parámetro first_row: primera fila de los resultados partir de la cual
      // se empezará a rellenar el listado
	  if (isset($_POST[$this->getFirstRowParamName()])){
		$this->setFirstRow($_POST[$this->getFirstRowParamName()]);
	  }else{
		$this->setFirstRow(0);
	  }

      // parametro order_how: si la ordenación de filas será ascendente o descendente
	  if (isset($_POST[$this->getOrderHowParamName()])){
		$this->setOrderHow($_POST[$this->getOrderHowParamName()]);
	  }else{
		$this->setOrderHow(0);
	  }

      // parametro order_by: columna por la que se ha de realizar la ordenación
	  if (isset($_POST[$this->getOrderByParamName()])){
		$this->setOrderBy($_POST[$this->getOrderByParamName()]);
	  }else{
		$this->setOrderBy(0);
	  }

      if ($nRows <= $this->getMaxRowsNumber())
			$nPages = 1;
      else
      {
			if (($nRows%$this->getMaxRowsNumber()) == 0)
				$nPages = intval(($nRows/$this->getMaxRowsNumber()));
         else
				$nPages = intval(($nRows/$this->getMaxRowsNumber()) + 1);
      }

      $this->setRowsNumber($nRows);
      $this->setPagesNumber($nPages);

      if ($nPages < $this->getCurrentPage())
			setCurrentPage($nPages);
   }

   /**
    * Lee de la petición el parámetro solicitado y construye la cláusula del where
	 * correspondiente
    *
    * @return						cadena con el sql
    * @param  oReq				request
    * @param  sParameter		nombre del parametro en el formulario enviado
    * @param  sTableParameter columna de la base de datos correspondiente a ese parametro
    *                         o bien cadena que se pondrá en la primera parte de la clausula justo antes del operador
    * @param  sTable				tabla de la que se obtendrán los registros
    * @param  sParameterType	tipo de parámetro, (Constantes definidas en el interfaz CRMDataTypes)
	 *									STRING_TYPE
	 *									DOUBLE_TYPE
	 *									INTEGER_TYPE
	 *									DATE_TYPE
	 *									LOGICAL_TYPE
    * @since             CRM 3.0.0
    */

	public function getWhereClause(		$sParameter,
										$sTableParameter,
										$sTable,
										$sParameterType,
										$sConditionExcepted,
										$sOperatorName)
	{

		$format = new Format();

		if ($sConditionExcepted == null)
			$sConditionExcepted = "";

		if ($sOperatorName == null)
			$sOperatorName = "";

		//Sacarlo de la configuración del idioma... ?
		//$sShortDatePattern = oSessionData.getShortDatePattern();
		$sShortDatePattern = "dd/MM/yyyy";

		$sResult = "";
		$sCondition = "";
		$sOperator = "";
		$sTableField = $sTableParameter;
		//echo "<br>....".$sParameterType;
		try{

			if (isset($_POST["$sParameter"])){
				$sCondition = (null == $_POST["$sParameter"])?"":$_POST["$sParameter"];
			}else{
				$sCondition = "";
			}

			if (""!=($sOperatorName)){
				if (isset($_POST["$sOperatorName"])){
					$sOperator = (null==$_POST["$sOperatorName"])?"=":$this->getOperatorSymbol($_POST["$sOperatorName"]);
				}
			}else{
				if (isset($_POST["op_$sParameter"])){
					$sOperator = (null==$_POST["op_$sParameter"])?"=":$this->getOperatorSymbol($_POST["op_$sParameter"]);
				}
			}

			if (""!=($sCondition) && $sConditionExcepted!=($sCondition) && ""!=($sOperator))
			{
				if ($sTable!=null && ""!=($sTable))
					$sTableField = $sTable . "." . $sTableParameter;

				if ($this->STRING_TYPE==($sParameterType))
				{	// String
					$sCondition = $sCondition;
					$sResult = " AND (" . $sTableField . ") " .	str_replace("$1", $sCondition, $sOperator );

				}else if ($this->DATE_TYPE==($sParameterType))
				{	// Date

					$sCondition = $sCondition;

					$sCondition = $format->formatea_fecha_bd($sCondition);

					//if (sOperator.indexOf("<>")!=-1)
					//	sResult = " AND COALESCE(" + sTableField + ", " + sCondition + "+1) " + StringFormatter.replaceString(sOperator, "$1", sCondition);
					//else
					//	sResult = " AND " + $sTableField + " " +  StringFormatter.replaceString(sOperator, "$1", sCondition);

					if ( strpos($sOperator, "<>") )
						$sResult = " AND COALESCE(" . $sTableField . ", " . $sCondition + "+1) " . str_replace("$1", $sCondition, $sOperator );
					else
						$sResult = " AND " . $sTableField . " " .  str_replace("$1", "'" . $sCondition . "'", $sOperator );




				/*
				}else if (DATE_TYPE_2.equals(sParameterType))
				{	// Date
               		sCondition = DBFormatter.toDBDateFormat(DateFormatter.getDate(sCondition, oSessionData.getShortDatePattern()));
					isyc.website.util.DebugLog.write("Date, patron para la fecha DATE_TYPE_2=" +  oSessionData.getShortDatePattern(), getClass().getName()+".getWhereClause");

					if (sOperator.indexOf("<>")!=-1)
						sResult = " AND COALESCE(to_date(" + sTableField + ", 'YYYY-MM-DD'), " + sCondition + "+1) " + StringFormatter.replaceString(sOperator, "$1", sCondition);
					else
						sResult = " AND to_date(" + sTableField + ", 'YYYY-MM-DD') " +  StringFormatter.replaceString(sOperator, "$1", sCondition);
				}else if (INTEGER_TYPE.equals(sParameterType))
				{	// Integer
					sCondition = DBFormatter.toDBString(sCondition, false, true, false);
					try{
						Integer nInt = new Integer(sCondition);
						sResult = " AND " + sTableField + " " +	StringFormatter.replaceString(sOperator, "$1", nInt.toString());
					}catch(Exception e){
						isyc.website.util.DebugLog.write("Imposible crear Integer con " +  sCondition, getClass().getName()+".getWhereClause");
					}

				}else if (DOUBLE_TYPE.equals(sParameterType))
				{	// Double
					sCondition = DBFormatter.toDBString(sCondition, false, true, false);
					try{
                  Double nDefaultDiscount = isyc.website.util.format.NumberFormatter.getDouble(sCondition, oSessionData.getLanguage());
						sResult = " AND " + sTableField + " " +	StringFormatter.replaceString(sOperator, "$1", nDefaultDiscount.toString() + "::NUMERIC");
					}catch(Exception e){
						isyc.website.util.DebugLog.write("Imposible crear Double con " +  sCondition, getClass().getName()+".getWhereClause");

					}
				*/
				}else if ($this->LOGICAL_TYPE==($sParameterType))
				{
					//echo "*********";


					$sCondition = $sCondition;
					if (is_int(intval($sCondition))){
						if ($sCondition>0){
							$sResult = " AND " . $sTableField . " " . str_replace("$1", $sCondition, $sOperator );
						}
					}

				}
			}


		}catch(Exception $ex){

		}
		//echo "<br>....".$sResult;
		return $sResult;
	}


   /**
    * Lee una serie de parámetros e inicializa las variables de esta clase
    *
    * @return String			cadena con el simbolo del operador almacenada en BD
    * @param  sOperatorID  identificador del operador
    * @since					CRM 3.0.0
    */
   public function getOperatorSymbol($sOperatorID)
   {

		$sResult = "";

		global $mysqli;

	  try {
		$query_select= sprintf("SELECT operator_symbol
								FROM ica_operator
								WHERE operator_id = %d", $sOperatorID);

		//echo $query_select;

		 if ($result = $mysqli->query($query_select)){

				$row = $result->fetch_array();

		 		 $sResult = ($row["operator_symbol"]);

				$result->close();

			}

	  } catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
	  }

	  return $sResult;

   }//end getOperatorSymbol




      /**
    * Lee una serie de parámetros e inicializa las variables de esta clase
    *
    * @return String			cadena con el simbolo del operador almacenada en BD
    * @param  sOperatorID  identificador del operador
    * @since					CRM 3.0.0
    */
   public function buildOperatorSymbol($name, $type, $selected)
   {
		$sResult = "";
		global $mysqli;

		try{

			$query_select="SELECT ica_operator.operator_id, ica_operator.operator_name";
			$sFrom = " FROM ica_operator ";
			$sWhere= " WHERE ica_operator.language = '".$_SESSION["language"]."'";

			if ($type=="char"){
					$sWhere.= " AND char_flag = 1";
			}else if ($type=="equal"){
				$sWhere.= " AND equal_flag = 1";
			}else if ($type=="number"){
				$sWhere.= " AND number_flag = 1";
			}else if ($type=="date"){
				$sWhere.= " AND date_flag = 1";
			}else if ($type=="logical"){
				$sWhere.= " AND logical_flag = 1";
			}

		   $query_select.= $sFrom . $sWhere;
		   $query_select.= " ORDER BY 1";

       	   //echo $query_select;

		   if ($result = $mysqli->query($query_select))
			{

				$sResult.= "\n<select id=\"op_$name\" name=\"op_$name\" class=\"form1\" style=\"width:100px;\">\n";

				while($row = $result->fetch_array())
				{
					$sSelected="";
					if ($selected==$row["operator_id"]){
						$sSelected="selected = \"selected\"";
					}

					$sResult.= "\n<option value=\"".$row["operator_id"]."\" $sSelected>".$row["operator_name"]."</option>\n";

				}

				$sResult.= "\n</select>";
			}
		}catch(Exception $ex){

		}


     return $sResult;

   }//end getOperatorSymbol










}
?>