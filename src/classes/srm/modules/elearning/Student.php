<?php
// -----------------------------------------
// Student.php
// -----------------------------------------
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/database/DB_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/database/DB_Moodle_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/elearning/StudentData.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/Authentication.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/Functions.php');
require_once($_SERVER['DOCUMENT_ROOT']."/../src/lang/en/config.php");
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/admin/Company.php');



class Student extends StudentData
{
	public $saveUserID = false;

	public function getSaveUserID(){
		return $this->saveUserID;
	}

	public function setSaveUserID($value){
		$this->saveUserID = $value;
	}

	public function loadData ($id){

		if (null == $id || "new"==$id)
		{

			$this->setID("");
			$this->setName("");
			$this->setCompanyID(1);

			$this->setUserID("");
			$this->setPersonLastName1("");
			$this->setPersonLastName2("");

			$this->setRegistrationDate(date(CFG_FORMATO_FECHA));

			$this->setCardID("");
			$this->setPhone("");
			$this->setMobile("");
			$this->setBirthPlace(""); //Ciudad de residencia

			$this->setPictogram1("");
			$this->setPictogram2("");
			$this->setPictogram3("");

			$this->setLevel(1);
			$this->setCourseParticularID("");

			$this->setLocution(1);
			$this->setHighContrast(2);

			$this->setPictureFile("");
			$this->setPictureName("");
			$this->setPictureWidth("");
			$this->setPictureHeight("");

			$this->setInternalID("");
			$this->setEmpresa("");
			$this->setSeccion("");

			$this->setRemarks("");
			$this->setActiveFlag("");
			$this->setModifiedBy("");
			$this->setTimestamp("");
			$this->setSaveUserID(false);

			return;
		}

		global $mysqli;
		global $errorLog;
		$format = new Format();

		try {
			$query_select= sprintf("SELECT ic_student.student_id,
				ic_student.first_name,
				ic_student.last_name1,
				ic_student.last_name2,
				ic_student.user_id,
				DATE_FORMAT(ic_student.registration_date,'%%Y/%%m/%%d') as registration_date,

				ic_user.language,
				ic_user.e_mail,

				ic_student.card_id,
				ic_student.phone,
				ic_student.mobile,
				ic_student.city,

				ic_student.pictograma1,
				ic_student.pictograma2,
				ic_student.pictograma3,

				ic_student.level,
				ic_student.course_particular_id,

				ic_student.locution,
				ic_student.high_contrast,

				ic_student.picture_name,
				ic_student.picture_file,
				ic_student.picture_width,
				ic_student.picture_height,

				ic_student.internal_id,
				ic_student.empresa,
				ic_student.seccion,

				ic_student.company_id,
				ic_student.remarks,
				ic_student.active_flag,
				ic_student.timestamp,
				ic_student.modified_by

				FROM ic_student LEFT JOIN ic_user ON ic_student.user_id = ic_user.user_id

				WHERE ic_student.student_id = %d ", $id);

$query_select.=sprintf(" AND ic_student.company_id = %d ", $_SESSION["company_id"]);

$errorLog->LogDebug("SELECT: $query_select");

if ($result = $mysqli->query($query_select)){

	if ($result->num_rows > 0){

		$row = $result->fetch_array();

		$this->setID($row["student_id"]);

		$this->setName($row["first_name"]);
		$this->setPersonLastName1($row["last_name1"]);
		$this->setPersonLastName2($row["last_name2"]);

		$this->setUserID($row["user_id"]);
		if ($this->getUserID()!=null && $this->getUserID()!="")
			$this->setIsUser(true);
		else
			$this->setIsUser(false);

		$this->setRegistrationDate($format->formatea_fecha($row["registration_date"]));

		$this->setUserLang($row["language"]);
		$this->setUserEmail($row["e_mail"]);

		$this->setCardID($row["card_id"]);
		$this->setPhone($row["phone"]);
		$this->setMobile($row["mobile"]);
		$this->setBirthPlace($row["city"]);

		$this->setPictogram1($row["pictograma1"]);
		$this->setPictogram2($row["pictograma2"]);
		$this->setPictogram3($row["pictograma3"]);

		$this->setLevel($row["level"]);
		$this->setCourseParticularID($row["course_particular_id"]);

		$this->setLocution($row["locution"]);
		$this->setHighContrast($row["high_contrast"]);

		$this->setPictureName($row["picture_name"]);
		$this->setPictureFile($row["picture_file"]);
		$this->setPictureWidth($row["picture_width"]);
		$this->setPictureHeight($row["picture_height"]);

		$this->setInternalID($row["internal_id"]);
		$this->setEmpresa($row["empresa"]);
		$this->setSeccion($row["seccion"]);

		$this->setCompanyID($row["company_id"]);
		$this->setRemarks($row["remarks"]);
		$this->setActiveFlag($row["active_flag"]);
		$this->setTimestamp($row["timestamp"]);
		$this->setModifiedBy($row["modified_by"]);

		$this->setAssignedCourses($this->loadCourses());
		$this->setNoAssignedCourses($this->loadPossibleCourses());

					//$ruta_upload = str_replace("$1",$this->getCompanyID(),CFG_PICTURE_STUDENT_URL);
					//$this->setPictureFileExists(file_exists($ruta_upload.$this->getID() . "_" . $this->getPictureFile()));
					//$this->setThumbnailFileExists(file_exists($ruta_upload.$this->getID() . "_th_" . $this->getPictureFile()));

		$result->close();

		$this->setSaveUserID(true);
	}else{
		$errorLog->LogError("Location: /srm/index.php?error=3. ERROR: ".$query_select);
		exit;
	}

}

if ($mysqli->error){
	$errorLog->LogError($mysqli->error);
}

} catch (Exception $e) {
	$errorLog->LogFatal("Caught exception: ". $e->getMessage());
}

	}//end loadData



	public function createNew($datuak)
	{

		global $mysqli;
		global $errorLog;
		$format = new Format();
		$error="";

		try {

			$query_insert=sprintf("INSERT INTO ic_student
				( 	company_id,

					first_name,
					last_name1,
					last_name2,

					user_id,
					registration_date,

					card_id,
					phone,
					mobile,
					city,

					pictograma1,
					pictograma2,
					pictograma3,

					level,
					course_particular_id,
					locution,
					high_contrast,

					internal_id,
					empresa,
					seccion,

					remarks,
					active_flag,
					modified_by,
					timestamp)
				VALUES (
					%d,

					'%s',
					'%s',
					'%s',

					'%s',
					'%s',

					'%s',
					'%s',
					'%s',
					'%s',

					%d,
					%d,
					%d,

					%d,
					%d,
					%d,
					%d,

					'%s',
					'%s',
					'%s',

					'%s',
					'%s',
					'%s',
					CURRENT_TIMESTAMP
					)",

			$this->getCompanyID(),

			$this->getName(),
			$this->getPersonLastName1(),
			$this->getPersonLastName2(),

			$this->getUserID(),
			$format->formatea_fecha_bd($this->getRegistrationDate()),

			$this->getCardID(),
			$this->getPhone(),
			$this->getMobile(),
			$this->getBirthPlace(),

			$this->getPictogram1(),
			$this->getPictogram2(),
			$this->getPictogram3(),

			$this->getLevel(),
			$this->getCourseParticularID(),
			$this->getLocution(),
			$this->getHighContrast(),

			$this->getInternalID(),
			$this->getEmpresa(),
			$this->getSeccion(),

			$this->getRemarks(),
			1,
			'iibarguren-txindoki-vs-plataforma'
			);

			//echo $query_insert.

$result = $mysqli->query($query_insert);

if ($mysqli->error){
	$errorLog->LogError($mysqli->error);
}

// Calculo el Último ID insertado.
// $query_max = "SELECT LAST_INSERT_ID() AS max_id";
$query_max = "SELECT MAX(student_id) AS max_id FROM ic_student ORDER BY student_id";
// $result = $conn->fetchAll($query_max, array());
// if( $result ){
if( $result = $mysqli->query($query_max) ){
	$row = $result->fetch_array();

	$this->setID($row["max_id"]);

	$result->close();
}
// // Crea el registro en ic_user.
// $this->updateUserID();

$query_insert=sprintf("INSERT INTO ic_user
	(user_id,
		password,
		language,
		e_mail,
		active_flag,
		modified_by,
		timestamp)
VALUES (
	'%s',
	'%s',
	'%s',
	'%s',
	'%d',
	'%s',
	CURRENT_TIMESTAMP
	)",
$this->getUserID(),
$this->encodePassword($datuak['npass']),
$datuak["user_lang"],
$datuak["user_email"],
1,
'txindoki-vs-plataforma'
);

$result = $mysqli->query($query_insert);


// 			///Sube la imágen.
// if ($this->getUpload()=="yes"){
// 	$upload = $this->uploadPicture($this->getID(), $this->getCompanyID());
// 	$error=$upload;
// }

} catch (Exception $e) {
	$errorLog->LogFatal("Caught exception: ". $e->getMessage());
}

return $error;

	}//END create_new



	public function update($id)
	{
		global $mysqli;
		global $errorLog;
		$format = new Format();
		$error = "";

		try {

			$query_update=sprintf("UPDATE ic_student
				SET first_name = '%s',
				last_name1 = '%s',
				last_name2 = '%s',

				registration_date = '%s',

				card_id = '%s',
				phone = '%s',
				mobile = '%s',
				city = '%s',

				pictograma1 = %d,
				pictograma2 = %d,
				pictograma3 = %d,

				level = %d,
				course_particular_id = %d,
				locution = %d,
				high_contrast = %d,

				internal_id = '%s',
				empresa = '%s',
				seccion = '%s',

				remarks = '%s',
				modified_by = '%s',
				timestamp = CURRENT_TIMESTAMP,
				company_id = %d
				WHERE
				student_id = %d",

				$this->getName(),
				$this->getPersonLastName1(),
				$this->getPersonLastName2(),

				$format->formatea_fecha_bd($this->getRegistrationDate()),

				$this->getCardID(),
				$this->getPhone(),
				$this->getMobile(),
				$this->getBirthPlace(),

				$this->getPictogram1(),
				$this->getPictogram2(),
				$this->getPictogram3(),

				$this->getLevel(),
				$this->getCourseParticularID(),
				$this->getLocution(),
				$this->getHighContrast(),

				$this->getInternalID(),
				$this->getEmpresa(),
				$this->getSeccion(),

				$this->getRemarks(),
				'txindoki-vs-plataforma',
				$this->getCompanyID(),
				$id
				);


$errorLog->LogDebug("UPDATE: $query_update");
$result = $mysqli->query($query_update);

if ($mysqli->error){
	$errorLog->LogError($mysqli->error);
}

			//Modifica el registro en ic_user.
$this->updateUserID();

$ruta_upload = str_replace("$1",$this->getCompanyID(),CFG_PICTURE_STUDENT_URL);

			///Sube la imágen.
if ($this->getUpload()=="yes"){
	$upload = $this->uploadPicture($this->getID(), $this->getCompanyID());

	$error=$upload;
}

			//Borramos la anterior imágen si estamos subiendo una nueva, solamente si no hay error al intentar subirla.
if ($this->getUpload()=="yes" && $error==""){
	if (file_exists($ruta_upload.$this->getID() . "_" . $this->getPictureFileOld())){
		unlink($ruta_upload.$this->getID() . "_" . $this->getPictureFileOld());
	}
	if (file_exists($ruta_upload.$this->getID() . "_th_" . $this->getPictureFileOld())){
		unlink($ruta_upload.$this->getID() . "_th_" . $this->getPictureFileOld());
	}
}


} catch (Exception $e) {
	$errorLog->LogFatal("Caught exception: ". $e->getMessage());
}

return $error;

	}//END UPDATE

	public function delete($id)
	{

		global $mysqli;
		global $mysqliMoodle;

		global $errorLog;


		$this->loadData($id);
		$username=$this->getUserID();

		$time_unix = mktime();

		//BORRO Y RENOMBRO EL ALUMNO EN SRM.
		try {

			//ic_student
			$query_delete=sprintf("UPDATE ic_student
				SET active_flag=0,
				modified_by='%s',
				timestamp=CURRENT_TIMESTAMP,
				user_id = CONCAT(user_id, '.', '%s')
				WHERE
				student_id = %d",
				'txindoki-vs-plataforma',
				$time_unix,
				$_POST['id']
				);

			$errorLog->LogDebug("DELETE: $query_delete");
			$result = $mysqli->query($query_delete);


			//ic_user
			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}

			$query_delete=sprintf("UPDATE ic_user
				SET active_flag=0,
				modified_by='%s',
				timestamp=CURRENT_TIMESTAMP,
				user_id = CONCAT(user_id, '.', '%s')
				WHERE
				user_id = '%s'",

				'txindoki-vs-plataforma',
				$time_unix,
				$username
				);

			$errorLog->LogDebug("DELETE: $query_delete");
			$result = $mysqli->query($query_delete);

			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}

		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}


		//BORRO Y RENOMBRO EL ALUMNO EN MOODLE.
		try {



			$query_delete=sprintf("UPDATE mdl_user
				SET deleted=1,
				username = CONCAT(username, '.', '%s'),
				email = CONCAT(email, '.', '%s')
				WHERE
				username = '%s'",

				$time_unix,
				$time_unix,
				$username
				);

			$errorLog->LogDebug("DELETE: $query_delete");
			$result = $mysqliMoodle->query($query_delete);

			if ($mysqliMoodle->error){
				$errorLog->LogError($mysqliMoodle->error);
			}

		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}



	}//END UPDATE

	public function init($datuak)
	{
		$error=false;
		$error_txt="";
		$valor = "";

		//Param id
//		$valor = $_POST['id'];
		$valor = $datuak['id'];
		if (trim($valor)!=""){
			$this->setID($valor);
		}else{
			$this->setID("new");
		}

		//Param name
		$valor = $datuak['name'];
		if (trim($valor)!=""){
			$this->setName($valor);
		}else{
			$error=true;
			$error_txt.="\\n- ".str_replace("$1",LBL_FIRST_NAME,MSG_REQUIRED_FIELD);
			$this->setName("");
		}

		//Param last_name_1
		$valor = $datuak['last_name_1'];
		if (trim($valor)!=""){
			$this->setPersonLastName1($valor);
		}else{
			$error=true;
			$error_txt.="\\n- ".str_replace("$1",LBL_LAST_NAME1,MSG_REQUIRED_FIELD);
			$this->setPersonLastName1("");
		}

		$this->setPersonLastName2($datuak["last_name_2"]);

		$this->setRegistrationDate($datuak["registration_date"]);


		//user_name
		//Miramos si tiene creado un usuario que no exista.
		if (isset($datuak["user_valido"]) && $datuak["user_valido"]=="NO EXISTE"){

			$this->setUserID($datuak["user_name"]);

		}else if (isset($datuak["user_valido"]) && $datuak["user_valido"]=="EXISTE"){
			if (isset($datuak["user_name"]) && $datuak["user_name"]!=""){
				$this->setUserID("");

				if ($this->existsUserID($datuak["user_name"])){
					$error_txt.="\\n- ".MSG_USERDATA_TEXT03;
				}
			}else{
				$error_txt.="\\n- ".MSG_USERDATA_TEXT04;
			}
		}

		//user_email
		if (isset($datuak["user_email"]) && $datuak["user_email"]!=""){
			$this->setUserEmail($datuak["user_email"]);
		}else{
			$this->setUserEmail("");
			$error_txt.="\\n- ".MSG_EMAIL_TEXT01;
		}

		//user_lang
		$this->setUserLang($datuak["user_lang"]);



		//npass y cpass
		//Si vienen ambos campos y ambos son iguales, cambiarle la contraseña también.
		if (isset($datuak["npass"]) && $datuak["npass"]!="" && isset($datuak["cpass"]) && $datuak["cpass"]!=""){

			if ($datuak["npass"]==$datuak["cpass"]){

				//Todo OK, podemos cambiar la contraseña.
			}else{
				$error_txt.="\\n- ".MSG_USERDATA_TEXT01; //La nueva contraseña y su confirmación deben ser iguales.
			}


		}else{
			if (isset($datuak["npass"]) && ($datuak["npass"]!="" || $datuak["cpass"]!=""))
				$error_txt.="\\n- ".MSG_USERDATA_TEXT02; //Debe escribir la nueva contraseña y su confirmación
		}


		//Param Level
		$valor = $datuak["level"];
		if (trim($valor)!="0"){
			$this->setLevel($valor);
		}else{
			$error=true;
			$error_txt.="\\n- ".str_replace("$1",LBL_LEVEL,MSG_REQUIRED_FIELD);
			$this->setLevel(0);
		}

		//Param Level
		$valor = $datuak["course_particular_id"];
		if (trim($valor)=="0" && $datuak["level"]==4){
			$error=true;
			$error_txt.="\\n- ".str_replace("$1",LBL_COURSE_LEVEL4,MSG_REQUIRED_FIELD);
			$this->setCourseParticularID(0);
		}else if (trim($valor)!="0" && $datuak["level"]==4){
			$this->setCourseParticularID($valor);
		}else{
			$this->setCourseParticularID(0);
		}


		//error_log ("LEVEL=>".$this->getLevel());

		//Param pictograma1
		$valor = $datuak['pictograma1'];
		if (trim($valor)!="0"){
			$this->setPictogram1($valor);
		}else if (trim($valor)=="0" && ($this->getLevel()==1 || $this->getLevel()==2)){ //Que no sea obligotorio seleccionar pictogramas para los usuarios de nivel 1 y 2
			$this->setPictogram1($valor);
		}else{
			$error=true;
			$error_txt.="\\n- ".str_replace("$1",LBL_PICTOGRAM." 1 ",MSG_REQUIRED_FIELD);
			$this->setPictogram1(0);
		}

		//Param pictograma2
		$valor = $datuak['pictograma2'];
		if (trim($valor)!="0"){
			$this->setPictogram2($valor);
		}else if (trim($valor)=="0" && ($this->getLevel()==1 || $this->getLevel()==2)){ //Que no sea obligotorio seleccionar pictogramas para los usuarios de nivel 1 y 2
			$this->setPictogram2($valor);
		}else{
			$error=true;
			$error_txt.="\\n- ".str_replace("$1",LBL_PICTOGRAM." 2 ",MSG_REQUIRED_FIELD);
			$this->setPictogram2(0);
		}

		//Param pictograma3
		$valor = $datuak['pictograma3'];
		if (trim($valor)!="0"){
			$this->setPictogram3($valor);
		}else if (trim($valor)=="0" && ($this->getLevel()==1 || $this->getLevel()==2)){ //Que no sea obligotorio seleccionar pictogramas para los usuarios de nivel 1 y 2
			$this->setPictogram3($valor);
		}else{
			$error=true;
			$error_txt.="\\n- ".str_replace("$1",LBL_PICTOGRAM." 3 ",MSG_REQUIRED_FIELD);
			$this->setPictogram3(0);
		}





		$this->setLocution($datuak["locution"]);
		$this->setHighContrast($datuak["high_contrast"]);

		$this->setCardID($datuak["card_id"]);
		$this->setPhone($datuak["phone"]);
		$this->setMobile($datuak["mobile"]);
		$this->setBirthPlace($datuak["city"]);

		$this->setInternalID($datuak["internal_id"]);
		$this->setEmpresa($datuak["empresa"]);
		$this->setSeccion($datuak["seccion"]);

		$valor = $datuak['bupload_image'];
		$this->setUpload($valor);

		$valor = $datuak['picture_file_old'];
		$this->setPictureFileOld($valor);

		$this->setCompanyID($datuak["company_id"]);
		$this->setRemarks($datuak["remarks"]);
		$this->setModifiedBy("iibarguren-txindoki-vs-plataforma");

		return $error_txt;

   }//END init


   private function existsUserID($sUserID)
   {
   	global $mysqli;
   	global $errorLog;

   	$bExists = false;

   	if (!isset($sUserID))
   		return false;

   	$query_select = "";
   	$query_select = sprintf("SELECT * FROM ic_user WHERE UPPER(ic_user.user_id) = UPPER('%s')", $sUserID);

   	$errorLog->LogDebug("SELECT: $query_select");

   	if ($result = $mysqli->query($query_select)){

   		if ($result->num_rows>0){
   			$bExists = true;
   		}
   		$result->close();
   	}

   	return $bExists;
   }


	//Función que crea o modifica el registro en ic_user.

   public function updateUserID()
   {
   	global $mysqli;
   	global $errorLog;

   	$user_id = $_POST["user_name"];
   	$language = $_POST["user_lang"];
   	$user_email = $_POST["user_email"];


   	try {

			//Si no existe creamos nuevo
   		if (!$this->existsUserID($user_id)){

   			$mypass=$this->genRandomPassword();


   			$query_insert=sprintf("INSERT INTO ic_user
   				(user_id,
   					password,
   					language,
   					e_mail,
   					active_flag,
   					modified_by,
   					timestamp)
   			VALUES (
   				'%s',
   				'%s',
   				'%s',
   				'%s',
   				'%d',
   				'%s',
   				CURRENT_TIMESTAMP
   				)",
   			$user_id,
   			$this->encodePassword($mypass),
   			$language,
   			$user_email,
   			1,
   			'txindoki-vs-plataforma'
   			);

   			$errorLog->LogDebug("INSERT: $query_insert");

   			$result = $mysqli->query($query_insert);

   			if ($mysqli->error){
   				$errorLog->LogError($mysqli->error);
   			}

					//ENVIO POR CORREO DEL USUARIO.
   			$mail = new Mail();

   			$to=$user_email;
   			$from=CFG_EMAIL_FROM_DEFAULT;
   			$subject=MSG_SUBJECT_USER_PASSWORD;

   			$contents=MSG_NEW_USER_TEXT_01;
   			$contents.="\n\n".LBL_USER_ID.": ". $user_id;
   			$contents.="\n".LBL_OLD_PASSWORD.": ". $mypass;
   			$contents.="\n\n".MSG_USER_SIGNATURE;

					////send_mail($to, $from, $asunto, $contenido, $cc, $bcc)
   			if ($subject!="" && $to!="" && $from!=""){
   				$ok = $mail->send_mail($to, $from, $subject, $contents , "", "");
   			}

				}else{ //Si existe lo modificamos el correo por si hubiera cambiado.

					$newPass="";

					if (isset($_POST["npass"]) && $_POST["npass"]!="" && isset($_POST["cpass"]) && $_POST["cpass"]!=""){
						if ($_POST["npass"]==$_POST["cpass"]){

							$newPass=$_POST["npass"];
						}
					}

					if ($newPass==""){
						$query_update=sprintf("UPDATE ic_user
							SET
							e_mail='%s',
							language='%s',
							timestamp=timestamp
							WHERE user_id = '%s'
							",
							$user_email,
							$language,
							$user_id
							);

						$errorLog->LogDebug("UPDATE: $query_update");

						$result = $mysqli->query($query_update);

						if ($mysqli->error){
							$errorLog->LogError($mysqli->error);
						}
					}else if ($newPass!=""){
						$query_update=sprintf("UPDATE ic_user
							SET
							e_mail='%s',
							password='%s',
							language='%s',
							timestamp=timestamp
							WHERE user_id = '%s'
							",
							$user_email,
							$this->encodePassword($newPass),
							$language,
							$user_id
							);

						$errorLog->LogDebug("UPDATE: $query_update");

						$result = $mysqli->query($query_update);

						if ($mysqli->error){
							$errorLog->LogError($mysqli->error);
						}

						//ENVIO POR CORREO DEL USUARIO.
						$mail = new Mail();

						$to=$user_email;
						$from=CFG_EMAIL_FROM_DEFAULT;
						$subject=MSG_SUBJECT_USER_PASSWORD;


						$contents=MSG_NEW_USER_TEXT_01;
						$contents.="\n\n".LBL_USER_ID.": ". $user_id;
						$contents.="\n".LBL_OLD_PASSWORD.": ". $newPass;
						$contents.="\n\n".MSG_USER_SIGNATURE;
						////send_mail($to, $from, $asunto, $contenido, $cc, $bcc)
						if ($subject!="" && $to!="" && $from!=""){
							$ok = $mail->send_mail($to, $from, $subject, $contents , "", "");
						}

					}

				}

			} catch (Exception $e) {
				$errorLog->LogFatal("Caught exception: ". $e->getMessage());
			}

	}//END createNewUserID


	function encodePassword($pass)
	{
		return md5($pass);
	}

	function genRandomPassword()
	{
		$str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
		$cad = "";
		for($i=0;$i<8;$i++) {
			$cad .= substr($str,rand(0,62),1);
		}
		return $cad;
	}

	private function loadCourses()
	{

		global $mysqli;
		global $errorLog;

		$query_select = "SELECT ic_course.course_id, ".
		"       ic_course.course_name ".
		"  FROM ic_course, ic_student_course ".
		" WHERE ic_course.course_id = ic_student_course.course_id ".
		"   AND ic_student_course.student_id = ".$this->getID().
		"   AND ic_course.active_flag = 1 ".
		"   AND ic_course.company_id = 1";

		$errorLog->LogDebug("SELECT: $query_select");
		$result = $mysqli->query($query_select);
		return $result;

	}//loadRoles

	private function loadPossibleCourses()
	{

		global $mysqli;
		global $errorLog;

		$query_select = "";

		$query_select = "SELECT DISTINCT (ic_course.course_id), ".
		"       ic_course.course_name ".
		"  FROM ic_course LEFT JOIN ic_student_course ".
		"       ON (ic_course.course_id = ic_student_course.course_id) ".
		" WHERE ic_course.course_id NOT IN ( ".
			"          SELECT ic_course.course_id  ".
			"            FROM ic_course, ic_student_course ".
			"           WHERE ic_course.course_id = ic_student_course.course_id ".
			"             AND ic_student_course.student_id = ".$this->getID().
			"             AND ic_course.company_id = 1".
			"             AND ic_course.active_flag = 1 ) ".
"   AND ic_course.active_flag = 1 ".
"   AND ic_course.company_id = 1";

$errorLog->LogDebug("SELECT: $query_select");
$result = $mysqli->query($query_select);
return $result;

}

public function saveCourses($id)
{
	global $mysqli;
	global $errorLog;

	try {
			// Se borran todos primero para luego guardar los seleccionados
		$query_delete=sprintf("DELETE from ic_student_course WHERE student_id=%d", $id);
		$errorLog->LogDebug("DELETE: $query_delete");

		$result = $mysqli->query($query_delete);


		$selectedCourses = $_POST["assig_courses"];
		if ($selectedCourses!=""){
			$tok = strtok ($selectedCourses,", ");
			while ($tok !== false) {
				$query_insert = "INSERT INTO ic_student_course (course_id, student_id) ".
				" VALUES ( ".$tok.", ".$id.") ";
				$result = $mysqli->query($query_insert);
				$errorLog->LogDebug("INSERT: $query_insert");
				$tok = strtok(", ");
			}
		}

	} catch (Exception $e) {
		$errorLog->LogFatal("Caught exception: ". $e->getMessage());
	}

}












//Función que sube o modifica una imagen existente en la ruta indicada.

public function uploadPicture($picture_id, $company_id){

	global $errorLog;

	$logo_error=false;
	$logo_error_num=0;
	$archivo_final="";

	$ruta_upload = str_replace("$1",$company_id,CFG_PICTURE_STUDENT_URL);

	if (is_uploaded_file($_FILES['picture']['tmp_name'])) {

		$archivo_name= $_FILES['picture']['name'];
		$archivo_size= $_FILES['picture']['size'];
		$archivo_type=  $_FILES['picture']['type'];
		$archivo= $_FILES['picture']['tmp_name'];

			//echo "<br>archivo_name=".$archivo_name;
			//echo "<br>archivo_size=".$archivo_size;
			//echo "<br>archivo_type=".$archivo_type;
			//echo "<br>archivo=".$archivo;

		$lim_tamano= $_POST['lim_tamano'];

			//echo "<br>archivo=".$lim_tamano;

		$extension = explode(".",$archivo_name);
		$num = count($extension)-1;
		if(($extension[$num] == "jpg" || $extension[$num] == "gif" || $extension[$num] == "png" )){

			if ($extension[$num] == "jpg"){
				$archivo_final=$picture_id."_".$archivo_name;
			}

			if ($extension[$num] == "gif"){
				$archivo_final=$picture_id."_".$archivo_name;
			}

			if ($extension[$num] == "png"){
				$archivo_final=$picture_id."_".$archivo_name;
			}

				//echo "<br>IMAGEN BUENA";

			$tamano = getimagesize($archivo);
				//echo "<br>alto=".$tamano[1];
				//echo "<br>ancho=".$tamano[0];

				//Valida las dimensiones del logotipo.
			if (($tamano[0]>CFG_PICTURE_STUDENT_WIDTH || $tamano[1]>CFG_PICTURE_STUDENT_HEIGHT)){

				$logo_error=true;
				$logo_error_num=2;
			}

			if (!$logo_error){

					//echo "<br>->archivo_name=$archivo_name";
					//echo "<br>->archivo_size=$archivo_size";
					//echo "<br>->lim_tamano=$lim_tamano";

				if ($archivo_name != "none" AND $archivo_size != 0 AND $archivo_size<=$lim_tamano){
					if (copy ($archivo, $ruta_upload.$archivo_final)) {
						$logo_error_num=100;
					}
				}else{
					$logo_error=true;
					$logo_error_num=3;
				}
			}

		}

	}else{
		$logo_error=true;
		$logo_error_num=1;
	}

	$error_msg="";
	if ($logo_error_num==1){
		$error_msg="";
	}else if ($logo_error_num==2){
		$error_msg=
		str_replace("$4", CFG_PICTURE_STUDENT_HEIGHT,
			str_replace("$3", CFG_PICTURE_STUDENT_WIDTH,
				str_replace("$2", $tamano[1],str_replace("$1",$tamano[0],MSG_INFO_BIG_SIZE))));
	}else if ($logo_error_num==3){
		$error_msg=
		str_replace("$2", CFG_SIZE_STUDENT_BYTES,str_replace("$1",$archivo_size,MSG_INFO_LARGE));
	}

		//echo $error_msg;
		//echo $logo_error_num;

		if ($logo_error_num==0 || $logo_error_num==100){ //si no hay error guardo la imagen.

			//Reduzco la imagen y la guardo. Thumbnail.



			$archivo_final = $ruta_upload.$archivo_final;

			$original = "";
			if (preg_match('/.png$/', $archivo_final)) {
				$original = imagecreatefrompng($archivo_final);
			} else if (preg_match('/.gif$/', $archivo_final)) {
				$original = imagecreatefromgif($archivo_final);
			} else if (preg_match('/.jpg$/', $archivo_final)) {
				$original = imagecreatefromjpeg($archivo_final);
			}


			$ancho = imagesx($original);
			$alto = imagesy($original);


			/* if ($ancho>=$alto){
				$ratio = $ancho/CFG_TH_PICTURE_STUDENT_WIDTH;
				$aux_height = $alto/$ratio;

				$thumb = imagecreatetruecolor(CFG_TH_PICTURE_STUDENT_WIDTH,$aux_height); // Lo haremos de un tamaño 150x150
				imagecopyresampled($thumb,$original,0,0,0,0,CFG_TH_PICTURE_STUDENT_WIDTH,$aux_height,$ancho,$alto);
			}else{
				$ratio = $alto/CFG_TH_PICTURE_CATEGORY_HEIGHT;
				$aux_width = $ancho/$ratio;

				$thumb = imagecreatetruecolor($aux_width, CFG_TH_PICTURE_STUDENT_HEIGHT); // Lo haremos de un tamaño 150x150
				 imagecopyresampled($thumb,$original,0,0,0,0,$aux_width,CFG_TH_PICTURE_STUDENT_HEIGHT,$ancho,$alto);
				} */


			//La imagen del alumno siempre tiene que ser del mismo tamaño.
				$thumb = imagecreatetruecolor(CFG_TH_PICTURE_STUDENT_WIDTH, CFG_TH_PICTURE_STUDENT_HEIGHT);
				imagecopyresampled($thumb, $original, 0, 0, 0, 0, CFG_TH_PICTURE_STUDENT_WIDTH, CFG_TH_PICTURE_STUDENT_HEIGHT, $ancho,$alto);

				if (preg_match('/.png$/', $archivo_final)) {
				imagepng($thumb,$ruta_upload.$picture_id."_th_".$archivo_name); // Suprimida la calidad de compresión daba error.
			} else if (preg_match('/.gif$/', $archivo_final)) {
				imagegif($thumb,$ruta_upload.$picture_id."_th_".$archivo_name,90); // 90 es la calidad de compresión
			} else if (preg_match('/.jpg$/', $archivo_final)) {
				imagejpeg($thumb,$ruta_upload.$picture_id."_th_".$archivo_name,90); // 90 es la calidad de compresión
			}


			//Save image...
			global $mysqli;

			try {

				$query_update=sprintf("UPDATE ic_student
					SET
					picture_file='%s',
					picture_width='%s',
					picture_height='%s',
					timestamp=CURRENT_TIMESTAMP
					WHERE
					student_id = %d",

					$archivo_name,
					$tamano[0],
					$tamano[1],
					$picture_id
					);

				$errorLog->LogDebug("UPDATE: $query_update");
				$result = $mysqli->query($query_update);

				if ($mysqli->error){
					$errorLog->LogError($mysqli->error);
				}

				/*

					Borrar las imágenes

				*/


				} catch (Exception $e) {
					$errorLog->LogFatal("Caught exception: ". $e->getMessage());
				}

			}else{

				return $error_msg;
			}


}//end upload_picture



	//Función que dado el user_id obtiene la ruta para la imagen del perfil en Moodle.
public function loadMoodleProfilePhoto($userid)
{
	global $mysqliMoodle;
	global $errorLog;

	$ruta="";

	try {
		$query_select= sprintf("SELECT
			DISTINCT mdl_user.username, mdl_context.id FROM mdl_user
			INNER JOIN mdl_context ON mdl_user.id = mdl_context.instanceid
			WHERE contextlevel = 30 AND mdl_user.username = '%s' ",  $userid);

		$errorLog->LogDebug("SELECT: $query_select");

		if ($result = $mysqliMoodle->query($query_select)){

			if ($result->num_rows > 0){
				$row = $result->fetch_array();

				$ruta = "/".$row["id"]."/user/icon/f1";
			}
		}

	} catch (Exception $e) {
		$errorLog->LogFatal("Caught exception: ". $e->getMessage());
	}

	return $ruta;
	}//loadMoodleProfilePhoto



}

?>