<?php
// -----------------------------------------
// CourseData.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/AbstractCRMObjectImage.php');

class CourseData extends AbstractCRMObjectImage  
{
	public $msDescription = "";
	
	public $mnCategoryID = "";
	public $msCategoryName = "";
	
	public $mnLevelID = "";
	public $msLevelName = "";
	
	public $msLanguageInfo = "";
	
	public $mnCourseMoodleID = "";
	public $msCourseMoodleName = "";
	
	public $oAssignedStudents = "";
	public $oNoAssignedStudents = "";
	
	public $msDuracion = "";
	public $msResponsable = "";
	public $msTipoInscripcion = "";
	
	public $msInternalID;
	
	public $mnValidateFlag = "";
	
	public function getDescription(){
		return $this->msDescription;
	}
	public function setDescription($nValue){
		$this->msDescription = $nValue;
	}
	
	
	public function getCategoryID(){
		return $this->mnCategoryID;
	}
	public function setCategoryID($nValue){
		$this->mnCategoryID = $nValue;
	}
	
	public function getCategoryName(){
		return $this->msCategoryName;
	}
	public function setCategoryName($nValue){
		$this->msCategoryName = $nValue;
	}
	
	
	public function getLevelID(){
		return $this->mnLevelID;
	}
	public function setLevelID($nValue){
		$this->mnLevelID = $nValue;
	}
	
	public function getLevelName(){
		return $this->msLevelName;
	}
	public function setLevelName($nValue){
		$this->msLevelName = $nValue;
	}
	
	
	public function getLanguageInfo(){
		return $this->msLanguageInfo;
	}
	public function setLanguageInfo($nValue){
		$this->msLanguageInfo = $nValue;
	}
	
	
	public function getCourseMoodleID(){
		return $this->mnCourseMoodleID;
	}
	public function setCourseMoodleID($nValue){
		$this->mnCourseMoodleID = $nValue;
	}
	
	public function getCourseMoodleName(){
		return $this->msCourseMoodleName;
	}
	public function setCourseMoodleName($nValue){
		$this->msCourseMoodleName = $nValue;
	}
	
	
	
	public function getAssignedStudents(){
		return $this->oAssignedStudents;
	}
	public function setAssignedStudents($value){
		$this->oAssignedStudents = $value;
	}	

	public function getNoAssignedStudents(){
		return $this->oNoAssignedStudents;
	}
	public function setNoAssignedStudents($value){
		$this->oNoAssignedStudents = $value;
	}
	
	
	public function getDuracion(){
		return $this->msDuracion;
	}
	public function setDuracion($nValue){
		$this->msDuracion = $nValue;
	}
	public function getResponsable(){
		return $this->msResponsable;
	}
	public function setResponsable($nValue){
		$this->msResponsable = $nValue;
	}
	public function getTipoInscripcion(){
		return $this->msTipoInscripcion;
	}
	public function setTipoInscripcion($nValue){
		$this->msTipoInscripcion = $nValue;
	}
	
	public function getInternalID(){
		return $this->msInternalID;
	}
	public function setInternalID($nValue){
		$this->msInternalID = $nValue;
	}
	
	
	public function getValidateFlag(){
		return $this->mnValidateFlag;
	}
	public function setValidateFlag($nValue){
		$this->mnValidateFlag = $nValue;
	}
	public function isValidateFlag(){
		return (1 == $this->getValidateFlag());
	}
	
	
}

?>