<?php
// -----------------------------------------
// Tutor.php
// -----------------------------------------
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/database/DB_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/database/DB_Moodle_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/elearning/TutorData.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/Authentication.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/Functions.php');
require_once($_SERVER['DOCUMENT_ROOT']."/lang/en/config.php"); 
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/admin/Company.php');



class Tutor extends TutorData
{
	public $saveUserID = false;
	
	public function getSaveUserID(){
		return $this->saveUserID;
	}
					
	public function setSaveUserID($value){
		$this->saveUserID = $value;
	}
	
	public function loadData ($id){
	
		if (null == $id || "new"==$id)
		{
	  
            $this->setID("");
            $this->setName("");
            $this->setCompanyID(1);
			
            $this->setUserID("");
            $this->setPersonLastName1("");
            $this->setPersonLastName2("");
            
			$this->setRegistrationDate(date(CFG_FORMATO_FECHA));
			
			$this->setCardID("");
			$this->setPhone("");
			$this->setMobile("");
			$this->setBirthPlace(""); //Ciudad de residencia
			
			$this->setRemarks("");
            $this->setActiveFlag("");
			$this->setModifiedBy("");
			$this->setTimestamp("");
			$this->setSaveUserID(false);

			return;
		}
      
		global $mysqli;
		global $errorLog;
		$format = new Format();
	  
		try {
			$query_select= sprintf("SELECT ic_tutor.tutor_id, 
										ic_tutor.first_name, 
										ic_tutor.last_name1, 
										ic_tutor.last_name2, 
										ic_tutor.user_id, 
										DATE_FORMAT(ic_tutor.registration_date,'%%Y/%%m/%%d') as registration_date, 
										
										ic_user.language,
										ic_user.e_mail,
										
										ic_tutor.card_id, 
										ic_tutor.phone, 
										ic_tutor.mobile, 
										ic_tutor.city, 
										
										ic_tutor.company_id, 
										ic_tutor.remarks, 
										ic_tutor.active_flag, 
										ic_tutor.timestamp, 
										ic_tutor.modified_by
										
								FROM ic_tutor LEFT JOIN ic_user ON ic_tutor.user_id = ic_user.user_id
												
                          		WHERE ic_tutor.tutor_id = %d ", $id);
						
						$query_select.=sprintf(" AND ic_tutor.company_id = %d ", $_SESSION["company_id"]);

			$errorLog->LogDebug("SELECT: $query_select");
			
			if ($result = $mysqli->query($query_select)){
				
				if ($result->num_rows > 0){
					
					$row = $result->fetch_array();
				
					$this->setID($row["tutor_id"]);
					
					$this->setName($row["first_name"]);
					$this->setPersonLastName1($row["last_name1"]);
					$this->setPersonLastName2($row["last_name2"]);
					
					$this->setUserID($row["user_id"]);
					if ($this->getUserID()!=null && $this->getUserID()!="")
						$this->setIsUser(true);
					else
						$this->setIsUser(false);				
					
					$this->setRegistrationDate($format->formatea_fecha($row["registration_date"]));
					
					$this->setUserLang($row["language"]);
					$this->setUserEmail($row["e_mail"]);
				
					$this->setCardID($row["card_id"]);
					$this->setPhone($row["phone"]);
					$this->setMobile($row["mobile"]);
					$this->setBirthPlace($row["city"]); 
					
					$this->setCompanyID($row["company_id"]);
					$this->setRemarks($row["remarks"]);
					$this->setActiveFlag($row["active_flag"]);
					$this->setTimestamp($row["timestamp"]);
					$this->setModifiedBy($row["modified_by"]);
					
					// $this->setAssignedCourses($this->loadCourses());
					// $this->setNoAssignedCourses($this->loadPossibleCourses());
					
					$this->setMoodleCourses($this->loadMoodleCourses());
					
					$result->close();
					
					$this->setSaveUserID(true);
				}else{
					$errorLog->LogError("Location: /srm/index.php?error=3. ERROR: ".$query_select);
					exit;
				}	

			}
			
			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}			
		
		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}
		
	}//end loadData
	
	
	
	public function createNew()
	{
   
		global $mysqli;
		global $errorLog;
		$format = new Format();
   
		try {
		
			$query_insert=sprintf("INSERT INTO ic_tutor 
								    ( 	company_id, 
									
										first_name, 
										last_name1, 
										last_name2, 
										
										user_id,
										registration_date,
										
										card_id,
										phone,
										mobile,
										city,
										
										remarks, 
										active_flag, 
										modified_by,
										timestamp) 
								  VALUES (
								  %d,
								  
								  '%s',
								  '%s',
								  '%s',
								  
								  '%s',
								  '%s',
								  
								  '%s',
								  '%s',
								  '%s',
								  '%s',	
								  
								  '%s',
								  '%s',
								  '%s',
								  CURRENT_TIMESTAMP
								  )",

									$this->getCompanyID(),
									
									$this->getName(),
									$this->getPersonLastName1(),
									$this->getPersonLastName2(), 
									
									$this->getUserID(),
									$format->formatea_fecha_bd($this->getRegistrationDate()),
									
									$this->getCardID(),
									$this->getPhone(),
									$this->getMobile(),
									$this->getBirthPlace(), 
									
									$this->getRemarks(),	
									1,	
									$_SESSION["user_id"]								
								);
			
			//echo $query_insert.
			
			$result = $mysqli->query($query_insert);
			
			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}
			
			//Calculo el Último ID insertado.
			$query_max = "SELECT LAST_INSERT_ID() AS max_id";
			if( $result = $mysqli->query($query_max) ){
				$row = $result->fetch_array();
				
				$this->setID($row["max_id"]);
				
				$result->close();
			}
			
			//Crea el registro en ic_user.
			$this->updateUserID();
		
		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}
	  
	}//END create_new
   
	
	
	public function update($id)
	{
		global $mysqli;
		global $errorLog;
		$format = new Format();
		
		try {
		
			$query_update=sprintf("UPDATE ic_tutor 
								SET first_name = '%s', 
									last_name1 = '%s', 
									last_name2 = '%s', 
									
									registration_date = '%s', 
									
									card_id = '%s',
									phone = '%s',
									mobile = '%s',
									city = '%s',
									
									remarks = '%s', 
									modified_by = '%s',
									timestamp = CURRENT_TIMESTAMP,
									company_id = %d
								WHERE 
									tutor_id = %d",								

									$this->getName(),
									$this->getPersonLastName1(),
									$this->getPersonLastName2(), 
									
									$format->formatea_fecha_bd($this->getRegistrationDate()),
									
									$this->getCardID(),
									$this->getPhone(),
									$this->getMobile(),
									$this->getBirthPlace(),
									
									$this->getRemarks(),	
									$_SESSION["user_id"],
									$this->getCompanyID(),
									$id
    							);
			
			
			$errorLog->LogDebug("UPDATE: $query_update");
			$result = $mysqli->query($query_update);
			
			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);;
			}else{
				$this->saveMoodleCourses($id);
			}
			
			
			//Modifica el registro en ic_user.
			$this->updateUserID();

			//Actualiza este alumno en los grupos de Moodle.
			//$this->updateGroupsMembers($id);
			
			
		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}
	  
	}//END UPDATE

	public function delete($id)
	{
   
		global $mysqli;
		global $errorLog;
   
		try {
		
			$query_delete=sprintf("UPDATE ic_tutor
								SET active_flag=0, 
									modified_by='%s', 
									timestamp=CURRENT_TIMESTAMP
							WHERE 
									tutor_id = %d",
									$_SESSION["user_id"],
									$_POST['id']
    							);
			
			//echo "*".$query_delete."*";
			$result = $mysqli->query($query_delete);
			
			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}
			
		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}
		
			  
	}//END UPDATE    

   public function init()
   {
		$error=false;
		$error_txt="";
		$valor = "";
		
		//Param name
		$valor = $_POST['name'];
		if (trim($valor)!=""){
			$this->setName($valor);
		}else{
			$error=true;
			$error_txt.="\\n- ".str_replace("$1",LBL_FIRST_NAME,MSG_REQUIRED_FIELD);
			$this->setName("");
		}
		
		//Param last_name_1
		$valor = $_POST['last_name_1'];
		if (trim($valor)!=""){
			$this->setPersonLastName1($valor);
		}else{
			$error=true;
			$error_txt.="\\n- ".str_replace("$1",LBL_LAST_NAME1,MSG_REQUIRED_FIELD);
			$this->setPersonLastName1("");
		}
		
        $this->setPersonLastName2($_POST["last_name_2"]);
		
		$this->setRegistrationDate($_POST["registration_date"]);
		
		
		//user_name
		//Miramos si tiene creado un usuario que no exista.
		if (isset($_POST["user_valido"]) && $_POST["user_valido"]=="NO EXISTE"){
		
			$this->setUserID($_POST["user_name"]);
			
		}else if (isset($_POST["user_valido"]) && $_POST["user_valido"]=="EXISTE"){
			if (isset($_POST["user_name"]) && $_POST["user_name"]!=""){
			$this->setUserID("");
			
			if ($this->existsUserID($_POST["user_name"])){
				$error_txt.="\\n- ".MSG_USERDATA_TEXT03;
			}
			}else{
				$error_txt.="\\n- ".MSG_USERDATA_TEXT04;
			}	
		}
		
		//user_email
		if (isset($_POST["user_email"]) && $_POST["user_email"]!=""){
			$this->setUserEmail($_POST["user_email"]);	
		}else{
			$this->setUserEmail("");	
			$error_txt.="\\n- ".MSG_EMAIL_TEXT01;
		}
		
		//user_lang
		$this->setUserLang($_POST["user_lang"]);	
		
		
		
		//npass y cpass
		//Si vienen ambos campos y ambos son iguales, cambiarle la contraseña también.
		if (isset($_POST["npass"]) && $_POST["npass"]!="" && isset($_POST["cpass"]) && $_POST["cpass"]!=""){
			
			if ($_POST["npass"]==$_POST["cpass"]){
				
				//Todo OK, podemos cambiar la contraseña.
			}else{
				$error_txt.="\\n- ".MSG_USERDATA_TEXT01; //La nueva contraseña y su confirmación deben ser iguales.
			}
			
			
		}else{
			if ($_POST["npass"]!="" || $_POST["cpass"]!="")
				$error_txt.="\\n- ".MSG_USERDATA_TEXT02; //Debe escribir la nueva contraseña y su confirmación
		}
		
		$this->setCardID($_POST["card_id"]);
		$this->setPhone($_POST["phone"]);
		$this->setMobile($_POST["mobile"]);
		$this->setBirthPlace($_POST["city"]); 
		
		$this->setCompanyID($_POST["company_id"]);
        $this->setRemarks($_POST["remarks"]);
		$this->setModifiedBy($_SESSION['user_id']);
		
		
		
		
		// lectura de los cursos moodle.
		// primero se lee el valor de la variable num_courses_moodle
		$num = $_POST["num_courses_moodle"];
		$vector = array();
		$i=0;
		$v=1;
		while($i<$num-1){
			$aux = array();
			$valor = $_POST["tutor_course_id_".$v];
			$aux[0] = $valor;
			
			$valor = $_POST["course_id_".$v];
			$aux[1] = $valor;
			
			$valor = $_POST["role_moodle_".$v];
			$aux[2] = $valor;
			
			$vector[$i] = $aux;
			$i++;
			$v++;
		}
		
		//Añado si hay uno nuevo al final.
		if (isset($_POST["course_id_0"]) && $_POST["course_id_0"]!="0"){
			$aux = array();
			$valor = $_POST["tutor_course_id_0"];
			$aux[0] = $valor;
			
			$valor = $_POST["course_id_0"];
			$aux[1] = $valor;
			
			$valor = $_POST["role_moodle_0"];
			$aux[2] = $valor;
			
			$vector[$i] = $aux;
		}
		//var_dump($vector);
		$this->setMoodleCourses($vector);
		
		return $error_txt;
	
   }//END init


    private function existsUserID($sUserID)
    {	
		global $mysqli;
		global $errorLog;
	
        $bExists = false;

        if (!isset($sUserID))
            return false;
		
		$query_select = "";
		$query_select = sprintf("SELECT * FROM ic_user WHERE UPPER(ic_user.user_id) = UPPER('%s')", $sUserID);
		
		$errorLog->LogDebug("SELECT: $query_select");
		
		if ($result = $mysqli->query($query_select)){
		
			if ($result->num_rows>0){
				$bExists = true;
			}
			$result->close();
		}

        return $bExists;
    }
	
	
	//Función que crea o modifica el registro en ic_user.
	
	public function updateUserID()
	{
		global $mysqli;
		global $errorLog;
		
		$user_id = $_POST["user_name"];
		$language = $_POST["user_lang"];
		$user_email = $_POST["user_email"];
		
		
		try {
		
			//Si no existe creamos nuevo
			if (!$this->existsUserID($user_id)){
				
				$mypass=$this->genRandomPassword();
				
				
					$query_insert=sprintf("INSERT INTO ic_user
											(user_id, 
											password, 
											language, 
											e_mail, 
											active_flag, 
											modified_by,
											timestamp) 
										 VALUES (
										  '%s',
										  '%s',
										  '%s',
										  '%s',
										  '%d',
										  '%s',
										  CURRENT_TIMESTAMP
										  )",
											$user_id,
											$this->encodePassword($mypass),
											$language,
											$user_email, 
											1,	
											$_SESSION["user_id"]
										);
					
					$errorLog->LogDebug("INSERT: $query_insert");
					
					$result = $mysqli->query($query_insert);
					
					if ($mysqli->error){
						$errorLog->LogError($mysqli->error);
					}
				
					//ENVIO POR CORREO DEL USUARIO.
					$mail = new Mail();
					
					$to=$user_email;
					$from=CFG_EMAIL_FROM_DEFAULT;
					$subject=MSG_SUBJECT_USER_PASSWORD;
					
					$contents=MSG_NEW_USER_TEXT_01;
					$contents.="\n\n".LBL_USER_ID.": ". $user_id;
					$contents.="\n".LBL_OLD_PASSWORD.": ". $newPass;
					$contents.="\n\n".MSG_USER_SIGNATURE;
					
					////send_mail($to, $from, $asunto, $contenido, $cc, $bcc)
					if ($subject!="" && $to!="" && $from!=""){
						$ok = $mail->send_mail($to, $from, $subject, $contents , "", "");
					}
				
				}else{ //Si existe lo modificamos el correo por si hubiera cambiado.
					
					$newPass="";
					
					if (isset($_POST["npass"]) && $_POST["npass"]!="" && isset($_POST["cpass"]) && $_POST["cpass"]!=""){
						if ($_POST["npass"]==$_POST["cpass"]){
							
							$newPass=$_POST["npass"];
						}
					}
					
					if ($newPass==""){
						$query_update=sprintf("UPDATE ic_user
												SET
													e_mail='%s',
													language='%s',
													timestamp=timestamp
												WHERE user_id = '%s'	
											 ",
												$user_email,
												$language,
												$user_id
											);
						
						$errorLog->LogDebug("UPDATE: $query_update");
						
						$result = $mysqli->query($query_update);
						
						if ($mysqli->error){
							$errorLog->LogError($mysqli->error);
						}
					}else if ($newPass!=""){
						$query_update=sprintf("UPDATE ic_user
												SET
													e_mail='%s',
													password='%s',
													language='%s',
													timestamp=timestamp
												WHERE user_id = '%s'	
											 ",
												$user_email,
												$this->encodePassword($newPass),
												$language,
												$user_id
											);
						
						$errorLog->LogDebug("UPDATE: $query_update");
						
						$result = $mysqli->query($query_update);
						
						if ($mysqli->error){
							$errorLog->LogError($mysqli->error);
						}
						
						//ENVIO POR CORREO DEL USUARIO.
						$mail = new Mail();
						
						$to=$user_email;
						$from=CFG_EMAIL_FROM_DEFAULT;
						$subject=MSG_SUBJECT_USER_PASSWORD;
						
						
						$contents=MSG_NEW_USER_TEXT_01;
						$contents.="\n\n".LBL_USER_ID.": ". $user_id;
						$contents.="\n".LBL_OLD_PASSWORD.": ". $newPass;
						$contents.="\n\n".MSG_USER_SIGNATURE;
						////send_mail($to, $from, $asunto, $contenido, $cc, $bcc)
						if ($subject!="" && $to!="" && $from!=""){
							$ok = $mail->send_mail($to, $from, $subject, $contents , "", "");
						}
					
					}

				}
			
		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}
	  
	}//END createNewUserID
  

	function encodePassword($pass)
	{
		return md5($pass);
	}
	
	function genRandomPassword()
	{
		$str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
		$cad = "";
		for($i=0;$i<8;$i++) {
			$cad .= substr($str,rand(0,62),1);
		}
		return $cad;
	}
	
	
	private function loadMoodleCourses()
	{
    
		global $mysqli;
		global $errorLog;		
		
        $query_select = sprintf("SELECT 
							ic_tutor_course.tutor_course_id,
							ic_tutor_course.course_id,
							ic_tutor_course.role_moodle
						FROM ic_tutor_course 
						WHERE ic_tutor_course.tutor_id = %d 
						ORDER BY tutor_course_id ", $this->getID());
						
		$errorLog->LogDebug("SELECT: $query_select");
        	
        $result = $mysqli->query($query_select);
		return $result;

	}//end loadMoodleCourses

	public function saveMoodleCourses($id) 
	{
		global $mysqli;
		global $errorLog;
		
		try {
			$vector = $this->getMoodleCourses();
			
			// Se borran todos primero para luego guardar los seleccionados
			$query_delete=sprintf("DELETE from ic_tutor_course WHERE tutor_id=%d", $id);

			$result = $mysqli->query($query_delete);
			$errorLog->LogDebug("DELETE: $query_delete");
			
			$contador=0;
			for($i=0; $i<count($vector); $i++){
				$aux = $vector[$i];
				
				if ($aux[0]!=null && $aux[0]!="" && $aux[0]!="0" && $aux[1]!=null && $aux[1]!="" && $aux[1]!="0" ){
					$query_insert = "INSERT INTO ic_tutor_course (tutor_id, course_id, role_moodle ) ".
									" VALUES ( ".$id.",'".$aux[1]."','".$aux[2]."') ";
					$errorLog->LogDebug("INSERT: $query_insert");
					$result = $mysqli->query($query_insert);
				}
				
			}
			
		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}		
	
	}//end saveMoodleCourses

	
	
	//Función que dado el user_id obtiene la ruta para la imagen del perfil en Moodle.
   	public function loadMoodleProfilePhoto($userid) 
	{
		global $mysqliMoodle;
		global $errorLog;
		
		$ruta="";
		
		try {
			$query_select= sprintf("SELECT 
										DISTINCT mdl_user.username, mdl_context.id FROM mdl_user
										INNER JOIN mdl_context ON mdl_user.id = mdl_context.instanceid
										WHERE contextlevel = 30 AND mdl_user.username = '%s' ",  $userid);
		
			$errorLog->LogDebug("SELECT: $query_select");
		
		 if ($result = $mysqliMoodle->query($query_select)){
				
				if ($result->num_rows > 0){
					$row = $result->fetch_array();
						
					$ruta = "/".$row["id"]."/user/icon/f1";
				}
		  }

		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}		
	
		return $ruta;
	}//loadMoodleProfilePhoto
	
	
	
	
	
	
	
	
}

?>