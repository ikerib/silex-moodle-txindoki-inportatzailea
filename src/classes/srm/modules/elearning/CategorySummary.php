<?php
// -----------------------------------------
// CategorySummary.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/classes/database/DB_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/AbstractCRMObjectSummary.php');

class CategorySummary extends AbstractCRMObjectSummary
{
	
	
   public function CategorySummary()  
   {
   }	

   public function getOrderByColumn()
   {
      $sOrderBy = $this->getOrderBy();
      
      if (null == $sOrderBy)
         return "ic_course_category.course_category_name";
		 
      try {
         switch( $sOrderBy) {
            case 2:  $sOrderBy = "ic_course_category.course_category_desc"; break;
			case 3:  $sOrderBy = "ic_course_category.course_category_shortname"; break;
			case 4:  $sOrderBy = "ic_course_category.order_appearance"; break;
			case 5:  $sOrderBy = "ic_course_category.language"; break;
			default: $sOrderBy = "ic_course_category.course_category_name";
         }
      } catch (Exception $ex) {
         return $sOrderBy;
      }
      return $sOrderBy;
   }//END
   
   
   
   
   /**
    * Crea una lista con los objetos que responden a las
    * restricciones establecidas en la petición.
    *
    * @param  oReq     petición
    * @throws          isyc.website.CRMException
    *                  si se produce algún error al obtener los datos.
    * @throws          NullPointerException
    *                  si la petición especificada es un valor <CODE>null</CODE>.
    * @since           CRM 1.0.0
    */
   public function  load()
   {
    
     $nCompanyID = $_SESSION["company_id"];
	 $sLanguage = $_SESSION["language"];
     
     global $mysqli;
	 global $errorLog;
	  
      try
      {

      $sFrom = " FROM ic_course_category ";
            
      $sWhere = sprintf(" WHERE ic_course_category.active_flag = 1 ");
      
      $sOperator = " = ";
      $sCondition = "";

	  // Param name
	  $sWhere.=$this->getWhereClause("name", "course_category_name", "ic_course_category", $this->STRING_TYPE, "", "");

	  // Param description
	  $sWhere.=$this->getWhereClause("description", "course_category_desc", "ic_course_category", $this->STRING_TYPE, "", "");

	  
	  $query_select = "SELECT COUNT(*) " . $sFrom . $sWhere;
	  
	  //echo $query_select;
	  
	  if ($result = $mysqli->query($query_select)){
			$row = $result->fetch_array();
			$this->init($row[0]);
			$result->close();
	  }else{
			$this->init(0);
	  }
		 
       $query_select="SELECT ic_course_category.course_category_id,
				  COALESCE(ic_course_category.course_category_name,'') as course_category_name,
				  COALESCE(ic_course_category.course_category_desc,'') as course_category_desc,
				  COALESCE(ic_course_category.course_category_shortname,'') as course_category_shortname,
				  ic_course_category.language,
				  ic_course_category.order_appearance  ";
       $query_select.= $sFrom . $sWhere;
       $query_select.= " ORDER BY " . $this->getOrderByColumn() . " " . $this->getOrderHowString() . " ";
       $query_select.= " LIMIT " . $this->getMaxRowsNumber() . " OFFSET " . ($this->getCurrentPage()-1)*$this->getMaxRowsNumber();
		
       //echo $query_select;
	   
       $result = $mysqli->query($query_select);
	   
	   return $result;
		
      }
      catch (Exception $ex)
      {
           
      }
      
   }//end load
   
   
   
   
   
   
   /**
    * Crea una lista con los objetos que responden a las
    * restricciones establecidas en la petición.
    *
    * @param  sType    tipo de filtrado
    * @param  sID      clave de filtrado
    * @param  oReq     petición
    * @throws          isyc.website.CRMException
    *                  si se produce algún error al obtener los datos.
    * @throws          NullPointerException
    *                  si alguno de los parámetros es un valor <CODE>null</CODE>.
    * @since           CRM 1.0.0
    */
   public function loadFilter($sType, $sID)
   {
   
		
   
   
   }
   
   
   
  
	
	
}

?>