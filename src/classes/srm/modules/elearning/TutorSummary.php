<?php
// -----------------------------------------
// TutorSummary.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/classes/database/DB_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/AbstractCRMObjectSummary.php');

class TutorSummary extends AbstractCRMObjectSummary
{
	
	
   public function TutorSummary()  
   {
   }	

   public function getOrderByColumn()
   {
      $sOrderBy = $this->getOrderBy();
      
      if (null == $sOrderBy)
         return "ic_tutor.first_name";
		 
      try {
         switch( $sOrderBy) {
            case 2:  $sOrderBy = "ic_tutor.last_name1"; break;
            case 3:  $sOrderBy = "ic_tutor.user_id"; break;
            case 4:  $sOrderBy = "ic_tutor.registration_date"; break;
			default: $sOrderBy = "ic_tutor.first_name";
         }
      } catch (Exception $ex) {
         return $sOrderBy;
      }
      return $sOrderBy;
   }//END
   
   
   
   
   /**
    * Crea una lista con los objetos que responden a las
    * restricciones establecidas en la petición.
    *
    * @param  oReq     petición
    * @throws          isyc.website.CRMException
    *                  si se produce algún error al obtener los datos.
    * @throws          NullPointerException
    *                  si la petición especificada es un valor <CODE>null</CODE>.
    * @since           CRM 1.0.0
    */
	public function  load()
	{
    
		$nCompanyID = $_SESSION["company_id"];
		$sLanguage = $_SESSION["language"];
     
		global $mysqli;
		global $errorLog;
	  
		try
		{

			$sFrom = " FROM ic_tutor ";
				
			$sWhere = " WHERE ic_tutor.active_flag = 1 ";
			
			$sOperator = " = ";
			$sCondition = "";

			// Param name
			$sWhere.=$this->getWhereClause("name", "first_name", "ic_tutor", $this->STRING_TYPE, "", "");

			// param last_name_1
			$sWhere.=$this->getWhereClause("last_name_1", "last_name1", "ic_tutor", $this->STRING_TYPE, "", "");
			
			// parametro registration_date
			$sWhere.=$this->getWhereClause("registration_date", "registration_date", "ic_tutor", $this->DATE_TYPE, "", "");
			
			$query_select = "SELECT COUNT(*) " . $sFrom . $sWhere;
		  
			//echo $query_select;
		  
			if ($result = $mysqli->query($query_select)){
				$row = $result->fetch_array();
				$this->init($row[0]);
				$result->close();
			}else{
				$this->init(0);
			}
			 
			$query_select="SELECT ic_tutor.tutor_id,
						ic_tutor.first_name,
						ic_tutor.last_name1,
						ic_tutor.user_id,
						DATE_FORMAT(ic_tutor.registration_date,'%Y/%m/%d') as registration_date ";
						
			$query_select.= $sFrom . $sWhere;
			$query_select.= " ORDER BY " . $this->getOrderByColumn() . " " . $this->getOrderHowString() . " ";
			$query_select.= " LIMIT " . $this->getMaxRowsNumber() . " OFFSET " . ($this->getCurrentPage()-1)*$this->getMaxRowsNumber();
			
			//echo $query_select;
		   
			$result = $mysqli->query($query_select);
		   
			return $result;
		
		}
		catch (Exception $ex)
		{
           
		}
      
	  
      
   }//end load
   
   
   
   
   
   
   public function  loadFilter($sType, $sID)
	{
    
		$nCompanyID = $_SESSION["company_id"];
		$sLanguage = $_SESSION["language"];
     
		global $mysqli;
		global $errorLog;
	  
		try
		{

			$sFrom = " FROM ic_tutor ";
				
			$sWhere = " WHERE ic_tutor.active_flag = 1 ";
			$sWhere .= " AND ic_tutor.company_id = ".$nCompanyID;
			
			$sOperator = " = ";
			$sCondition = "";

			// Param name
			$sWhere.=$this->getWhereClause("name", "student_first_name", "ic_tutor", $this->STRING_TYPE, "", "");

			// param last_name_1
			$sWhere.=$this->getWhereClause("last_name_1", "student_last_name1", "ic_tutor", $this->STRING_TYPE, "", "");
			
			if ($sType=="GROUP")
			{
				$sWhere.=" AND ic_tutor.group_id = " . $sID ;
			}  
			
			
			
			
	  
			$query_select = "SELECT COUNT(*) " . $sFrom . $sWhere;
		  
			//echo $query_select;
		  
			if ($result = $mysqli->query($query_select)){
				$row = $result->fetch_array();
				$this->init($row[0]);
				$result->close();
			}else{
				$this->init(0);
			}
			 
			$query_select="SELECT ic_tutor.tutor_id,
						ic_tutor.first_name,
						ic_tutor.last_name1,
						ic_tutor.user_id,
						DATE_FORMAT(ic_tutor.registration_date,'%Y/%m/%d') as registration_date ";
						
			$query_select.= $sFrom . $sWhere;
			$query_select.= " ORDER BY " . $this->getOrderByColumn() . " " . $this->getOrderHowString() . " ";
			$query_select.= " LIMIT " . $this->getMaxRowsNumber() . " OFFSET " . ($this->getCurrentPage()-1)*$this->getMaxRowsNumber();
			
			//echo $query_select;
		   
			$result = $mysqli->query($query_select);
		   
			return $result;
		
		}
		catch (Exception $ex)
		{
           
		}
      
	  
      
   }//end loadFilter
   
	
	
}

?>