<?php
// -----------------------------------------
// StudentData.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/AbstractCRMPersonData.php');

class StudentData extends AbstractCRMPersonData
{

	public $isUser;
	public $userID;
	public $userPass;
	public $userLang;
	public $userEmail;
	public $moRegistrationDate = "";

	public $msPictogram1;
	public $msPictogram2;
	public $msPictogram3;

	public $mnLevel;
	public $mnCourseParticularID;

	public $mnLocution;
	public $mnHighContrast;

	public $oAssignedCourses = "";
	public $oNoAssignedCourses = "";

	public $msInternalID;
	public $msEmpresa;
	public $msSeccion;


	public function getIsUser(){
		return $this->isUser;
	}

	public function setIsUser($value){
		$this->isUser = $value;
	}

	public function getUserID(){
		return $this->userID;
	}

	public function setUserID($value){
		$this->userID = $value;
	}

	public function getUserLang(){
		return $this->userLang;
	}

	public function setUserLang($value){
		$this->userLang = $value;
	}

	public function getUserEmail(){
		return $this->userEmail;
	}

	public function setUserEmail($value){
		$this->userEmail = $value;
	}

	public function getRegistrationDate(){
		return $this->moRegistrationDate;
	}
	public function setRegistrationDate($value){
		$this->moRegistrationDate = $value;
	}


	public function getPictogram1(){
		return $this->msPictogram1;
	}
	public function setPictogram1($nValue){
		$this->msPictogram1 = $nValue;
	}

	public function getPictogram2(){
		return $this->msPictogram2;
	}
	public function setPictogram2($nValue){
		$this->msPictogram2 = $nValue;
	}
	public function getPictogram3(){
		return $this->msPictogram3;
	}
	public function setPictogram3($nValue){
		$this->msPictogram3 = $nValue;
	}

	public function getLevel(){
		return $this->mnLevel;
	}
	public function setLevel($nValue){
		$this->mnLevel = $nValue;
	}

	public function getCourseParticularID(){
		return $this->mnCourseParticularID;
	}
	public function setCourseParticularID($nValue){
		$this->mnCourseParticularID = $nValue;
	}

	public function getLocution(){
		return $this->mnLocution;
	}
	public function setLocution($nValue){
		$this->mnLocution = $nValue;
	}
	public function getHighContrast(){
		return $this->mnHighContrast;
	}
	public function setHighContrast($nValue){
		$this->mnHighContrast = $nValue;
	}




	public function getAssignedCourses(){
		return $this->oAssignedCourses;
	}
	public function setAssignedCourses($value){
		$this->oAssignedCourses = $value;
	}

	public function getNoAssignedCourses(){
		return $this->oNoAssignedCourses;
	}
	public function setNoAssignedCourses($value){
		$this->oNoAssignedCourses = $value;
	}

	public function getInternalID(){
		return $this->msInternalID;
	}
	public function setInternalID($nValue){
		$this->msInternalID = $nValue;
	}

	public function getEmpresa(){
		return $this->msEmpresa;
	}
	public function setEmpresa($nValue){
		$this->msEmpresa = $nValue;
	}

	public function getSeccion(){
		return $this->msSeccion;
	}
	public function setSeccion($nValue){
		$this->msSeccion = $nValue;
	}







}

?>