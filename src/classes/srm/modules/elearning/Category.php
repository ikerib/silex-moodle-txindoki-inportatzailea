<?php
// -----------------------------------------
// Category.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/classes/database/DB_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/elearning/CategoryData.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/Functions.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/admin/Attachment.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/admin/Picture.php');

class Category extends CategoryData
{
	
	public function loadData ($id){
		
	  global $errorLog;
	  
	  if (null == $id || "new"==$id)
      {
			$this->setID("new");
			$this->setName("");
			$this->setLanguage("");
			$this->setDescription("");	
			$this->setShortname("");
			$this->setOrderAppearence(1);
			
			$this->setPictureFile("");
			$this->setPictureName("");
			$this->setPictureWidth("");
			$this->setPictureHeight("");
			
			$this->setPictureFileAC("");
			$this->setPictureNameAC("");
			$this->setPictureWidthAC("");
			$this->setPictureHeightAC("");
			
			$this->setCompanyID(1);
			$this->setRemarks("");
			$this->setActiveFlag("");
			$this->setModifiedBy("");
			$this->setTimestamp("");
			return;
      }
      
      global $mysqli;
	  $format = new Format();	
	  try {
		$query_select= sprintf("SELECT ic_course_category.course_category_id,  
                     	ic_course_category.course_category_name,
                     	ic_course_category.course_category_desc,

						ic_course_category.language,
						
						ic_course_category.course_category_shortname,
						ic_course_category.order_appearance,
						
						ic_course_category.picture_name,
						ic_course_category.picture_file,
						ic_course_category.picture_width,
						ic_course_category.picture_height,
						
						ic_course_category.picture_name_ac,
						ic_course_category.picture_file_ac,
						ic_course_category.picture_width_ac,
						ic_course_category.picture_height_ac,
						
						ic_course_category.company_id,
						ic_course_category.remarks,
						ic_course_category.active_flag,
						ic_course_category.modified_by,
						ic_course_category.timestamp
						
                 FROM ic_course_category 
							    
                 WHERE  active_flag=1 AND
						course_category_id = %d", $id);
		
		$errorLog->LogDebug("SELECT: $query_select");
		
		 if ($result = $mysqli->query($query_select)){
				
				if ($result->num_rows > 0){
					$row = $result->fetch_array();
				
					$this->setID($row["course_category_id"]);
					$this->setName($row["course_category_name"]);
					$this->setLanguage($row["language"]);
					$this->setDescription($row["course_category_desc"]);
					$this->setShortname($row["course_category_shortname"]);
					$this->setOrderAppearence($row["order_appearance"]);
					
					$this->setPictureName($row["picture_name"]);
					$this->setPictureFile($row["picture_file"]);
					$this->setPictureWidth($row["picture_width"]);
					$this->setPictureHeight($row["picture_height"]);
					
					$this->setPictureNameAC($row["picture_name_ac"]);
					$this->setPictureFileAC($row["picture_file_ac"]);
					$this->setPictureWidthAC($row["picture_width_ac"]);
					$this->setPictureHeightAC($row["picture_height_ac"]);
					
					$this->setCompanyID($row["company_id"]);
					$this->setRemarks($row["remarks"]);
					$this->setActiveFlag($row["active_flag"]);
					$this->setModifiedBy($row["modified_by"]);
					$this->setTimestamp($row["timestamp"]);
					
					$ruta_upload = str_replace("$1",$this->getCompanyID(),CFG_PICTURE_CATEGORY_URL);
					$this->setPictureFileExists(file_exists($ruta_upload.$this->getID() . "_" . $this->getPictureFile()));
					$this->setThumbnailFileExists(file_exists($ruta_upload.$this->getID() . "_th_" . $this->getPictureFile()));
					
					$ruta_upload = str_replace("$1",$this->getCompanyID(),CFG_PICTURE_CATEGORY_URL_AC);
					$this->setPictureFileACExists(file_exists($ruta_upload.$this->getID() . "_" . $this->getPictureFileAC()));
					$this->setThumbnailFileACExists(file_exists($ruta_upload.$this->getID() . "_th_" . $this->getPictureFileAC()));
					
					
					$result->close();
				}else{
					$errorLog->LogError("Location: /srm/index.php?error=3. ERROR: ".$query_select);
					header("Location: /srm/index.php?error=3"); 	
					exit;
				}	
				
			}		 
		
	  } catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
	  }
	
	}//end loadData
	
	
	
   public function createNew()
   {
	  
	  global $errorLog;
	  global $mysqli;
	  $error="";
	  
	  $format = new Format();	  
	 	
      try {
		
			$query_insert=sprintf("INSERT INTO ic_course_category 
								    (
									course_category_name, 
									course_category_desc, 
									
									language,
									
									course_category_shortname, 
									order_appearance, 
									
									company_id,
									active_flag,
									remarks, 
									modified_by, 
									timestamp) 
								  VALUES (
								  '%s',
								  '%s',
								  
								  '%s',
								  
								  '%s',
								  %d,
								  
								  %d,
								  1,
								  '%s',
								  '%s',
								  CURRENT_TIMESTAMP
								  )",
									$this->getName(),
									$this->getDescription(),
									
									$this->getLanguage(),
									
									$this->getShortname(),
									$this->getOrderAppearence(),
									
									$this->getCompanyID(),
									$this->getRemarks(),
									$this->getModifiedBy()									
								);
			
			$errorLog->LogDebug("INSERT: $query_insert");
			
			$result = $mysqli->query($query_insert);
			
			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}
			
			//Calculo el último ID insertado.
			$query_max = "SELECT LAST_INSERT_ID() AS max_id";
			if( $result = $mysqli->query($query_max) ){
				$row = $result->fetch_array();
				
				$this->setID($row["max_id"]);
				
				$result->close();
			}
			
			
			///Sube la imágen.
			
			//echo "this->getUpload() = ".$this->getUpload();
			
			if ($this->getUpload()=="yes"){
				$upload = $this->uploadPicture($this->getID(), $this->getCompanyID());
				$error=$upload;
			}
			
		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}
		
		return $error;
	  
   }//END create_new
   
	
	
   public function update($id)
   {
	  global $mysqli;
	  global $errorLog;
	  $error="";
	  
	  $format = new Format();
		
      try {
		
			$query_update=sprintf("UPDATE ic_course_category 
								SET course_category_name='%s', 
									course_category_desc='%s',
									
									language='%s',
									
									course_category_shortname='%s',
									order_appearance=%d,
									
									company_id=%d, 
									remarks='%s', 
									modified_by='%s', 
									timestamp=CURRENT_TIMESTAMP
							WHERE 
									course_category_id = %d",

									$this->getName(),									
									$this->getDescription(),
									
									$this->getLanguage(),
									
									$this->getShortname(),
									$this->getOrderAppearence(),
									
									$this->getCompanyID(), 
									$this->getRemarks(),
									$_SESSION["user_id"],
									$id
    							);
			
			
			$errorLog->LogDebug("UPDATE: $query_update");
			$result = $mysqli->query($query_update);
			
			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}
			
			
			
			
			
			///Sube la imágen.
			if ($this->getUpload()=="yes"){
				$upload = $this->uploadPicture($this->getID(), $this->getCompanyID());
					
				$error=$upload;
			}
			
			
			///Sube la imágen.
			if ($this->getUploadAC()=="yes"){
				$upload = $this->uploadPictureAC($this->getID(), $this->getCompanyID());
					
				$error=$upload;
			}
			
			$ruta_upload = str_replace("$1",$this->getCompanyID(),CFG_PICTURE_CATEGORY_URL);
			
			//Borramos la anterior imágen si estamos subiendo una nueva, solamente si no hay error al intentar subirla.
			if ($this->getUpload()=="yes" && $error==""){
				if (file_exists($ruta_upload.$this->getID() . "_" . $this->getPictureFileOld())){
					unlink($ruta_upload.$this->getID() . "_" . $this->getPictureFileOld()); 
				}
				if (file_exists($ruta_upload.$this->getID() . "_th_" . $this->getPictureFileOld())){
					unlink($ruta_upload.$this->getID() . "_th_" . $this->getPictureFileOld()); 
				}
			}
			
			$ruta_upload = str_replace("$1",$this->getCompanyID(),CFG_PICTURE_CATEGORY_URL_AC);
			
			//Borramos la anterior imágen si estamos subiendo una nueva, solamente si no hay error al intentar subirla.
			if ($this->getUploadAC()=="yes" && $error==""){
				if (file_exists($ruta_upload.$this->getID() . "_" . $this->getPictureFileOldAC())){
					unlink($ruta_upload.$this->getID() . "_" . $this->getPictureFileOldAC()); 
				}
				if (file_exists($ruta_upload.$this->getID() . "_th_" . $this->getPictureFileOldAC())){
					unlink($ruta_upload.$this->getID() . "_th_" . $this->getPictureFileOldAC()); 
				}
			}
			
			
		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}
		
		return $error;
		
   }//END UPDATE
	
   

   public function delete($id)
   {
	 global $mysqli;
     global $errorLog;
	 
      try {
		
			$query_delete=sprintf("UPDATE ic_course_category 
								SET active_flag=0, 
									modified_by='%s', 
									timestamp=CURRENT_TIMESTAMP
							WHERE 
									course_category_id = %d",

									$_SESSION["user_id"],
									$_POST['id']
    							);
			
			$errorLog->write_error("DELETE: ".$query_delete);
			
			$result = $mysqli->query($query_delete);
			
			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}
			
			
			//Al borrar el curso borrar otro objetos:
			
			//Attachment relacionados
			$objAttachment = new Attachment();
			$objAttachment->deleteRelated(102,$id);
			
			//Pictures relacionados
			$objPicture = new Picture();
			$objPicture->deleteRelated(102,$id);
			
		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}
		
			  
   }//END DELETE    
	





   public function init()
   {
		 global $errorLog;
		 
		$error=false;
		$error_txt="";
		$valor = "";
		
		
		//Param id
		$valor = $_POST['id'];
		if (trim($valor)!=""){
			$this->setID($valor);
		}else{
			$this->setID("new");
		}
		
		//Param name
		$valor = $_POST['name'];
		if (trim($valor)!=""){
			$this->setName(trim($valor));
		}		
		if ($this->getName()=="" && $this->getName()==""){
			$error=true;
			$error_txt.="\\n- ".str_replace("$1",LBL_CATEGORY,MSG_REQUIRED_FIELD);
		}

		//Param language
		$this->setLanguage($_POST['language']);	
		
		//Param description
		$this->setDescription($_POST['description']);	
		
		//Param shortname
		$valor = $_POST['shortname'];
		if (trim($valor)!=""){
			$this->setShortname($valor);
		}else{
			$error=true;
			$error_txt.="\\n- ".str_replace("$1",LBL_SHORTNAME,MSG_REQUIRED_FIELD);
			$this->setShortname("");
		}
		
		//Param order_appearance
		$valor = $_POST['order_appearance'];
		if (trim($valor)!=""){
			$this->setOrderAppearence($valor);
		}else{
			$error=true;
			$error_txt.="\\n- ".str_replace("$1",LBL_ORDER_APPEARANCE,MSG_REQUIRED_FIELD);
			$this->setOrderAppearence("");
		}
		
		$valor = $_POST['bupload_image'];
		$this->setUpload($valor);
		
		$valor = $_POST['picture_file_old'];
		$this->setPictureFileOld($valor);
		
		$valor = $_POST['bupload_image_ac'];
		$this->setUploadAC($valor);
		
		$valor = $_POST['picture_file_old_ac'];
		$this->setPictureFileOldAC($valor);
		
		
		$errorLog->LogDebug("-->1. ".$_POST['bupload_image_ac']);
		$errorLog->LogDebug("-->2. ".$_POST['picture_file_old_ac']);
		
		
		$this->setCompanyID($_POST['company_id']);
		$this->setRemarks($_POST['remarks']);
		$this->setModifiedBy($_SESSION['user_id']);
		return $error_txt;
	
   }//END init

   
   
   

    
//Función que sube o modifica una imagen existente en la ruta indicada.   
   
public function uploadPicture($picture_id, $company_id){
		
		 global $errorLog;
		
		$logo_error=false;
		$logo_error_num=0;
		$archivo_final="";
		
		$ruta_upload = str_replace("$1",$company_id,CFG_PICTURE_CATEGORY_URL);
	
		if (is_uploaded_file($_FILES['picture']['tmp_name'])) {
			
			$archivo_name= $_FILES['picture']['name'];
			$archivo_size= $_FILES['picture']['size'];
			$archivo_type=  $_FILES['picture']['type'];
			$archivo= $_FILES['picture']['tmp_name'];
			
			$errorLog->LogInfo("archivo_name=".$archivo_name);
			$errorLog->LogInfo("archivo_size=".$archivo_size);
			$errorLog->LogInfo("archivo_type=".$archivo_type);
			$errorLog->LogInfo("archivo=".$archivo);
			
			$lim_tamano= $_POST['lim_tamano'];
			
			$errorLog->LogInfo("archivo=".$lim_tamano);
			
			$extension = explode(".",$archivo_name);
			$num = count($extension)-1;
			if(($extension[$num] == "jpg" || $extension[$num] == "gif" || $extension[$num] == "png" )){
				
				if ($extension[$num] == "jpg"){
					$archivo_final=$picture_id."_".$archivo_name;
				}
				
				if ($extension[$num] == "gif"){
					$archivo_final=$picture_id."_".$archivo_name;
				}
				
				if ($extension[$num] == "png"){
					$archivo_final=$picture_id."_".$archivo_name;
				}
				
				//echo "<br>IMAGEN BUENA";
				
				$tamano = getimagesize($archivo);
				$errorLog->LogInfo("alto=".$tamano[1]);
				$errorLog->LogInfo("ancho=".$tamano[0]);
				
				//Valida las dimensiones del logotipo.
				if (($tamano[0]>CFG_PICTURE_CATEGORY_WIDTH || $tamano[1]>CFG_PICTURE_CATEGORY_HEIGHT)){
					
					$logo_error=true;
					$logo_error_num=2;
				}
				
				if (!$logo_error){
					
					$errorLog->LogInfo("archivo_name=$archivo_name");
					$errorLog->LogInfo("archivo_size=$archivo_size");
					$errorLog->LogInfo("lim_tamano=$lim_tamano");
					
					$errorLog->LogInfo("ruta_upload=".$ruta_upload.$archivo_final);
					
					if ($archivo_name != "none" AND $archivo_size != 0 AND $archivo_size<=$lim_tamano){
						if (copy ($archivo, $ruta_upload.$archivo_final)) {
						  $logo_error_num=100;
						}
					}else{
						$logo_error=true;
						$logo_error_num=3;
					}
				}
				
			}
				
		}else{
			$logo_error=true;
			$logo_error_num=1;
		}
		
		$error_msg="";
		if ($logo_error_num==1){
			$error_msg="";
		}else if ($logo_error_num==2){
			$error_msg= 
				str_replace("$4", CFG_PICTURE_CATEGORY_HEIGHT,
				str_replace("$3", CFG_PICTURE_CATEGORY_WIDTH,
				str_replace("$2", $tamano[1],str_replace("$1",$tamano[0],MSG_INFO_BIG_SIZE))));
		}else if ($logo_error_num==3){
			$error_msg= 
				str_replace("$2", CFG_SIZE_CATEGORY_BYTES,str_replace("$1",$archivo_size,MSG_INFO_LARGE));
		}
		
		$errorLog->LogInfo("error_msg = $error_msg");
		$errorLog->LogInfo("logo_error_num = $logo_error_num");
		
		if ($logo_error_num==0 || $logo_error_num==100){ //si no hay error guardo la imagen.
		
			//Reduzco la imagen y la guardo. Thumbnail.
			
			
			
			$archivo_final = $ruta_upload.$archivo_final;
			
			$original = "";
			if (preg_match('/.png$/', $archivo_final)) {
				$original = imagecreatefrompng($archivo_final);
			} else if (preg_match('/.gif$/', $archivo_final)) {
				$original = imagecreatefromgif($archivo_final);
			} else if (preg_match('/.jpg$/', $archivo_final)) {
				$original = imagecreatefromjpeg($archivo_final);
			}
			
			
			$ancho = imagesx($original);
			$alto = imagesy($original);
			
			$ratio=1;
			$aux_height=1;
			$aux_width=1;
			
			$thumb=""; 
			
			try {
				if ($ancho>=$alto){
					$ratio = $ancho/CFG_TH_PICTURE_CATEGORY_WIDTH;
					$aux_height = $alto/$ratio;
					
					$thumb = imagecreatetruecolor(CFG_TH_PICTURE_CATEGORY_WIDTH,$aux_height); // Lo haremos de un tamaño 150x150

					imagecopyresampled($thumb,$original,0,0,0,0,CFG_TH_PICTURE_CATEGORY_WIDTH,$aux_height,$ancho,$alto);
				
				}else{
					$ratio = $alto/CFG_TH_PICTURE_CATEGORY_HEIGHT;
					$aux_width = $ancho/$ratio;
					
					$thumb = imagecreatetruecolor($aux_width, CFG_TH_PICTURE_CATEGORY_HEIGHT); // Lo haremos de un tamaño 150x150
		
					imagecopyresampled($thumb,$original,0,0,0,0,$aux_width,CFG_TH_PICTURE_CATEGORY_HEIGHT,$ancho,$alto);
				}
					
				
				
				
				if (preg_match('/.png$/', $archivo_final)) {
					imagepng($thumb,$ruta_upload.$picture_id."_th_".$archivo_name); // Suprimida la calidad de compresión daba error.
				} else if (preg_match('/.gif$/', $archivo_final)) {
					imagegif($thumb,$ruta_upload.$picture_id."_th_".$archivo_name,90); // 90 es la calidad de compresión
				} else if (preg_match('/.jpg$/', $archivo_final)) {
					imagejpeg($thumb,$ruta_upload.$picture_id."_th_".$archivo_name,90); // 90 es la calidad de compresión
				}
			
			} catch (Exception $e) {
				$errorLog->LogFatal("Caught exception: ". $e->getMessage());
			}
			
			//Save image...
			global $mysqli;
	   
			try {
			
				$query_update=sprintf("UPDATE ic_course_category 
									SET 
										picture_file='%s', 
										picture_width='%s', 
										picture_height='%s', 
										timestamp=CURRENT_TIMESTAMP
								WHERE 
										course_category_id = %d",
										
										$archivo_name,
										$tamano[0],
										$tamano[1],
										$picture_id
									);
				
				$errorLog->LogDebug("".$query_update);
				$result = $mysqli->query($query_update);
				
				if ($mysqli->error){
					$errorLog->LogFatal("Caught exception: ". $e->getMessage());
				}
				
				/*
				
					Borrar las imágenes
				
				*/
				
				
			} catch (Exception $e) {
				$errorLog->LogFatal("Caught exception: ". $e->getMessage());
			}
			
		}else{
		
			return $error_msg;
		}
		

}//end upload_picture
      
   
   
   
   

//Función que sube o modifica una imagen en ALTO CONTRASTE existente en la ruta indicada.   
   
public function uploadPictureAC($picture_id, $company_id){
		
		 global $errorLog;
		
		$logo_error=false;
		$logo_error_num=0;
		$archivo_final="";
		
		$ruta_upload = str_replace("$1",$company_id,CFG_PICTURE_CATEGORY_URL_AC);
	
		if (is_uploaded_file($_FILES['picture_ac']['tmp_name'])) {
			
			$archivo_name= $_FILES['picture_ac']['name'];
			$archivo_size= $_FILES['picture_ac']['size'];
			$archivo_type=  $_FILES['picture_ac']['type'];
			$archivo= $_FILES['picture_ac']['tmp_name'];
			
			$errorLog->LogInfo("archivo_name=".$archivo_name);
			$errorLog->LogInfo("archivo_size=".$archivo_size);
			$errorLog->LogInfo("archivo_type=".$archivo_type);
			$errorLog->LogInfo("archivo=".$archivo);
			
			$lim_tamano= $_POST['lim_tamano_ac'];
			
			$errorLog->LogInfo("archivo=".$lim_tamano);
			
			$extension = explode(".",$archivo_name);
			$num = count($extension)-1;
			if(($extension[$num] == "jpg" || $extension[$num] == "gif" || $extension[$num] == "png" )){
				
				if ($extension[$num] == "jpg"){
					$archivo_final=$picture_id."_".$archivo_name;
				}
				
				if ($extension[$num] == "gif"){
					$archivo_final=$picture_id."_".$archivo_name;
				}
				
				if ($extension[$num] == "png"){
					$archivo_final=$picture_id."_".$archivo_name;
				}
				
				//echo "<br>IMAGEN BUENA";
				
				$tamano = getimagesize($archivo);
				$errorLog->LogInfo("alto=".$tamano[1]);
				$errorLog->LogInfo("ancho=".$tamano[0]);
				
				//Valida las dimensiones del logotipo.
				if (($tamano[0]>CFG_PICTURE_CATEGORY_WIDTH_AC || $tamano[1]>CFG_PICTURE_CATEGORY_HEIGHT_AC)){
					
					$logo_error=true;
					$logo_error_num=2;
				}
				
				if (!$logo_error){
					
					$errorLog->LogInfo("archivo_name=$archivo_name");
					$errorLog->LogInfo("archivo_size=$archivo_size");
					$errorLog->LogInfo("lim_tamano=$lim_tamano");
					
					$errorLog->LogInfo("ruta_upload=".$ruta_upload.$archivo_final);
					
					if ($archivo_name != "none" AND $archivo_size != 0 AND $archivo_size<=$lim_tamano){
						if (copy ($archivo, $ruta_upload.$archivo_final)) {
						  $logo_error_num=100;
						}
					}else{
						$logo_error=true;
						$logo_error_num=3;
					}
				}
				
			}
				
		}else{
			$logo_error=true;
			$logo_error_num=1;
		}
		
		$error_msg="";
		if ($logo_error_num==1){
			$error_msg="";
		}else if ($logo_error_num==2){
			$error_msg= 
				str_replace("$4", CFG_PICTURE_CATEGORY_HEIGHT_AC,
				str_replace("$3", CFG_PICTURE_CATEGORY_WIDTH_AC,
				str_replace("$2", $tamano[1],str_replace("$1",$tamano[0],MSG_INFO_BIG_SIZE_AC))));
		}else if ($logo_error_num==3){
			$error_msg= 
				str_replace("$2", CFG_SIZE_CATEGORY_BYTES_AC,str_replace("$1",$archivo_size,MSG_INFO_LARGE_AC));
		}
		
		$errorLog->LogInfo("error_msg = $error_msg");
		$errorLog->LogInfo("logo_error_num = $logo_error_num");
		
		if ($logo_error_num==0 || $logo_error_num==100){ //si no hay error guardo la imagen.
		
			//Reduzco la imagen y la guardo. Thumbnail.
			
			
			
			$archivo_final = $ruta_upload.$archivo_final;
			
			$original = "";
			if (preg_match('/.png$/', $archivo_final)) {
				$original = imagecreatefrompng($archivo_final);
			} else if (preg_match('/.gif$/', $archivo_final)) {
				$original = imagecreatefromgif($archivo_final);
			} else if (preg_match('/.jpg$/', $archivo_final)) {
				$original = imagecreatefromjpeg($archivo_final);
			}
			
			
			$ancho = imagesx($original);
			$alto = imagesy($original);
			
			$ratio=1;
			$aux_height=1;
			$aux_width=1;
			
			$thumb=""; 
			
			try {
				if ($ancho>=$alto){
					$ratio = $ancho/CFG_TH_PICTURE_CATEGORY_WIDTH_AC;
					$aux_height = $alto/$ratio;
					
					$thumb = imagecreatetruecolor(CFG_TH_PICTURE_CATEGORY_WIDTH_AC,$aux_height); // Lo haremos de un tamaño 150x150

					imagecopyresampled($thumb,$original,0,0,0,0,CFG_TH_PICTURE_CATEGORY_WIDTH_AC,$aux_height,$ancho,$alto);
				
				}else{
					$ratio = $alto/CFG_TH_PICTURE_CATEGORY_HEIGHT_AC;
					$aux_width = $ancho/$ratio;
					
					$thumb = imagecreatetruecolor($aux_width, CFG_TH_PICTURE_CATEGORY_HEIGHT_AC); // Lo haremos de un tamaño 150x150
		
					imagecopyresampled($thumb,$original,0,0,0,0,$aux_width,CFG_TH_PICTURE_CATEGORY_HEIGHT_AC,$ancho,$alto);
				}
					
				
				
				
				if (preg_match('/.png$/', $archivo_final)) {
					imagepng($thumb,$ruta_upload.$picture_id."_th_".$archivo_name); // Suprimida la calidad de compresión daba error.
				} else if (preg_match('/.gif$/', $archivo_final)) {
					imagegif($thumb,$ruta_upload.$picture_id."_th_".$archivo_name,90); // 90 es la calidad de compresión
				} else if (preg_match('/.jpg$/', $archivo_final)) {
					imagejpeg($thumb,$ruta_upload.$picture_id."_th_".$archivo_name,90); // 90 es la calidad de compresión
				}
			
			} catch (Exception $e) {
				$errorLog->LogFatal("Caught exception: ". $e->getMessage());
			}
			
			//Save image...
			global $mysqli;
	   
			try {
			
				$query_update=sprintf("UPDATE ic_course_category 
									SET 
										picture_file_ac='%s', 
										picture_width_ac='%s', 
										picture_height_ac='%s', 
										timestamp=CURRENT_TIMESTAMP
								WHERE 
										course_category_id = %d",
										
										$archivo_name,
										$tamano[0],
										$tamano[1],
										$picture_id
									);
				
				$errorLog->LogDebug("".$query_update);
				$result = $mysqli->query($query_update);
				
				if ($mysqli->error){
					$errorLog->LogFatal("Caught exception: ". $e->getMessage());
				}
				
				/*
				
					Borrar las imágenes
				
				*/
				
				
			} catch (Exception $e) {
				$errorLog->LogFatal("Caught exception: ". $e->getMessage());
			}
			
		}else{
		
			return $error_msg;
		}
		

}//end upload_picture   
   
   
   
   
   
}

?>