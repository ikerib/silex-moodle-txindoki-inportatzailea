<?php
// -----------------------------------------
// Course.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/classes/database/DB_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/elearning/CourseData.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/Functions.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/admin/Attachment.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/admin/Picture.php');

class Course extends CourseData
{
	
	public function loadData ($id){
		
	  global $errorLog;
	  
	  if (null == $id || "new"==$id)
      {
			$this->setID("new");
			$this->setName("");
			$this->setDescription("");	
			
			$this->setPictureFile("");
			$this->setPictureName("");
			$this->setPictureWidth("");
			$this->setPictureHeight("");
			
			$this->setCategoryID("");
			$this->setCategoryName("");
			
			$this->setLevelID("");
			$this->setLevelName("");
			
			$this->setLanguageInfo("");
			$this->setDuracion("");
			$this->setResponsable("");
			$this->setTipoInscripcion("");
			
			$this->setCourseMoodleID("");
			$this->setCourseMoodleName("");
			
			$this->setInternalID("");
			
			$this->setValidateFlag("1");
			
			$this->setCompanyID(1);
			$this->setRemarks("");
			$this->setActiveFlag("");
			$this->setModifiedBy("");
			$this->setTimestamp("");
			return;
      }
      
      global $mysqli;
	  global $errorLog;
	  $format = new Format();	
	  try {
		$query_select= sprintf("SELECT ic_course.course_id,  
                     	ic_course.course_name,
                     	ic_course.course_desc,	
						
						ic_course.picture_name,
						ic_course.picture_file,
						ic_course.picture_width,
						ic_course.picture_height,
						
						ic_course.course_category_id,
						ic_course_category.course_category_name,
						
						ic_course.level_id,
						ic_level.level_name,
						
						ic_course.language_info,
						ic_course.duracion,
						ic_course.responsable,
						ic_course.tipo_inscripcion,
						
						ic_course.course_moodle_id,
						
						ic_course.internal_id,
						
						ic_course.validate_flag,
						
						ic_course.company_id,
						ic_course.remarks,
						ic_course.active_flag,
						ic_course.modified_by,
						ic_course.timestamp
						
                 FROM ic_course 
							LEFT JOIN ic_course_category ON ic_course.course_category_id = ic_course_category.course_category_id 
							LEFT JOIN ic_level ON ic_course.level_id = ic_level.level_id AND ic_level.language = '%s'
							    
                 WHERE  ic_course.active_flag=1 AND
						ic_course.course_id = %d",  $_SESSION["language"], $id);
		
		$errorLog->LogDebug("SELECT: $query_select");
		
		//$objMoodleCourse = new MoodleCourse();
		
		 if ($result = $mysqli->query($query_select)){
				
				if ($result->num_rows > 0){
					$row = $result->fetch_array();
				
					$this->setID($row["course_id"]);
					$this->setName($row["course_name"]);
					$this->setDescription($row["course_desc"]);
					
					$this->setPictureName($row["picture_name"]);
					$this->setPictureFile($row["picture_file"]);
					$this->setPictureWidth($row["picture_width"]);
					$this->setPictureHeight($row["picture_height"]);
					
					$this->setCategoryID($row["course_category_id"]);
					$this->setCategoryName($row["course_category_name"]);
					
					$this->setLevelID($row["level_id"]);
					$this->setLevelName($row["level_name"]);
					
					$this->setLanguageInfo($row["language_info"]);
					$this->setDuracion($row["duracion"]);
					$this->setResponsable($row["responsable"]);
					$this->setTipoInscripcion($row["tipo_inscripcion"]);
					
					$this->setCourseMoodleID($row["course_moodle_id"]);
						//$objMoodleCourse->loadDataByShortname($row["course_moodle_id"]);
						//$this->setCourseMoodleName($objMoodleCourse->getName());
					
					$this->setInternalID($row["internal_id"]); 
					
					$this->setValidateFlag($row["validate_flag"]);
					
					$this->setCompanyID($row["company_id"]);
					$this->setRemarks($row["remarks"]);
					$this->setActiveFlag($row["active_flag"]);
					$this->setModifiedBy($row["modified_by"]);
					$this->setTimestamp($row["timestamp"]);
					
					
					$search="";
					if (isset($_GET["search"]) && $_GET["search"]!=""){
						$search=$_GET["search"];
					}
					
					$this->setAssignedStudents($this->loadStudents());
					$this->setNoAssignedStudents($this->loadPossibleStudents($search));
					
					$ruta_upload = str_replace("$1",$this->getCompanyID(),CFG_PICTURE_COURSE_URL);
					
					$this->setPictureFileExists(file_exists($ruta_upload.$this->getID() . "_" . $this->getPictureFile()));
					$this->setThumbnailFileExists(file_exists($ruta_upload.$this->getID() . "_th_" . $this->getPictureFile()));
					
					
					$result->close();
				}else{
					$errorLog->LogError("Location: /srm/index.php?error=3. ERROR: ".$query_select);
					header("Location: /srm/index.php?error=3"); 	
					exit;
				}	
				
			}		 
		
	  } catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
	  }
	
	}//end loadData
	
	
	
   public function createNew()
   {
	  
	  global $errorLog;
	  global $mysqli;
	  $error="";
	  
	  $format = new Format();	  
	 	
      try {
		
			$query_insert=sprintf("INSERT INTO ic_course 
								    (
									course_name, 
									course_desc, 
									
									course_category_id,
									level_id,
									
									language_info,
									course_moodle_id,
									
									duracion,
									responsable,
									tipo_inscripcion,
									
									internal_id,
									
									validate_flag,
									
									company_id,
									active_flag,
									remarks, 
									modified_by, 
									timestamp) 
								  VALUES (
								  '%s',
								  '%s',
								  
								  %d,
								  %d,
								  
								  '%s',
								  '%s',
								  
								  '%s',
								  '%s',
								  '%s',
								  
								  '%s',
								  
								  %d,
								  
								  %d,
								  1,
								  '%s',
								  '%s',
								  CURRENT_TIMESTAMP
								  )",
									$this->getName(),
									$this->getDescription(),
									
									$this->getCategoryID(), 
									$this->getLevelID(), 
									
									$this->getLanguageInfo(),
									$this->getCourseMoodleID(),
									
									$this->getDuracion(),
									$this->getResponsable(),
									$this->getTipoInscripcion(),
									
									$this->getInternalID(),
									
									$this->getValidateFlag(),
									
									$this->getCompanyID(),
									$this->getRemarks(),
									$this->getModifiedBy()									
								);
			
			$errorLog->LogDebug("INSERT: $query_insert");
			
			$result = $mysqli->query($query_insert);
			
			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}
			
			//Calculo el último ID insertado.
			$query_max = "SELECT LAST_INSERT_ID() AS max_id";
			if( $result = $mysqli->query($query_max) ){
				$row = $result->fetch_array();
				
				$this->setID($row["max_id"]);
				
				$result->close();
			}
			
			
			///Sube la imágen.
			
			//echo "this->getUpload() = ".$this->getUpload();
			
			if ($this->getUpload()=="yes"){
				$upload = $this->uploadPicture($this->getID(), $this->getCompanyID());
				$error=$upload;
			}
			
		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}
		
		return $error;
	  
   }//END create_new
   
	
	
   public function update($id)
   {
	  global $mysqli;
	  global $errorLog;
	  $error="";
	  
	  $format = new Format();
		
      try {
		
			$query_update=sprintf("UPDATE ic_course 
								SET course_name='%s', 
									course_desc='%s',
									course_category_id=%d,
									level_id=%d,
									
									language_info='%s',
									course_moodle_id='%s',
									
									duracion='%s',
									responsable='%s',
									tipo_inscripcion='%s',
									
									internal_id = '%s',
									
									validate_flag=%d,
									
									company_id=%d, 
									remarks='%s', 
									modified_by='%s', 
									timestamp=CURRENT_TIMESTAMP
							WHERE 
									course_id = %d",

									$this->getName(),									
									$this->getDescription(),
									$this->getCategoryID(),
									$this->getLevelID(),
									
									$this->getLanguageInfo(),
									$this->getCourseMoodleID(),
									
									$this->getDuracion(),
									$this->getResponsable(),
									$this->getTipoInscripcion(),
									
									$this->getInternalID(),
									
									$this->getValidateFlag(),
									
									$this->getCompanyID(), 
									$this->getRemarks(),
									$_SESSION["user_id"],
									$id
    							);
			
			
			$errorLog->LogDebug("UPDATE: $query_update");
			$result = $mysqli->query($query_update);
			
			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}
			
			
			$ruta_upload = str_replace("$1",$this->getCompanyID(),CFG_PICTURE_COURSE_URL);
			
			
			
			///Sube la imágen.
			if ($this->getUpload()=="yes"){
				$upload = $this->uploadPicture($this->getID(), $this->getCompanyID());
					
				$error=$upload;
			}
			
			
			//Borramos la anterior imágen si estamos subiendo una nueva, solamente si no hay error al intentar subirla.
			if ($this->getUpload()=="yes" && $error==""){
				if (file_exists($ruta_upload.$this->getID() . "_" . $this->getPictureFileOld())){
					unlink($ruta_upload.$this->getID() . "_" . $this->getPictureFileOld()); 
				}
				if (file_exists($ruta_upload.$this->getID() . "_th_" . $this->getPictureFileOld())){
					unlink($ruta_upload.$this->getID() . "_th_" . $this->getPictureFileOld()); 
				}
			}
			
			
			
		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}
		
		return $error;
		
   }//END UPDATE
	
   

   public function delete($id)
   {
	 //No podremos borrar ninguna company con vital_flag=1
   
     global $mysqli;
     global $errorLog;
	 
      try {
		
			$query_delete=sprintf("UPDATE ic_course 
								SET active_flag=0, 
									modified_by='%s', 
									timestamp=CURRENT_TIMESTAMP
							WHERE 
									course_id = %d",

									$_SESSION["user_id"],
									$_POST['id']
    							);
			
			$errorLog->LogDebug("DELETE: $query_delete");
			
			$result = $mysqli->query($query_delete);
			
			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}
			
			
			//Al borrar el curso borrar otro objetos:
			
			//Attachment relacionados
			$objAttachment = new Attachment();
			$objAttachment->deleteRelated(101,$id);
			
			//Pictures relacionados
			$objPicture = new Picture();
			$objPicture->deleteRelated(101,$id);
			
			
			//Borramos las imágenes ???
			
		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}
		
			  
   }//END DELETE    
	





   public function init()
   {
		$error=false;
		$error_txt="";
		$valor = "";
		
		
		//Param id
		$valor = $_POST['id'];
		if (trim($valor)!=""){
			$this->setID($valor);
		}else{
			$this->setID("new");
		}
		
		//Param name
		$valor = $_POST['name'];
		if (trim($valor)!=""){
			$this->setName(trim($valor));
		}		
		if ($this->getName()=="" && $this->getName()==""){
			$error=true;
			$error_txt.="\\n- ".str_replace("$1",LBL_COURSE,MSG_REQUIRED_FIELD);
		}

		$this->setDescription($_POST['description']);	
		
		//Param category
		$valor = $_POST['category_id'];
		if (trim($valor)!=""){
			$this->setCategoryID(trim($valor));
		}		
		if ($this->getCategoryID()=="" || $this->getCategoryID()=="0"){
			$error=true;
			$error_txt.="\\n- ".str_replace("$1",LBL_CATEGORY,MSG_REQUIRED_FIELD);
		}
		
		//Param level
		$valor = $_POST['level_id'];
		if (trim($valor)!=""){
			$this->setLevelID(trim($valor));
		}		
		if ($this->getLevelID()=="" || $this->getLevelID()=="0"){
			$error=true;
			$error_txt.="\\n- ".str_replace("$1",LBL_LEVEL,MSG_REQUIRED_FIELD);
		}
		
		//Param language_info
		$valor = $_POST['language_info'];
		if (trim($valor)!=""){
			$this->setLanguageInfo($valor);
		}else{
			$error=true;
			$error_txt.="\\n- ".str_replace("$1",LBL_LANGUAGE,MSG_REQUIRED_FIELD);
			$this->setLanguageInfo("");
		}
		
		$this->setDuracion($_POST['duracion']);
		$this->setResponsable($_POST['responsable']);
		$this->setTipoInscripcion($_POST['tipo_inscripcion']);
		
		//Param course_moodle_id
		$valor = $_POST['course_moodle_id'];
		if (trim($valor)!=""){
			$this->setCourseMoodleID(trim($valor));
		}		
		if ($this->getCourseMoodleID()=="" || $this->getCourseMoodleID()=="0"){
			$error=true;
			$error_txt.="\\n- ".str_replace("$1",LBL_COURSE_MOODLE,MSG_REQUIRED_FIELD);
		}
		
		$this->setInternalID($_POST["internal_id"]); 

		if (isset($_POST['validate_flag'])){
			$this->setValidateFlag($_POST['validate_flag']);
		}else{
			$this->setValidateFlag(0);
		}	
		
		$valor = $_POST['bupload_image'];
		$this->setUpload($valor);
		
		$valor = $_POST['picture_file_old'];
		$this->setPictureFileOld($valor);
		
		$this->setCompanyID($_POST['company_id']);
		$this->setRemarks($_POST['remarks']);
		$this->setModifiedBy($_SESSION['user_id']);
		return $error_txt;
	
   }//END init

   
   
   
   	private function loadStudents()
	{
    
		global $mysqli;
		
        $query_select = sprintf("SELECT ic_student.student_id,  
									 concat(ic_student.last_name1, ', ', ic_student.first_name) as student_name
									FROM ic_student, ic_student_course 
								   WHERE ic_student.student_id = ic_student_course.student_id 
									 AND ic_student_course.course_id = %d
									 AND ic_student.company_id = 1
									 AND ic_student.active_flag = 1 
									 AND ic_student.level <= %d
								   ORDER BY ic_student.last_name1, ic_student.first_name ", $this->getID(), $this->getLevelID());

		$result = $mysqli->query($query_select);
		
		if ($mysqli->error){
			$errorLog->LogError($mysqli->error);
		}
			
		return $result;

	}//loadStudents
   
	private function loadPossibleStudents($search)
	{
    
		global $mysqli;
		global $errorLog;		
		
		$query_select = "";
		
		$query_select = sprintf("SELECT DISTINCT (ic_student.student_id),
							   concat(ic_student.last_name1, ', ', ic_student.first_name) as student_name

						  FROM ic_student LEFT JOIN ic_student_course 
							   ON (ic_student.student_id = ic_student_course.student_id) 
						 WHERE ic_student.student_id NOT IN ( 
								  SELECT ic_student.student_id  
									FROM ic_student, ic_student_course 
								   WHERE ic_student.student_id = ic_student_course.student_id 
									 AND ic_student_course.course_id = %d
									 AND ic_student.company_id = 1
									 AND ic_student.active_flag = 1 ) 
						   AND ic_student.active_flag = 1
						   AND ic_student.company_id = 1
						   AND ic_student.level <= %d
						   
						   AND UPPER(concat(ic_student.last_name1, ', ', ic_student.first_name)) LIKE upper('%%".$search."%%')
						   
						 ORDER BY ic_student.last_name1, ic_student.first_name ", $this->getID(), $this->getLevelID());
		
		$errorLog->LogDebug("SELECT: $query_select");
        $result = $mysqli->query($query_select);
		return $result;

	}//loadPossibleStudents
	
	public function saveStudents($id) 
	{
		global $mysqli;
		global $errorLog;
		
		try {
			// Se borran todos primero para luego guardar los seleccionados
			$query_delete=sprintf("DELETE from ic_student_course WHERE course_id=%d", $id);
			$errorLog->LogDebug("DELETE: $query_delete");
			
			$result = $mysqli->query($query_delete);
			
			
			$selectedStudents = $_POST["assig_students"];
			if ($selectedStudents!=""){
				$tok = strtok ($selectedStudents,", ");
				while ($tok !== false) {
					$query_insert = "INSERT INTO ic_student_course (student_id, course_id) ".
								" VALUES ( ".$tok.", ".$id.") ";
					$result = $mysqli->query($query_insert);
					$errorLog->LogDebug("INSERT: $query_insert");
					$tok = strtok(", ");
				}
			}

		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}		
	
	}//saveStudents
   
   
   
   
   
    
//Función que sube o modifica una imagen existente en la ruta indicada.   
   
public function uploadPicture($picture_id, $company_id){
	
		$logo_error=false;
		$logo_error_num=0;
		$archivo_final="";
		
		$ruta_upload = str_replace("$1",$company_id,CFG_PICTURE_COURSE_URL);
		
		if (is_uploaded_file($_FILES['picture']['tmp_name'])) {
			
			$archivo_name= $_FILES['picture']['name'];
			$archivo_size= $_FILES['picture']['size'];
			$archivo_type=  $_FILES['picture']['type'];
			$archivo= $_FILES['picture']['tmp_name'];
			
			//echo "<br>archivo_name=".$archivo_name;
			//echo "<br>archivo_size=".$archivo_size;
			//echo "<br>archivo_type=".$archivo_type;
			//echo "<br>archivo=".$archivo;
			
			$lim_tamano= $_POST['lim_tamano'];
			
			//echo "<br>archivo=".$lim_tamano;
			
			$extension = explode(".",$archivo_name);
			$num = count($extension)-1;
			if(($extension[$num] == "jpg" || $extension[$num] == "gif" || $extension[$num] == "png" )){
				
				if ($extension[$num] == "jpg"){
					$archivo_final=$picture_id."_".$archivo_name;
				}
				
				if ($extension[$num] == "gif"){
					$archivo_final=$picture_id."_".$archivo_name;
				}
				
				if ($extension[$num] == "png"){
					$archivo_final=$picture_id."_".$archivo_name;
				}
				
				//echo "<br>IMAGEN BUENA";
				
				$tamano = getimagesize($archivo);
				//echo "<br>alto=".$tamano[1];
				//echo "<br>ancho=".$tamano[0];
				
				//Valida las dimensiones del logotipo.
				if (($tamano[0]>CFG_PICTURE_COURSE_WIDTH || $tamano[1]>CFG_PICTURE_COURSE_HEIGHT)){
					
					$logo_error=true;
					$logo_error_num=2;
				}
				
				if (!$logo_error){
					
					//echo "<br>->archivo_name=$archivo_name";
					//echo "<br>->archivo_size=$archivo_size";
					//echo "<br>->lim_tamano=$lim_tamano";
					
					if ($archivo_name != "none" AND $archivo_size != 0 AND $archivo_size<=$lim_tamano){
						if (copy ($archivo, $ruta_upload.$archivo_final)) {
						  $logo_error_num=100;
						}
					}else{
						$logo_error=true;
						$logo_error_num=3;
					}
				}
				
			}
				
		}else{
			$logo_error=true;
			$logo_error_num=1;
		}
		
		$error_msg="";
		if ($logo_error_num==1){
			$error_msg="";
		}else if ($logo_error_num==2){
			$error_msg= 
				str_replace("$4", CFG_PICTURE_COURSE_HEIGHT,
				str_replace("$3", CFG_PICTURE_COURSE_WIDTH,
				str_replace("$2", $tamano[1],str_replace("$1",$tamano[0],MSG_INFO_BIG_SIZE))));
		}else if ($logo_error_num==3){
			$error_msg= 
				str_replace("$2", CFG_SIZE_COURSE_BYTES,str_replace("$1",$archivo_size,MSG_INFO_LARGE));
		}
		
		//echo $error_msg;
		//echo $logo_error_num;
		
		if ($logo_error_num==0 || $logo_error_num==100){ //si no hay error guardo la imagen.
		
			//Reduzco la imagen y la guardo. Thumbnail.
			
			
			
			$archivo_final = $ruta_upload.$archivo_final;
			
			$original = "";
			if (preg_match('/.png$/', $archivo_final)) {
				$original = imagecreatefrompng($archivo_final);
			} else if (preg_match('/.gif$/', $archivo_final)) {
				$original = imagecreatefromgif($archivo_final);
			} else if (preg_match('/.jpg$/', $archivo_final)) {
				$original = imagecreatefromjpeg($archivo_final);
			}
			
			
			$ancho = imagesx($original);
			$alto = imagesy($original);
			
			if ($ancho>=$alto){
				$ratio = $ancho/CFG_TH_PICTURE_COURSE_WIDTH;
				$aux_height = $alto/$ratio;
				
				$thumb = imagecreatetruecolor(CFG_TH_PICTURE_COURSE_WIDTH,$aux_height); // Lo haremos de un tamaño 150x150

				imagecopyresampled($thumb,$original,0,0,0,0,CFG_TH_PICTURE_COURSE_WIDTH,$aux_height,$ancho,$alto);
			
			}else{
				$ratio = $alto/CFG_TH_PICTURE_COURSE_HEIGHT;
				$aux_width = $ancho/$ratio;
				
				$thumb = imagecreatetruecolor($aux_width, CFG_TH_PICTURE_COURSE_HEIGHT); // Lo haremos de un tamaño 150x150
	
				imagecopyresampled($thumb,$original,0,0,0,0,$aux_width,CFG_TH_PICTURE_COURSE_HEIGHT,$ancho,$alto);
			}
			
			
			if (preg_match('/.png$/', $archivo_final)) {
				imagepng($thumb,$ruta_upload.$picture_id."_th_".$archivo_name); // Suprimida la calidad de compresión daba error.
			} else if (preg_match('/.gif$/', $archivo_final)) {
				imagegif($thumb,$ruta_upload.$picture_id."_th_".$archivo_name,90); // 90 es la calidad de compresión
			} else if (preg_match('/.jpg$/', $archivo_final)) {
				imagejpeg($thumb,$ruta_upload.$picture_id."_th_".$archivo_name,90); // 90 es la calidad de compresión
			}
			
			
			//Save image...
			global $mysqli;
	   
			try {
			
				$query_update=sprintf("UPDATE ic_course 
									SET 
										picture_file='%s', 
										picture_width='%s', 
										picture_height='%s', 
										timestamp=CURRENT_TIMESTAMP
								WHERE 
										course_id = %d",
										
										$archivo_name,
										$tamano[0],
										$tamano[1],
										$picture_id
									);
				
				//echo "*".$query_update."*";
				$result = $mysqli->query($query_update);
				
				if ($mysqli->error){
					$errorLog->LogError($mysqli->error);
				}
				
				/*
				
					Borrar las imágenes
				
				*/
				
				
			} catch (Exception $e) {
				$errorLog->LogFatal("Caught exception: ". $e->getMessage());
			}
			
		}else{
		
			return $error_msg;
		}
		

}//end upload_picture
   
   
   
   
   
   
   
}

?>