<?php
// -----------------------------------------
// AbstractCRMObjectChannels.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/database/DB_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/AbstractCRMObjectImage.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/CRMObjectChannels.php');

abstract class AbstractCRMObjectChannels  extends AbstractCRMObjectImage
										  //implements CRMObjectChannels

{

	public $mnAddressID = "";
	public $msAddress = "";
	public $mnPhoneID = "";
	public $msPhone = "";
	public $mnMobileID = "";
	public $msMobile = "";
	public $mnFaxID = "";
	public $msFax = "";
	public $mnEmailID = "";
	public $msEmail = "";
	///public $mbSaveChannels = "";

	public function getAddressID(){
		return $this->mnAddressID;
	}
	public function setAddressID($nValue){
		$this->mnAddressID = $nValue;
	}
	public function getAddress(){
		return $this->msAddress;
	}
	public function setAddress($nValue){
		$this->msAddress = $nValue;
	}


	public function getPhoneID(){
		return $this->mnPhoneID;
	}
	public function setPhoneID($nValue){
		$this->mnPhoneID = $nValue;
	}
	public function getPhone(){
		return $this->msPhone;
	}
	public function setPhone($nValue){
		$this->msPhone = $nValue;
	}


	public function getMobileID(){
		return $this->mnMobileID;
	}
	public function setMobileID($nValue){
		$this->mnMobileID = $nValue;
	}
	public function getMobile(){
		return $this->msMobile;
	}
	public function setMobile($nValue){
		$this->msMobile = $nValue;
	}


	public function getFaxID(){
		return $this->mnFaxID;
	}
	public function setFaxID($nValue){
		$this->mnFaxID = $nValue;
	}
	public function getFax(){
		return $this->msFax;
	}
	public function setFax($nValue){
		$this->msFax = $nValue;
	}


	public function getEmailID(){
		return $this->mnEmailID;
	}
	public function setEmailID($nValue){
		$this->mnEmailID = $nValue;
	}
	public function getEmail(){
		return $this->msEmail;
	}
	public function setEmail($nValue){
		$this->msEmail = $nValue;
	}



   protected function loadMainChannels($object_type_id, $object_id)
   {
	 global $mysqli;

	  //ADDRESS
	  try {
		/*
		$query_select= sprintf("SELECT ic_address.address_id AS object_id,
									    CONCAT(
										ic_address.street,
										(CASE WHEN ic_address.street_number IS NOT NULL THEN ' ' ELSE '' END),
										COALESCE(ic_address.street_number, ''),
										(CASE WHEN ic_address.floor IS NOT NULL THEN ' ' ELSE '' END),
										COALESCE(ic_address.floor, ''),
										(CASE WHEN ic_address.zip_code IS NOT NULL THEN ', ' ELSE '' END),
										COALESCE(ic_address.zip_code, ''),
										(CASE WHEN ic_address.city IS NOT NULL THEN ' ' ELSE '' END),
										COALESCE(ic_address.city, ''),
										(CASE WHEN ic_address.province IS NOT NULL THEN ', ' ELSE '' END),
										COALESCE(ic_address.province, ''),
										(CASE WHEN ic_address.region_state IS NOT NULL THEN ', ' ELSE '' END),
										COALESCE(ic_address.region_state, '') ) AS object_name
								FROM ic_address
								WHERE  object_type_id = %d AND object_id = %d AND active_flag=1 AND main_flag = 1", $object_type_id, $object_id);
		*/

		$query_select= sprintf("SELECT ic_address.address_id AS object_id,
									    ic_address.street AS object_name
								FROM ic_address
								WHERE  object_type_id = %d AND object_id = %d AND active_flag=1 AND main_flag = 1", $object_type_id, $object_id);


		//echo $query_select;
		if ($result = $mysqli->query($query_select)){

				$row = $result->fetch_array();

				$this->setAddressID($row["object_id"]);
				$this->setAddress($row["object_name"]);

				$result->close();
			}

	  } catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
	  }



	  //PHONE
	  try {
		$query_select= sprintf("SELECT
								ic_phone.phone_id AS object_id,
								ic_phone.phone_name AS object_name
								FROM ic_phone
								WHERE object_type_id = %d AND object_id = %d AND active_flag=1 AND main_flag = 1", $object_type_id, $object_id);
		//echo $query_select;
		 if ($result = $mysqli->query($query_select)){

				$row = $result->fetch_array();

				$this->setPhoneID($row["object_id"]);
				$this->setPhone($row["object_name"]);

				$result->close();

			}

	  } catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
	  }


	  //EMAIL
	  try {
		$query_select= sprintf("SELECT
								ic_email.email_id AS object_id,
								ic_email.email_name AS object_name
								FROM ic_email
								WHERE object_type_id = %d AND object_id = %d AND active_flag=1 AND main_flag = 1", $object_type_id, $object_id);
		//echo $query_select;
		 if ($result = $mysqli->query($query_select)){

				$row = $result->fetch_array();

				$this->setEmailID($row["object_id"]);
				$this->setEmail($row["object_name"]);

				$result->close();
			}

	  } catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
	  }


	  //FAX
	  try {
		$query_select= sprintf("SELECT
								ic_fax.fax_id AS object_id,
								ic_fax.fax_name AS object_name
								FROM ic_fax
								WHERE object_type_id = %d AND object_id = %d AND active_flag=1 AND main_flag = 1", $object_type_id, $object_id);
		//echo $query_select;
		 if ($result = $mysqli->query($query_select)){

					$row = $result->fetch_array();

					$this->setFaxID($row["object_id"]);
					$this->setFax($row["object_name"]);

					$result->close();

			}

	  } catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
	  }


	  //MOBILE
	  try {
		$query_select= sprintf("SELECT
								ic_mobile.mobile_id AS object_id,
								ic_mobile.mobile_name AS object_name
								FROM ic_mobile
								WHERE object_type_id = %d AND object_id = %d AND active_flag=1 AND main_flag = 1", $object_type_id, $object_id);
			//echo $query_select;
		 if ($result = $mysqli->query($query_select)){

					$row = $result->fetch_array();

					$this->setMobileID($row["object_id"]);
					$this->setMobile($row["object_name"]);

					$result->close();

			}

	  } catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
	  }


   }

}

?>