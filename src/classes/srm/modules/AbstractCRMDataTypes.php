<?php

// -----------------------------------------
// AbstractCRMDataTypes.php
// -----------------------------------------


abstract class AbstractCRMDataTypes {

   public $STRING_TYPE = "String";

   /**
    * Tipo de datos decimal.
    */
   public $DOUBLE_TYPE = "Double";

   /**
    * Tipo de datos entero.
    */
   public $INTEGER_TYPE = "Integer";

   /**
    * Tipo de datos fecha.
    */
   public $DATE_TYPE = "Date";

   /**
    * Tipo de datos fecha. Diferencia en el where.
    */
   public $DATE_TYPE_2 = "Date2";

   /**
    * Tipo de datos fecha y hora.
    */
   public $TIMESTAMP_TYPE = "Timestamp";

   /**
    * Tipo de datos logico.
    */
   public $LOGICAL_TYPE = "Logical";

   /**
    * Tipo de datos logico y string.
    */
   public $LOGICAL_STRING_TYPE = "LogicalString";


   /**
    * Tipo de datos fecha para periodos.
    */
   public $DATE_PERIOD_TYPE = "Period";



}
?>