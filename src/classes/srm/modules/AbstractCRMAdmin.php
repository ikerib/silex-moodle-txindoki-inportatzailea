<?php
// -----------------------------------------
// AbstractCRMAdmin.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/CRMAdmin.php');

abstract class AbstractCRMAdmin implements CRMAdmin
{

	public $moAlertMessageList = ""; // ES un array...
	public $mnViewCode = 0;
	public $msView = "";
	public $moSelectedOperators = ""; // Es un hashtable

	//public abstract function getObjectTypeID();



	public function getAlertMessage(){
		return $this->moAlertMessageList;
	}
	public function setAlertMessage($s){
		$this->moAlertMessageList = $s;
	}
	public function addAlertMessage($s){
		$this->moAlertMessageList+=$s;
	}

	public function showAlertMessage($s){
		return  $this->moAlertMessageList;
	}

	public function getView(){
		return $this->msView;
	}
	public function setView($s){
		$this->msView = $s;
	}

	public function getViewCode(){
		return $this->mnViewCode;
	}
	public function setViewCode($nViewCode){
		$this->mnViewCode = $nViewCode;
	}

	public function getSelectedOperator(){
		return $this->moSelectedOperators;
	}



   /**
    * Returns the selected operator for given attribute/field code.
    * It is used to show operators used in locators criteria fields.
    *
    * @return      the selected operator for given code;
    *              <CODE>null</CODE> if no operator has been selected before.
    * @since       CRM 1.0.0
    */
   // public String getSelectedOperator(String sFieldCode)
   // {
      // return (String)moSelectedOperators.get(sFieldCode);
   // }


   /**
    * Sets the selected operator for given attribute/field code.
    * It is used to set operators used in locators criteria fields.
    *
    * @param  sFieldCode   code of the attribute/field
    * @param  sSelectedOp  code of the selected operator
    * @throws              NullPointerException
    *                      if the attribute/field code is <CODE>null</CODE>.
    * @since               CRM 1.0.0
    */
   // public void setSelectedOperator(String sFieldCode, String sSelectedOp)
   // {
      // if (null == sFieldCode)
         // throw new NullPointerException("Field code for operators drop down in locator views cannot be null!");

      // moSelectedOperators.put(sFieldCode, sSelectedOp);
   // }


   /**
    * Loads search criteria operator values from the request.
    *
    * @param  oReq     request
    * @throws          isyc.website.CRMExceptions
    *                  if an error raises.
    * @throws          NullPointerException
    *                  if the request is <CODE>null</CODE>.
    * @since           CRM 1.0.0
    */
   // public void loadSearchOperators(javax.servlet.http.HttpServletRequest oReq)
       // throws NullPointerException, isyc.website.CRMException
   // {
      // if (null == oReq)
         // throw new NullPointerException("Request object is null. Not able to read locator criteria operators.");

      // for (java.util.Enumeration oNames = oReq.getParameterNames() ; oNames.hasMoreElements() ;)
      // {
         // String sName = (String)oNames.nextElement();
         // if (null != sName && sName.startsWith("op_"))
            // setSelectedOperator(sName, oReq.getParameter(sName));
      // }
   // }


}

?>