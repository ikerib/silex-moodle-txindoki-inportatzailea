<?php
// -----------------------------------------
// CompanyData_class.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/../src/classes/srm/modules/AbstractCRMObject.php');

class ObjectTypeData extends AbstractCRMObject
{

	public $msPath = "";

	public function getPath(){
		return $this->msPath;
	}

	public function setPath($nValue){
		$this->msPath = $nValue;
	}

}//end class
?>