<?php 
$buttons="buttons_detail.php";
$tab=1;
$tabs="tabs.php";
$object_type_id=22 ;

include($_SERVER['DOCUMENT_ROOT']."/includes/control.php"); 

require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/admin/Info.php'); 
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/Functions.php'); 
include($_SERVER['DOCUMENT_ROOT']."/lang/".$_SESSION["language"]."/lbl_information.php"); 


$id="";
$enctype="";
$action_url="/modules/admin/information/detail.php";
$action="";

$obj = new Info();
$util = new Util();

if (isset($_GET["id"]) && $_GET["id"]!="")
	$id=$_GET["id"];
else if (isset($_POST["id"]) && $_POST["id"]!="")
	$id=$_POST["id"];



//Comprobamos si viene de un borrado
if (isset($_POST['action']) && $_POST['action']=="DELETE"){
	//echo "---->ACTIONnnn=".$_POST['action'];
	include($_SERVER['DOCUMENT_ROOT'].'/modules/admin/information/detail_action.php');
	exit;
}
	
	
if ($id!=""){
	
	if ($id=="new"){
		$obj->loadData("new");
		$action="NEW";
		$id="new";
	}else if (is_numeric($id)){	
		$obj->loadData($id);
		$action="UPDATE";
	}else{
		echo "error1"; // o producir un error hacia una web
	}
}else{
	echo "eerror2"; // o producir un error hacia una web
}	

if (isset($_POST["id"])){
	include($_SERVER['DOCUMENT_ROOT'].'/modules/admin/information/detail_action.php');
}



include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMBeginDocument.php');

include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMHeaderTags.php'); 

?>
<script  type="text/javascript">
	var languageTinyCode = '<?php echo $_SESSION['language']; ?>';
</script>
<script language="javascript" type="text/javascript" src="/editor/tiny_mce.js"></script>

<SCRIPT src="/modules/admin/information/js/data_check.js" type="text/javascript" CHARSET="ISO-8859-1"></SCRIPT>
<link type="text/css" rel="stylesheet" href="/js/calendar/calendar.css?random=20051112" media="screen"></LINK>
<script  type="text/javascript">
	var languageCode = '<?php echo $_SESSION['language']; ?>';
	
	<?php if (isset($id) && $id!="" && $id!="new") {	?>
		var my_preview_page = '<?php echo CFG_URL_PORTAL."noticias_ficha_preview.php?id=".$id; ?>';
	<?php }else{ ?>
		var my_preview_page = '';
	<?php } ?>	
</script>
<SCRIPT type="text/javascript" src="/js/calendar/calendar.js?random=20060118"></script>

<?php



?>

<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMScreenHeader.php'); ?>



	<fieldset>			
		
		
		<p>
			<label><span class="obligatorio"><?php echo MSG_CAMPOS_OBLIGATORIOS;?></span></label>
		</p>
		<br class="limpiar"/>
		<br/>
		<p>
			
			<label><img src="/images/flag_spanish.png" border="0" alt="<?php echo LBL_ESPANOL; ?>" title="<?php echo LBL_ESPANOL; ?>" />&nbsp;<?php echo LBL_TITLE; ?>: <span class="obligatorio">*</span></label><br/>
			<input type="text" name="name" id="name" onChange="setDirty(this);" size="114" maxlength="200" value="<?php echo $obj->getName(); ?>"/>
		</p>

		<br class="limpiar"/>
		
		<p>
			
			<label><img src="/images/flag_euskara.png" border="0" alt="<?php echo LBL_EUSKARA; ?>" title="<?php echo LBL_EUSKARA; ?>" />&nbsp;<?php echo LBL_TITLE; ?>: <span class="obligatorio">*</span></label><br/>
			<input type="text" name="name_eu" id="name_eu" onChange="setDirty(this);" size="114" maxlength="200" value="<?php echo $obj->getNameEU(); ?>"/>
		</p>

		<br class="limpiar"/>
		<br/>
		
		<p>
			<label><img src="/images/flag_spanish.png" border="0" alt="<?php echo LBL_ESPANOL; ?>" title="<?php echo LBL_ESPANOL; ?>" />&nbsp;<?php echo LBL_DESCRIPTION; ?>:</label><br/>
			<input type="text" name="info_desc" id="info_desc" onChange="setDirty(this);" size="114" maxlength="200" value="<?php echo $obj->getDescription(); ?>"/>
		</p>

		<br class="limpiar"/>
		
		<p>
			<label><img src="/images/flag_euskara.png" border="0" alt="<?php echo LBL_EUSKARA; ?>" title="<?php echo LBL_EUSKARA; ?>" />&nbsp;<?php echo LBL_DESCRIPTION; ?>:</label><br/>
			<input type="text" name="info_desc_eu" id="info_desc_eu" onChange="setDirty(this);" size="114" maxlength="200" value="<?php echo $obj->getDescriptionEU(); ?>"/>
		</p>

		<br class="limpiar"/>
		<br/>
		<p>
			<label><?php echo LBL_DATE; ?>: <span class="obligatorio">*</span></label><br/>
			<input type="text" name="info_date" class="readonly" size="16" readonly value="<?php echo $obj->getDate(); ?>" onChange="setDirty(this);" />&nbsp;&nbsp;
            <a href="javascript:void(null);" onClick="displayCalendar(document.forms[0].info_date,'dd/mm/yyyy',this);"><img src="/images/calendar.gif" border="0" alt="<?php echo LBL_ALT_CALENDAR; ?>" title="<?php echo LBL_ALT_CALENDAR; ?>" /></a>
		</p>
		
		<p>
			
			<label><?php echo LBL_INFO_SOURCE; ?>: </label><br/>
			<input type="text" name="info_source" id="info_source" onChange="setDirty(this);" size="63" maxlength="200" value="<?php echo $obj->getInfoSource(); ?>"/>
		</p>
		
	</fieldset>
	<br class="limpiar"/>
		
	<fieldset class="coloreado">	
		<p><?php echo LBL_PUBLICATION_DATE_INFO; ?></p>
		<br class="limpiar"/>
		<p>
			<label><?php echo LBL_PUBLICATION_DATE; ?>: <span class="obligatorio">*</span></label><br/>
			<input type="text" name="publication_date" class="readonly" size="16" readonly value="<?php echo $obj->getPublicationDate(); ?>" onChange="setDirty(this);" />&nbsp;&nbsp;
            <a href="javascript:void(null);" onClick="displayCalendar(document.forms[0].publication_date,'dd/mm/yyyy',this);"><img src="/images/calendar.gif" border="0" alt="<?php echo LBL_ALT_CALENDAR; ?>" title="<?php echo LBL_ALT_CALENDAR; ?>" /></a>
			
		</p>
		<p>
			<label><?php echo LBL_EXPIRATION_DATE; ?>: <span class="obligatorio">*</span></label><br/>
			<input type="text" name="expiration_date" class="readonly" size="16" readonly value="<?php echo $obj->getExpirationDate(); ?>" onChange="setDirty(this);" />&nbsp;&nbsp;
            <a href="javascript:void(null);" onClick="displayCalendar(document.forms[0].expiration_date,'dd/mm/yyyy',this);"><img src="/images/calendar.gif" border="0" alt="<?php echo LBL_ALT_CALENDAR; ?>" title="<?php echo LBL_ALT_CALENDAR; ?>" /></a>
			
		</p>
	</fieldset>
	
		<br class="limpiar"/>
		<br/>
	
	
	<fieldset>
		<p>
			<span class="obligatorio">**</span> <?php echo MSG_INFO_TEXT_03; ?>
		</p>
	
		<p>
			<label><img src="/images/flag_spanish.png" border="0" alt="<?php echo LBL_ESPANOL; ?>" title="<?php echo LBL_ESPANOL; ?>" />&nbsp;<?php echo LBL_INFORMATION; ?>:</label><br/>
			<textarea name="contents" id="contents" rows="4" cols="100" onChange="setDirty(this);" ><?php echo $obj->getContents(); ?></textarea>
		</p>

		<br class="limpiar"/>
		
		<p>
			<label><img src="/images/flag_euskara.png" border="0" alt="<?php echo LBL_EUSKARA; ?>" title="<?php echo LBL_EUSKARA; ?>" />&nbsp;<?php echo LBL_INFORMATION; ?>:</label><br/>
			<textarea name="contents_eu" id="contents_eu" rows="4" cols="100" onChange="setDirty(this);"><?php echo $obj->getContentsEU(); ?></textarea>
		</p>

		<br class="limpiar"/>
		<br/>
		<p>
			<span class="obligatorio">***</span> <?php echo MSG_INFO_TEXT_04; ?>
		</p>
		<p>
			<span class="obligatorio">***</span> <?php echo MSG_INFO_TEXT_05; ?>
		</p>
		<p>
			<label><?php echo LBL_INFO_EXTRA; ?>:</label><br/>
			<textarea name="info_extra" class="mceNoEditor" id="info_extra" rows="7" cols="100" onChange="setDirty(this);" ><?php echo $obj->getInfoExtra(); ?></textarea>
		</p>

		<br class="limpiar"/>

		
		<p>
			<label><?php echo LBL_REMARKS; ?>:</label><br/>
			<textarea name="remarks" class="mceNoEditor" cols="100" rows="7" onChange="setDirty(this);" ><?php echo $obj->getRemarks(); ?></textarea>
		</p>

	</fieldset>








<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMObjectTimestamp.php'); ?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMScreenBottom.php'); ?>


<script type="text/javascript" CHARSET="ISO-8859-1" defer >
<!--

//document.data_frm.name.focus(); 

// -->
</script>

<input type="hidden" name="id" value="<?php echo $id; ?>"/>
<input type="hidden" name="link_name" value=""/>
<input type="hidden" name="link" value=""/>
<input type="hidden" name="info_type_id" value="1"/>


<?php $type = ""; ?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMEndDocument.php'); ?>

