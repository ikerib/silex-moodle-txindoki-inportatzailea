<?php 
$buttons="buttons_detail.php";
$tab=2;
$tabs="tabs.php";
$object_type_id=22;

include($_SERVER['DOCUMENT_ROOT']."/includes/control.php"); 

//Cargo la clase para los mostrar los datos.
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/admin/PictureSummary.php'); 
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/Functions.php'); 
include($_SERVER['DOCUMENT_ROOT']."/lang/".$_SESSION["language"]."/lbl_locator.php"); 
include($_SERVER['DOCUMENT_ROOT']."/lang/".$_SESSION["language"]."/lbl_picture.php"); 
require_once($_SERVER['DOCUMENT_ROOT']."/config_app.php"); 

include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMBeginDocument.php');

include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMHeaderTags.php');
?>

<script src="/modules/admin/picture/js/data_check.js" type="text/javascript" charset="iso-8859-1"></script>
<script  type="text/javascript">
// NUEVO ++++++++++++++++++++++	
	var resultado = new Array();
	var contador = 0;	
// ++++++++++++++++++++++++++
</script>

<?php

$id="";
$enctype="";
$action_url="/modules/admin/information/pictures.php";
$action="SEARCH";

$columnas=2;

$util = new Util();

$obj = new PictureSummary();

if (isset($_GET["id"]) && $_GET["id"]!="")
	$id=$_GET["id"];
else if (isset($_POST["id"]) && $_POST["id"]!="")
	$id=$_POST["id"];

	
//Comprobamos si viene de un borrado
if (isset($_POST['action']) && $_POST['action']=="DELETE"){
	echo "---->ACTIONnnn=".$_POST['action'];
	include($_SERVER['DOCUMENT_ROOT'].'/modules/admin/information/detail_action.php');
	exit;
}	
	
include($_SERVER['DOCUMENT_ROOT'].'/modules/admin/information/pictures_action.php');

?>

<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMScreenHeader.php'); ?>
	
	<fieldset class="locator">
		<p>
			<label><?php echo LBL_NAME; ?>:</label>
			<?php echo $obj->buildOperatorSymbol("name","char",(isset($_POST['op_name'])?$_POST['op_name']:"9") ); ?>
			<input type="text" name="name" id="name" value="<?php echo (isset($_POST['name'])?$_POST['name']:""); ?>" size="25" maxlength="50"/>
		</p>
		
		<br class="limpiar"/>

		<input type="submit" name="search" value="<?php echo LBL_LOC_ACTION; ?>" class="boton" onClick="doSearch()"/>

	</fieldset>

      <table class="tabla1" summary="<?php echo LBL_LOC_SUM_TITLE1; ?>">
			<caption><?php echo LBL_LOC_SUM_TITLE1; ?></caption>
			<thead>
			<tr>
                <th width="5%">&nbsp;</th>
                <th width="95%"><?php echo LBL_NAME; ?>&nbsp;<a href="javascript:doOrderBy('1')"><img src="/images/flechas.gif" width="7" height="10" alt=""/></a></th>
			</tr>
			<tr>
				<td colspan="<?php echo $columnas; ?>" class="separa">&nbsp;</td>
			</tr>

			</thead>


<?php 
	//Cargo el footer
	include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMListFooter.php'); 
?>
		
			<tbody>
			 
				<?php
					//Muestro los datos.
					$contador = 0;	
					while($row = $resultado->fetch_array())		
					{
				?>
			        <tr <?php if (fmod($contador,2)){ ?>class="fondo"<?php } ?>>
						<td class="centro">
<SCRIPT language="JavaScript" type="text/javascript">
// NUEVO ++++++++++++++++++++++
var aux = new Array();
aux[0] = "<?php echo $row["picture_id"]; ?>";
aux[1] = "<?php echo $row["picture_name"]; ?>";
resultado[contador] = aux;
contador++;
// ++++++++++++++++++++++++++
</SCRIPT>					

						<?php 
						$ruta_upload = str_replace("$1",$row["company_id"],CFG_PICTURE_URL);
						
						if (file_exists($ruta_upload.$row["picture_id"] . "_th_" . $row["picture_file"]) ) { ?>
							<a class="thumbnail" href="javascript:void(0)">
							<img src="/images/camara.gif" width="13" height="11" alt=""/>
				    		<span><img src="/includes/CRMViewImage.php?cid=<?php echo $row["company_id"]; ?>&amp;nim=<?php echo $row["picture_id"]; ?>_th_<?php echo $row["picture_file"]; ?>&amp;path=/picture/" alt=""/></span>
				    		</a>
						<?php }else{ ?>
							&nbsp;
						<?php } ?>
						</td>
					
						<td>
						  <a href="javascript:goFromLoc('23',resultado[<?php echo $contador; ?>],'0');" >&nbsp;<?php echo $row["picture_name"]; ?></a>
						</td>
						
					</tr>
					<?php $contador++; ?>
                <?php } ?>
			  
			</tbody>
			
	  </table>
	  
	   <br class="limpiar"/><br/>
	   
	  <?php 
	  
		if ($resultado->num_rows == 0) { ?>
	   
		  <?php $ruta_new="/modules/admin/picture/detail.php?id=new&openerID=$id&openerType=$object_type_id"; ?>
		  <input type="button" name="create_new" value="<?php echo LBL_NEW_PICTURE; ?>" class="boton" onclick="doNewObject('<?php echo $ruta_new; ?>')"/>
		
	   <?php } ?>
		  
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMScreenBottom.php'); ?>


<script type="text/javascript" CHARSET="ISO-8859-1" defer >
<!--
document.data_frm.name.focus(); 
-->
</script>


<!--
  <input type="hidden" name="target_id_field" value=""/>
  <input type="hidden" name="target_name_field" value=""/>
  <input type="hidden" name="frm_action_type" value="Search"/>
-->
  <input type="hidden" name="id" id="id" value="<?php echo $id; ?>"/>
  




<?php $type = "summary"; ?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMEndDocument.php'); ?>


