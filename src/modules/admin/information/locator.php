<?php 
$locator="yes" ;
$buttons="buttons_locator.php";
$tabs="tabs_locator.php";
$object_type_id=22 ;
// NUEVO ++++++++++++++++++
$from = 0;
if (isset($_GET["from"]))
	$from = $_GET["from"];
else if (isset($_POST["from"]))
	$from = $_POST["from"];
// ++++++++++++++++++++++++

include($_SERVER['DOCUMENT_ROOT']."/includes/control.php"); 

//Cargo la clase para los mostrar los datos.
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/admin/InfoSummary.php'); 
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/Functions.php'); 
include($_SERVER['DOCUMENT_ROOT']."/lang/".$_SESSION["language"]."/lbl_locator.php"); 
include($_SERVER['DOCUMENT_ROOT']."/lang/".$_SESSION["language"]."/lbl_information.php"); 

include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMBeginDocument.php');

include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMHeaderTags.php');
?>

<script src="/modules/admin/information/js/data_check.js" type="text/javascript" charset="iso-8859-1"></script>

<link type="text/css" rel="stylesheet" href="/js/calendar/calendar.css?random=20051112" media="screen"></LINK>

<script  type="text/javascript">
	var languageCode = '<?php echo $_SESSION['language']; ?>'; //Cambiar por la variable de sessión.
	// NUEVO ++++++++++++++++++++++
	var resultado = new Array();
	var contador = 0;	
	// ++++++++++++++++++++++++++++
		
</script>

<SCRIPT type="text/javascript" src="/js/calendar/calendar.js?random=20060118"></script>

<?php

$id="";
$enctype="";

$action_url="/modules/admin/information/locator.php";

$action="SEARCH";

$columnas=4;

$util = new Util();

$obj = new InfoSummary();

include($_SERVER['DOCUMENT_ROOT'].'/modules/admin/information/locator_action.php');

?>


<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMScreenHeader.php'); ?>

<?php
// NUEVO ++++++++++++++++++++++
if ($from!=0){
?>
	<input type="hidden" name="from" value="<?php echo $from; ?>" />
<?php
}
// ++++++++++++++++++++++++++++
?>

	<fieldset class="locator">
	
		<p>
			<label><?php echo LBL_TITLE;?>:</label>
			<?php echo $obj->buildOperatorSymbol("name","char",(isset($_POST['op_name'])?$_POST['op_name']:"9") ); ?>
			<input type="text" name="name" id="name" value="<?php echo (isset($_POST['name'])?$_POST['name']:""); ?>" size="25" maxlength="50"/>
		</p>
		
		<p>
			<label><?php echo LBL_DESCRIPTION;?>:</label>
			<?php echo $obj->buildOperatorSymbol("info_desc","char",(isset($_POST['op_info_desc'])?$_POST['op_info_desc']:"9") ); ?>
			<input type="text" name="info_desc" id="info_desc" value="<?php echo (isset($_POST['info_desc'])?$_POST['info_desc']:""); ?>" size="25" maxlength="50"/>
		</p>
		
		<p>
			<label><?php echo LBL_CONTENTS;?>:</label>
			<?php echo $obj->buildOperatorSymbol("contents","char",(isset($_POST['op_contents'])?$_POST['op_contents']:"9") ); ?>
			<input type="text" name="contents" id="contents" value="<?php echo (isset($_POST['contents'])?$_POST['contents']:""); ?>" size="25" maxlength="50"/>
		</p>
		
		<p>
			<label><?php echo LBL_DATE;?>:</label>
			<?php echo $obj->buildOperatorSymbol("info_date","date",(isset($_POST['op_info_date'])?$_POST['op_info_date']:"") ); ?>
			<input type="text" name="info_date" id="info_date" class="readonly" size="16" readonly value="<?php echo (isset($_POST['info_date'])?$_POST['info_date']:""); ?>"/>&nbsp;&nbsp;
			<a href="javascript:void(null);" onClick="displayCalendar(document.forms[0].info_date,'dd/mm/yyyy',this);"><img src="/images/calendar.gif" border="0" alt="<?php echo LBL_ALT_CALENDAR; ?>" title="<?php echo LBL_ALT_CALENDAR; ?>" /></a>
			<a href="javascript:cleanField('info_date');"><img src="/images/delete.gif" alt="<?php echo LBL_DELETE; ?>" title="<?php echo LBL_DELETE; ?>" /></a>
		</p>
		
		<br class="limpiar"/>

		<input type="submit" name="search" value="<?php echo LBL_LOC_ACTION; ?>" class="boton" onClick="doSearch()"/>

	</fieldset>

      <table class="tabla1" summary="<?php echo LBL_LOC_SUM_TITLE1; ?>">
			<caption><?php echo LBL_LOC_SUM_TITLE1; ?></caption>
			<thead>
			<tr>
                <th><?php echo LBL_TITLE;?>&nbsp;<a href="javascript:doOrderBy('1')"><img src="/images/flechas.gif" width="7" height="10" alt=""/></a></th>
                <th><?php echo LBL_DESCRIPTION;?>&nbsp;<a href="javascript:doOrderBy('2')"><img src="/images/flechas.gif" width="7" height="10" alt=""/></a></th>
				<th><?php echo LBL_DATE;?>&nbsp;<a href="javascript:doOrderBy('4')"><img src="/images/flechas.gif" width="7" height="10" alt=""/></a></th>
			</tr>
			<tr>
				<td colspan="<?php echo $columnas; ?>" class="separa">&nbsp;</td>
			</tr>

			</thead>


<?php 
	//Cargo el footer
	include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMListFooter.php'); 
?>
		
			<tbody>

<?php
					//Muestro los datos.
					$contador = 0;
					while($row = $resultado->fetch_array())		
					{
				?>
			        <tr <?php if (fmod($contador,2)){ ?>class="fondo"<?php } ?>>
						<td>
<SCRIPT language="JavaScript" type="text/javascript">
// NUEVO ++++++++++++++++++++++
var aux = new Array();
aux[0] = "<?php echo $row["info_id"]; ?>";
aux[1] = "<?php echo $row["info_title"]; ?>";
resultado[contador] = aux;
contador++;
// ++++++++++++++++++++++++++
</SCRIPT>			

						  <a href="javascript:goFromLoc('<?php echo $object_type_id; ?>',resultado[<?php echo $contador; ?>],'<?php echo $from; ?>');" title="<?php echo $util->truncate($row["info_desc"],25,true); ?>">&nbsp;<?php echo $util->truncate($row["info_title"],25); ?></a> 
						</td>

						<td>
						  <a href="javascript:goFromLoc('<?php echo $object_type_id; ?>',resultado[<?php echo $contador; ?>],'<?php echo $from; ?>');" title="<?php echo $util->truncate($row["info_desc"],30,true); ?>">&nbsp;<?php echo $util->truncate($row["info_desc"],30); ?></a>
						</td>
						
						<td>
						  <a href="javascript:goFromLoc('<?php echo $object_type_id; ?>',resultado[<?php echo $contador; ?>],'<?php echo $from; ?>');" >&nbsp;<?php echo $format->formatea_fecha($row["info_date"]); ?></a>
						</td>
						
					</tr>
              
                <?php $contador++;} ?>
			  
			</tbody>
			
	  </table>
      
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMScreenBottom.php'); ?>


<script type="text/javascript" CHARSET="ISO-8859-1" defer >
<!--
document.data_frm.name.focus(); 
-->
</script>


<?php $type = "summary"; ?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMEndDocument.php'); ?>