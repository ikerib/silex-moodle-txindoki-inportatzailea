<?php 
$buttons="buttons_detail.php";
$tab=1;
$tabs="tabs.php";
$object_type_id=57 ;


include($_SERVER['DOCUMENT_ROOT']."/includes/control.php"); 

require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/admin/Attachment.php'); 
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/Functions.php'); 
include($_SERVER['DOCUMENT_ROOT']."/lang/".$_SESSION["language"]."/lbl_attachment.php"); 
require_once($_SERVER['DOCUMENT_ROOT']."/config_app.php"); 


$id="";
$enctype="multipart/form-data";
$action_url="/modules/admin/attachment/detail.php";
$action="";

$obj = new Attachment();
$util = new Util();

if (isset($_GET["id"]) && $_GET["id"]!="")
	$id=$_GET["id"];
else if (isset($_POST["id"]) && $_POST["id"]!="")
	$id=$_POST["id"];



//Comprobamos si viene de un borrado
if (isset($_POST['action']) && $_POST['action']=="DELETE"){
	include($_SERVER['DOCUMENT_ROOT'].'/modules/admin/attachment/detail_action.php');
	exit;
}
	
	
if ($id!=""){
	
	if ($id=="new"){
		$obj->loadData("new");
		$action="NEW";
		$id="new";
	}else if (is_numeric($id)){	
		$obj->loadData($id);
		$action="UPDATE";
	}else{
		echo "error1"; // o producir un error hacia una web
	}
}else{
	echo "eerror2"; // o producir un error hacia una web
}	

if (isset($_POST["id"])){
	include($_SERVER['DOCUMENT_ROOT'].'/modules/admin/attachment/detail_action.php');
}



include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMBeginDocument.php');


//Chequeo que el propietario del objeto es la compa��a actual, entonces podr� modificarlo.
if ($_SESSION["company_id"]==$obj->getCompanyID()){
	$bReadOnly = false;
}

?>

<script language="JavaScript" type="text/javascript" CHARSET="ISO-8859-1">
	var mesg_WARNING = "<?php echo LBL_WARNING; ?>";
	var mesg_SELECT_FILE = "<?php echo MSG_ATT_SELECT_FILE; ?>";
	var mesg_CHECK_EXTENSION_FILE = "<?php echo MSG_CHECK_EXTENSION_FILE; ?>";
</script>
	

<script src="/modules/admin/attachment/js/data_check.js" type="text/javascript" charset="iso-8859-1"></script>
<?php 
include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMHeaderTags.php'); 

?>


<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMScreenHeader.php'); ?>

	<fieldset>			
		
		<p>
			<label><span class="obligatorio"><?php echo MSG_CAMPOS_OBLIGATORIOS;?></span></label>
		</p>
		<br class="limpiar"/>
		<br/>
		
		<p>
			<label><?php echo LBL_FILE_NAME; ?>: <span class="obligatorio">*</span></label><br/>
			<input type="text" name="name" onChange="setDirty(this);" size="80" maxlength="200" <?php echo ($bReadOnly)?"readonly=\"readonly\" class=\"readonly\" ":""; ?> value="<?php echo $obj->getName(); ?>"/>
		</p>

		<br class="limpiar"/>

		<p>
			<label><?php echo LBL_DESCRIPTION; ?>:</label><br/>
			<input type="text" name="attachment_desc" onChange="setDirty(this);" size="80" maxlength="200" <?php echo ($bReadOnly)?"readonly=\"readonly\" class=\"readonly\" ":""; ?> value="<?php echo $obj->getAttachmentDesc(); ?>"/>
		</p>
			
			
		<br class="limpiar"/>	
		<p>
			<label><?php echo LBL_FILE; ?>:</label>
			<br/>
			
			<input type="file" id="archive" name="archive" size="50" maxlength="100" onChange="setDirty(this);"/>
			<input type="hidden" name="lim_tamano" value="<?php echo CFG_ATTACHMENT_SIZE_BYTES; ?>"/> 
		</p>
		
		<br class="limpiar"/>	
		
		<p><b><?php echo strtoupper(LBL_ATT_NOTE); ?></b>:<?php echo str_replace("$1",CFG_ATTACHMENT_SIZE_KB,MSG_ATT_INFO_SIZE); ?></p>
		<br class="limpiar"/>
		
		<p>
			<center>
			<?php if ($obj->getAttachmentFileExists() && $obj->getAttachmentFile()!=""){ ?>
				<p><a target="_blank" href="/modules/admin/attachment/file.php?company=<?php echo $obj->getCompanyID();?>&amp;id=<?php echo $obj->getID();?>&amp;name=<?php echo $obj->getAttachmentFile();?>"><img name="img_file" src="/images/attachment.gif" width="16" height="16" border="0"/>&nbsp;<?php echo $obj->getAttachmentFile();?></a></p>
			<?php }else{ ?>
				<font color="#ff0000"><?php echo MSG_ATT_SELECT_FILE; ?></font>
			<?php } ?>
			</center>		
			<br class="limpiar"/>
		</p>
			
		<br class="limpiar"/>
		
		<p>
			<label><?php echo LBL_OBJECT_TYPE; ?>: <span class="obligatorio">*</span></label><br/>
			<?php if ($_SESSION["company_id"]==1){ ?>
				<select name="foreign_object_type_id" onChange="setDirty(this);cleanObject();">
					<?php echo $util->desplegable("ica_object_type", "object_type_id", "object_type_name", $obj->getObjectTypeID(), $_SESSION["language"], true, 0, "object_type_name", "AND object_type_id IN (7,19,22,300,301,302,303)"); ?>
				</select>
			<?php }else{ ?>	
				<select name="foreign_object_type_id" onChange="setDirty(this);cleanObject();">
					<?php echo $util->desplegable("ica_object_type", "object_type_id", "object_type_name", $obj->getObjectTypeID(), $_SESSION["language"], true, 0, "object_type_name", "AND object_type_id IN (7,19,22,300,301,302,303)"); ?>
				</select>
			<?php } ?>
			
		</p>
		
		<p>
			<label><?php echo LBL_RELATED_OBJECT; ?>: <span class="obligatorio">*</span></label><br/>
			<input type="text" name="foreign_object_name" onChange="setDirty(this);cleanObject();" readonly="readonly" class="readonly" size="50" maxlength="100" <?php echo ($bReadOnly)?"readonly=\"readonly\" class=\"readonly\" ":""; ?> value="<?php echo $obj->getObjectName(); ?>"/>
			<input type="hidden" name="foreign_object_id" id="foreign_object_id" value="<?php echo $obj->getObjectID(); ?>"/>
			
			<a href="javascript:goToLoc(document.data_frm.foreign_object_type_id.value,'<?php echo $object_type_id; ?>');"><img src="/images/search.gif" alt="<?php echo LBL_SEARCH; ?>" title="<?php echo LBL_SEARCH; ?>" border="0"></a>
			<a href="javascript:goToObject(document.data_frm.foreign_object_type_id.options[document.data_frm.foreign_object_type_id.selectedIndex].value,document.data_frm.foreign_object_id.value);">
                <img src="/images/seedetail.gif" alt="<?php echo LBL_VIEW; ?>" title="<?php echo LBL_VIEW; ?>" border="0"></a>

			<a href="javascript:cleanObject();"><img src="/images/delete.gif" alt="<?php echo LBL_DELETE; ?>" title="<?php echo LBL_DELETE; ?>" border="0" /></a>
		</p>
		
		<br class="limpiar"/>
		
		<p>
			<label><?php echo LBL_REMARKS; ?>:</label><br/>
			<textarea name="remarks" class="mceNoEditor" cols="100" rows="7" onChange="setDirty(this);" <?php echo ($bReadOnly)?"readonly=\"readonly\" class=\"readonly\" ":""; ?> ><?php echo $obj->getRemarks(); ?></textarea>
		</p>

	</fieldset>








<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMObjectTimestamp.php'); ?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMScreenBottom.php'); ?>


<script type="text/javascript" CHARSET="ISO-8859-1" defer >
<!--

//document.data_frm.name.focus(); 

// -->
</script>

<?php /*


<input type="hidden" name="picture_file_exists" value="<%=oScreenData.getPictureFileExists()?"true":"false"%>"/>
<input type="hidden" name="picture_file_old" value="<crm:format type="String"><jsp:getProperty name="oScreenData" property="pictureFile"/></crm:format>"/>
<input type="hidden" name="picture_name_old" value="<crm:format type="String"><jsp:getProperty name="oScreenData" property="pictureName"/></crm:format>"/>
<input type="hidden" name="picture_width_old" value="<crm:format type="String"><jsp:getProperty name="oScreenData" property="pictureWidth"/></crm:format>"/>
<input type="hidden" name="picture_height_old" value="<crm:format type="String"><jsp:getProperty name="oScreenData" property="pictureHeight"/></crm:format>"/>

<input type="hidden" name="picture_file" value="<crm:format type="String"><jsp:getProperty name="oScreenData" property="pictureFile"/></crm:format>"/>
<input type="hidden" name="picture_file_delete" value="<crm:format type="String"><jsp:getProperty name="oScreenData" property="pictureFile"/></crm:format>"/>
<input type="hidden" name="picture_name" value="<crm:format type="String"><jsp:getProperty name="oScreenData" property="pictureName"/></crm:format>"/>
<input type="hidden" name="picture_width" value="<crm:format type="String"><jsp:getProperty name="oScreenData" property="pictureWidth"/></crm:format>"/>
<input type="hidden" name="picture_height" value="<crm:format type="String"><jsp:getProperty name="oScreenData" property="pictureHeight"/></crm:format>"/>
*/
?>
<input type="hidden" name="bupload_attachment" value="no"/>
<input type="hidden" name="attachment_file_old" value="<?php echo $obj->getAttachmentFile(); ?>"/>

<!--
<input type="hidden" name="tab" value="1"/>
<input type="hidden" name="frm_action_type" value=""/>
-->
<input type="hidden" name="id" value="<?php echo $id; ?>"/>

  




<?php $type = ""; ?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMEndDocument.php'); ?>

