

function checkRequiredData()
{
	
	if (validar_attachment()){
		return true;
	}else
		return false;
		
}  // checkRequiredData 


function checkSearchFields()
{
   return true;
}  // checkSearchFields 

function doCancel()
{
   document.data_frm.tab.value=1;
}  // checkSearchFields 




//validar_attachment
function validar_attachment(){

		if (validar_attachment2()){
			return true;
		}
}//end validar_attachment

function validar_attachment2(){
		
		if (document.data_frm.archive.value!=""){
			if (comprueba_extension(document.data_frm, document.data_frm.archive.value)){
					
			
				document.data_frm.bupload_attachment.value="yes";
				return true;
			}else{
				document.data_frm.bupload_attachment.value="no";
				return false; 
			}
		}else{
			if (document.data_frm.attachment_file_old.value!=""){ //Ya existe un archivo, se puede modificar si quiere.
				document.data_frm.bupload_attachment.value="no";
				return true;
			}else{
				document.data_frm.bupload_attachment.value="no";
				alert(mesg_WARNING + '\n' + mesg_SELECT_FILE);
				return false;
			}
		}
		
}//end validar_attachment2


function comprueba_extension(formulario, archivo) {
	extensiones_permitidas = new Array(".pdf", ".doc", ".xls", ".pps", ".jpg", ".gif", ".png", ".txt", ".zip", ".rar");
	mierror = "";
	if (!archivo) {
		//Si no tengo archivo, es que no se ha seleccionado un archivo en el formulario
		mierror = mesg_SELECT_FILE;
	}else{
		//recupero la extensi�n de este nombre de archivo
		extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
		//alert (extension);
		//compruebo si la extensi�n est� entre las permitidas
		permitida = false;
		for (var i = 0; i < extensiones_permitidas.length; i++) {
			if (extensiones_permitidas[i] == extension) {
				permitida = true;
				break;
			}
		}
		if (!permitida) {
			mierror = mesg_CHECK_EXTENSION_FILE + extensiones_permitidas.join();
		}else{
			return 1;
		}
	}
	//si estoy aqui es que no se ha podido submitir
	alert(mesg_WARNING + '\n' + mierror);
	
	return 0;
} 


function cleanObject()
{
   document.data_frm.foreign_object_id.value="";
   document.data_frm.foreign_object_name.value="";
   setDirty(document.data_frm.foreign_object_name);
} //clearObject 
