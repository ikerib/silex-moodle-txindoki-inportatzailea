<?php
//Falta controlar el acceso a este documento... 
//Que solo sea el propietario o la company 1.

require_once($_SERVER['DOCUMENT_ROOT']."/config_app.php"); 

require_once($_SERVER['DOCUMENT_ROOT']."/classes/srm/MimeType.php"); 

function extension_archivo ($ruta) {
    $res = explode(".", $ruta);
    $extension = $res[count($res)-1];
    return $extension ;
}// fin extension_archivo  

$sCompanyID=$_GET["company"];
$sID=$_GET["id"];
$sName=$_GET["name"];
$sFilesPath = $path_contents_secure_absolute . "company" . $sCompanyID . "/files/attachment/";
$sFilesPath = $sFilesPath . $sID . "_" . $sName;

$sExtension = extension_archivo($sName);

$mime_type="";

if (array_key_exists($sExtension , $mime_types)) {
	if (isset($mime_types[$sExtension])){
		$mime_type = $mime_types[$sExtension];
		
		//Abro el documento.
		
		if (is_file($sFilesPath)){
			header("Pragma: public");
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Content-Type: " . $mime_type);
			header('Content-Disposition: attachment; filename="'.$sName.'"'); 
			header("Content-Transfer-Encoding: binary");
			header("Content-Length: ".filesize($sFilesPath));
			ob_clean();
            flush();
			readfile($sFilesPath);
		} else { die("The parameters received were not valid, you can not open the file.");}
		
	} else { 
		die("The parameters received were not valid");
	}
} else { 
	die("The parameters received were not valid.");
}
exit;
?>