<?php $object_type_id=7;
	  require_once($_SERVER['DOCUMENT_ROOT']."/includes/control.php");
	  require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/admin/Employee.php'); 

$obj = new Employee();

if (isset($action)){
	
	if ($action=="NEW"){
		$resultado = $obj->init();
		if ($resultado==""){ //NO hay error.
			$obj->createNew();
			
			$id = $obj->getID(); //�ltimo insertado
			
			//MENSAJE...
			$_SESSION["aviso_tipo"] = "success";
			$_SESSION["aviso_mensaje"] = MSG_INFO_CHANGED_CORRECTLY;
			
			session_write_close();
			Header("Location: detail.php?id=$id"); 
			exit();
			
		}else{ //hay error redirecciono y muestro error.
			$_SESSION["aviso_tipo"] = "warning";  
			$_SESSION["aviso_mensaje"] = $resultado;
			
		}
	}else if ($action=="UPDATE"){
		
		if (isset($action) && is_numeric($id) ) {
			
			$resultado = $obj->init();
			
			if ($resultado==""){ //NO hay error.
				$obj->update($id);
				
				$_SESSION["aviso_tipo"] = "success";
				$_SESSION["aviso_mensaje"] = MSG_INFO_CHANGED_CORRECTLY;
			}else{//hay error redirecciono y muestro error.
				$_SESSION["aviso_tipo"] = "warning";  
				$_SESSION["aviso_mensaje"] = $resultado;
			}
			
			session_write_close();
			Header("Location: detail.php?id=$id"); 
			exit();
		}
	}else if ($action=="ENTER_AS"){
				
				echo "llego";
				
				$obj->enterAs();
				
				session_write_close();
				Header("Location: detail.php?id=$id"); 
				exit();
				
	}else if ($action=="LOGOUT_AS"){
				
				echo "llego";
				
				$obj->logoutAs();
				
				session_write_close();
				Header("Location: ../../main/detail.php"); 
				exit();
				
	}		
			
		
		
}
	
if (isset($_POST['action']) && $_POST['action']=="DELETE"){
	
	if (isset($_POST['id']) && is_numeric($_POST['id']) ) {
			//echo "<br>id=".$_POST['id'];
			$obj->delete($_POST['id']);
			
			//Cargar mensaje de borrado OK.
			$_SESSION["aviso_tipo"] = "success";
			$_SESSION["aviso_mensaje"] = MSG_INFO_DELETED_CORRECTLY;
			
			session_write_close();
			Header("Location: locator.php"); 
			exit();
			
	}
}else{ //hay error redirecciono y muestro error.
	
}


?>
