<?php 
$buttons="buttons_detail.php";
$tab=1;
$tabs="tabs.php";
$object_type_id=7 ;

include($_SERVER['DOCUMENT_ROOT']."/includes/control.php"); 
include($_SERVER['DOCUMENT_ROOT']."/lang/".$_SESSION["language"]."/lbl_employee.php"); 
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/admin/Employee.php'); 
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/Functions.php'); 


$id="";
$enctype="";
$action_url="/modules/admin/employee/detail.php";
$action="";

$obj = new Employee();

if (isset($_GET["id"]) && $_GET["id"]!="")
	$id=$_GET["id"];
else if (isset($_POST["id"]) && $_POST["id"]!="")
	$id=$_POST["id"];



//Comprobamos si viene de un borrado
if (isset($_POST['action']) && $_POST['action']=="DELETE"){
	//echo "---->ACTION=".$_POST['action'];
	include($_SERVER['DOCUMENT_ROOT'].'/modules/admin/employee/detail_action.php');
	exit;
}



//Comprobamos si viene de un "Entrar como"
if (isset($_POST['action']) && $_POST['action']=="ENTER_AS"){
	$action=$_POST['action'];
	include($_SERVER['DOCUMENT_ROOT'].'/modules/admin/employee/detail_action.php');
	exit;
}	
	
if ($id!=""){
	
	if ($id=="new"){
		$obj->loadData("new");
		$action="NEW";
		$id="new";
	}else if (is_numeric($id)){	
		$obj->loadData($id);
		$action="UPDATE";
	}else{
		echo "error1"; // o producir un error hacia una web
	}
}else{
	echo "eerror2"; // o producir un error hacia una web
}	

if (isset($_POST["id"])){
	include($_SERVER['DOCUMENT_ROOT'].'/modules/admin/employee/detail_action.php');
}

$util = new Util();

include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMBeginDocument.php');

include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMHeaderTags.php'); 

?>

<SCRIPT src="/modules/admin/employee/js/data_check.js" type="text/javascript" CHARSET="ISO-8859-1"></SCRIPT>
<link type="text/css" rel="stylesheet" href="/js/calendar/calendar.css?random=20051112" media="screen"></LINK>
<script src="/js/ajax.js" type="text/javascript" charset="iso-8859-1"></script>

<script  type="text/javascript">
	var languageCode = '<?php echo $_SESSION['language'];?>';
	
	var mesg_VALID = "<?php echo LBL_VALID; ?>";
	var mesg_INVALID = "<?php echo LBL_INVALID; ?>";
	var mesg_USERNAME_VALID = "<?php echo MSG_USERNAME_VALID; ?>";
	var mesg_USERNAME_INVALID = "<?php echo MSG_USERNAME_INVALID; ?>";
	
</script>

<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMScreenHeader.php'); ?>


	<fieldset>
		
		
		<p>
			<label><span class="obligatorio"><?php echo MSG_CAMPOS_OBLIGATORIOS;?></span></label>
		</p>
		<br class="limpiar"/>
		<br/>
		
		<p>
			<label><?php echo LBL_FIRST_NAME; ?>: <span class="obligatorio">*</span></label><br/>
			<input type="text" name="name" size="20" maxlength="50" value="<?php echo $obj->getName(); ?>" onChange="setDirty(this);"/>
		</p>
		<p>
			<label><?php echo LBL_LAST_NAME1; ?>: </label><br/>
			<input type="text" name="last_name_1" size="20" maxlength="50" value="<?php echo $obj->getPersonLastName1(); ?>" onChange="setDirty(this);"/>
		</p>
		<p>
			<label><?php echo LBL_INTERNAL_ID4; ?>: </label><br/>
			<input type="text" name="internal_id" size="20" maxlength="50" value="<?php echo $obj->getInternalID(); ?>" onChange="setDirty(this);"/>
		</p>
		
		<br class="limpiar"/>
		
		<p>
			<label><?php echo LBL_DEPARTMENT; ?>: </label><br/>
			<input type="text" name="department" size="48" maxlength="200" value="<?php echo $obj->getDepartment(); ?>" onChange="setDirty(this);"/>
		</p>
		
	</fieldset>
	
	
	<br/>

	<p><strong><?php echo LBL_USER_DATA; ?>:</strong></p><br/>

	<fieldset class="coloreado">

	<?php if (!$obj->getIsUser()){?>
		<p><?php echo MSG_USER_DATA_INFO1; ?></p>
		<br class="limpiar"/>
		<p>
			<label><?php echo LBL_USER_LOGIN; ?>: <span class="obligatorio">*</span></label><br/>
			<input type="text" name="user_name" size="25" maxlength="30" value="<?php echo $obj->getUserID(); ?>" onBlur="checkUserID();" onChange="setDirty(this);"/>
			<img src="/images/ko.gif" id="img_validate" alt="" />
			<input type="hidden" name="user_valido" id="user_valido" value=""/>
		</p>
	<?php }else{ ?>
		<p>
			<label><?php echo LBL_USER_LOGIN; ?>: <span class="obligatorio">*</span></label><br/>
			<input type="text" name="user_name" size="25" maxlength="30" readonly class="readonly" value="<?php echo $obj->getUserID(); ?>" />
		</p>
	<?php } ?>
		<p>
			<label><?php echo LBL_USER_EMAIL; ?>: <span class="obligatorio">*</span></label><br/>
			<input type="text" name="user_email" size="40" maxlength="200" value="<?php echo $obj->getUserEmail(); ?>" onChange="setDirty(this);"/>
		</p>
		
		
		
		
	<?php if ($obj->getIsUser()){?>	
		<br class="limpiar"/>
		<br/>
		
		<p><?php echo MSG_USER_DATA_INFO2; ?></p>
		<br class="limpiar"/>
		
		<p>
		<label><?php echo LBL_NEW_PASSWORD; ?>:</label><br />
		<input type="password" name="npass" size="20" maxlength="32" value="" onChange="setDirty(this);"/>
		</p>			

		<p>
		<label><?php echo LBL_CONFIRM_PASSWORD; ?>:</label><br />
		<input type="password" name="cpass" size="20" maxlength="32" value="" onChange="setDirty(this);"/>
		</p>			
	<?php } ?>
	
		
	</fieldset>

	<br class="limpiar"/>
	

	<fieldset>
	
		<p>
			<label><?php echo LBL_REMARKS1; ?>: </label><br/>
			<textarea name="remarks" cols="100" rows="7" onChange="setDirty(this);"><?php echo $obj->getRemarks(); ?></textarea>
		</p>

	</fieldset>

<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMObjectTimestamp.php'); ?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMScreenBottom.php'); ?>


<script type="text/javascript" CHARSET="ISO-8859-1" defer >
<!--
<?php if (!$obj->getIsUser()){?>
	checkUserID();
<?php } ?>

// -->
</script>

	<input type="hidden" name="id" value="<?php echo $id; ?>"/>
	<input type="hidden" name="is_user" value="<?php echo $obj->getIsUser(); ?>"/>	
	<input type="hidden" name="sex" value="1"/>
	<input type="hidden" name="national_id_type" value="1"/>
	<input type="hidden" name="id_card" value="1"/>
	<input type="hidden" name="birthdate" value=""/>
	
	<input type="hidden" name="birthplace" value=""/>
	<input type="hidden" name="birthregion" value=""/>
	<input type="hidden" name="birthcountry" value=""/>
	
	<input type="hidden" name="nationality" value=""/>
	<input type="hidden" name="mother_tongue" value=""/>
	<input type="hidden" name="date_start" value=""/>
	<input type="hidden" name="date_end" value=""/>
	<input type="hidden" name="last_name_2" value=""/>
	
<?php $type = ""; ?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMEndDocument.php'); ?>

