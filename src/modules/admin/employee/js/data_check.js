

function checkRequiredData()
{
	return true;
}  // checkRequiredData 


function checkSearchFields()
{
   return true;
}  // checkSearchFields 

function doCancel()
{
   document.data_frm.tab.value=1;
}  // checkSearchFields 

function doChangeMainChannel(valor)
{
  document.data_frm.main_flag_ok.value=valor;
}

function doDeleteChannel(valor)
{
   if( confirm(mesg_delete)==true){
		document.data_frm.action.value="DELETE_CHANNEL";
		document.data_frm.delete_channel.value=valor;
	}else
		return;
	doSubmit();
	
}  // doDeleteChannel


function doEnterAs()
{
   	document.data_frm.action.value="ENTER_AS";
	doSubmit();
	
}  // doEnterAs



function checkUserID(){
	
	document.data_frm.user_name.value = userValidate(document.data_frm.user_name.value);
	cargar_asincrono('/modules/admin/employee/checkUser.php?user_name='+document.data_frm.user_name.value , 'user_valido');
	
	if (document.data_frm.user_valido.value=="NO EXISTE"){ //OK
		document.getElementById('img_validate').src="/images/ok.gif";
		document.getElementById('img_validate').title=mesg_USERNAME_VALID;
		document.getElementById('img_validate').alt=mesg_VALID;
	}else if (document.data_frm.user_valido.value=="EXISTE"){ //KO
		document.getElementById('img_validate').src="/images/ko.gif";
		document.getElementById('img_validate').title=mesg_USERNAME_INVALID;
		document.getElementById('img_validate').alt=mesg_INVALID;
		return false;
	}
}


//Funci�n que valida y reemplaza los caracteres no permitidos en javascript.

function userValidate(cadena){
	
	var caracteres_validos = "abcdefghijklmnopqrstuvwxyz0123456789@.";
	
	var cadena_valida = "";
	
	for (var i=0; i<cadena.length; i++)
	{
	
		var caracter = cadena.charAt(i);
		
		if (caracteres_validos.indexOf(caracter)!=-1){
			cadena_valida=cadena_valida+caracter;
		}
		
	}
	
	return cadena_valida;
}
