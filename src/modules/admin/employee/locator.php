<?php 
$locator="yes" ;
$buttons="buttons_locator.php";
$tabs="tabs_locator.php";
$object_type_id=7 ;
// NUEVO ++++++++++++++++++
$from = 0;
if (isset($_GET["from"]))
	$from = $_GET["from"];
else if (isset($_POST["from"]))
	$from = $_POST["from"];
// ++++++++++++++++++++++++

include($_SERVER['DOCUMENT_ROOT']."/includes/control.php"); 

//Cargo la clase para los mostrar los datos.
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/admin/EmployeeSummary.php'); 
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/Functions.php'); 
include($_SERVER['DOCUMENT_ROOT']."/lang/".$_SESSION["language"]."/lbl_locator.php"); 
include($_SERVER['DOCUMENT_ROOT']."/lang/".$_SESSION["language"]."/lbl_employee.php"); 

include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMBeginDocument.php');

include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMHeaderTags.php');
?>

<script src="/modules/admin/employee/js/data_check.js" type="text/javascript" charset="iso-8859-1"></script>
<link type="text/css" rel="stylesheet" href="/js/calendar/calendar.css?random=20051112" media="screen"></LINK>

<script  type="text/javascript">
	var languageCode = '<?php echo $_SESSION['language'];?>';
</script>

<script type="text/javascript" src="/js/calendar/calendar.js?random=20060118"></script>

<script  type="text/javascript">
	// NUEVO ++++++++++++++++++++++
	var resultado = new Array();
	var contador = 0;	
	// ++++++++++++++++++++++++++++
		
</script>

<?php

$id="";
$enctype="";
$action_url="/modules/admin/employee/locator.php";
$action="SEARCH";

$columnas=4;

$obj = new EmployeeSummary();

$util = new Util();
$format = new Format();

include($_SERVER['DOCUMENT_ROOT'].'/modules/admin/employee/locator_action.php');

?>

<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMScreenHeader.php'); ?>

<?php
// NUEVO ++++++++++++++++++++++
if ($from!=0){
?>
	<input type="hidden" name="from" value="<?php echo $from; ?>" />
<?php
}
// ++++++++++++++++++++++++++++
?>

	<fieldset class="locator">
	
		<p>
			<label><?php echo LBL_FIRST_NAME; ?>:</label>
			<?php echo $obj->buildOperatorSymbol("name","char",(isset($_POST['op_name'])?$_POST['op_name']:"9") ); ?>
			<input type="text" name="name" id="name" value="<?php echo (isset($_POST['name'])?$_POST['name']:""); ?>" size="25" maxlength="50"/>
		</p>

		<p>
			<label><?php echo LBL_LAST_NAME1; ?>:</label>
			<?php echo $obj->buildOperatorSymbol("last_name_1","char",(isset($_POST['op_last_name_1'])?$_POST['op_last_name_1']:"9") ); ?>
			<input type="text" name="last_name_1" id="last_name_1" value="<?php echo (isset($_POST['last_name_1'])?$_POST['last_name_1']:""); ?>" size="25" maxlength="50"/>
		</p>

		<p>
			<label><?php echo LBL_INTERNAL_ID4; ?>:</label>
			<?php echo $obj->buildOperatorSymbol("internal_id","char",(isset($_POST['op_internal_id'])?$_POST['op_internal_id']:"9") ); ?>
			<input type="text" name="internal_id" id="internal_id" value="<?php echo (isset($_POST['internal_id'])?$_POST['internal_id']:""); ?>" size="25" maxlength="50"/>
		</p>		
		
		<p>
			<label><?php echo LBL_USER_LOGIN; ?>:</label>
			<?php echo $obj->buildOperatorSymbol("user_id","char",(isset($_POST['op_user_id'])?$_POST['op_user_id']:"9") ); ?>
			<input type="text" name="user_id" id="user_id" value="<?php echo (isset($_POST['user_id'])?$_POST['user_id']:""); ?>" size="25" maxlength="50"/>
		</p>
	
		<br class="limpiar"/>

		<input type="submit" name="search" value="<?php echo LBL_LOC_ACTION; ?>" class="boton" onClick="doSearch()"/>

	</fieldset>

      <table class="tabla1" summary="<?php echo LBL_LOC_SUM_TITLE1; ?>">
			<caption><?php echo LBL_LOC_SUM_TITLE1; ?></caption>
			<thead>
			<tr>
                <th><?php echo LBL_FIRST_NAME; ?>&nbsp;<a href="javascript:doOrderBy('1')"><img src="/images/flechas.gif" width="7" height="10" alt=""/></a></th>
				<th><?php echo LBL_LAST_NAME1; ?>&nbsp;<a href="javascript:doOrderBy('2')"><img src="/images/flechas.gif" width="7" height="10" alt=""/></a></th>
                <th><?php echo LBL_INTERNAL_ID4; ?>&nbsp;<a href="javascript:doOrderBy('4')"><img src="/images/flechas.gif" width="7" height="10" alt=""/></a></th>
                <th><?php echo LBL_USER_LOGIN; ?>&nbsp;<a href="javascript:doOrderBy('3')"><img src="/images/flechas.gif" width="7" height="10" alt=""/></a></th>
			</tr>
			<tr>
				<td colspan="<?php echo $columnas; ?>" class="separa">&nbsp;</td>
			</tr>

			</thead>


<?php 
	//Cargo el footer
	include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMListFooter.php'); 
?>
		
			<tbody>
			 
				<?php
					//Muestro los datos.
					$contador = 0;
					while($row = $resultado->fetch_array())		
					{
				?>

				
			        <tr <?php if (fmod($contador,2)){ ?>class="fondo"<?php } ?>>
						<td>
<SCRIPT language="JavaScript" type="text/javascript">
// NUEVO ++++++++++++++++++++++
var aux = new Array();
aux[0] = "<?php echo $row["employee_id"]; ?>";
aux[1] = "<?php echo $row["employee_first_name"]; ?>";
aux[2] = "<?php echo $row["employee_last_name1"]; ?>";
aux[3] = "<?php echo $row["user_id"]; ?>";
resultado[contador] = aux;
contador++;
// ++++++++++++++++++++++++++
</SCRIPT>	

						  <a href="javascript:goFromLoc('<?php echo $object_type_id; ?>',resultado[<?php echo $contador; ?>],'<?php echo $from; ?>');" title="<?php echo $util->truncate($row["employee_first_name"],25,true); ?>">&nbsp;<?php echo $util->truncate($row["employee_first_name"],25); ?></a> 
						</td>
						<td>
							<a href="javascript:goFromLoc('<?php echo $object_type_id; ?>',resultado[<?php echo $contador; ?>],'<?php echo $from; ?>');" title="<?php echo $util->truncate($row["employee_last_name1"],25,true); ?>">&nbsp;<?php echo $util->truncate($row["employee_last_name1"],25); ?></a> 
						</td>
						<td>
							<a href="javascript:goFromLoc('<?php echo $object_type_id; ?>',resultado[<?php echo $contador; ?>],'<?php echo $from; ?>');" >&nbsp;<?php echo $row["internal_id"]; ?></a>
						</td>
						<td>
							<a href="javascript:goFromLoc('<?php echo $object_type_id; ?>',resultado[<?php echo $contador; ?>],'<?php echo $from; ?>');" >&nbsp;<?php echo $row["user_id"]; ?></a>
						</td>
					</tr>
					<?php $contador++; ?>
                <?php } ?>
			  
			</tbody>
			
	  </table>
      
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMScreenBottom.php'); ?>


<script type="text/javascript" CHARSET="ISO-8859-1" defer >
<!--
document.data_frm.name.focus(); 
-->
</script>

<?php $type = "summary"; ?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMEndDocument.php'); ?>