<?php 
$buttons="buttons_detail.php";
$tab=3;
$tabs="tabs.php";
$object_type_id=7 ;

include($_SERVER['DOCUMENT_ROOT']."/includes/control.php"); 
require_once($_SERVER['DOCUMENT_ROOT']."/lang/".$_SESSION["language"]."/lbl_role.php"); 
require_once($_SERVER['DOCUMENT_ROOT']."/lang/".$_SESSION["language"]."/lbl_employee.php"); 
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/admin/Employee.php'); 
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/Functions.php'); 


$id="";
$enctype="";
$action_url="/modules/admin/employee/roles.php";
$action="";

$obj = new Employee();

if (isset($_GET["id"]) && $_GET["id"]!="")
	$id=$_GET["id"];
else if (isset($_POST["id"]) && $_POST["id"]!="")
	$id=$_POST["id"];
	
	
//Comprobamos si viene de un borrado
if (isset($_POST['action']) && $_POST['action']=="DELETE"){
	echo "---->ACTION=".$_POST['action'];
	include($_SERVER['DOCUMENT_ROOT'].'/modules/admin/employee/detail_action.php');
	exit;
}	

	
if ($id!="" && is_numeric($id)){
	$obj->loadData($id);
	$action="UPDATE";
}else{
	echo "eerror2"; // o producir un error hacia una web
}	

if (isset($_POST["id"])){
	include($_SERVER['DOCUMENT_ROOT'].'/modules/admin/employee/roles_action.php');
}

$rolesAsignados = $obj->getAssignedRoles();
$rolesNoAsignados = $obj->getNoAssignedRoles();	

include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMBeginDocument.php');

include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMHeaderTags.php'); 

?>

<SCRIPT src="/modules/admin/employee/js/roles.js" type="text/javascript" CHARSET="ISO-8859-1"></SCRIPT>
<script src="/js/select_events.js"  type="text/javascript" CHARSET="ISO-8859-1"></script>

<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMScreenHeader.php'); ?>




	<div id="roles">
		<div class="posibles">
			<p><?php echo LBL_POSSIBLE_ROLES; ?>:</p>
			<select name="from" size="10" multiple onChange="setDirty(this);">
<?php
	while($row = $rolesNoAsignados->fetch_array())		
	{
?>	
				<option value="<?php echo $row["role_id"]; ?>"><?php echo $row["role_name"]; ?></option>
<?php
	}
?>
			</select>				
		</div>
		<div class="botones">
			<p><input type="button" style="width:180px;" name="add_all" class="boton" value="<?php echo LBL_ADD_ROLE_ALL; ?> &gt;&gt;" onClick="doAddAll(true); setDirty(this);"/></p>
            <p><input type="button" style="width:180px;" name="add" class="boton" value="<?php echo LBL_ADD_ROLE; ?> &gt;" onClick="doAdd(true); setDirty(this);"/></p>
            <p><input type="button" style="width:180px;" name="remove" class="boton" value="&lt; <?php echo LBL_REMOVE_ROLE; ?>" onClick="doRemove(true); setDirty(this);"/></p>
            <p><input type="button" style="width:180px;" name="remove_all" class="boton" value="&lt;&lt; <?php echo LBL_REMOVE_ROLE_ALL; ?>" onClick="doRemoveAll(true); setDirty(this);"/></p>		
		</div>
		<div class="asignados">
			<p><?php echo LBL_ASSIGNED_ROLES; ?>:</p>
			 <select name="to" size="10" multiple onChange="setDirty(this);">
<?php
	while($row = $rolesAsignados->fetch_array())		
	{
?>		
				<option value="<?php echo $row["role_id"]; ?>"><?php echo $row["role_name"]; ?></option>
<?php
	}
?>
			</select>	
		</div>
		<br class="limpiar" />
	</div>

<br class="limpiar"/>

<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMScreenBottom.php'); ?>


<script type="text/javascript" CHARSET="ISO-8859-1" defer >
<!--

//document.data_frm.name.focus(); 

// -->
</script>

<input type="hidden" name="id" value="<?php echo $obj->getID(); ?>"/>
<input type="hidden" name="assig_roles" value=""/>


<?php $type = ""; ?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMEndDocument.php'); ?>

