<?php 
$buttons="buttons_detail.php";
$tab=4;
$tabs="tabs.php";
$object_type_id=7;

include($_SERVER['DOCUMENT_ROOT']."/includes/control.php"); 
require_once($_SERVER['DOCUMENT_ROOT']."/lang/".$_SESSION["language"]."/lbl_general.php");
require_once($_SERVER['DOCUMENT_ROOT']."/lang/".$_SESSION["language"]."/lbl_userdata.php");
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/admin/Employee.php'); 
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/admin/User.php'); 


$id="";

if (isset($_GET["id"]) && $_GET["id"]!="")
	$id=$_GET["id"];
else if (isset($_POST["id"]) && $_POST["id"]!="")
	$id=$_POST["id"];

$main_flag_ok="";	
if (isset($_GET["main_flag_ok"]) && $_GET["main_flag_ok"]!="")
	$main_flag_ok=$_GET["main_flag_ok"];
else if (isset($_POST["main_flag_ok"]) && $_POST["main_flag_ok"]!="")
	$main_flag_ok=$_POST["main_flag_ok"];

$columnas=2;	
$campo1="";
$ruta_new="";

$obj = new Employee();
$obj->loadData($id);
$objUser = new User($obj->getUserID());
$objUser->loadData();

//Comprobamos si viene de un guardado de la pestaña de canales
if (isset($_POST['action']) && $_POST['action']=="SAVE"){
	//echo "---->ACTIONnnn=".$_POST['action'];
	$action=$_POST['action'];
	include($_SERVER['DOCUMENT_ROOT'].'/modules/admin/employee/userdata_action.php');

//Comprobamos si viene de un borrado de un canal
}else if (isset($_POST['action']) && $_POST['action']=="DELETE_CHANNEL"){
	//echo "---->ACTIONnnn=".$_POST['action'];
	$action=$_POST['action'];
	include($_SERVER['DOCUMENT_ROOT'].'/modules/admin/employee/userdata_action.php');
}

$main_flag_ok="";

require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/Functions.php'); 
include($_SERVER['DOCUMENT_ROOT']."/lang/".$_SESSION["language"]."/lbl_locator.php"); 

require_once($_SERVER['DOCUMENT_ROOT']."/config_app.php"); 

include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMBeginDocument.php');

include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMHeaderTags.php');
?>
<script type="text/javascript" CHARSET="ISO-8859-1" defer >
	<!--
	var mesg_email_text01="<?php echo MSG_EMAIL_TEXT01;?>";
	-->
</script>

<script src="/modules/admin/employee/js/userdata.js" type="text/javascript" charset="iso-8859-1"></script>

<?php

$enctype="";
$action_url="/modules/admin/employee/userdata.php";
$action="NOTHING";

$util = new Util();
	
//Comprobamos si viene de un guardado de datos

if (isset($_POST['action']) && ($_POST['action']=="SAVE_EMAIL_LANGUAGE" || $_POST['action']=="SAVE_PASSWORD") ){
	//echo "---->ACTIONnnn=".$_POST['action'];
	$action=$_POST['action'];
	include($_SERVER['DOCUMENT_ROOT'].'/modules/admin/employee/userdata_action.php');
	exit;
}

	
include($_SERVER['DOCUMENT_ROOT'].'/modules/admin/employee/userdata_action.php');

?>

<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMScreenHeader.php'); ?>
	
	<?php if ($obj->getUserID()!=""){ ?>

		<div class="columna_izquierda">
			
			<fieldset>

				<p>
					<label><?php echo LBL_USER_ID; ?>:</label><br />
					<input type="text" name="user_id" class="readonly" readonly size="25" maxlength="20" value="<?php echo $obj->getUserID(); ?>"/>
				</p>

				<br class="limpiar" />

				<p>
					<label><?php echo LBL_LANGUAGE; ?>:</label><br />
					<select name="language" onChange="setDirty(this);" style="width:142px;">
					<?php echo $util->desplegable("ica_language", "language_code", "language_name", $objUser->getLanguage(), $_SESSION["language"], false, 0); ?>
					</select>
				</p>

				<br class="limpiar" />

				<p>
				<label><?php echo LBL_EMAIL1; ?>:</label><br />
				<input type="text" name="email" size="35" maxlength="200" value="<?php echo $objUser->getEmail(); ?>"/>
				</p>
				
				<br class="limpiar" />
				
				<p>
					<input type="button" name="send_info" value="<?php echo LBL_SAVE; ?>" class="boton" onClick="javascript:checkEmailLanguage()"/>&nbsp;&nbsp;&nbsp;
				</p>

			</fieldset>
			
		</div>
		<div class="columna_derecha">

			<fieldset>			

				<p>
				<label><?php echo LBL_NEW_PASSWORD; ?>:</label><br />
				<input type="password" name="npass" size="20" maxlength="32" value=""/>
				</p>			

				<br class="limpiar" />

				<p>
				<label><?php echo LBL_CONFIRM_PASSWORD; ?>:</label><br />
				<input type="password" name="cpass" size="20" maxlength="32" value=""/>
				</p>			

				<br class="limpiar" />

				<p>
				 <input type="button" name="passwd_ch" value="<?php echo LBL_CHANGE_PASSWORD; ?>" class="boton" onClick="javascript:checkPassword()"/>
				</p>

			</fieldset>			
		
		</div>
	<?php }else{ ?>	
		<br/><br/><br/><br/>
		<?php echo MSG_INFO_EMPLOYEE; ?>
		<br/><br/><br/><br/>
	
	<?php } ?>	
	<br class="limpiar" />


	  
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMScreenBottom.php'); ?>

<!--
  <input type="hidden" name="target_id_field" value=""/>
  <input type="hidden" name="target_name_field" value=""/>
  <input type="hidden" name="frm_action_type" value="Search"/>
-->
  <input type="hidden" name="id" id="id" value="<?php echo $id; ?>"/>
  
  <input type="hidden" name="main_flag_ok" id="main_flag_ok" value="<?php echo $main_flag_ok; ?>"/>  
  

  
<?php $type = ""; ?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMEndDocument.php'); ?>