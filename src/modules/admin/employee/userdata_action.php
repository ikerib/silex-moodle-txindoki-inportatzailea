<?php 
require_once($_SERVER['DOCUMENT_ROOT']."/includes/control.php"); 

if (isset($action)){
	if ($action=="SAVE_EMAIL_LANGUAGE"){
		
		$objUser->getPassword("");
		$objUser->setLanguage($_POST["language"]);
		$objUser->setEmail($_POST["email"]);
		
		$objUser->update();
		
		session_write_close();
		Header("Location: userdata.php?id=$id"); 
		exit();
		
	}else if ($action=="SAVE_PASSWORD"){
		
		$resultado = $objUser->init();
		if ($resultado==""){ //NO hay error.
			$objUser->updatePassword();
			
			//MENSAJE...
			$_SESSION["aviso_tipo"] = "success";
			$_SESSION["aviso_mensaje"] = MSG_PASSWORD_SUCCESSFULLY_CHANGED;

			$objUser = new User("");
		}else{ //hay error redirecciono y muestro error.
			$_SESSION["aviso_tipo"] = "warning";  
			$_SESSION["aviso_mensaje"] = $resultado;
			
		}
		
		session_write_close();
		Header("Location: userdata.php?id=$id"); 		
		exit();
		
	}else{ //hay error redirecciono y muestro error.
	}
}else{
}

?>