<?php 
$buttons="buttons_detail.php";
$tab=1;
$tabs="tabs.php";
$object_type_id=23 ;


include($_SERVER['DOCUMENT_ROOT']."/includes/control.php"); 

require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/admin/Picture.php'); 
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/Functions.php'); 
require_once($_SERVER['DOCUMENT_ROOT']."/lang/".$_SESSION["language"]."/lbl_picture.php"); 
require_once($_SERVER['DOCUMENT_ROOT']."/config_app.php"); 


$id="";
$enctype="multipart/form-data";
$action_url="/modules/admin/picture/detail.php";
$action="";

$obj = new Picture();
$util = new Util();

if (isset($_GET["id"]) && $_GET["id"]!="")
	$id=$_GET["id"];
else if (isset($_POST["id"]) && $_POST["id"]!="")
	$id=$_POST["id"];



//Comprobamos si viene de un borrado
if (isset($_POST['action']) && $_POST['action']=="DELETE"){
	include($_SERVER['DOCUMENT_ROOT'].'/modules/admin/picture/detail_action.php');
	exit;
}
	
	
if ($id!=""){
	
	if ($id=="new"){
		$obj->loadData("new");
		$action="NEW";
		$id="new";
	}else if (is_numeric($id)){	
		$obj->loadData($id);
		$action="UPDATE";
	}else{
		echo "error1"; // o producir un error hacia una web
	}
}else{
	echo "eerror2"; // o producir un error hacia una web
}	

if (isset($_POST["id"])){
	include($_SERVER['DOCUMENT_ROOT'].'/modules/admin/picture/detail_action.php');
}

include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMBeginDocument.php');

?>

<script language="JavaScript" type="text/javascript" CHARSET="ISO-8859-1">
	<?php 
		/*var path_IMAGE_BLANK = "/images/spacer.gif";
		var mesg_FILE_DELETION = "borrado";*/
	?>
	var mesg_WARNING = "<?php echo LBL_WARNING; ?>";
	var mesg_SELECT_IMAGE = "<?php echo LBL_SELECT_IMAGE; ?>";
	var mesg_CHECK_EXTENSION = "<?php echo MSG_CHECK_EXTENSION; ?>";
</script>

<script src="/modules/admin/picture/js/data_check.js" type="text/javascript" charset="iso-8859-1"></script>
<?php 
include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMHeaderTags.php'); 

$fechaSegundos = time();
$strNoCache = "?nocache=$fechaSegundos";

?>


<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMScreenHeader.php'); ?>

	<fieldset>			
		
		<p>
			<label><span class="obligatorio"><?php echo MSG_CAMPOS_OBLIGATORIOS;?></span></label>
		</p>
		<br class="limpiar"/>
		<br/>
		
		<p>
			<label><?php echo LBL_IMAGE_NAME; ?>: <span class="obligatorio">*</span></label><br/>
			<input type="text" name="name" onChange="setDirty(this);" size="80" maxlength="200" <?php echo ($bReadOnly)?"readonly=\"readonly\" class=\"readonly\" ":""; ?> value="<?php echo $obj->getName(); ?>"/>
		</p>

		<br class="limpiar"/>

		<p>
			<label><?php echo LBL_DESCRIPTION; ?>:</label><br/>
			<input type="text" name="picture_desc" onChange="setDirty(this);" size="80" maxlength="200" <?php echo ($bReadOnly)?"readonly=\"readonly\" class=\"readonly\" ":""; ?> value="<?php echo $obj->getPictureDesc(); ?>"/>
		</p>
			
			
		<br class="limpiar"/>	
		<p>
			<label><?php echo LBL_PICTURE; ?>:</label>
			<br/>
			
			<input type="file" id="picture" name="picture" size="50" maxlength="100" onChange="setDirty(this);"/>
			<input type="hidden" name="lim_tamano" value="<?php echo CFG_SIZE_BYTES; ?>"/> 
		</p>
		
		<br class="limpiar"/>	
			
		<p><b><?php echo strtoupper(LBL_NOTE); ?></b>:<?php echo str_replace("$2", CFG_PICTURE_HEIGHT,str_replace("$1",CFG_PICTURE_WIDTH,MSG_INFO_SIZE)); ?></p>
		<br class="limpiar"/>
		
		<p>
			<center>
			<?php if ($obj->getThumbnailFileExists() && $obj->getPictureFileExists() && $obj->getPictureFile()!=""){ ?>
			
				<a href="javascript:NewWindow('/includes/CRMViewImage.php?cid=<?php echo $obj->getCompanyID();?>&nim=<?php echo $obj->getID()."_".$obj->getPictureFile(); ?>&path=/picture/','image',<?php echo $obj->getPictureWidth();?>,<?php echo $obj->getPictureHeight();?>,'no')">
					<img name="img_picture" src="/includes/CRMViewImage.php?cid=<?php echo $obj->getCompanyID();?>&nim=<?php echo $obj->getID()."_th_".$obj->getPictureFile(); ?>&path=/picture/" alt="imagen"/>
				</a>
				<br/>
				<br/>
				<?php echo MSG_INFO_ORIGINAL; ?>
				
			<?php }else{ ?>
				<font color="#ff0000"><?php echo MSG_INFO_SELECT; ?></font>
			<?php } ?>
			</center>		
			<br class="limpiar"/>	
		</p>
			
			
		<br class="limpiar"/>
		<br/>
		<p>
			<label><?php echo LBL_OBJECT_TYPE; ?>: <span class="obligatorio">*</span></label><br/>
				<select name="foreign_object_type_id" onChange="setDirty(this);cleanObject();">
					<?php echo $util->desplegable("ica_object_type", "object_type_id", "object_type_name", $obj->getObjectTypeID(), $_SESSION["language"], true, 0, "object_type_name", "AND object_type_id IN (22,301)"); ?>
				</select>
		</p>
		
		<p>
			<label><?php echo LBL_RELATED_OBJECT; ?>: <span class="obligatorio">*</span></label><br/>
			<input type="text" name="foreign_object_name" onChange="setDirty(this);cleanObject();" readonly="readonly" class="readonly" size="50" maxlength="100" <?php echo ($bReadOnly)?"readonly=\"readonly\" class=\"readonly\" ":""; ?> value="<?php echo $obj->getObjectName(); ?>"/>
			<input type="hidden" name="foreign_object_id" id="foreign_object_id" value="<?php echo $obj->getObjectID(); ?>"/>
			
			<a href="javascript:goToLoc(document.data_frm.foreign_object_type_id.value,'<?php echo $object_type_id; ?>');"><img src="/images/search.gif" alt="<?php echo LBL_SEARCH; ?>" title="<?php echo LBL_SEARCH; ?>" border="0"></a>
			<a href="javascript:goToObject(document.data_frm.foreign_object_type_id.options[document.data_frm.foreign_object_type_id.selectedIndex].value,document.data_frm.foreign_object_id.value);">
                <img src="/images/seedetail.gif" alt="<?php echo LBL_VIEW; ?>" title="<?php echo LBL_VIEW; ?>" border="0"></a>

			<a href="javascript:cleanObject();"><img src="/images/delete.gif" alt="<?php echo LBL_DELETE; ?>" title="<?php echo LBL_DELETE; ?>" border="0" /></a>
		</p>
		
		<br class="limpiar"/>
		
		<p>
			<label><?php echo LBL_REMARKS; ?>:</label><br/>
			<textarea name="remarks" class="mceNoEditor" cols="100" rows="7" onChange="setDirty(this);" <?php echo ($bReadOnly)?"readonly=\"readonly\" class=\"readonly\" ":""; ?> ><?php echo $obj->getRemarks(); ?></textarea>
		</p>

	</fieldset>








<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMObjectTimestamp.php'); ?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMScreenBottom.php'); ?>


<script type="text/javascript" CHARSET="ISO-8859-1" defer >
<!--

//document.data_frm.name.focus(); 

// -->
</script>

<?php /*


<input type="hidden" name="picture_file_exists" value="<%=oScreenData.getPictureFileExists()?"true":"false"%>"/>
<input type="hidden" name="picture_file_old" value="<crm:format type="String"><jsp:getProperty name="oScreenData" property="pictureFile"/></crm:format>"/>
<input type="hidden" name="picture_name_old" value="<crm:format type="String"><jsp:getProperty name="oScreenData" property="pictureName"/></crm:format>"/>
<input type="hidden" name="picture_width_old" value="<crm:format type="String"><jsp:getProperty name="oScreenData" property="pictureWidth"/></crm:format>"/>
<input type="hidden" name="picture_height_old" value="<crm:format type="String"><jsp:getProperty name="oScreenData" property="pictureHeight"/></crm:format>"/>

<input type="hidden" name="picture_file" value="<crm:format type="String"><jsp:getProperty name="oScreenData" property="pictureFile"/></crm:format>"/>
<input type="hidden" name="picture_file_delete" value="<crm:format type="String"><jsp:getProperty name="oScreenData" property="pictureFile"/></crm:format>"/>
<input type="hidden" name="picture_name" value="<crm:format type="String"><jsp:getProperty name="oScreenData" property="pictureName"/></crm:format>"/>
<input type="hidden" name="picture_width" value="<crm:format type="String"><jsp:getProperty name="oScreenData" property="pictureWidth"/></crm:format>"/>
<input type="hidden" name="picture_height" value="<crm:format type="String"><jsp:getProperty name="oScreenData" property="pictureHeight"/></crm:format>"/>
*/
?>
<input type="hidden" name="bupload_image" value="no"/>
<input type="hidden" name="picture_file_old" value="<?php echo $obj->getPictureFile(); ?>"/>
<!--
<input type="hidden" name="tab" value="1"/>
<input type="hidden" name="frm_action_type" value=""/>
-->
<input type="hidden" name="id" value="<?php echo $id; ?>"/>
<input type="hidden" name="picture_order" value="1"/>

  




<?php $type = ""; ?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMEndDocument.php'); ?>

