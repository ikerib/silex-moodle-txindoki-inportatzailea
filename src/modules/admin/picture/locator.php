<?php 
$locator="yes" ;
$buttons="buttons_locator.php";
$tabs="tabs_locator.php";
$object_type_id=23 ;

include($_SERVER['DOCUMENT_ROOT']."/includes/control.php"); 

//Cargo la clase para los mostrar los datos.
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/admin/PictureSummary.php'); 
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/Functions.php'); 
include($_SERVER['DOCUMENT_ROOT']."/lang/".$_SESSION["language"]."/lbl_locator.php"); 
include($_SERVER['DOCUMENT_ROOT']."/lang/".$_SESSION["language"]."/lbl_picture.php"); 
require_once($_SERVER['DOCUMENT_ROOT']."/config_app.php"); 

include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMBeginDocument.php');

include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMHeaderTags.php');
?>

<script src="/modules/admin/picture/js/data_check.js" type="text/javascript" charset="iso-8859-1"></script>

<?php

$id="";
$enctype="";
$action_url="/modules/admin/picture/locator.php";
$action="SEARCH";

$columnas=4;

$util = new Util();

$obj = new PictureSummary();

include($_SERVER['DOCUMENT_ROOT'].'/modules/admin/picture/locator_action.php');

?>


<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMScreenHeader.php'); ?>

	<fieldset class="locator">
		<p>
			<label><?php echo LBL_NAME; ?>:</label>
			<?php echo $obj->buildOperatorSymbol("name","char",(isset($_POST['op_name'])?$_POST['op_name']:"") ); ?>
			<input type="text" name="name" id="name" value="<?php echo (isset($_POST['name'])?$_POST['name']:""); ?>" size="25" maxlength="50"/>
		</p>
		
		<p>
			<label><?php echo LBL_OBJECT_TYPE; ?>:</label>
			<input type="hidden" id="op_info_type_id" name="op_foreign_object_type_id" value="1"/>
			<select name="foreign_object_type_id" onChange="setDirty(this);cleanObject();" style="width:279px">
				<?php echo $util->desplegable("ica_object_type", "object_type_id", "object_type_name", (isset($_POST['foreign_object_type_id'])?$_POST['foreign_object_type_id']:""), $_SESSION["language"], true, 0, "object_type_name", "AND object_type_id IN (22,301)"); ?>
            </select>
		</p>
		
		<p>
			<label><?php echo LBL_RELATED_OBJECT; ?>: </label>
			<input type="text" name="foreign_object_name" onChange="setDirty(this);cleanObject();" readonly="readonly" class="readonly" size="38" maxlength="100" <?php echo ($bReadOnly)?"readonly=\"readonly\" class=\"readonly\" ":""; ?> value="<?php echo (isset($_POST['foreign_object_name'])?$_POST['foreign_object_name']:""); ?>"/>
			<input type="hidden" id="op_foreign_object_id" name="op_foreign_object_id" value="1"/>
			<input type="hidden" name="foreign_object_id" id="foreign_object_id" value="<?php echo (isset($_POST['foreign_object_id'])?$_POST['foreign_object_id']:""); ?>"/>
			
			<a href="javascript:goToLoc(document.data_frm.foreign_object_type_id.value,'<?php echo $object_type_id; ?>');"><img src="/images/search.gif" border="0" alt="<?php echo LBL_SEARCH; ?>" title="<?php echo LBL_SEARCH; ?>" /></a>
			<a href="javascript:cleanObject();"><img src="/images/delete.gif" alt="<?php echo LBL_DELETE; ?>" title="<?php echo LBL_DELETE; ?>" border="0" /></a>
		</p>
		
		<br class="limpiar"/>

		<input type="submit" name="search" value="<?php echo LBL_LOC_ACTION; ?>" class="boton" onClick="doSearch()"/>

	</fieldset>

      <table class="tabla1" summary="<?php echo LBL_LOC_SUM_TITLE1; ?>">
			<caption><?php echo LBL_LOC_SUM_TITLE1; ?></caption>
			<thead>
			<tr>
                <th width="5%">&nbsp;</th>
                <th width="50%"><?php echo LBL_NAME; ?>&nbsp;<a href="javascript:doOrderBy('1')"><img src="/images/flechas.gif" width="7" height="10" alt=""/></a></th>
				<th width="20%"><?php echo LBL_OBJECT_TYPE; ?>&nbsp;<a href="javascript:doOrderBy('5')"><img src="/images/flechas.gif" width="7" height="10" alt=""/></a></th>
				<th width="25%"><?php echo LBL_RELATED_OBJECT; ?>&nbsp;<a href="javascript:doOrderBy('6')"><img src="/images/flechas.gif" width="7" height="10" alt=""/></a></th>
			</tr>
			<tr>
				<td colspan="<?php echo $columnas; ?>" class="separa">&nbsp;</td>
			</tr>

			</thead>


<?php 
	//Cargo el footer
	include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMListFooter.php'); 
?>
		
			<tbody>
			 
				<?php
					//Muestro los datos.
					$contador = 0;
					while($row = $resultado->fetch_array())		
					{
				?>
				
			       <tr <?php if (fmod($contador,2)){ ?>class="fondo"<?php } ?>>
						<td class="centro">
						<?php 
						$ruta_upload = str_replace("$1",$row["company_id"],CFG_PICTURE_URL);
						if (file_exists($ruta_upload.$row["picture_id"] . "_th_" . $row["picture_file"]) ) { ?>
							<a class="thumbnail" href="javascript:void(0)">
							<img src="/images/camara.gif" width="13" height="11" alt=""/>
				    		<span><img src="/includes/CRMViewImage.php?cid=<?php echo $row["company_id"]; ?>&amp;nim=<?php echo $row["picture_id"]; ?>_th_<?php echo $row["picture_file"]; ?>&amp;path=/picture/" alt=""/></span>
				    		</a>
						<?php }else{ ?>
							&nbsp;
						<?php } ?>
						</td>
					
						<td>
						  <a href="detail.php?id=<?php echo $row["picture_id"]; ?>" >&nbsp;<?php echo $row["picture_name"]; ?></a>
						</td>
						
						<td>
						  <a href="detail.php?id=<?php echo $row["picture_id"]; ?>" title="<?php echo $util->truncate($row["object_type_name"],15,true); ?>">&nbsp;<?php echo $util->truncate($row["object_type_name"],15); ?></a>
						</td>
						
						<td>
						  <a href="detail.php?id=<?php echo $row["picture_id"]; ?>" title="<?php echo $util->truncate($row["object_name"],20,true); ?>">&nbsp;<?php echo $util->truncate($row["object_name"],20); ?></a>
						</td>
					</tr>
					<?php $contador++; ?>
                <?php } ?>
			  
			</tbody>
			
	  </table>
      
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMScreenBottom.php'); ?>


<script type="text/javascript" CHARSET="ISO-8859-1" defer >
<!--
document.data_frm.name.focus(); 
-->
</script>


<!--
  <input type="hidden" name="target_id_field" value=""/>
  <input type="hidden" name="target_name_field" value=""/>
  <input type="hidden" name="frm_action_type" value="Search"/>

  <input type="hidden" name="ID" value="openLoc"/>
 --> 




<?php $type = "summary"; ?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMEndDocument.php'); ?>


