

function checkRequiredData()
{
	/* if (document.data_frm.picture.value!=""){
		if (validar_picture()){
			return true;
		}else
			return false;
	}else{
		return true;
	} */
	
	return true;
}  // checkRequiredData 


function checkSearchFields()
{
   return true;
}  // checkSearchFields 

function doCancel()
{
   document.data_frm.tab.value=1;
}  // checkSearchFields 

function doChangeMainChannel(valor)
{
  document.data_frm.main_flag_ok.value=valor;
}

function writeUserID(){

	var new_user_id = document.data_frm.name.value + document.data_frm.last_name_1.value;
	
	new_user_id = new_user_id.toLowerCase();
	
	document.data_frm.user_name.value = (new_user_id);
	
	checkUserID();
}



function checkUserID(){
	
	document.data_frm.user_name.value = userValidate(document.data_frm.user_name.value);
	cargar_asincrono('/modules/elearning/student/checkUser.php?user_name='+document.data_frm.user_name.value , 'user_valido');
	
	if (document.data_frm.user_valido.value=="NO EXISTE"){ //OK
		document.getElementById('img_validate').src="/images/ok.gif";
		document.getElementById('img_validate').title=mesg_USERNAME_VALID;
		document.getElementById('img_validate').alt=mesg_VALID;
	}else if (document.data_frm.user_valido.value=="EXISTE"){ //KO
		document.getElementById('img_validate').src="/images/ko.gif";
		document.getElementById('img_validate').title=mesg_USERNAME_INVALID;
		document.getElementById('img_validate').alt=mesg_INVALID;
		return false;
	}
}


//Funci�n que valida y reemplaza los caracteres no permitidos en javascript.

function userValidate(cadena){
	
	var caracteres_validos = "abcdefghijklmnopqrstuvwxyz0123456789@.";
	
	var cadena_valida = "";
	
	for (var i=0; i<cadena.length; i++)
	{
	
		var caracter = cadena.charAt(i);
		
		if (caracteres_validos.indexOf(caracter)!=-1){
			cadena_valida=cadena_valida+caracter;
		}
		
	}
	
	return cadena_valida;
}



function checkEmailLanguage()
{
	
	if (document.data_frm.email.value==""){
		document.data_frm.email.focus();
		alert(mesg_warning + '\n' + mesg_email_text01);
		return false;
	}

   document.data_frm.action.value="SAVE_EMAIL_LANGUAGE";
   doSubmit();
}  // checkEmailLanguage

function checkPassword()
{
   document.data_frm.action.value="SAVE_PASSWORD";
   doSubmit();
}  // checkPassword


function checkNewUser()
{
	
   document.data_frm.action.value="SAVE_NEW_USER";
   doSubmit();
}  // checkNewUser


//validar_picture
function validar_picture(){

		if (validar_picture2()){
			return true;
		}
}//end validar_picture

function validar_picture2(){
		
		if (document.data_frm.picture.value!=""){
			if (comprueba_extension(document.data_frm, document.data_frm.picture.value)){
			
				document.data_frm.bupload_image.value="yes";
				return true;
			}else{
				document.data_frm.bupload_image.value="no";
				return false; 
			}
		}else{
			if (document.data_frm.picture_file_old.value!=""){ //Ya existe una imagen, se puede modificar si quiere.
				document.data_frm.bupload_image.value="no";
				return true;
			}else{
				document.data_frm.bupload_image.value="no";
				alert(mesg_WARNING + '\n' + mesg_SELECT_IMAGE);
				return false;
			}
		}
		
}//end validar_picture2


function comprueba_extension(formulario, archivo) {
	extensiones_permitidas = new Array(".gif", ".jpg", ".png");
	mierror = "";
	if (!archivo) {
		//Si no tengo archivo, es que no se ha seleccionado un archivo en el formulario
		mierror = mesg_SELECT_IMAGE;
	}else{
		//recupero la extensi�n de este nombre de archivo
		extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
		//alert (extension);
		//compruebo si la extensi�n est� entre las permitidas
		permitida = false;
		for (var i = 0; i < extensiones_permitidas.length; i++) {
			if (extensiones_permitidas[i] == extension) {
				permitida = true;
				break;
			}
		}
		if (!permitida) {
			mierror = mesg_CHECK_EXTENSION + extensiones_permitidas.join();
		}else{
			return 1;
		}
	}
	//si estoy aqui es que no se ha podido submitir
	alert(mesg_WARNING + '\n' + mierror);
	
	return 0;
} 

