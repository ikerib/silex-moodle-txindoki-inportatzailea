<?php 
	$object_type_id=100;
	require_once($_SERVER['DOCUMENT_ROOT']."/includes/control.php");
	require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/elearning/Student.php'); 

$obj = new Student();

if (isset($action)){
	
	if ($action=="NEW"){
		$resultado = $obj->init();
		if ($resultado==""){ //NO hay error.
			$error = $obj->createNew();
			
			$id = $obj->getID(); //�ltimo insertado
			
			if ($error==""){
				//MENSAJE...
				$_SESSION["aviso_tipo"] = "success";
				$_SESSION["aviso_mensaje"] = MSG_INFO_CHANGED_CORRECTLY;
				
				session_write_close();
				Header("Location: detail.php?id=$id"); 
				exit();
				
			}else{
				$_SESSION["aviso_tipo"] = "warning";  
				$_SESSION["aviso_mensaje"] = $error;
			}; 
		}else{//hay error redirecciono y muestro error.
				
			//echo $resultado;
			$_SESSION["aviso_tipo"] = "warning";  
			$_SESSION["aviso_mensaje"] = $resultado; 
		}
	}else if ($action=="UPDATE"){
		
		if (isset($action) && is_numeric($id) ) {
			
			$resultado = $obj->init();
			
			if ($resultado==""){ //NO hay error.
				$error = $obj->update($id);
				
				if ($error==""){
					//MENSAJE...
					$_SESSION["aviso_tipo"] = "success";
					$_SESSION["aviso_mensaje"] = MSG_INFO_CHANGED_CORRECTLY;
				}else{
					$_SESSION["aviso_tipo"] = "warning";  
					$_SESSION["aviso_mensaje"] = $error;
				}
			}else{//hay error redirecciono y muestro error.
				
				//echo $resultado;
				$_SESSION["aviso_tipo"] = "warning";  
				$_SESSION["aviso_mensaje"] = $resultado; 
			}
				
			session_write_close();
			Header("Location: detail.php?id=$id"); 
			exit();
			
		}

	}		
		
		
}
	
if (isset($_POST['action']) && $_POST['action']=="DELETE"){
	
	if (isset($_POST['id']) && is_numeric($_POST['id']) ) {
			$obj->delete($_POST['id']);
			
			//Cargar mensaje de borrado OK.
			$_SESSION["aviso_tipo"] = "success";
			$_SESSION["aviso_mensaje"] = MSG_INFO_DELETED_CORRECTLY; 
			Header("Location: locator.php"); 
			
	}
}else{ //hay error redirecciono y muestro error.
	
}


?>