<?php 
$buttons="buttons_detail.php";
$tab=1;
$tabs="tabs.php";
$object_type_id=100 ;

include($_SERVER['DOCUMENT_ROOT']."/includes/control.php"); 
include($_SERVER['DOCUMENT_ROOT']."/lang/".$_SESSION["language"]."/lbl_student.php"); 
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/elearning/Student.php'); 
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/Functions.php'); 

$id="";
$enctype="multipart/form-data";
$action_url="/modules/elearning/student/detail.php";
$action="";

$obj = new Student();

if (isset($_GET["id"]) && $_GET["id"]!="")
	$id=$_GET["id"];
else if (isset($_POST["id"]) && $_POST["id"]!="")
	$id=$_POST["id"];

//Comprobamos si viene de un borrado
if (isset($_POST['action']) && $_POST['action']=="DELETE"){
	//echo "---->ACTION=".$_POST['action'];
	include($_SERVER['DOCUMENT_ROOT'].'/modules/elearning/student/detail_action.php');
	exit;
}
	
	
if ($id!=""){
	
	if ($id=="new" && $_SESSION['user_type']=="TUTOR"){ //EL TUTOR NO PUEDE CREAR NUEVOS ALUMNOS, SOLO LOS EMPLEADOS
		echo "error0"; // o producir un error hacia una web
		exit;
	}else if ($id=="new" && $_SESSION['user_type']!="TUTOR"){
		$obj->loadData("new");
		$action="NEW";
		$id="new";
	}else if (is_numeric($id)){	
		$obj->loadData($id);
		$action="UPDATE";
	}else{
		echo "error1"; // o producir un error hacia una web
	}
}else{
	echo "eerror2"; // o producir un error hacia una web
}	

if (isset($_POST["id"])){
	include($_SERVER['DOCUMENT_ROOT'].'/modules/elearning/student/detail_action.php');
}

$util = new Util();

include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMBeginDocument.php');

include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMHeaderTags.php'); 

?>

<script language="JavaScript" type="text/javascript" CHARSET="ISO-8859-1">
	<?php 
		/*var path_IMAGE_BLANK = "/images/spacer.gif";
		var mesg_FILE_DELETION = "borrado";*/
	?>
	var mesg_WARNING = "<?php echo LBL_WARNING; ?>";
	var mesg_SELECT_IMAGE = "<?php echo LBL_SELECT_IMAGE; ?>";
	var mesg_CHECK_EXTENSION = "<?php echo MSG_CHECK_EXTENSION; ?>";
</script>

<SCRIPT src="/modules/elearning/student/js/data_check.js" type="text/javascript" CHARSET="ISO-8859-1"></SCRIPT>
<link type="text/css" rel="stylesheet" href="/js/calendar/calendar.css?random=20051112" media="screen"></LINK>
<script src="/js/ajax.js" type="text/javascript" charset="iso-8859-1"></script>

<script  type="text/javascript">
	var languageCode = '<?php echo $_SESSION['language'];?>';
	
	var mesg_VALID = "<?php echo LBL_VALID; ?>";
	var mesg_INVALID = "<?php echo LBL_INVALID; ?>";
	var mesg_USERNAME_VALID = "<?php echo MSG_USERNAME_VALID; ?>";
	var mesg_USERNAME_INVALID = "<?php echo MSG_USERNAME_INVALID; ?>";
	
</script>

<script type="text/javascript" src="/js/calendar/calendar.js?random=20060118"></script>

<?php

?>

<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMScreenHeader.php'); ?>

	
	<fieldset>
	
		<p>
			<label><span class="obligatorio"><?php echo MSG_CAMPOS_OBLIGATORIOS;?></span></label>
		</p>
		<br class="limpiar"/>
		<br/>
		
		<p>
			<label><?php echo LBL_FIRST_NAME; ?>: <span class="obligatorio">*</span></label><br/>
			<input type="text" name="name" size="20" maxlength="50" value="<?php echo $obj->getName(); ?>" onChange="setDirty(this);"/>
		</p>
		
		<br class="limpiar"/>
		
		<p>
			<label><?php echo LBL_LAST_NAME1; ?>: <span class="obligatorio">*</span></label><br/>
			<input type="text" name="last_name_1" size="40" maxlength="50" value="<?php echo $obj->getPersonLastName1(); ?>" onChange="setDirty(this);"/>
		</p>
		
		<p>
			<label><?php echo LBL_LAST_NAME2; ?>: </label><br/>
			<input type="text" name="last_name_2" size="40" maxlength="50" value="<?php echo $obj->getPersonLastName2(); ?>" onChange="setDirty(this);"/>
		</p>
		
		<br class="limpiar"/>	
		<br/>
		
		<p>
			<label><?php echo LBL_PHOTO; ?>:</label>
		</p>
	
		<br class="limpiar"/>	
		<br/>
		
		<p>
			<center>
				<?php 
					$ruta_imagen_pefil = CFG_MOODLE_URL_GET_IMAGE.$obj->loadMoodleProfilePhoto($obj->getUserID());
				?>
					<img src="<?php echo $ruta_imagen_pefil;?>" alt="Imágen del perfil extraida de Moodle"/>
			</center>		
			<br class="limpiar"/>	
		</p>
		
		<br class="limpiar"/>
		<br/>	
		<br/>
		
		
		
		
		
		
		<br class="limpiar"/>
		
		<p>
			<label><?php echo LBL_USER_EMAIL; ?>: <span class="obligatorio">*</span></label><br/>
			<input type="text" name="user_email" size="40" maxlength="200" value="<?php echo $obj->getUserEmail(); ?>" onChange="setDirty(this);"/>
		</p>
		
	
		<br class="limpiar"/>
		
		<p>
			<label><?php echo LBL_CARD_ID; ?>: </label><br/>
			<input type="text" name="card_id" size="40" maxlength="50" value="<?php echo $obj->getCardID(); ?>" onChange="setDirty(this);"/>
		</p>
		
		<p>
			<label><?php echo LBL_CITY; ?>: </label><br/>
			<input type="text" name="city" size="40" maxlength="50" value="<?php echo $obj->getBirthPlace(); ?>" onChange="setDirty(this);"/>
		</p>
	
		<br class="limpiar"/>
		
		<p>
			<label><?php echo LBL_PHONE; ?>: </label><br/>
			<input type="text" name="phone" size="40" maxlength="50" value="<?php echo $obj->getPhone(); ?>" onChange="setDirty(this);"/>
		</p>
		
		<p>
			<label><?php echo LBL_MOBILE; ?>: </label><br/>
			<input type="text" name="mobile" size="40" maxlength="50" value="<?php echo $obj->getMobile(); ?>" onChange="setDirty(this);"/>
		</p>
	
		<br class="limpiar"/>
		
		<p>
			<label><?php echo LBL_INTERNAL_ID4; ?>: </label><br/>
			<input type="text" name="internal_id" size="40" maxlength="100" value="<?php echo $obj->getInternalID(); ?>" onChange="setDirty(this);"/>
		</p>
	
		<br class="limpiar"/>
		
		<p>
			<label><?php echo LBL_ENTERPRISE; ?>: </label><br/>
			<input type="text" name="empresa" size="40" maxlength="100" value="<?php echo $obj->getEmpresa(); ?>" onChange="setDirty(this);"/>
		</p>
		
		<p>
			<label><?php echo LBL_SECTION; ?>: </label><br/>
			<input type="text" name="seccion" size="40" maxlength="100" value="<?php echo $obj->getSeccion(); ?>" onChange="setDirty(this);"/>
		</p>
		
		
		
		<br class="limpiar"/>
		<br/>
		
		<p>
			<label><?php echo LBL_REGISTRATION_DATE; ?>: </label><br/>
			<input type="text" name="registration_date" class="readonly" size="16" readonly value="<?php echo $obj->getRegistrationDate(); ?>" onChange="setDirty(this);"/>&nbsp;&nbsp;
            <a href="javascript:void(null);" onClick="displayCalendar(document.forms[0].registration_date,'dd/mm/yyyy',this);"><img src="/images/calendar.gif" border="0" alt="<?php echo LBL_ALT_CALENDAR; ?>" title="<?php echo LBL_ALT_CALENDAR; ?>" /></a>
		</p>
		
		<br class="limpiar"/>
	</fieldset>
	
	<br/>

	<p><strong><?php echo LBL_USER_DATA; ?>:</strong></p><br/>

	<fieldset class="coloreado">

	<?php if (!$obj->getIsUser()){?>
		<p><?php echo MSG_USER_DATA_INFO1; ?></p>
		<p><?php echo MSG_USER_IDENTIFICATOR; ?></p>
		<br class="limpiar"/>
		<p>
			<label><?php echo LBL_USER_LOGIN; ?>: <span class="obligatorio">*</span></label><br/>
			<input type="text" name="user_name" size="25" maxlength="30" value="<?php echo $obj->getUserID(); ?>" onBlur="checkUserID();" onChange="setDirty(this);"/>
			<img src="" id="img_validate" alt="" />
			<input type="hidden" name="user_valido" id="user_valido" value=""/>
		</p>
	<?php }else{ ?>
		<p>
			<label><?php echo LBL_USER_LOGIN; ?>: <span class="obligatorio">*</span></label><br/>
			<input type="text" name="user_name" size="25" maxlength="30" readonly class="readonly" value="<?php echo $obj->getUserID(); ?>" />
		</p>
	<?php } ?>
		
	<?php if ($obj->getIsUser()){?>	
		<br class="limpiar"/>
		<br/>
		
		<p><?php echo MSG_USER_DATA_INFO2; ?></p>
		<br class="limpiar"/>
		
		<p>
		<label><?php echo LBL_NEW_PASSWORD; ?>:</label><br />
		<input type="password" name="npass" size="20" maxlength="32" value="" onChange="setDirty(this);"/>
		</p>			

		<p>
		<label><?php echo LBL_CONFIRM_PASSWORD; ?>:</label><br />
		<input type="password" name="cpass" size="20" maxlength="32" value="" onChange="setDirty(this);"/>
		</p>			
	<?php } ?>

	</fieldset>
	
	<br/>

	<p><strong><?php echo LBL_PROFILE_SETTINGS; ?>:</strong></p><br/>

	<fieldset class="coloreado">

		
		<p>
			<label><?php echo LBL_LEVEL; ?>:  <span class="obligatorio">*</span></label><br/>
			<select name="level" onChange="setDirty(this);" <?php echo ($bReadOnly)?"disabled class=\"readonly\" ":""; ?> >
				<?php echo $util->desplegable("ic_level", "level_id", "level_name", $obj->getLevel(), $_SESSION["language"], true, 0); ?>
			</select>
		</p>
		
		<p>	
			<label><?php echo LBL_COURSE_LEVEL4; ?>: <span class="obligatorio">* <?php echo LBL_COURSE_LEVEL4_COMPULSORY; ?></span></label><br/>
			<select id="course_particular_id" name="course_particular_id" onChange="setDirty(this);" >
				<?php echo $util->desplegable("ic_course", "course_id", "course_name", $obj->getCourseParticularID(), "", true, 1); ?>
			</select>
		</p>
		
		<br class="limpiar"/>
		
		<p>
			<label><?php echo LBL_USER_LANGUAGE; ?>: <span class="obligatorio">*</span> </label><br/>
			<select name="user_lang" onChange="setDirty(this);">
			<?php echo $util->desplegable("ica_language", "language_code", "language_name", $obj->getUserLang(), $_SESSION["language"], false, 0); ?>
            </select>
		</p>
		
		<p>
			<label><?php echo LBL_HIGH_CONTRAST; ?>: <span class="obligatorio">*</span> </label><br/>
			<select name="high_contrast" onChange="setDirty(this);">
			<?php echo $util->desplegable("ic_response", "response_id", "response_name", $obj->getHighContrast(), $_SESSION["language"], false, 0); ?>
            </select>
		</p>	
		
		<p>
			<label><?php echo LBL_LOCUTION; ?>: <span class="obligatorio">*</span> </label><br/>
			<select name="locution" onChange="setDirty(this);">
			<?php echo $util->desplegable("ic_response", "response_id", "response_name", $obj->getLocution(), $_SESSION["language"], false, 0); ?>
            </select>
		</p>	
		
		<br class="limpiar"/>
		<br/>
		<p>
			<label><span class="obligatorio"><?php echo LBL_PICTOGRAM_NOTE; ?></span></label>
		</p>
		<br class="limpiar"/>
		<p>
			<label><?php echo LBL_PICTOGRAM; ?> 1 <span class="obligatorio">*</span></label><br />
			<select name="pictograma1" onChange="setDirty(this);">
				<option value="0">-</option>
				<?php echo $util->desplegable("ic_pictogram", "pictogram_id", "pictogram_name", $obj->getPictogram1(), $_SESSION["language"], false, 0, "", " AND pictogram_type=1 "); ?>
			</select>
		</p>				
		
		<p>
			<label><?php echo LBL_PICTOGRAM; ?> 2 <span class="obligatorio">*</span></label><br />
			<select name="pictograma2" onChange="setDirty(this);">
				<option value="0">-</option>
				<?php echo $util->desplegable("ic_pictogram", "pictogram_id", "pictogram_name", $obj->getPictogram2(), $_SESSION["language"], false, 0, "", " AND pictogram_type=2 "); ?>
			</select>
		</p>
		
	
		<p>
			<label><?php echo LBL_PICTOGRAM; ?> 3 <span class="obligatorio">*</span></label><br />
			<select name="pictograma3" onChange="setDirty(this);">
				<option value="0">-</option>
				<?php echo $util->desplegable("ic_pictogram", "pictogram_id", "pictogram_name", $obj->getPictogram3(), $_SESSION["language"], false, 0, "", " AND pictogram_type=3 "); ?>
			</select>
		</p>
		
		
		
	
		
	</fieldset>

	<br class="limpiar"/>
		
	<fieldset>
	
		<p>
			<label><?php echo LBL_REMARKS1; ?>: </label><br/>
			<textarea name="remarks" cols="100" rows="7" onChange="setDirty(this);"><?php echo $obj->getRemarks(); ?></textarea>
		</p>

	</fieldset>

<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMObjectTimestamp.php'); ?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMScreenBottom.php'); ?>


<script type="text/javascript" CHARSET="ISO-8859-1" defer >
<!--
<?php if (!$obj->getIsUser()){?>
	writeUserID();
<?php } ?>

// -->
</script>


	<input type="hidden" name="tab" value="1"/>
	<input type="hidden" name="id" value="<?php echo $id; ?>"/>
	<input type="hidden" name="is_user" value="<?php echo $obj->getIsUser(); ?>"/>	
	
	<input type="hidden" name="bupload_image" value="no"/>
	<input type="hidden" name="picture_file_old" value="<?php echo $obj->getPictureFile(); ?>"/>

<?php $type = ""; ?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMEndDocument.php'); ?>