<?php 
$buttons="buttons_detail.php";
$tab=1;
$tabs="tabs.php";
$object_type_id=103 ;

include($_SERVER['DOCUMENT_ROOT']."/includes/control.php"); 
include($_SERVER['DOCUMENT_ROOT']."/lang/".$_SESSION["language"]."/lbl_tutor.php"); 
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/elearning/Tutor.php'); 
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/Functions.php'); 
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/FunctionsMoodle.php'); 

$id="";
$enctype="";
$action_url="/modules/elearning/tutor/detail.php";
$action="";

$obj = new Tutor();
$utilMoodle = new UtilMoodle();

if (isset($_GET["id"]) && $_GET["id"]!="")
	$id=$_GET["id"];
else if (isset($_POST["id"]) && $_POST["id"]!="")
	$id=$_POST["id"];

//Comprobamos si viene de un borrado
if (isset($_POST['action']) && $_POST['action']=="DELETE"){
	//echo "---->ACTION=".$_POST['action'];
	include($_SERVER['DOCUMENT_ROOT'].'/modules/elearning/tutor/detail_action.php');
	exit;
}
	
	
if ($id!=""){
	
	if ($id=="new"){
		$obj->loadData("new");
		$action="NEW";
		$id="new";
	}else if (is_numeric($id)){	
		$obj->loadData($id);
		$action="UPDATE";
	}else{
		echo "error1"; // o producir un error hacia una web
	}
}else{
	echo "eerror2"; // o producir un error hacia una web
}	

if (isset($_POST["id"])){
	include($_SERVER['DOCUMENT_ROOT'].'/modules/elearning/tutor/detail_action.php');
}

$util = new Util();

include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMBeginDocument.php');

include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMHeaderTags.php'); 

?>

<SCRIPT src="/modules/elearning/tutor/js/data_check.js" type="text/javascript" CHARSET="ISO-8859-1"></SCRIPT>
<link type="text/css" rel="stylesheet" href="/js/calendar/calendar.css?random=20051112" media="screen"></LINK>
<script src="/js/ajax.js" type="text/javascript" charset="iso-8859-1"></script>

<script  type="text/javascript">
	var languageCode = '<?php echo $_SESSION['language'];?>';
	
	var mesg_VALID = "<?php echo LBL_VALID; ?>";
	var mesg_INVALID = "<?php echo LBL_INVALID; ?>";
	var mesg_USERNAME_VALID = "<?php echo MSG_USERNAME_VALID; ?>";
	var mesg_USERNAME_INVALID = "<?php echo MSG_USERNAME_INVALID; ?>";
	
</script>

<script type="text/javascript" src="/js/calendar/calendar.js?random=20060118"></script>

<?php

?>

<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMScreenHeader.php'); ?>

	
	<fieldset>
		
		<p>
			<label><span class="obligatorio"><?php echo MSG_CAMPOS_OBLIGATORIOS;?></span></label>
		</p>
		<br class="limpiar"/>
		<br/>
	
		<p>
			<label><?php echo LBL_FIRST_NAME; ?>: <span class="obligatorio">*</span></label><br/>
			<input type="text" name="name" size="20" maxlength="50" value="<?php echo $obj->getName(); ?>" onChange="setDirty(this);"/>
		</p>
		
		<br class="limpiar"/>
		
		<p>
			<label><?php echo LBL_LAST_NAME1; ?>: <span class="obligatorio">*</span></label><br/>
			<input type="text" name="last_name_1" size="40" maxlength="50" value="<?php echo $obj->getPersonLastName1(); ?>" onChange="setDirty(this);"/>
		</p>
		
		<p>
			<label><?php echo LBL_LAST_NAME2; ?>: </label><br/>
			<input type="text" name="last_name_2" size="40" maxlength="50" value="<?php echo $obj->getPersonLastName2(); ?>" onChange="setDirty(this);"/>
		</p>
		
		<br class="limpiar"/>
		<br/>
		
		<p>
			<label><?php echo LBL_PHOTO; ?>:</label>
		</p>
	
		<br class="limpiar"/>	
		<br/>
		
		<p>
			<center>
				<?php 
					$ruta_imagen_pefil = CFG_MOODLE_URL_GET_IMAGE.$obj->loadMoodleProfilePhoto($obj->getUserID());
				?>
					<img src="<?php echo $ruta_imagen_pefil;?>" alt="Imágen del perfil extraida de Moodle"/>
			</center>		
			<br class="limpiar"/>	
		</p>
		
		<br class="limpiar"/>
		<p>
			<label><?php echo LBL_USER_EMAIL; ?>: <span class="obligatorio">*</span></label><br/>
			<input type="text" name="user_email" size="40" maxlength="200" value="<?php echo $obj->getUserEmail(); ?>" onChange="setDirty(this);"/>
		</p>
		
	
		<br class="limpiar"/>
		
		<p>
			<label><?php echo LBL_CARD_ID; ?>: </label><br/>
			<input type="text" name="card_id" size="40" maxlength="50" value="<?php echo $obj->getCardID(); ?>" onChange="setDirty(this);"/>
		</p>
		
		<p>
			<label><?php echo LBL_CITY; ?>: </label><br/>
			<input type="text" name="city" size="40" maxlength="50" value="<?php echo $obj->getBirthPlace(); ?>" onChange="setDirty(this);"/>
		</p>
	
		<br class="limpiar"/>
		
		<p>
			<label><?php echo LBL_PHONE; ?>: </label><br/>
			<input type="text" name="phone" size="40" maxlength="50" value="<?php echo $obj->getPhone(); ?>" onChange="setDirty(this);"/>
		</p>
		
		<p>
			<label><?php echo LBL_MOBILE; ?>: </label><br/>
			<input type="text" name="mobile" size="40" maxlength="50" value="<?php echo $obj->getMobile(); ?>" onChange="setDirty(this);"/>
		</p>
	
		<br class="limpiar"/>
		
		<p>
			<label><?php echo LBL_REGISTRATION_DATE; ?>: </label><br/>
			<input type="text" name="registration_date" class="readonly" size="16" readonly value="<?php echo $obj->getRegistrationDate(); ?>" onChange="setDirty(this);"/>&nbsp;&nbsp;
            <a href="javascript:void(null);" onClick="displayCalendar(document.forms[0].registration_date,'dd/mm/yyyy',this);"><img src="/images/calendar.gif" border="0" alt="<?php echo LBL_ALT_CALENDAR; ?>" title="<?php echo LBL_ALT_CALENDAR; ?>" /></a>
		</p>
		
		<br class="limpiar"/>
	</fieldset>
	
	<br/>

	<p><strong><?php echo LBL_USER_DATA; ?>:</strong></p><br/>

	<fieldset class="coloreado">

	<?php if (!$obj->getIsUser()){?>
		<p><?php echo MSG_USER_DATA_INFO1; ?></p>
		<p><?php echo MSG_USER_IDENTIFICATOR; ?></p>
		<br class="limpiar"/>
		<p>
			<label><?php echo LBL_USER_LOGIN; ?>: <span class="obligatorio">*</span></label><br/>
			<input type="text" name="user_name" size="25" maxlength="30" value="<?php echo $obj->getUserID(); ?>" onBlur="checkUserID();" onChange="setDirty(this);"/>
			<img src="" id="img_validate" alt="" />
			<input type="hidden" name="user_valido" id="user_valido" value=""/>
		</p>
	<?php }else{ ?>
		<p>
			<label><?php echo LBL_USER_LOGIN; ?>: <span class="obligatorio">*</span></label><br/>
			<input type="text" name="user_name" size="25" maxlength="30" readonly class="readonly" value="<?php echo $obj->getUserID(); ?>" />
		</p>
	<?php } ?>
		
	<?php if ($obj->getIsUser()){?>	
		<br class="limpiar"/>
		<br/>
		
		<p><?php echo MSG_USER_DATA_INFO2; ?></p>
		<br class="limpiar"/>
		
		<p>
		<label><?php echo LBL_NEW_PASSWORD; ?>:</label><br />
		<input type="password" name="npass" size="20" maxlength="32" value="" onChange="setDirty(this);"/>
		</p>			

		<p>
		<label><?php echo LBL_CONFIRM_PASSWORD; ?>:</label><br />
		<input type="password" name="cpass" size="20" maxlength="32" value="" onChange="setDirty(this);"/>
		</p>			
	<?php } ?>
	
	<p>
		<label><?php echo LBL_USER_LANGUAGE; ?>: <span class="obligatorio">*</span> </label><br/>
		<select name="user_lang" onChange="setDirty(this);">
		<?php echo $util->desplegable("ica_language", "language_code", "language_name", $obj->getUserLang(), $_SESSION["language"], false, 0); ?>
		</select>
	</p>

	</fieldset>
	
	<br class="limpiar"/>
	<br/>
		
		
		
	

	<?php 
	$contador=0;
	if (is_numeric($id)){
?>
<br class="limpiar"/>	
<fieldset>
	<table class="tabla1" summary="<?php echo LBL_MOODLE_COURSES; ?>">
			<caption><?php echo LBL_MOODLE_COURSES; ?></caption>
			<thead>
				<tr>
					<th><?php echo LBL_COURSE;?></th>
					<th><?php echo LBL_ROLE;?></th>
					<th>&nbsp;</th>
				</tr>
				<tr>
					<td colspan="2" class="separa">&nbsp;</td>
				</tr>
			</thead>
			<tbody>
<?php
		
		$resultado = $obj->getMoodleCourses();
		if ($resultado!=null){
			while($row = $resultado->fetch_array()){
				$contador++;	
?>			
				<tr <?php if (fmod($contador,2)){ ?>class="fondo"<?php } ?>>
					<td>
						<input type="hidden" id="tutor_course_id_<?php echo $contador; ?>" name="tutor_course_id_<?php echo $contador; ?>" value="<?php echo $row["tutor_course_id"];?>"/>
						<select id="course_id_<?php echo $contador; ?>" name="course_id_<?php echo $contador; ?>" onChange="setDirty(this);" <?php echo ($bReadOnly)?"disabled class=\"readonly\" ":""; ?> >
							<?php echo $utilMoodle->desplegableMoodle("mdl_course", "shortname", "CONCAT(shortname,' - ',fullname)", $row["course_id"], "", true, 0, "shortname", " AND id>1 AND (shortname IS NOT NULL AND trim(shortname)<>'') "); ?>
						</select>
					</td>
					
					<td>
						<label><?php echo LBL_ROLE; ?></label>
						<select id="role_moodle_<?php echo $contador; ?>" name="role_moodle_<?php echo $contador; ?>" onChange="setDirty(this);" <?php echo ($bReadOnly)?"disabled class=\"readonly\" ":""; ?> >
							<?php echo $utilMoodle->desplegableMoodle("mdl_role", "shortname", "name", $row["role_moodle"], "", true, 0, "name", " AND id in (3,4) "); ?>
						</select>
					</td>
					
					<td align="right">
						<a href="javascript:doDeleteMoodleCourse('<?php echo $contador; ?>')" title="<?php echo LBL_ALT_DELETE_MOOD_COUR;?>"><img src="/images/delete.gif" alt="<?php echo LBL_ALT_DELETE_MOOD_COUR;?>" title="<?php echo LBL_DELETE; ?>" /></a>
					</td>	
				</tr>
				
<?php	
				
			}
		}
?>			
			<?php if (!$bReadOnly){ ?>
				<tr <?php if (fmod($contador+1,2)){ ?>class="fondo"<?php } ?>>
					<td colspan="1">
						<input type="hidden" id="tutor_course_id_0" name="tutor_course_id_0" value="new"/>
						<select id="course_id_0" name="course_id_0" onChange="setDirty(this);" <?php echo ($bReadOnly)?"disabled class=\"readonly\" ":""; ?> >
							<?php echo $utilMoodle->desplegableMoodle("mdl_course", "shortname", "CONCAT(shortname,' - ',fullname)", "", "", true, 0, "shortname", " AND id>1 AND (shortname IS NOT NULL AND trim(shortname)<>'') "); ?>
						</select>
						<img src="/images/new.gif" title="<?php echo LBL_ALT_NEW1;?>" alt="<?php echo LBL_ALT_NEW1;?>"></a>
					</td>
					<td>
						<label><?php echo LBL_ROLE; ?></label>
						<select id="role_moodle_0" name="role_moodle_0" onChange="setDirty(this);" <?php echo ($bReadOnly)?"disabled class=\"readonly\" ":""; ?> >
							<?php echo $utilMoodle->desplegableMoodle("mdl_role", "shortname", "name", "1", "", true, 0, "name", " AND id in (3,4) "); ?>
						</select>
					</td>
					<td>
						&nbsp;
					</td>
				</tr>
			<?php } ?>	
			</tbody>
	</table>
</fieldset>

<input type="hidden" name="num_courses_moodle" value="<?php echo $contador+1; ?>"/>
	
<?php
	}else{
?>

<input type="hidden" name="num_courses_moodle" value="0"/>

<?php } ?>
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	<fieldset>
	
		<p>
			<label><?php echo LBL_REMARKS1; ?>: </label><br/>
			<textarea name="remarks" cols="100" rows="7" onChange="setDirty(this);"><?php echo $obj->getRemarks(); ?></textarea>
		</p>

	</fieldset>

<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMObjectTimestamp.php'); ?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMScreenBottom.php'); ?>


<script type="text/javascript" CHARSET="ISO-8859-1" defer >
<!--
<?php if (!$obj->getIsUser()){?>
	writeUserID();
<?php } ?>

// -->
</script>


	<input type="hidden" name="tab" value="1"/>
	<input type="hidden" name="id" value="<?php echo $id; ?>"/>
	<input type="hidden" name="is_user" value="<?php echo $obj->getIsUser(); ?>"/>	

<?php $type = ""; ?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMEndDocument.php'); ?>