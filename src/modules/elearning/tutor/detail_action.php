<?php 
	$object_type_id=103;
	require_once($_SERVER['DOCUMENT_ROOT']."/includes/control.php");
	require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/elearning/Tutor.php'); 

$obj = new Tutor();

if (isset($action)){
	
	if ($action=="NEW"){
		$resultado = $obj->init();
		if ($resultado==""){ //NO hay error.
			$obj->createNew();
			
			$id = $obj->getID(); //�ltimo insertado
			
			//MENSAJE...
			$_SESSION["aviso_tipo"] = "success";
			$_SESSION["aviso_mensaje"] = MSG_INFO_CHANGED_CORRECTLY; 
			Header("Location: detail.php?id=$id"); 
			
		}else{ //hay error redirecciono y muestro error.
			$_SESSION["aviso_tipo"] = "warning";  
			$_SESSION["aviso_mensaje"] = $resultado;
			//Header("Location: detail.php?id=new"); 
		}
	}else if ($action=="UPDATE"){
		
		if (isset($action) && is_numeric($id) ) {
			
			$resultado = $obj->init();
			
			if ($resultado==""){ //NO hay error.
				$obj->update($id);
				
				//MENSAJE...
				$_SESSION["aviso_tipo"] = "success";
				$_SESSION["aviso_mensaje"] = MSG_INFO_CHANGED_CORRECTLY; 
			}else{//hay error redirecciono y muestro error.
				
				//echo $resultado;
				$_SESSION["aviso_tipo"] = "warning";  
				$_SESSION["aviso_mensaje"] = $resultado; 
			}
				
			session_write_close();
			Header("Location: detail.php?id=$id"); 
			exit();
			
		}

	}		
		
		
}
	
if (isset($_POST['action']) && $_POST['action']=="DELETE"){
	
	if (isset($_POST['id']) && is_numeric($_POST['id']) ) {
			$obj->delete($_POST['id']);
			
			//Cargar mensaje de borrado OK.
			$_SESSION["aviso_tipo"] = "success";
			$_SESSION["aviso_mensaje"] = MSG_INFO_DELETED_CORRECTLY; 
			Header("Location: locator.php"); 
			
	}
}else{ //hay error redirecciono y muestro error.
	
}


?>