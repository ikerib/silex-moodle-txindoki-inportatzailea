<?php 
$buttons="buttons_detail.php";
$tab=2;
$tabs="tabs.php";
$object_type_id=103;

include($_SERVER['DOCUMENT_ROOT']."/includes/control.php"); 
require_once($_SERVER['DOCUMENT_ROOT']."/lang/".$_SESSION["language"]."/lbl_course.php"); 
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/elearning/Tutor.php'); 
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/Functions.php'); 


$id="";
$enctype="";
$action_url="/modules/elearning/student/courses.php";
$action="";

$obj = new Tutor();

if (isset($_GET["id"]) && $_GET["id"]!="")
	$id=$_GET["id"];
else if (isset($_POST["id"]) && $_POST["id"]!="")
	$id=$_POST["id"];
	
	
//Comprobamos si viene de un borrado
if (isset($_POST['action']) && $_POST['action']=="DELETE"){
	echo "---->ACTION=".$_POST['action'];
	include($_SERVER['DOCUMENT_ROOT'].'/modules/elearning/student/detail_action.php');
	exit;
}	

	
if ($id!="" && is_numeric($id)){
	$obj->loadData($id);
	$action="UPDATE";
}else{
	echo "eerror2"; // o producir un error hacia una web
}	

if (isset($_POST["id"])){
	include($_SERVER['DOCUMENT_ROOT'].'/modules/elearning/student/courses_action.php');
}

$cursosAsignados = $obj->getAssignedCourses();
$cursosNoAsignados = $obj->getNoAssignedCourses();	

include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMBeginDocument.php');

include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMHeaderTags.php'); 

?>

<SCRIPT src="/modules/elearning/student/js/courses.js" type="text/javascript" CHARSET="ISO-8859-1"></SCRIPT>
<script src="/js/select_events.js"  type="text/javascript" CHARSET="ISO-8859-1"></script>

<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMScreenHeader.php'); ?>




	<div id="roles">
		<div class="posibles">
			<p><?php echo LBL_POSSIBLE_COURSES; ?>:</p>
			<select name="from" size="10" multiple style="width:180px">
<?php
	while($row = $cursosNoAsignados->fetch_array())		
	{
?>	
				<option value="<?php echo $row["course_id"]; ?>"><?php echo $row["course_name"]; ?></option>
<?php
	}
?>
			</select>				
		</div>
		<div class="botones">
			<p><input type="button" style="width:180px;" name="add_all" class="boton" value="<?php echo LBL_ADD_COURSE_ALL; ?> &gt;&gt;" onClick="doAddAll(true); setDirty(this);"/></p>
            <p><input type="button" style="width:180px;" name="add" class="boton" value="<?php echo LBL_ADD_COURSE; ?> &gt;" onClick="doAdd(true); setDirty(this);"/></p>
            <p><input type="button" style="width:180px;" name="remove" class="boton" value="&lt; <?php echo LBL_REMOVE_COURSE; ?>" onClick="doRemove(true); setDirty(this);"/></p>
            <p><input type="button" style="width:180px;" name="remove_all" class="boton" value="&lt;&lt; <?php echo LBL_REMOVE_COURSE_ALL; ?>" onClick="doRemoveAll(true); setDirty(this);"/></p>		
		</div>
		<div class="asignados">
			<p><?php echo LBL_ASSIGNED_COURSES; ?>:</p>
			 <select name="to" size="10" multiple style="width:200px">
<?php
  if ($cursosAsignados->num_rows > 0){
	while($row = $cursosAsignados->fetch_array())		
	{
?>		
				<option value="<?php echo $row["course_id"]; ?>"><?php echo $row["course_name"]; ?></option>
<?php
	}
  }
?>
			</select>	
		</div>
		<br class="limpiar" />
	</div>

<br class="limpiar"/>

<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMScreenBottom.php'); ?>


<script type="text/javascript" CHARSET="ISO-8859-1" defer >
<!--

//document.data_frm.name.focus(); 

// -->
</script>

<input type="hidden" name="id" value="<?php echo $obj->getID(); ?>"/>
<input type="hidden" name="assig_courses" value=""/>


<?php $type = ""; ?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMEndDocument.php'); ?>

