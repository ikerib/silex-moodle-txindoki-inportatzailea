<!-- Control de permisos a los objetos  y pestaña seleccionada -->

<?php 
$object_type_id=101 ;

require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/Functions.php');
require_once($_SERVER['DOCUMENT_ROOT']."/lang/".$_SESSION["language"]."/lbl_general.php"); 
$buildTab = new Tabs();

//id --> Puede ser new o un id.
//$tab --> Pestaña en la que se encuentra.

if (isset($_GET["id"]) && $_GET["id"]!="")
	$id=$_GET["id"];
else if (isset($_POST["id"]) && $_POST["id"]!="")
	$id=$_POST["id"];
	
if (isset($id) && $tab!=""){
	
?>	
		<?php echo $buildTab->buildTab(1, $tab, $object_type_id, $id, LBL_DETAIL, "detail"); ?>
		<?php echo $buildTab->buildTab(2, $tab, $object_type_id, $id, LBL_STUDENTS, "students"); ?>
<?php	
	
}else{
	echo "error2"; // o producir un error hacia una web
}	

?>

