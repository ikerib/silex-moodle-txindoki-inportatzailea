<?php 
$buttons="buttons_detail.php";
$tab=1;
$tabs="tabs.php";
$object_type_id=101 ;

include($_SERVER['DOCUMENT_ROOT']."/includes/control.php"); 
require_once($_SERVER['DOCUMENT_ROOT']."/lang/".$_SESSION["language"]."/lbl_general.php"); 
require_once($_SERVER['DOCUMENT_ROOT']."/lang/".$_SESSION["language"]."/lbl_course.php"); 
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/elearning/Course.php'); 
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/Functions.php'); 
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/FunctionsMoodle.php'); 


$id="";
$enctype="multipart/form-data";
$action_url="/modules/elearning/course/detail.php";
$action="";

$obj = new Course();
$util = new Util();
$utilMoodle = new UtilMoodle();

if (isset($_GET["id"]) && $_GET["id"]!="")
	$id=$_GET["id"];
else if (isset($_POST["id"]) && $_POST["id"]!="")
	$id=$_POST["id"];



//Comprobamos si viene de un borrado
if (isset($_POST['action']) && $_POST['action']=="DELETE"){
	//echo "---->ACTIONnnn=".$_POST['action'];
	include($_SERVER['DOCUMENT_ROOT'].'/modules/elearning/course/detail_action.php');
	exit;
}
	
	
if ($id!=""){
	
	if ($id=="new"){
		$obj->loadData("new");
		$action="NEW";
		$id="new";
	}else if (is_numeric($id)){	
		$obj->loadData($id);
		$action="UPDATE";
	}else{
		echo "error1"; // o producir un error hacia una web
	}
}else{
	echo "eerror2"; // o producir un error hacia una web
}	

if (isset($_POST["id"])){
	include($_SERVER['DOCUMENT_ROOT'].'/modules/elearning/course/detail_action.php');
}







include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMBeginDocument.php');

include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMHeaderTags.php'); 

?>

<script language="JavaScript" type="text/javascript" CHARSET="ISO-8859-1">
	<?php 
		/*var path_IMAGE_BLANK = "/images/spacer.gif";
		var mesg_FILE_DELETION = "borrado";*/
	?>
	var mesg_WARNING = "<?php echo LBL_WARNING; ?>";
	var mesg_SELECT_IMAGE = "<?php echo LBL_SELECT_IMAGE; ?>";
	var mesg_CHECK_EXTENSION = "<?php echo MSG_CHECK_EXTENSION; ?>";
</script>

<SCRIPT src="/modules/elearning/course/js/data_check.js" type="text/javascript" CHARSET="ISO-8859-1"></SCRIPT>

<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMScreenHeader.php'); ?>

<fieldset>			
	<p>
		<label><span class="obligatorio"><?php echo MSG_CAMPOS_OBLIGATORIOS;?></span></label>
	</p>
	<br class="limpiar"/>
	<br/>
	
	<p>
		<label><?php echo LBL_COURSE; ?>: <span class="obligatorio">*</span></label><br/>
		<input type="text" name="name" size="80" maxlength="100" <?php echo ($bReadOnly)?"readonly=\"readonly\" class=\"readonly\" ":""; ?> value="<?php echo $obj->getName(); ?>" onChange="setDirty(this);"/>
	</p>
	
	<p>
		<br/>
		<input type="checkbox" name="validate_flag" value="1" <?php if ($obj->isValidateFlag()){ ?>checked<?php } ?> onChange="setDirty(this);"/><label><?php echo LBL_VALIDATE_FLAG; ?></label>
	</p>
	<br class="limpiar"/>
	
	<p>
			<label><?php echo LBL_INTERNAL_ID; ?>: </label><br/>
			<input type="text" name="internal_id" size="40" maxlength="100" value="<?php echo $obj->getInternalID(); ?>" onChange="setDirty(this);"/>
	</p>
	
	<br class="limpiar"/>	
	<br/>
	
	<p>
		<label><?php echo LBL_ICON; ?>:</label>
		<br/>
		
		<input type="file" id="picture" name="picture" size="50" maxlength="100" onChange="setDirty(this);"/>
		<input type="hidden" name="lim_tamano" value="<?php echo CFG_SIZE_COURSE_BYTES; ?>"/> 
	</p>
	
	<br class="limpiar"/>	
	
	<?php $nota="";
		  $nota=str_replace("$1",CFG_PICTURE_COURSE_WIDTH,MSG_INFO_SIZE);
		  $nota=str_replace("$2", CFG_PICTURE_COURSE_HEIGHT,$nota);
		  $nota=str_replace("$3", CFG_TH_PICTURE_COURSE_WIDTH,$nota);
		  $nota=str_replace("$4", CFG_TH_PICTURE_COURSE_HEIGHT,$nota);
		  
	?>
	<p><b><?php echo strtoupper(LBL_NOTE); ?></b>:<?php echo $nota; ?></p>
	<br class="limpiar"/>
	
	<p>
		<center>
		<?php if ($obj->getThumbnailFileExists() && $obj->getPictureFileExists() && $obj->getPictureFile()!=""){ ?>
		
			<a href="javascript:NewWindow('/includes/CRMViewImage.php?cid=<?php echo $obj->getCompanyID();?>&nim=<?php echo $obj->getID()."_".$obj->getPictureFile(); ?>&path=/course/','image',<?php echo $obj->getPictureWidth();?>,<?php echo $obj->getPictureHeight();?>,'no')">
				<img name="img_picture" src="/includes/CRMViewImage.php?cid=<?php echo $obj->getCompanyID();?>&nim=<?php echo $obj->getID()."_th_".$obj->getPictureFile(); ?>&path=/course/" alt="imagen"/>
			</a>
			<br/>
			<br/>
			<?php echo MSG_INFO_ORIGINAL; ?>
			
		<?php }else{ ?>
			<font color="#ff0000"><?php echo MSG_INFO_SELECT; ?></font>
		<?php } ?>
		</center>		
		<br class="limpiar"/>	
	</p>
	
	<br class="limpiar"/>
	<br/>	
	
	<p>
		<label><?php echo LBL_OBJECTIVE; ?>: </label><br/>
		<textarea name="description" cols="100" rows="5" onChange="setDirty(this);" <?php echo ($bReadOnly)?"readonly=\"readonly\" class=\"readonly\" ":""; ?> ><?php echo $obj->getDescription(); ?></textarea>
	</p>	
	
	<br class="limpiar"/>
		
	<p>
		<label><?php echo LBL_PROGRAM; ?>: </label><br/>
		<textarea name="remarks" cols="100" rows="5" onChange="setDirty(this);" <?php echo ($bReadOnly)?"readonly=\"readonly\" class=\"readonly\" ":""; ?> ><?php echo $obj->getRemarks(); ?></textarea>
	</p>

	<br class="limpiar"/>
	
	<p>
		<label><?php echo LBL_LEVEL; ?>:  <span class="obligatorio">*</span></label><br/>
		<select name="level_id" onChange="setDirty(this);" <?php echo ($bReadOnly)?"disabled class=\"readonly\" ":""; ?> >
			<?php echo $util->desplegable("ic_level", "level_id", "level_name", $obj->getLevelID(), $_SESSION["language"], false, 0); ?>
		</select>
	</p>
	
	<p>
		<label><?php echo LBL_CATEGORY; ?>:  <span class="obligatorio">*</span></label><br/>
		<select name="category_id" onChange="setDirty(this);" <?php echo ($bReadOnly)?"disabled class=\"readonly\" ":""; ?> >
			<?php echo $util->desplegable("ic_course_category", "course_category_id", " concat(course_category_name,' - ', language) ", $obj->getCategoryID(), "", true, 1, "language" ); ?>
		</select>
	</p>
	
	<p>	
		<label><?php echo LBL_COURSE_MOODLE; ?>: <span class="obligatorio">*</span></label><br/>
		<select id="course_moodle_id" name="course_moodle_id" onChange="setDirty(this);" >
			<?php echo $utilMoodle->desplegableMoodle("mdl_course", "shortname", "CONCAT(shortname,' - ',fullname)", $obj->getCourseMoodleID(), "", true, 0, "shortname", " AND id>1 AND (shortname IS NOT NULL AND trim(shortname)<>'') "); ?>
		</select>
	</p>	
	
	<br class="limpiar"/>
	
	<p>
		<label><?php echo LBL_LANGUAGE; ?>: <span class="obligatorio">*</span></label><br/>
		<input type="text" name="language_info" size="40" maxlength="100" <?php echo ($bReadOnly)?"readonly=\"readonly\" class=\"readonly\" ":""; ?> value="<?php echo $obj->getLanguageInfo(); ?>" onChange="setDirty(this);"/>
	</p>	

	<br class="limpiar"/>
	
	<p>
		<label><?php echo LBL_DURACION; ?>: </label><br/>
		<input type="text" name="duracion" size="80" maxlength="100" <?php echo ($bReadOnly)?"readonly=\"readonly\" class=\"readonly\" ":""; ?> value="<?php echo $obj->getDuracion(); ?>" onChange="setDirty(this);"/>
	</p>
	
	<br class="limpiar"/>
	
	<p>
		<label><?php echo LBL_RESPONSABLE; ?>: </label><br/>
		<input type="text" name="responsable" size="80" maxlength="100" <?php echo ($bReadOnly)?"readonly=\"readonly\" class=\"readonly\" ":""; ?> value="<?php echo $obj->getResponsable(); ?>" onChange="setDirty(this);"/>
	</p>
	
	<br class="limpiar"/>
	
	<p>
		<label><?php echo LBL_TIPO_INSCRIPCION; ?>: </label><br/>
		<input type="text" name="tipo_inscripcion" size="80" maxlength="100" <?php echo ($bReadOnly)?"readonly=\"readonly\" class=\"readonly\" ":""; ?> value="<?php echo $obj->getTipoInscripcion(); ?>" onChange="setDirty(this);"/>
	</p>
	

	
	
	
</fieldset>	
	

<br class="limpiar" />	


<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMObjectTimestamp.php'); ?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMScreenBottom.php'); ?>


<script type="text/javascript" CHARSET="ISO-8859-1" defer >
<!--

document.data_frm.name.focus(); 

// -->
</script>

	<input type="hidden" name="id" value="<?php echo $id; ?>"/>

	<input type="hidden" name="bupload_image" value="no"/>
	<input type="hidden" name="picture_file_old" value="<?php echo $obj->getPictureFile(); ?>"/>

	
<?php $type = ""; ?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMEndDocument.php'); ?>

