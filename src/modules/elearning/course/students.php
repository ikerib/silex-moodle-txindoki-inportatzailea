<?php 
$buttons="buttons_detail.php";
$tab=2;
$tabs="tabs.php";
$object_type_id=101 ;

include($_SERVER['DOCUMENT_ROOT']."/includes/control.php"); 
require_once($_SERVER['DOCUMENT_ROOT']."/lang/".$_SESSION["language"]."/lbl_student.php"); 
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/elearning/Course.php'); 
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/Functions.php'); 


$id="";
$enctype="";
$action_url="/modules/elearning/course/students.php";
$action="";

$obj = new Course();

if (isset($_GET["id"]) && $_GET["id"]!="")
	$id=$_GET["id"];
else if (isset($_POST["id"]) && $_POST["id"]!="")
	$id=$_POST["id"];
	
	
//Comprobamos si viene de un borrado
if (isset($_POST['action']) && $_POST['action']=="DELETE"){
	echo "---->ACTION=".$_POST['action'];
	include($_SERVER['DOCUMENT_ROOT'].'/modules/elearning/course/detail_action.php');
	exit;
}	

	
if ($id!="" && is_numeric($id)){
	$obj->loadData($id);
	$action="UPDATE";
}else{
	echo "eerror2"; // o producir un error hacia una web
}	

if (isset($_POST["id"])){
	include($_SERVER['DOCUMENT_ROOT'].'/modules/elearning/course/students_action.php');
}

$alumnosNoAsignados = $obj->getNoAssignedStudents();
$alumnosAsignados = $obj->getAssignedStudents();

include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMBeginDocument.php');

include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMHeaderTags.php'); 

?>

<SCRIPT src="/modules/elearning/course/js/students.js" type="text/javascript" CHARSET="ISO-8859-1"></SCRIPT>
<script src="/js/select_events.js"  type="text/javascript" CHARSET="ISO-8859-1"></script>

<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMScreenHeader.php'); ?>




	<div id="roles">
		<div class="posibles">
			<p><?php echo LBL_POSSIBLE_STUDENTS; ?>:</p>
			<select name="from" size="10" multiple style="width:180px">
<?php
	while($row = $alumnosNoAsignados->fetch_array())		
	{
?>	
				<option value="<?php echo $row["student_id"]; ?>"><?php echo $row["student_name"]; ?></option>
<?php
	}
?>
			</select>	
			
<?php
			$search="";
			if (isset($_GET["search"]) && $_GET["search"]!=""){
				$search=$_GET["search"];
			}
?>
			<br class="limpiar"/>
			<input type="text" name="course_search" id="course_search" value="<?php echo $search;?>"/> 
			<input type="button" name="search_one" id="search_one" class="boton" value="Buscar" onClick="doSearchCourse('filter',<?php echo $id; ?>)"/> 
			<input type="button" name="search_all" id="search_all" class="boton" value="Mostrar todo" onClick="doSearchCourse('all',<?php echo $id; ?>);"/> 
			
		</div>
		<div class="botones">
			<p><input type="button" style="width:180px;" name="add_all" class="boton" value="<?php echo LBL_ADD_STUDENT_ALL; ?> &gt;&gt;" onClick="doAddAll(true); setDirty(this);"/></p>
            <p><input type="button" style="width:180px;" name="add" class="boton" value="<?php echo LBL_ADD_STUDENT; ?> &gt;" onClick="doAdd(true); setDirty(this);"/></p>
            <p><input type="button" style="width:180px;" name="remove" class="boton" value="&lt; <?php echo LBL_REMOVE_STUDENT; ?>" onClick="doRemove(true); setDirty(this);"/></p>
            <p><input type="button" style="width:180px;" name="remove_all" class="boton" value="&lt;&lt; <?php echo LBL_REMOVE_STUDENT_ALL; ?>" onClick="doRemoveAll(true); setDirty(this);"/></p>		
		</div>
		
		<div class="asignados">
			<p><?php echo LBL_ASSIGNED_STUDENTS; ?>:</p>
			 <select name="to" size="10" multiple style="width:180px">
<?php



 if ($alumnosAsignados->num_rows > 0){
 
	while($row = $alumnosAsignados->fetch_array())		
	{
?>		
				<option value="<?php echo $row["student_id"]; ?>"><?php echo $row["student_name"]; ?></option>
<?php
	}
 }	
?>
			</select>	
		</div>
		<br class="limpiar" />
	</div>

<br class="limpiar"/>

<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMScreenBottom.php'); ?>


<script type="text/javascript" CHARSET="ISO-8859-1" defer >
<!--

//document.data_frm.name.focus(); 

// -->
</script>

<input type="hidden" name="id" value="<?php echo $obj->getID(); ?>"/>
<input type="hidden" name="assig_students" value=""/>


<?php $type = ""; ?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMEndDocument.php'); ?>

