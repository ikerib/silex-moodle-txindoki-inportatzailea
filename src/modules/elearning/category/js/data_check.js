

function checkRequiredData()
{
	if (document.data_frm.picture.value!=""){
		if (validar_picture()){
			return true;
		}else
			return false;
	}else if (document.data_frm.picture_ac.value!=""){
		if (validar_picture_ac()){
			return true;
		}else
			return false;
	}else{
		return true;
	}
}  // checkRequiredData 


function checkSearchFields()
{
	return true;
}  // checkSearchFields 

function doCancel()
{
	document.data_frm.tab.value=1;
}  // checkSearchFields 

function doDeleteMoodleCategory(id){
	var x = document.getElementById("moodle_course_id_" + id);
	setDirty(x);
	x.value="";
	doSave();
}


//validar_picture
function validar_picture(){

		if (validar_picture2()){
			return true;
		}
}//end validar_picture

function validar_picture2(){
		
		if (document.data_frm.picture.value!=""){
			if (comprueba_extension(document.data_frm, document.data_frm.picture.value)){
			
				document.data_frm.bupload_image.value="yes";
				return true;
			}else{
				document.data_frm.bupload_image.value="no";
				return false; 
			}
		}else{
			if (document.data_frm.picture_file_old.value!=""){ //Ya existe una imagen, se puede modificar si quiere.
				document.data_frm.bupload_image.value="no";
				return true;
			}else{
				document.data_frm.bupload_image.value="no";
				alert(mesg_WARNING + '\n' + mesg_SELECT_IMAGE);
				return false;
			}
		}
		
}//end validar_picture2



//validar_picture
function validar_picture_ac(){

		if (validar_picture_ac2()){
			return true;
		}
}//end validar_picture

function validar_picture_ac2(){
		
		if (document.data_frm.picture_ac.value!=""){
			if (comprueba_extension(document.data_frm, document.data_frm.picture_ac.value)){
			
				document.data_frm.bupload_image_ac.value="yes";
				return true;
			}else{
				document.data_frm.bupload_image_ac.value="no";
				return false; 
			}
		}else{
			if (document.data_frm.picture_file_old_ac.value!=""){ //Ya existe una imagen, se puede modificar si quiere.
				document.data_frm.bupload_image_ac.value="no";
				return true;
			}else{
				document.data_frm.bupload_image_ac.value="no";
				alert(mesg_WARNING + '\n' + mesg_SELECT_IMAGE);
				return false;
			}
		}
		
		
}//end validar_picture2


function comprueba_extension(formulario, archivo) {
	extensiones_permitidas = new Array(".gif", ".jpg", ".png");
	mierror = "";
	if (!archivo) {
		//Si no tengo archivo, es que no se ha seleccionado un archivo en el formulario
		mierror = mesg_SELECT_IMAGE;
	}else{
		//recupero la extensi�n de este nombre de archivo
		extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
		//alert (extension);
		//compruebo si la extensi�n est� entre las permitidas
		permitida = false;
		for (var i = 0; i < extensiones_permitidas.length; i++) {
			if (extensiones_permitidas[i] == extension) {
				permitida = true;
				break;
			}
		}
		if (!permitida) {
			mierror = mesg_CHECK_EXTENSION + extensiones_permitidas.join();
		}else{
			return 1;
		}
	}
	//si estoy aqui es que no se ha podido submitir
	alert(mesg_WARNING + '\n' + mierror);
	
	return 0;
} 