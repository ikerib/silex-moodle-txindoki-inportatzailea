<?php 
$buttons="buttons_detail.php";
$tab=1;
$tabs="tabs.php";
$object_type_id=102 ;

include($_SERVER['DOCUMENT_ROOT']."/includes/control.php"); 
require_once($_SERVER['DOCUMENT_ROOT']."/lang/".$_SESSION["language"]."/lbl_general.php"); 
require_once($_SERVER['DOCUMENT_ROOT']."/lang/".$_SESSION["language"]."/lbl_category.php"); 
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/elearning/Category.php'); 
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/Functions.php'); 
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/FunctionsMoodle.php'); 


$id="";
$enctype="multipart/form-data";
$action_url="/modules/elearning/category/detail.php";
$action="";

$obj = new Category();
$util = new Util();
$utilMoodle = new UtilMoodle();

if (isset($_GET["id"]) && $_GET["id"]!="")
	$id=$_GET["id"];
else if (isset($_POST["id"]) && $_POST["id"]!="")
	$id=$_POST["id"];



//Comprobamos si viene de un borrado
if (isset($_POST['action']) && $_POST['action']=="DELETE"){
	//echo "---->ACTIONnnn=".$_POST['action'];
	include($_SERVER['DOCUMENT_ROOT'].'/modules/elearning/category/detail_action.php');
	exit;
}
	
	
if ($id!=""){
	
	if ($id=="new"){
		$obj->loadData("new");
		$action="NEW";
		$id="new";
	}else if (is_numeric($id)){	
		$obj->loadData($id);
		$action="UPDATE";
	}else{
		echo "error1"; // o producir un error hacia una web
	}
}else{
	echo "eerror2"; // o producir un error hacia una web
}	

if (isset($_POST["id"])){
	include($_SERVER['DOCUMENT_ROOT'].'/modules/elearning/category/detail_action.php');
}

include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMBeginDocument.php');

include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMHeaderTags.php'); 

?>

<script language="JavaScript" type="text/javascript" CHARSET="ISO-8859-1">
	<?php 
		/*var path_IMAGE_BLANK = "/images/spacer.gif";
		var mesg_FILE_DELETION = "borrado";*/
	?>
	var mesg_WARNING = "<?php echo LBL_WARNING; ?>";
	var mesg_SELECT_IMAGE = "<?php echo LBL_SELECT_IMAGE; ?>";
	var mesg_CHECK_EXTENSION = "<?php echo MSG_CHECK_EXTENSION; ?>";
</script>

<SCRIPT src="/modules/elearning/category/js/data_check.js" type="text/javascript" CHARSET="ISO-8859-1"></SCRIPT>

<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMScreenHeader.php'); ?>

<fieldset>			
	
	<p>
		<label><span class="obligatorio"><?php echo MSG_CAMPOS_OBLIGATORIOS;?></span></label>
	</p>
	<br class="limpiar"/>
	<br/>
	
	<p>
		<label><?php echo LBL_CATEGORY; ?>: <span class="obligatorio">*</span></label><br/>
		<input type="text" name="name" size="80" maxlength="100" <?php echo ($bReadOnly)?"readonly=\"readonly\" class=\"readonly\" ":""; ?> value="<?php echo $obj->getName(); ?>" onChange="setDirty(this);"/>
	</p>
	
	<p>
		<label><?php echo LBL_LANGUAGE; ?>:</label><br />
		<select name="language" onChange="setDirty(this);" style="width:142px;">
		<?php echo $util->desplegable("ica_language", "language_code", "language_name", $obj->getLanguage(), $_SESSION["language"], false, 0); ?>
		</select>
	</p>
	
	<br class="limpiar"/>
	<br/>
	
	<p>
		<label><?php echo LBL_ICON; ?>:</label>
		<br/>
		
		<input type="file" id="picture" name="picture" size="50" maxlength="100" onChange="setDirty(this);"/>
		<input type="hidden" name="lim_tamano" value="<?php echo CFG_SIZE_CATEGORY_BYTES; ?>"/> 
	</p>
	
	<br class="limpiar"/>	
	
	<?php $nota="";
		  $nota=str_replace("$1",CFG_PICTURE_CATEGORY_WIDTH,MSG_INFO_SIZE);
		  $nota=str_replace("$2", CFG_PICTURE_CATEGORY_HEIGHT,$nota);
		  $nota=str_replace("$3", CFG_TH_PICTURE_CATEGORY_WIDTH,$nota);
		  $nota=str_replace("$4", CFG_TH_PICTURE_CATEGORY_HEIGHT,$nota);
		  
	?>
	<p><b><?php echo strtoupper(LBL_NOTE); ?></b>:<?php echo $nota; ?></p>
	<br class="limpiar"/>
	
	<p>
		<center>
		<?php if ($obj->getThumbnailFileExists() && $obj->getPictureFileExists() && $obj->getPictureFile()!=""){ ?>
		
			<a href="javascript:NewWindow('/includes/CRMViewImage.php?cid=<?php echo $obj->getCompanyID();?>&nim=<?php echo $obj->getID()."_".$obj->getPictureFile(); ?>&path=/category/','image',<?php echo $obj->getPictureWidth();?>,<?php echo $obj->getPictureHeight();?>,'no')">
				<img name="img_picture" src="/includes/CRMViewImage.php?cid=<?php echo $obj->getCompanyID();?>&nim=<?php echo $obj->getID()."_th_".$obj->getPictureFile(); ?>&path=/category/" alt="imagen"/>
			</a>
			<br/>
			<br/>
			<?php echo MSG_INFO_ORIGINAL; ?>
			
		<?php }else{ ?>
			<font color="#ff0000"><?php echo MSG_INFO_SELECT; ?></font>
		<?php } ?>
		</center>		
		<br class="limpiar"/>	
	</p>
	
	
	<br class="limpiar"/>
	<br/>
	
	<p>
		<label><?php echo LBL_ICON_AC; ?>:</label>
		<br/>
		
		<input type="file" id="picture_ac" name="picture_ac" size="50" maxlength="100" onChange="setDirty(this);"/>
		<input type="hidden" name="lim_tamano_ac" value="<?php echo CFG_SIZE_CATEGORY_BYTES; ?>"/> 
	</p>
	
	<br class="limpiar"/>	
	
	<?php $nota="";
		  $nota=str_replace("$1",CFG_PICTURE_CATEGORY_WIDTH,MSG_INFO_SIZE_AC);
		  $nota=str_replace("$2", CFG_PICTURE_CATEGORY_HEIGHT_AC,$nota);
		  $nota=str_replace("$3", CFG_TH_PICTURE_CATEGORY_WIDTH_AC,$nota);
		  $nota=str_replace("$4", CFG_TH_PICTURE_CATEGORY_HEIGHT_AC,$nota);
		  
	?>
	<p><b><?php echo strtoupper(LBL_NOTE); ?></b>:<?php echo $nota; ?></p>
	<br class="limpiar"/>
	
	<p>
		<center>
		<?php if ($obj->getThumbnailFileACExists() && $obj->getPictureFileACExists() && $obj->getPictureFileAC()!=""){ ?>
		
			<a href="javascript:NewWindow('/includes/CRMViewImage.php?cid=<?php echo $obj->getCompanyID();?>&nim=<?php echo $obj->getID()."_".$obj->getPictureFileAC(); ?>&path=/category/ac/','image',<?php echo $obj->getPictureWidthAC();?>,<?php echo $obj->getPictureHeightAC();?>,'no')">
				<img name="img_picture_ac" src="/includes/CRMViewImage.php?cid=<?php echo $obj->getCompanyID();?>&nim=<?php echo $obj->getID()."_th_".$obj->getPictureFileAC(); ?>&path=/category/ac/" alt="imagen"/>
			</a>
			<br/>
			<br/>
			<?php echo MSG_INFO_ORIGINAL_AC; ?>
			
		<?php }else{ ?>
			<font color="#ff0000"><?php echo MSG_INFO_SELECT_AC; ?></font>
		<?php } ?>
		</center>		
		<br class="limpiar"/>	
	</p>
	
	
	<br class="limpiar"/>
	<br/>	
	
	<p>
		<label><?php echo LBL_DESCRIPTION; ?>: </label><br/>
		<input type="text" name="description" size="80" maxlength="250" <?php echo ($bReadOnly)?"readonly=\"readonly\" class=\"readonly\" ":""; ?> value="<?php echo $obj->getDescription(); ?>" onChange="setDirty(this);"/>
	</p>
	
	<br class="limpiar"/>
	
	<p>
		<label><?php echo LBL_SHORTNAME; ?>: <span class="obligatorio">*</span></label><br/>
		<input type="text" name="shortname" size="30" maxlength="20" <?php echo ($bReadOnly)?"readonly=\"readonly\" class=\"readonly\" ":""; ?> value="<?php echo $obj->getShortname(); ?>" onChange="setDirty(this);"/>
	</p>
	
	<p>
		<label><?php echo LBL_ORDER_APPEARANCE; ?>:  <span class="obligatorio">*</span></label><br/>
		<select name="order_appearance" onChange="setDirty(this);" <?php echo ($bReadOnly)?"disabled class=\"readonly\" ":""; ?> >
			<option value="1" <?php if($obj->getOrderAppearence()==1){echo "selected=\"selected\"";}  ?>>1</option>
			<option value="2" <?php if($obj->getOrderAppearence()==2){echo "selected=\"selected\"";}  ?>>2</option>
			<option value="3" <?php if($obj->getOrderAppearence()==3){echo "selected=\"selected\"";}  ?>>3</option>
			<option value="4" <?php if($obj->getOrderAppearence()==4){echo "selected=\"selected\"";}  ?>>4</option>
			<option value="5" <?php if($obj->getOrderAppearence()==5){echo "selected=\"selected\"";}  ?>>5</option>
			<option value="6" <?php if($obj->getOrderAppearence()==6){echo "selected=\"selected\"";}  ?>>6</option>
			<option value="7" <?php if($obj->getOrderAppearence()==7){echo "selected=\"selected\"";}  ?>>7</option>
			<option value="8" <?php if($obj->getOrderAppearence()==8){echo "selected=\"selected\"";}  ?>>8</option>
			<option value="9" <?php if($obj->getOrderAppearence()==9){echo "selected=\"selected\"";}  ?>>9</option>
		</select>
	</p>
	
	<br class="limpiar"/>
	
</fieldset>	
	

<br class="limpiar" />	
<fieldset>
	
	<p>
		<label><?php echo LBL_REMARKS1; ?>: </label><br/>
		<textarea name="remarks" cols="100" rows="7" onChange="setDirty(this);" <?php echo ($bReadOnly)?"readonly=\"readonly\" class=\"readonly\" ":""; ?> ><?php echo $obj->getRemarks(); ?></textarea>
	</p>

</fieldset>


<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMObjectTimestamp.php'); ?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMScreenBottom.php'); ?>


<script type="text/javascript" CHARSET="ISO-8859-1" defer >
<!--

document.data_frm.name.focus(); 

// -->
</script>

	<input type="hidden" name="id" value="<?php echo $id; ?>"/>
	
	<input type="hidden" name="bupload_image" value="no"/>
	<input type="hidden" name="picture_file_old" value="<?php echo $obj->getPictureFile(); ?>"/>
	
	<input type="hidden" name="bupload_image_ac" value="no"/>
	<input type="hidden" name="picture_file_old_ac" value="<?php echo $obj->getPictureFileAC(); ?>"/>

<?php $type = ""; ?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMEndDocument.php'); ?>