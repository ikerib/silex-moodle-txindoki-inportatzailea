<?php 
$locator="yes" ;
$buttons="buttons_locator.php";
$tabs="tabs_locator.php";
$object_type_id=102 ;
// NUEVO ++++++++++++++++++++++
$from = 0;
if (isset($_GET["from"]))
	$from = $_GET["from"];
else if (isset($_POST["from"]))
	$from = $_POST["from"];
	
$sp = "";
if (isset($_GET["sp"]))
	$sp = $_GET["sp"];
else if (isset($_POST["sp"]))
	$sp = $_POST["sp"];		
// ++++++++++++++++++++++++++	

include($_SERVER['DOCUMENT_ROOT']."/includes/control.php"); 
require_once($_SERVER['DOCUMENT_ROOT']."/lang/".$_SESSION["language"]."/lbl_locator.php"); 
require_once($_SERVER['DOCUMENT_ROOT']."/lang/".$_SESSION["language"]."/lbl_general.php"); 
require_once($_SERVER['DOCUMENT_ROOT']."/lang/".$_SESSION["language"]."/lbl_category.php"); 
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/Functions.php'); 

//Cargo la clase para los mostrar los datos.
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/elearning/CategorySummary.php'); 


include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMBeginDocument.php');

include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMHeaderTags.php');
?>

<script src="/modules/elearning/category/js/data_check.js" type="text/javascript" charset="iso-8859-1"></script>

<script  type="text/javascript">
	var languageCode = '<?php echo $_SESSION['language'];?>';
// NUEVO ++++++++++++++++++++++	
	var resultado = new Array();
	var contador = 0;	
// ++++++++++++++++++++++++++
</script>

<?php

$id="";
$enctype="";
$action_url="/modules/elearning/category/locator.php";
$action="SEARCH";

$columnas=5;

$obj = new CategorySummary();
$util = new Util();

include($_SERVER['DOCUMENT_ROOT'].'/modules/elearning/category/locator_action.php');

?>


<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMScreenHeader.php'); ?>

<?php
// NUEVO ++++++++++++++++++++++
if ($from!=0){
?>
	<input type="hidden" name="from" value="<?php echo $from; ?>" />
	<input type="hidden" name="sp" value="<?php echo $sp; ?>" />
<?php
}
// ++++++++++++++++++++++++++++
?>

	<fieldset class="locator">
		<p>
			<label><?php echo LBL_CATEGORY;?>:</label>
			<?php echo $obj->buildOperatorSymbol("name","char",(isset($_POST['op_name'])?$_POST['op_name']:"9") ); ?>
			<input type="text" name="name" id="name" value="<?php echo (isset($_POST['name'])?$_POST['name']:""); ?>" size="25" maxlength="50"/>
		</p>
		
		<p>
			<label><?php echo LBL_DESCRIPTION;?>:</label>
			<?php echo $obj->buildOperatorSymbol("description","char",(isset($_POST['op_description'])?$_POST['op_description']:"9") ); ?>
			<input type="text" name="description" id="description" value="<?php echo (isset($_POST['description'])?$_POST['description']:""); ?>" size="25" maxlength="50"/>
		</p>

		<br class="limpiar"/>

		<input type="submit" name="search" value="<?php echo LBL_LOC_ACTION; ?>" class="boton" onClick="doSearch()"/>

	</fieldset>

      <table class="tabla1" summary="<?php echo LBL_LOC_SUM_TITLE1; ?>">
			<caption><?php echo LBL_LOC_SUM_TITLE1; ?></caption>
			<thead>
			<tr>
                <th width="40%"><?php echo LBL_CATEGORY;?>&nbsp;<a href="javascript:doOrderBy('1')"><img src="/images/flechas.gif" width="7" height="10" alt=""/></a></th>
				<th width="25%"><?php echo LBL_DESCRIPTION;?>&nbsp;<a href="javascript:doOrderBy('2')"><img src="/images/flechas.gif" width="7" height="10" alt=""/></a></th>
				<th width="15%"><?php echo LBL_SHORTNAME;?>&nbsp;<a href="javascript:doOrderBy('3')"><img src="/images/flechas.gif" width="7" height="10" alt=""/></a></th>
				<th width="10%"><?php echo LBL_LANGUAGE;?>&nbsp;<a href="javascript:doOrderBy('5')"><img src="/images/flechas.gif" width="7" height="10" alt=""/></a></th>
				<th width="10%"><?php echo LBL_ORDER_APPEARANCE;?>&nbsp;<a href="javascript:doOrderBy('4')"><img src="/images/flechas.gif" width="7" height="10" alt=""/></a></th>
			</tr>
			<tr>
				<td colspan="<?php echo $columnas; ?>" class="separa">&nbsp;</td>
			</tr>

			</thead>


<?php 
	//Cargo el footer
	include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMListFooter.php'); 
?>
		
			<tbody>
			 
				<?php
					//Muestro los datos.
					$contador = 0;					
					while($row = $resultado->fetch_array())		
					{
				?>
				
			        <tr <?php if (fmod($contador,2)){ ?>class="fondo"<?php } ?>>
						
						
						<td>
<SCRIPT language="JavaScript" type="text/javascript">
// NUEVO ++++++++++++++++++++++
var aux = new Array();
aux[0] = "<?php echo $row["course_category_id"]; ?>";
aux[1] = "<?php echo $row["course_category_name"]; ?>";
resultado[contador] = aux;
contador++;
// ++++++++++++++++++++++++++
</SCRIPT>					

						  <a href="javascript:goFromLoc('<?php echo $object_type_id; ?>',resultado[<?php echo $contador; ?>],'<?php echo $from; ?>');" title="<?php echo $util->truncate($row["course_category_name"],100,true); ?>">&nbsp;<?php echo $util->truncate($row["course_category_name"],100); ?></a>
						</td>
					
						<td>
						  <a href="javascript:goFromLoc('<?php echo $object_type_id; ?>',resultado[<?php echo $contador; ?>],'<?php echo $from; ?>');" title="<?php echo $util->truncate($row["course_category_desc"],100,true); ?>">&nbsp;<?php echo $util->truncate($row["course_category_desc"],25); ?></a>
						</td>
						
						<td>
						  <a href="javascript:goFromLoc('<?php echo $object_type_id; ?>',resultado[<?php echo $contador; ?>],'<?php echo $from; ?>');" title="<?php echo $row["course_category_shortname"]; ?>">&nbsp;<?php echo $row["course_category_shortname"]; ?></a>
						</td>
						
						<td>
	  						  <a href="javascript:goFromLoc('<?php echo $object_type_id; ?>',resultado[<?php echo $contador; ?>],'<?php echo $from; ?>');" title="<?php echo $row["language"]; ?>">&nbsp;<?php echo $row["language"]; ?></a>
						</td>
						
						<td>
	  						  <a href="javascript:goFromLoc('<?php echo $object_type_id; ?>',resultado[<?php echo $contador; ?>],'<?php echo $from; ?>');" title="<?php echo $row["order_appearance"]; ?>">&nbsp;<?php echo $row["order_appearance"]; ?></a>
						</td>
						
					</tr>
              
                <?php	$contador++; 
					} ?>
			  
			</tbody>
			
	  </table>
      
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMScreenBottom.php'); ?>


<script type="text/javascript" CHARSET="ISO-8859-1" defer >
<!--
document.data_frm.name.focus(); 
-->
</script>

<?php $type = "summary"; ?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/CRMEndDocument.php'); ?>