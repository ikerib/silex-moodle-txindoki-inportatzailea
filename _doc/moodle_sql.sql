
-- Notas de los usuarios ACTUALMENTE MATRICULADOS, sus cursos y nota final
-- OJO: No saca las notas de los que en su día fueron matriculados, solo los actualmente matriculados

SELECT mu.id, mu.firstname, mu.lastname , c.fullname, gg.finalgrade
FROM mdl_grade_items AS gi
INNER JOIN mdl_course c
  ON c.id = gi.courseid
LEFT JOIN mdl_grade_grades AS gg
  ON gg.itemid = gi.id
INNER JOIN mdl_user AS mu
  ON gg.userid = mu.id
WHERE gi.itemtype = 'course'
ORDER BY c.id asc